﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessModel;
using BussinessLogic;
using System.Data;
using System.ComponentModel;

namespace iBus
{
    public partial class Demo : System.Web.UI.Page
    {
        static List<ShowBusListModel> listShowBusListOrigin = new List<ShowBusListModel>();
        static List<ShowBusListModel> listShowBusListWorking = new List<ShowBusListModel>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                listShowBusListOrigin.Clear();
                listShowBusListWorking.Clear();
                BusSearchBL busSearchBLObj = new BusSearchBL();
                //BusSearchModel busSearchModelObj = (BusSearchModel)Session["busSearchDetails"];
                BusSearchModel busSearchModelObj = new BusSearchModel();
                busSearchModelObj.FromCity = "Surat";
                busSearchModelObj.ToCity = "Mumbai";
                busSearchModelObj.DateOfJourney = "10/13/2013";


                listShowBusListOrigin = busSearchBLObj.GetShowBusList(busSearchModelObj);
                listShowBusListWorking = listShowBusListOrigin;

               

                //ListItem lst;
                //string text;
                //for (int i = 0; i < 10; i++)
                //{
                //    text = i.ToString() + " Checkbox in DDL";
                //    lst = new ListItem(text, i.ToString());
                //    chkList.Items.Add(lst);

                //}

                //List<CityDetailModel> listCityDetail = new List<CityDetailModel>();
                //listCityDetail = busSearchBLObj.GetCityDetail();
                //Repeater_Demo.DataSource = listCityDetail;
                //Repeater_Demo.DataBind();
            }
            Repeater_Demo.DataSource = listShowBusListWorking;
            Repeater_Demo.DataBind();
        }
    }
}