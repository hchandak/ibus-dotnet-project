﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DemoFilter.aspx.cs" Inherits="iBus.DemoFilter" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Script/jquery-2.0.3.js"></script>
    <script src="jquery.tablesorter.js"></script>
    <link href="Style/bootstrap.css" rel="stylesheet" />

    
</head>
<body>
    <form id="form1" runat="server">
        <span>
            <asp:TextBox ID="TextBox_BusOperator" runat="server" CssClass="textbox" ClientIDMode="Static" Text="Select bus operator"></asp:TextBox>
            <img id="ddlArrow" src="Image/down_arrow.jpg" style="margin-left: -23px; margin-bottom: -4px" onclick="return ShowFilter('effect')" />
        </span>
        <div id="effect">
            <asp:CheckBoxList ID="CheckBoxList_BusOperator" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="CheckBoxList_BusOperator_SelectedIndexChanged" AutoPostBack="true">
            </asp:CheckBoxList>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
               <asp:Repeater ID="Repeater_Demo" runat="server">
                <HeaderTemplate>
                    <table id="myTable" class="table table-responsive table-hover">
                        <thead>
                            <tr class="table-bordered">


                                <th>Bus Operator</th>
                                <th>Bus Type</th>
                                <th>Departure</th>
                                <th>Arrival</th>
                                <th>Duration</th>
                                <th>Seat</th>
                                <th>Fare</th>

                            </tr>
                        </thead>
                </HeaderTemplate>

                <ItemTemplate>

                    <tr class="table-bordered">
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="<%# ((BussinessModel.ShowBusListModel)Container.DataItem).TravelName %>"></asp:Label>

                        </td>
                        <td>
                            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).BustType %>
                        </td>
                        <td>
                            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).DepartureTime %>
                        </td>
                        <td>
                            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).ArrivalTime %>
                        </td>
                        <td>
                            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).Duration %>
                        </td>
                        <td>
                            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).AvailableSeats %>
                        </td>
                        <td>
                            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).Fare %>
                        </td>
                    </tr>

                </ItemTemplate>

            </asp:Repeater>
            </ContentTemplate>
           
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="CheckBoxList_BusOperator" EventName="SelectedIndexChanged" />
            </Triggers>
           
        </asp:UpdatePanel>
         <div>
              <%--<asp:GridView ID="GVDisplay" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered" 
        emptydatatext="No data available." 
        allowsorting="true"
        OnSorting="GVDisplay_Sorting" OnSelectedIndexChanged="GVDisplay_SelectedIndexChanged">
        
                    <Columns>
                        <asp:TemplateField HeaderText="Bus Operator" SortExpression="TravelName">
                            <ItemTemplate >
                                <asp:HiddenField ID="HiddenField_DailyTripID" Value="<%# ((BussinessModel.ShowBusListModel)Container.DataItem).DailyTripID %>" runat="server" />
                                <asp:Label ID="Label_BusOperator" runat="server" Text="<%# ((BussinessModel.ShowBusListModel)Container.DataItem).TravelName %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Bus Type">
                            <ItemTemplate>
                                <asp:Label ID="Label_BusType" runat="server" Text="<%# ((BussinessModel.ShowBusListModel)Container.DataItem).BustType %>"></asp:Label>
                            </ItemTemplate>
                           
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Departure" SortExpression="Departure">
                            <ItemTemplate>
                                <asp:Label ID="Label_DepartureTime" runat="server" Text='<%# ((BussinessModel.ShowBusListModel)Container.DataItem).DepartureTime %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Arrival" SortExpression="Arrival">
                            <ItemTemplate>
                                <asp:Label ID="Label_ArrivalTime" runat="server" Text='<%# ((BussinessModel.ShowBusListModel)Container.DataItem).ArrivalTime %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Duration">
                            <ItemTemplate>
                                <asp:Label ID="Label_Duration" runat="server" Text='<%# ((BussinessModel.ShowBusListModel)Container.DataItem).Duration %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Fare (in INR)" SortExpression="Fare">
                            <ItemTemplate>
                                <asp:Label ID="Label_Fare" runat="server" Text='<%# ((BussinessModel.ShowBusListModel)Container.DataItem).Fare %>'>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Seat">
                            <ItemTemplate>
                                <div>
                                    <asp:Label ID="Label2" runat="server" Text="Available seats : "></asp:Label>
                                    <asp:Label ID="Label1" runat="server" Text='<%# ((BussinessModel.ShowBusListModel)Container.DataItem).AvailableSeats %>'>'></asp:Label>
                                </div>
                                
                                <asp:Button ID="Button_BookSeat" runat="server" Text="Book Seat" CssClass="btn btn-success" OnClientClick=" SaveDailyTripID(this);"  OnClick="Button_BookSeat_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                </asp:GridView>--%>
        </div>
    </form>
    <script>

        function pageLoad(sender, args) {
            if (args.get_isPartialLoad()) {
                //Specific code for partial postbacks can go in here.
                $("#myTable").tablesorter();
            }
        }

        $(window).load(function () {
            $("#effect").hide();
            $("#busTypeFilter").hide();
          
                $("#myTable").tablesorter();
            

        });



        function SaveDailyTripID(bookBtn) {
            var row = bookBtn.parentNode.parentNode;
            index = row.rowIndex - 1;
            var dailyTripID = $("#ContentPlaceHolder1_GVDisplay_HiddenField_DailyTripID_" + index).val();
            $("#HiddenField_SelectedDailyTripID").val(dailyTripID);
        }

        function ShowFilter(filterID) {
            var abc = 123;
            runFilterEffect(filterID);
            return false;
        }

        function runFilterEffect(filterID) {
            if (!($('#' + filterID).is(":visible"))) {
                //run the effect
                $('#' + filterID).show('slow');
            }
            else {
                $('#' + filterID).hide('slow');
            }
        }

        function filter(filterCheckBox, colNo) {

            var checkboxList = filterCheckBox.getElementsByTagName("input");
            var table = document.getElementById('myTable');
            var busOpertor;
            for (var i = 0; i < checkboxList.length; i++) {
                if (checkboxList[i].checked) {
                    for (var r = 1; r < table.rows.length; r++) {
                        busOpertor = table.rows[r].cells[colNo].children;
                        var busOpertor1 = table.rows[r].cells[colNo].childNodes;
                        var busOpertor1 = table.rows[r].cells[colNo];

                        //busOpertor = table.rows[r].cells[colNo].innerHTML.toString();
                        if (busOpertor == checkboxList[i].parentNode.getElementsByTagName('label'))
                            table.rows[r].style.display = '';
                        else table.rows[r].style.display = 'none';
                    }
                }
            }



            //var suche = term.value.toLowerCase();
            //var table = document.getElementById(_id);
            //var ele;
            //for (var r = 1; r < table.rows.length; r++) {
            //    ele = table.rows[r].cells[cellNr].innerHTML.replace(/<[^>]+>/g, "");
            //    if (ele.toLowerCase().indexOf(suche) >= 0)
            //        table.rows[r].style.display = '';
            //    else table.rows[r].style.display = 'none';
            //}










            //var chkBox = document.getElementById("");
            //var checkbox = chkBox.getElementsByTagName("input");
            //var objTextBox = document.getElementById("");
            //var counter = 0;
            //objTextBox.value = "";
            //for (var i = 0; i < checkbox.length; i++) {
            //    if (checkbox[i].checked) {
            //        var chkBoxText = checkbox[i].parentNode.getElementsByTagName('label');
            //        if (objTextBox.value == "") {
            //            objTextBox.value = chkBoxText[0].innerHTML;
            //        }
            //        else {
            //            objTextBox.value = objTextBox.value + ", " + chkBoxText[0].innerHTML;
            //        }
            //    }
            //}
        }


    </script>
</body>
</html>
