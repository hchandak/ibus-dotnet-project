﻿using BussinessLogic;
using BussinessModel;
using DataValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iBus
{
    public partial class add_passenger_details : System.Web.UI.Page
    {
        static int no_of_passenger;
        string UserEmailId = ""; // Change valuse with session userId  
        string validatdestatus = "";
        int DailyTripID;
        int Fare ; // change with value of session
        int UserId ;
        UserBL userBL = new UserBL();
        UserValidation validator = new UserValidation();
        AddPassengerDetailModel passenger = new AddPassengerDetailModel();
        PassengerDetailModel passengerDetail = new PassengerDetailModel();
        ReservationDetailModel reservationDetail = new ReservationDetailModel();
        AddReservationDetailModel reservation = new AddReservationDetailModel();
        TicketModel ticketModel = new TicketModel();
        TripDetailModel tripDetailModel = new TripDetailModel();
        List<PassengerDetailModel> passengerList = new List<PassengerDetailModel>();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string[] seatIDs;

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"] != null && Session["userID"].ToString().Length > 0)
            {
            }
            else
            {
                Response.Redirect("ibus.aspx", false);
            }
                try
                {
                    if (Session["DailyTripID"] != null && Session["DailyTripID"].ToString().Trim().Length > 0)
                    {
                        DailyTripID = Convert.ToInt32(Session["DailyTripID"]);

                    }
                    if (Session["SeatIDs"] != null && Session["SeatIDs"].ToString().Trim().Length > 0)
                    {
                        string str_Seats = Session["SeatIDs"].ToString();
                        seatIDs = str_Seats.Split(',');
                        no_of_passenger = seatIDs.Length;
                        //no_of_passenger = 2;  //Assign value from session variable
                        if (no_of_passenger < 2) { Div_Passenger2.Style["display"] = "none"; }
                        if (no_of_passenger < 3) { Div_Passenger3.Style["display"] = "none"; }
                        if (no_of_passenger < 4) { Div_Passenger4.Style["display"] = "none"; }
                        if (no_of_passenger < 5) { Div_Passenger5.Style["display"] = "none"; }
                    }
                    if (Session["userEmail"] != null && Session["userEmail"].ToString().Trim().Length > 0)
                    {
                        UserEmailId = Session["userEmail"].ToString();
                    }
                    if (Session["userID"] != null && Session["userID"].ToString().Trim().Length > 0)
                    {
                        UserId = Convert.ToInt32(Session["userID"]);
                    }
                    //Initialising Gender dropdownList
                    DropDownList_Passenger1Gender.Items.Add("Male");
                    DropDownList_Passenger1Gender.Items.Add("Female");
                    DropDownList_Passenger2Gender.Items.Add("Male");
                    DropDownList_Passenger2Gender.Items.Add("Female");
                    DropDownList_Passenger3Gender.Items.Add("Male");
                    DropDownList_Passenger3Gender.Items.Add("Female");
                    DropDownList_Passenger4Gender.Items.Add("Male");
                    DropDownList_Passenger4Gender.Items.Add("Female");
                    DropDownList_Passenger5Gender.Items.Add("Male");
                    DropDownList_Passenger5Gender.Items.Add("Female");
                    TextBox_UserEmailId.Text = UserEmailId = Session["userEmail"].ToString();
                    TextBox_UserMobileNo.Text = Session["userMobileNo"].ToString();
                }
                catch (Exception ex)
                {
                    Logger.Error("\n\nError Class-> add_passenger_details.cs, Function->Page_Load  : \n " + ex);
                }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// This functions inserts passenger details, reservs ticket, and generates model for ticket
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_SubmitPassengersDetail_Click(object sender, EventArgs e)
        {
            string Passenger1FirstName, Passenger1LastName, Passenger2FirstName, Passenger2LastName, Passenger3FirstName,
                  Passenger3LastName, Passenger4FirstName, Passenger4LastName, Passenger5FirstName, Passenger5LastName,
                  PassengerEmailID, PassengerMobileNumber;
            int Passenger1Age, Passenger2Age, Passenger3Age, Passenger4Age, Passenger5Age;
            char Passenger1Gender, Passenger2Gender, Passenger3Gender, Passenger4Gender, Passenger5Gender;
            string Gender;
            int ReservationId;
            bool status = false;
            PassengerEmailID = TextBox_UserEmailId.Text;
            PassengerMobileNumber = TextBox_UserMobileNo.Text;
            DailyTripDetailModel response = new DailyTripDetailModel();

            try
            {   
                //Getting Fare for given DailyTripID
                tripDetailModel = userBL.GetFare(DailyTripID);

                //Getting Reservation date from Table DailyTripID
                string ResDate = userBL.GetResDateFromDailyTripID(DailyTripID);
                reservationDetail.UserID = UserId;
                reservationDetail.DailyTripID = DailyTripID;
                reservationDetail.ResDate = ResDate;
                reservationDetail.ResNoOfSeat = Convert.ToByte(no_of_passenger);
                reservationDetail.ResEmailID = PassengerEmailID;
                reservationDetail.ResMobileNo = PassengerMobileNumber;
                reservationDetail.BillAmount = (no_of_passenger * tripDetailModel.Fare);
                reservationDetail.CreatedBy = UserEmailId;
                reservationDetail.CreatedDate = System.DateTime.Now.ToShortDateString();
                reservationDetail.Status = AppConstant.BOOKED;
                reservation = userBL.SaveReservationDetails(reservationDetail);
                response = userBL.DecreaseSeatAvailability(no_of_passenger, DailyTripID);

                //Getting Ticket details from database
                ReservationId = reservation.Status;
                ticketModel = userBL.GetTicketDetails(ReservationId);
                //Inserting Passenger1Deatils
                if (no_of_passenger >= 0)
                {
                    Passenger1FirstName = TextBox_Passenger1FirstName.Text;
                    Passenger1LastName = TextBox_Passenger1LastName.Text;
                    Passenger1Age = Convert.ToInt32(TextBox_Passenger1Age.Text);
                    Gender = DropDownList_Passenger1Gender.SelectedValue.ToString();
                    if (Gender == "Male")
                    { Passenger1Gender = 'M'; }
                    else
                    { Passenger1Gender = 'F'; }

                    validatdestatus = validator.ValidatePassengerDetails(Passenger1FirstName, Passenger1LastName, Passenger1Age, Passenger1Gender, UserEmailId);
                    if (validatdestatus == DisplayMessage.VALIDATION_SUCCESS)
                    {
                        passengerDetail.FirstName = Passenger1FirstName;
                        passengerDetail.LastName = Passenger1LastName;
                        passengerDetail.Gender = Passenger1Gender.ToString();
                        passengerDetail.Age = Convert.ToByte(Passenger1Age);
                        passengerDetail.CreatedBy = UserEmailId;
                        passengerDetail.CreatedDate = System.DateTime.Now.ToShortDateString();
                        passengerDetail.ResID = ReservationId;
                        passengerDetail.SeatID = Convert.ToInt32(seatIDs[0]);
                        passengerDetail.Status = AppConstant.BOOKED;
                        passenger = userBL.AddPassengerDetails(passengerDetail);
                        if (passenger.Status > 0) { status = true; }
                        else { status = false; }
                    }

                    //Inserting Passenger2Deatils
                    if (no_of_passenger >= 2)
                    {
                        Passenger2FirstName = TextBox_Passenger2FirstName.Text;
                        Passenger2LastName = TextBox_Passenger2LastName.Text;
                        Passenger2Age = Convert.ToInt32(TextBox_Passenger2Age.Text);
                        Gender = DropDownList_Passenger2Gender.SelectedValue.ToString();
                        if (Gender == "Male")
                        { Passenger2Gender = 'M'; }
                        else
                        { Passenger2Gender = 'F'; }
                        validatdestatus = validator.ValidatePassengerDetails(Passenger2FirstName, Passenger2LastName, Passenger2Age, Passenger2Gender, UserEmailId);

                        if (validatdestatus == DisplayMessage.VALIDATION_SUCCESS)
                        {
                            passengerDetail.FirstName = Passenger2FirstName;
                            passengerDetail.LastName = Passenger2LastName;
                            passengerDetail.Gender = Passenger2Gender.ToString();
                            passengerDetail.Age = Convert.ToByte(Passenger2Age);
                            passengerDetail.CreatedBy = UserEmailId;
                            passengerDetail.CreatedDate = System.DateTime.Today.ToShortDateString();
                            passengerDetail.ResID = ReservationId;
                            passengerDetail.SeatID = Convert.ToInt32(seatIDs[1]);
                            passenger = userBL.AddPassengerDetails(passengerDetail);
                            passengerDetail.Status = AppConstant.BOOKED;
                            if (passenger.Status > 0) { status = true; }
                            else { status = false; }
                        }
                    }

                    //Inserting Passenger3Deatils
                    if (no_of_passenger >= 3)
                    {
                        Passenger3FirstName = TextBox_Passenger3FirstName.Text;
                        Passenger3LastName = TextBox_Passenger3LastName.Text;
                        Passenger3Age = Convert.ToInt32(TextBox_Passenger3Age.Text);
                        Gender = DropDownList_Passenger3Gender.SelectedValue.ToString();
                        if (Gender == "Male")
                        { Passenger3Gender = 'M'; }
                        else
                        { Passenger3Gender = 'F'; }
                        validatdestatus = validator.ValidatePassengerDetails(Passenger3FirstName, Passenger3LastName, Passenger3Age, Passenger3Gender, UserEmailId);
                        if (validatdestatus == DisplayMessage.VALIDATION_SUCCESS)
                        {
                            passengerDetail.FirstName = Passenger3FirstName;
                            passengerDetail.LastName = Passenger3LastName;
                            passengerDetail.Gender = Passenger3Gender.ToString();
                            passengerDetail.Age = Convert.ToByte(Passenger3Age);
                            passengerDetail.CreatedBy = UserEmailId;
                            passengerDetail.CreatedDate = System.DateTime.Today.ToShortDateString();
                            passengerDetail.ResID = ReservationId;
                            passengerDetail.SeatID = Convert.ToInt32(seatIDs[2]);
                            passengerDetail.Status = AppConstant.BOOKED;

                            passenger = userBL.AddPassengerDetails(passengerDetail);
                            if (passenger.Status > 0) { status = true; }
                            else { status = false; }
                        }
                    }

                    //Inserting Passenger4Deatils
                    if (no_of_passenger >= 4)
                    {
                        Passenger4FirstName = TextBox_Passenger4FirstName.Text;
                        Passenger4LastName = TextBox_Passenger4LastName.Text;
                        Passenger4Age = Convert.ToInt32(TextBox_Passenger4Age.Text);
                        Gender = DropDownList_Passenger4Gender.SelectedValue.ToString();
                        if (Gender == "Male")
                        { Passenger4Gender = 'M'; }
                        else
                        { Passenger4Gender = 'F'; }
                        validatdestatus = validator.ValidatePassengerDetails(Passenger4FirstName, Passenger4LastName, Passenger4Age, Passenger4Gender, UserEmailId);
                        if (validatdestatus == DisplayMessage.VALIDATION_SUCCESS)
                        {
                            passengerDetail.FirstName = Passenger4FirstName;
                            passengerDetail.LastName = Passenger4LastName;
                            passengerDetail.Gender = Passenger4Gender.ToString();
                            passengerDetail.Age = Convert.ToByte(Passenger4Age);
                            passengerDetail.CreatedBy = UserEmailId;
                            passengerDetail.CreatedDate = System.DateTime.Today.ToShortDateString();
                            passengerDetail.ResID = ReservationId;
                            passengerDetail.SeatID = Convert.ToInt32(seatIDs[3]);
                            passengerDetail.Status = AppConstant.BOOKED;
                            passenger = userBL.AddPassengerDetails(passengerDetail);
                            if (passenger.Status > 0) { status = true; }
                            else { status = false; }
                        }
                    }

                    //Inserting Passenger5Deatils
                    if (no_of_passenger >= 5)
                    {
                        Passenger5FirstName = TextBox_Passenger5FirstName.Text;
                        Passenger5LastName = TextBox_Passenger5LastName.Text;
                        Passenger5Age = Convert.ToInt32(TextBox_Passenger5Age.Text);
                        Gender = DropDownList_Passenger5Gender.SelectedValue.ToString();
                        if (Gender == "Male")
                        { Passenger5Gender = 'M'; }
                        else
                        { Passenger5Gender = 'F'; }
                        validatdestatus = validator.ValidatePassengerDetails(Passenger5FirstName, Passenger5LastName, Passenger5Age, Passenger5Gender, UserEmailId);
                        if (validatdestatus == DisplayMessage.VALIDATION_SUCCESS)
                        {
                            passengerDetail.FirstName = Passenger5FirstName;
                            passengerDetail.LastName = Passenger5LastName;
                            passengerDetail.Gender = Passenger5Gender.ToString();
                            passengerDetail.Age = Convert.ToByte(Passenger5Age);
                            passengerDetail.CreatedBy = UserEmailId;
                            passengerDetail.CreatedDate = System.DateTime.Today.ToShortDateString();
                            passengerDetail.ResID = ReservationId;
                            passengerDetail.SeatID = Convert.ToInt32(seatIDs[4]);
                            passengerDetail.Status = AppConstant.BOOKED;
                            passenger = userBL.AddPassengerDetails(passengerDetail);
                            if (passenger.Status > 0) { status = true; }
                            else { status = false; }
                        }
                    }

                    if (status == true)
                    {
                        //passing values of passengerList, ticketModel, passengerEmailID through session
                        passengerList = userBL.GetPassengerList(ReservationId);
                        Session["passengerList"] = passengerList;
                        Session["ticketModel"] = ticketModel;
                        Session["PassengerEmailId"] = PassengerEmailID;
                        Response.Redirect("ticket.aspx");
                    }
                    else
                    {
                        //showing error that reservation failed
                        Div_ErrorMessage.Style["display"] = "block";
                        Div_ErrorMessage.InnerHtml = "";
                        Div_ErrorMessage.InnerHtml = DisplayMessage.TICKET_BOOK_FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> ModalSigninSignup.cs, Function->Button_SubmitPassengersDetail_Click  : \n " + ex);
            }
        }
    }
}
