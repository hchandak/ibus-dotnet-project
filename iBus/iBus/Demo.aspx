﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Demo.aspx.cs" Inherits="iBus.Demo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Script/jquery-2.0.3.js"></script>
    <script src="jquery-latest.js"></script>
     <script src="jquery.tablesorter.js"></script>
    <link href="Style/bootstrap.css" rel="stylesheet" />
   <script>
       $(document).ready(function () {
           $("#myTable").tablesorter();
       }
);
   </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:Repeater ID="Repeater_Demo" runat="server">
        <HeaderTemplate>
    <table id="myTable" class="table table-responsive table-hover">
        <thead> 
         <tr class="table-bordered">
               
               
                <th>Bus Operator</th>
                <th>Bus Type</th>
                <th>Departure</th>
                <th>Arrival</th>
                <th>Duration</th>
                <th>Seat</th>
                <th>Fare</th>
                
        </tr>   
        </thead> 
    </HeaderTemplate> 
            
      <ItemTemplate>
          
        <tr class="table-bordered">
          <td>
              <asp:Label ID="Label1" runat="server" Text="<%# ((BussinessModel.ShowBusListModel)Container.DataItem).TravelName %>" ></asp:Label>
           
          </td>
          <td>
            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).BustType %>
          </td>
          <td>
            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).DepartureTime %>
          </td>
          <td>
            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).ArrivalTime %>
          </td>
          <td>
            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).Duration %>
          </td>
          <td>
            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).AvailableSeats %>
          </td>
          <td>
            <%# ((BussinessModel.ShowBusListModel)Container.DataItem).Fare %>
          </td>
        </tr>
              
      </ItemTemplate>
  
    </asp:Repeater>


        <table id="myTable1" class="tablesorter"> 
<thead> 
<tr> 
    <th>Last Name</th> 
    <th>First Name</th> 
    <th>Email</th> 
    <th>Due</th> 
    <th>Web Site</th> 
</tr> 
</thead> 
<tbody> 
<tr> 
    <td>Smith</td> 
    <td>John</td> 
    <td>jsmith@gmail.com</td> 
    <td>$50.00</td> 
    <td>http://www.jsmith.com</td> 
</tr> 
<tr> 
    <td>Bach</td> 
    <td>Frank</td> 
    <td>fbach@yahoo.com</td> 
    <td>$50.00</td> 
    <td>http://www.frank.com</td> 
</tr> 
<tr> 
    <td>Doe</td> 
    <td>Jason</td> 
    <td>jdoe@hotmail.com</td> 
    <td>$100.00</td> 
    <td>http://www.jdoe.com</td> 
</tr> 
<tr> 
    <td>Conway</td> 
    <td>Tim</td> 
    <td>tconway@earthlink.net</td> 
    <td>$50.00</td> 
    <td>http://www.timconway.com</td> 
</tr> 
</tbody> 
</table> 
    </div>
    </form>
</body>
</html>
