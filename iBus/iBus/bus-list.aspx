﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/default.Master" AutoEventWireup="true" CodeBehind="bus-list.aspx.cs" Inherits="iBus.bus_list" EnableEventValidation="false" %>

<%@ Register Src="~/UserControl/modal-select-seats.ascx" TagPrefix="iBus" TagName="modalselectseats" %>
<%@ Register Src="~/UserControl/modal-signin-signup.ascx" TagPrefix="iBus" TagName="modalsigninsignup" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
    <script src="Script/bus-search.js"></script>
    <link href="Style/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
   
    <link href="Style/table-style.css" rel="stylesheet" />
    <style>


        th {
            text-align:center;
        }
        .ui-autocomplete {
            max-height: 150px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
        }

        .capitalize {
            text-transform: capitalize;
        }

        .filter {
            width: 180px;
            height: 135px;
            padding: 0.4em;
            position: relative;
            overflow-wrap:break-word;
            overflow-y:scroll;
            background-color: white;
            font-size : 12px;
        }

        .filter td {
            padding-left:4px;
            padding-right:2px;
        }
         
        .filter input {
            margin-right: 4px;
            cursor:pointer;
         }
            
        .fix {
            position: absolute;
            padding:0px;
        }

        a {
            color:black;
        }

        .filter-textbox {
            width: 180px;
            height: 21px;
            cursor:pointer;
        }

        .filter-img {
            cursor:pointer;
        }

        .onhovercursor {
            cursor:pointer;
        }
        .amenities-busList {
        max-width: 125px;
        }
        .seats-busList {
            max-width: 75px;
        }
     
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--<div class="container">--%>
     
     <div id="Div_ErrorMessageLoginRegister" class="alert alert-danger" runat="server" clientidmode="Static" style="display: none"></div>
    <div class="row">
         
        <div class="col-md-4">

            <div class="well well-sm div_group">
                <div class="col-md-6">
                    <asp:Label ID="Label_FromCityTitle" runat="server" Text="From :" CssClass="group_header"></asp:Label>
                    <asp:Label ID="Label_FromCitySelected" runat="server" Text="Surat"></asp:Label>

                </div>
                <div>
                    <asp:Label ID="Label_ToCityTitle" runat="server" Text="To :" CssClass="group_header"></asp:Label>
                    <asp:Label ID="Label_ToCitySelected" runat="server" Text="Mumbai"></asp:Label>

                </div>

            </div>

        </div>
        <div class="col-md-3">

            <div class="well well-sm div_group">

                <asp:Label ID="Label_JourneyDateTitle" runat="server" Text="Journey Date :" CssClass="group_header"></asp:Label>
                <asp:Label ID="Label_JourneyDateSelected" runat="server" Text="13/10/2013"></asp:Label>

            </div>
        </div>
        <div class="col-md-3">
            <div class="well well-sm div_group onhovercursor" id="Div_FilterOptionsTab">
                <asp:Label ID="Label_FilterOptionsTab" runat="server" Text="Filter options" CssClass="group_header"></asp:Label>
            </div>

        </div>

        <div class="col-md-2 ">
            <div class="well well-sm div_group onhovercursor" id="Div_ModifySearchTab">
                <asp:Label ID="Label_ModifySearchTab" runat="server" Text="Modify Search" CssClass="group_header"></asp:Label>
            </div>
        </div>
    </div>

    <div class="row" id="Div_ModifySearchBox">
        <div class="col-md-12">
            <div class="well well-sm div_group form-inline">
                <div class="form-group">
                    <label class="sr-only" for="TextBox_From">From</label>
                    <asp:TextBox ID="TextBox_FromCity" runat="server" CssClass="form-control capitalize" ClientIDMode="Static" placeholder="From"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="TextBox_To">To</label>
                    <asp:TextBox ID="TextBox_ToCity" runat="server" CssClass="form-control capitalize" ClientIDMode="Static" placeholder="To"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="TextBox_DateOfJourney">Date of journey</label>
                    <asp:TextBox ID="TextBox_DateOfJourney" runat="server" CssClass="form-control" placeholder="Date of Journey" ClientIDMode="Static"></asp:TextBox>
                </div>
                <asp:Button ID="Button_SearchBus" runat="server" Text="Search Bus" CssClass="btn btn-success" OnClientClick="return ValidateAll();" OnClick="Button_SearchBus_Click" />
                <div class="alert alert-danger" id="Div_ErrorMessageBusSearch" ClientIDMode="Static" style="display:none"  runat="server"></div>
                <asp:HiddenField ID="HiddenField_Cities" runat="server" ClientIDMode="Static" />
            </div>
        </div>
    </div>

    <div class="row" id="Div_FilterOptionsBox">
        <div class="col-md-12">
            <div class="well well-lg div_group">
                <div class="col-md-1">
                    <asp:Label ID="Label_FilterTitle" runat="server" Text="Filter :" Font-Size="Medium"></asp:Label>
                </div>
                <div class="col-md-3 ">
                    <div>
                         <asp:TextBox ID="TextBox_BusOperator" runat="server" CssClass="filter-textbox" ClientIDMode="Static" Text="Select bus operator" autocomplete="off"></asp:TextBox>
                            
                        <img id="ddlArrow" src="Image/down_arrow.jpg" style="margin-left: -23px; margin-bottom: 2px" onclick="return ShowFilter('busOperatorFilter')" Class="filter-img" />
                    </div>
                    <div id="busOperatorFilter" class="fix filter">
                        <asp:CheckBoxList ID="CheckBoxList_BusOperator" CssClass="filter table-bordered" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="Filter_SelectedIndexChanged" AutoPostBack="true">
                        </asp:CheckBoxList>
                    </div>
                </div>
                <div class="col-md-3">
                    <div>
                        <asp:TextBox ID="TextBox_BusType" runat="server" CssClass="filter-textbox" ClientIDMode="Static" Text="Select bus type" autocomplete="off"></asp:TextBox>
                        <img class="ddlArrow" src="Image/down_arrow.jpg" style="margin-left: -23px; margin-bottom: 2px" onclick="return ShowFilter('busTypeFilter')" Class="filter-img" />
                    </div>
                    <div id="busTypeFilter" class="fix filter">
                        <asp:CheckBoxList ID="CheckBoxList_BusType" CssClass="filter table-bordered" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="Filter_SelectedIndexChanged" AutoPostBack="true">
                        </asp:CheckBoxList>
                    </div>
                </div>
            </div>

        </div>

    </div>
    
  
    <%--<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>--%>

    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div >
            <asp:Repeater ID="Repeater_BusList" runat="server" OnItemDataBound="Repeater_BusList_ItemDataBound">
                <HeaderTemplate>
                    <table id="Table_BusList" class="table table-hover table-responsive tablesorter table-bordered" >
                        <thead>
                            <tr class="table-bordered">

                                <th><span style="cursor:pointer;">Bus Operator </span></th>
                                <th><span style="cursor:pointer;">Bus Type </span></th>
                                <th>Amenities</th>
                                <th><span style="cursor:pointer;">Departure</span></th>
                                <th><span style="cursor:pointer;">Arrival</span></th>
                                <th><span style="cursor:pointer;">Duration</span></th>
                                <th><span style="cursor:pointer;">Fare</span></th>
                                <th>Seat</th>

                            </tr>
                        </thead>
                </HeaderTemplate>

                <ItemTemplate>
                    <label id="Label_Amenities" runat="server"  style="display:none" ><%# Eval("Amenities") %></label>
                    <tr class="table-bordered">
                        <td>
                            <asp:HiddenField ID="HiddenField_DailyTripID" Value='<%# Eval("DailyTripID") %>' runat="server" />
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("TravelName") %>'></asp:Label>
                        </td>
                        <td>
                            <%# Eval("BusType") %>
                           
                        </td>
                        <td class="amenities-busList">
                            <asp:Image id="Image_NoAmenities" CssClass="margin" runat="server" style="cursor:pointer;display:none" src="/Image/Amenities/no-amenities.jpg" title="No Amenities" />
                           <asp:Image id="Image_WaterBottle" CssClass="margin" runat="server" style="cursor:pointer;" src="/Image/Amenities/water-bottle.png" title="Water Bottle" />
                           <asp:Image id="Image_Blancket" CssClass="margin" runat="server" style="cursor:pointer;" src="/Image/Amenities/blancket1.png" title="Blanket" />
                           <asp:Image id="Image_Movies" CssClass="margin" runat="server" style="cursor:pointer;" src="/Image/Amenities/movies.png" title="Movies" />
                           <asp:Image id="Image_CharginPoint" CssClass="margin" runat="server" style="cursor:pointer;" src="/Image/Amenities/charging-point.jpg" title="Chargin Point" />
                           <asp:Image id="Image_ReadingLight" runat="server" class="margin" style="cursor:pointer;" src="/Image/Amenities/readinglight.png" title="Reading Light" />
                           
                        </td>
                        <td>
                                  <a href="#" class="marker" id='Departure_<%# Eval("TripID") %>'><%# Eval("DepartureTime") %></a>
                              
                        </td>
                        <td>
                            <%# Eval("ArrivalTime") %>
                        </td>
                        <td>
                            <%# Eval("Duration") %>
                        </td>
                        <td>
                            <%# Eval("Fare") %>
                        </td>
                        <td class="seats-busList">
                            <asp:Button ID="Button_BookSeat" runat="server" Text='<%# string.Format("Seats : {0}", Eval("AvailableSeats")) %>' OnClick="Button_BookSeat_Click" OnClientClick="SaveDailyTripID(this)" CssClass="btn btn-primary button-book-seat" />
                            <asp:HiddenField ID="HiddenField_AvailableSeats" Value='<%# Eval("AvailableSeats") %>' runat="server" />
                            <asp:HiddenField ID="HiddenField_BoardingPoints" Value='<%# Eval("BoardingPoints") %>' runat="server" />
                            <asp:HiddenField ID="HiddenField_BoardingDepartureTime" Value='<%# Eval("BoardingDepartureTime") %>' runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            </div>
            
            <asp:HiddenField ID="HiddenField_SelectedDailyTripID" runat="server" ClientIDMode="Static" />
             <iBus:modalselectseats runat="server" ID="modalselectseats" />
            <iBus:modalsigninsignup runat="server" ID="modalsigninsignup" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="CheckBoxList_BusOperator" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="CheckBoxList_BusType" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>        

 <div class="col-md-12" id="Div_NoRecordsFound" runat="server" ClientIDMode="Static">
     No Records Found
 </div>
    
    <script src="Script/jquery.tablesorter.js"></script>
    <script src="Script/bus-list-registration.js"></script>
     <script src="Script/bus-list.js"></script>
    <script src="Script/jquery-ui-1.10.3.custom.js"></script>
    <script src="Script/bootstrap-tooltip.js"></script>
   
   <%-- </div>--%>
   
</asp:Content>
