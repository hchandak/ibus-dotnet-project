﻿using BussinessLogic;
using BussinessModel;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iBus
{
    public partial class profile_page : System.Web.UI.Page
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        List<TicketModel> resultList = new List<TicketModel>();
        UserBL userBl = new UserBL();
        int userId;
        UserBL userBL = new UserBL();
        TicketModel ticketModel = new TicketModel();
        List<PassengerDetailModel> passengerList = new List<PassengerDetailModel>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"] != null && Session["userID"].ToString().Length > 0)
            {
                userId = Convert.ToInt32(Session["userID"]);
                lbl_userName.Text = Session["userEmail"].ToString();
                lbl_firstName.Text = Session["userFirstName"].ToString();
                lbl_lastName.Text = Session["userLastName"].ToString();
                lbl_mobileNo.Text = Session["userMobileNo"].ToString();

            }

            if (Session["ticketCancelled"] != null && Session["ticketCancelled"].ToString().Length > 0)
            {
                Div_PassengerList_SucessMessage.Style["display"] = "block";
                Div_PassengerList_SucessMessage.InnerHtml =Session["ticketCancelled"]+"seats has been cancelled..";
                Session["ticketCancelled"] = null;
            }

            //if (!IsPostBack)
            //{
                resultList = userBl.GetReservationHistory(userId);
                PassengerHistory.DataSource = resultList;
                PassengerHistory.DataBind();
            //}
          

        }

        //protected void Button_ShowHistory_Click(object sender, EventArgs e)
        //{
        //    resultList = userBl.GetReservationHistory(userId);
        //    PassengerHistory.DataSource = resultList;
        //    PassengerHistory.DataBind();
        //}

        public void PopulateTicket(TicketModel ticketModel, List<PassengerDetailModel> passengerList)
        {
            try
            {
                //Populate DataTable
                DataTable dt = new DataTable();
                dt.Columns.Add("PNR");
                dt.Columns.Add("TripCode");
                dt.Columns.Add("NoofSeats");
                dt.Columns.Add("StartPlace");
                dt.Columns.Add("DepartureTime");
                dt.Columns.Add("EndPlace");
                dt.Columns.Add("DOR");
                dt.Columns.Add("DOJ");
                dt.Columns.Add("BoardingPoint");
                dt.Columns.Add("TotalFare");
                dt.Rows.Add();
                dt.Rows[0]["PNR"] = ticketModel.PNR;
                dt.Rows[0]["TripCode"] = ticketModel.TripCode;
                dt.Rows[0]["NoofSeats"] = ticketModel.NumberofSeats;
                dt.Rows[0]["StartPlace"] = ticketModel.Source;
                dt.Rows[0]["DepartureTime"] = string.Format("{0:00}:{1:00}", ticketModel.DepartureTime.Hours, ticketModel.DepartureTime.Minutes);
                dt.Rows[0]["EndPlace"] = ticketModel.Destination;
                dt.Rows[0]["DOR"] = ticketModel.DOR.ToShortDateString();
                dt.Rows[0]["DOJ"] = ticketModel.DOJ.ToShortDateString();
                dt.Rows[0]["BoardingPoint"] = ticketModel.BoardingPoint;
                dt.Rows[0]["TotalFare"] = ticketModel.Fare;

                //Bind Datatable to Labels
                lblPNR.Text = dt.Rows[0]["PNR"].ToString();
                lblTripCode.Text = dt.Rows[0]["TripCode"].ToString();
                lblNoofSeats.Text = dt.Rows[0]["NoofSeats"].ToString();
                lblStartPlace.Text = dt.Rows[0]["StartPlace"].ToString();
                lblDepartureTime.Text = dt.Rows[0]["DepartureTime"].ToString();
                lblEndPlace.Text = dt.Rows[0]["EndPlace"].ToString();
                lblDateOfReservation.Text = dt.Rows[0]["DOR"].ToString();
                lblDateOfJourney.Text = dt.Rows[0]["DOJ"].ToString();
                lblTotalFare.Text = dt.Rows[0]["TotalFare"].ToString();
                //lblBoardingPoint.Text = dt.Rows[0]["BoardingPoint"].ToString();

                PassengerDetail.DataSource = passengerList;
                PassengerDetail.DataBind();
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> ticket.cs, Function->PopulateTicket  : \n " + ex);
            }
        }
        protected void DownloadTicket()
        {
            try
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=iBusTicket.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //this.Page.RenderControl(hw);
                Panel_Ticket.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> ticket.cs, Function->Button_Download_Click  : \n " + ex);
            }

        }

        protected void Link_Download_Ticket_Command(object sender, CommandEventArgs e)
        {
            int reservationID = Convert.ToInt32(e.CommandArgument);
            UpdateDailyTripDetailModel response = new UpdateDailyTripDetailModel();
            ReservationDetailModel reservation = new ReservationDetailModel();
            reservation.ResID = reservationID;
            response = userBl.CheckTicketStatus(reservation);
            if (response.Status == AppConstant.SUCCESS)
            {
                Div_PassengerList_SucessMessage.Style["display"] = "block";
                Div_PassengerList_SucessMessage.InnerHtml = DisplayMessage.TICKET_ALREADY_CANCELLED;

            }
            else
            {
                ticketModel = userBL.GetTicketDetails(reservationID);
                passengerList = userBL.GetPassengerList(reservationID);
                PopulateTicket(ticketModel, passengerList);
                DownloadTicket();              
            }
        }

        protected void Link_Email_Ticket_Command(object sender, CommandEventArgs e)
        {
            int reservationID = Convert.ToInt32(e.CommandArgument);
            ReservationDetailModel reservation = new ReservationDetailModel();
            reservation.ResID = reservationID;
            UpdateDailyTripDetailModel response = new UpdateDailyTripDetailModel();
            response = userBl.CheckTicketStatus(reservation);
            if (response.Status == AppConstant.SUCCESS)
            {
                Div_PassengerList_SucessMessage.Style["display"] = "block";
                Div_PassengerList_SucessMessage.InnerHtml = DisplayMessage.TICKET_ALREADY_CANCELLED;

            }
            else
            {
                ticketModel = userBL.GetTicketDetails(reservationID);
                passengerList = userBL.GetPassengerList(reservationID);
                PopulateTicket(ticketModel, passengerList);
                ReservationDetailModel reservationDetail = new ReservationDetailModel();
                reservationDetail = userBl.GetEmailID(reservationID);
                SendMail(reservationDetail.ResEmailID);
                Div_PassengerList_SucessMessage.Style["display"] = "block";
                Div_PassengerList_SucessMessage.InnerHtml = DisplayMessage.TICKET_MAIL_SENT;
            }

        }

        public void SendMail(String mailID)
        {
            try
            {
                string mailTo = null;
                mailTo = mailID;
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    {
                        Panel_Ticket.RenderControl(hw);
                        StringReader sr = new StringReader(sw.ToString());
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            PdfWriter.GetInstance(pdfDoc, memoryStream);
                            pdfDoc.Open();
                            htmlparser.Parse(sr);
                            pdfDoc.Close();
                            byte[] bytes = memoryStream.ToArray();
                            memoryStream.Close();

                            MailMessage mm = new MailMessage("iBusTicketing@gmail.com", mailTo);
                            mm.Subject = "iBusTicket";
                            mm.Body = "You have successfully booked your ticket";
                            mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "iBusPDF.pdf"));
                            mm.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                            NetworkCred.UserName = "iBusTicketing@gmail.com";
                            NetworkCred.Password = "ibus12345";
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> ticket.cs, Function->Button_GenerateTicketviaEmail_Click  : \n " + ex);
            }
        }

        protected void Link_Cancel_Ticket_Command(object sender, CommandEventArgs e)
        {
            int reservationID = Convert.ToInt32(e.CommandArgument);
            ReservationDetailModel reservation = new ReservationDetailModel();
            reservation.ResID = reservationID;
            UpdateDailyTripDetailModel response = new UpdateDailyTripDetailModel();
            response = userBl.CheckTicketStatus(reservation);
            if (response.Status == AppConstant.SUCCESS)
            {
                Div_PassengerList_SucessMessage.Style["display"] = "block";
                Div_PassengerList_SucessMessage.InnerHtml = DisplayMessage.TICKET_ALREADY_CANCELLED;

            }
            else
            {
                response = userBl.CancelReservation(reservation);
                if (response.Status == AppConstant.SUCCESS)
                {
                    Div_PassengerList_SucessMessage.Style["display"] = "block";
                    Div_PassengerList_SucessMessage.InnerHtml = DisplayMessage.TICKET_CANCELLED;
                }
            }
        }

        protected void Link_Cancel_Ticket_Passenger_Wise_Command(object sender, CommandEventArgs e)
        {
            int reservationID = Convert.ToInt32(e.CommandArgument);
            ReservationDetailModel reservation = new ReservationDetailModel();
            reservation.ResID = reservationID;
            UpdateDailyTripDetailModel response = new UpdateDailyTripDetailModel();
            response = userBl.CheckTicketStatus(reservation);
            if (response.Status == AppConstant.SUCCESS)
            {
                Div_PassengerList_SucessMessage.Style["display"] = "block";
                Div_PassengerList_SucessMessage.InnerHtml = DisplayMessage.TICKET_ALREADY_CANCELLED;

            }
            else
            {
                Session["PNR"] = reservationID;
                Response.Redirect("PassengerList.aspx",false);
            }
        }

    }

}
