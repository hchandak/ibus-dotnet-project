﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessLogic;
using BussinessModel;
using DataValidation;
namespace iBus
{

    public partial class ibus : System.Web.UI.Page
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static string strCities;
        protected void Page_Load(object sender, EventArgs e)
        {
                if (Session["Message"] != null)
                {
                    Div_ErrorMessageLoginRegister.Style["display"] = "block";
                    Div_ErrorMessageLoginRegister.InnerHtml = Session["Message"].ToString();
                    Session["Message"] = null;  
                    
                }

       

            Div_ErrorMessageBusSearch.InnerHtml = "";
            BusSearchBL busSearchBLObj = new BusSearchBL();
            try
            {
                strCities = busSearchBLObj.GetCityListString();
                HiddenField_Cities.Value = strCities;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Exception message->" + ex.Message + "\n  Exception source->" + ex.Source + "\n  Exception stack trace->" + ex.StackTrace + "\n  Exception TargetSite->" + ex.TargetSite);
            }
            finally
            {

            }

        }

        protected void Button_Search_Click(object sender, EventArgs e)
        {
            BusSearchModel busSearchDetailsObj = new BusSearchModel();

            try
            {
                busSearchDetailsObj.FromCity = TextBox_FromCity.Text.Trim().ToLower();
                busSearchDetailsObj.ToCity = TextBox_ToCity.Text.Trim().ToLower();
                busSearchDetailsObj.DateOfJourney = TextBox_DateOfJourney.Text.Trim().ToLower();
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Exception message->" + ex.Message + "\n  Exception source->" + ex.Source + "\n  Exception stack trace->" + ex.StackTrace + "\n  Exception TargetSite->" + ex.TargetSite);
            }

           
            UserValidation userValidationObj = new UserValidation();
            try
            {
                string strValidationResult = userValidationObj.ValidateBusSearch(ref busSearchDetailsObj, strCities);
                if (strValidationResult == AppConstant.SUCCESS.ToString())
                {
                    Session["busSearchDetails"] = busSearchDetailsObj;
                    Response.Redirect("bus-list.aspx");
                }
                else
                {
                    Div_ErrorMessageBusSearch.Style["display"] = "block";
                    Div_ErrorMessageBusSearch.InnerHtml = strValidationResult;

                }
            }

            catch (Exception ex)
            {
                Logger.Error("\n\nError Exception message->" + ex.Message + "\n  Exception source->" + ex.Source + "\n  Exception stack trace->" + ex.StackTrace + "\n  Exception TargetSite->" + ex.TargetSite);
            }
        }



    }
}