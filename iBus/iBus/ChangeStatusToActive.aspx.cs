﻿using DataAccessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessModel;

namespace iBus
{
    public partial class ChangeStatusToActive : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ClassToActiveStatus toActiveObj = new ClassToActiveStatus();
            toActiveObj.UpdateDailyTrip();
            toActiveObj.UpdateTrip();
            toActiveObj.UpdateRoute();
            toActiveObj.UpdateBus();
            toActiveObj.UpdateTravel();
        }
    }
}