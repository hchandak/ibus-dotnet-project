﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="modal-display-message.ascx.cs" Inherits="iBus.UserControl.ModalDisplayMessage" %>
 <!--SignIn Modal Popup Start-->
     <div class="modal fade  " id="Modal_DispalyMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog div-signin-signup ">
        <div class="modal-content ">
           
            <div class="modal-body">
                  <div id="Div_MessageBody" class="alert alert-danger" runat="server" ClientIDMode="Static"></div>
            </div>
                 
            <div class="modal-footer">
                <asp:Button ID="Button_DeleteTravel" runat="server" Text="Delete Travel" CssClass="button-blue pull-left" OnClientClick="return DeleteTravelDetail()" style="display:none" ClientIDMode="Static" data-dismiss="modal"/>
                <asp:Button ID="Button_DeleteBus" runat="server" Text="Delete Bus" CssClass="button-blue pull-left" OnClientClick="return DeleteBusDetail()" style="display:none"  ClientIDMode="Static"  data-dismiss="modal"/>
                <asp:Button ID="Button_DeleteRoute" runat="server" Text="Delete Route" CssClass="button-blue pull-left" OnClientClick="return DeleteRouteDetail()" style="display:none"  ClientIDMode="Static"  data-dismiss="modal"/>
                <asp:Button ID="Button_DeleteTrip" runat="server" Text="Delete Trip" CssClass="button-blue pull-left" OnClientClick="return DeleteTripDetail()" style="display:none"  ClientIDMode="Static"  data-dismiss="modal"/>
                <asp:Button ID="Button_Cancel" runat="server" Text="Cancel" CssClass="button-blue pull-right" ClientIDMode="Static" data-dismiss="modal" />
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--SignIn Modal Popup End-->
               