﻿using BussinessLogic;
using BussinessModel;
using DataValidation;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iBus.UserControl
{
    public partial class ModalSigninSignup : System.Web.UI.UserControl
    {
        UserBL userbl = new UserBL();
        UserDetailModel user = new UserDetailModel();
        LoginUserDetailModel loginUser = new LoginUserDetailModel();
        UserValidation validator = new UserValidation();
        AddUserDetailModel addUserDeatilModel = new AddUserDetailModel();
        string validatdestatus = "";
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Button_SignUp_Click1(object sender, EventArgs e)
        {
            string EmailSignUp, FirstNameSingnUp, LastNameSignUp, PassswordSignUp, ConfirmPasswordSignUp, ContactSignUp;
            EmailSignUp = TextBox_SignUpEmail.Text.ToString();
            FirstNameSingnUp = TextBox_FirstName.Text.ToString();
            LastNameSignUp = TextBox_LastName.Text.ToString();
            PassswordSignUp = TextBox_SignUpPassword.Text.ToString();
            ConfirmPasswordSignUp = TextBox_SignUpConfirmPassword.Text.ToString();
            ContactSignUp = TextBox_SignUpContact.Text.ToString();

            try
            {
                validatdestatus = validator.ValidateUserRegistrationDetails(EmailSignUp, FirstNameSingnUp, LastNameSignUp, PassswordSignUp, ConfirmPasswordSignUp, ContactSignUp);
                if (validatdestatus == DisplayMessage.VALIDATION_SUCCESS)
                {
                    user.UserTypeID = Convert.ToByte(AppConstant.USER_TYPE_ID[0]);
                    user.EmailID = EmailSignUp;
                    user.FirstName = FirstNameSingnUp;
                    user.LastName = LastNameSignUp;                     
                    user.Password = userbl.EncryptPassword(PassswordSignUp);
                    user.MobileNo = ContactSignUp;
                    user.CreatedBy = EmailSignUp;
                    user.Status = Convert.ToInt32(AppConstant.STATUS.ACTIVE);
                    user.CreatedDate = System.DateTime.Today.ToShortDateString();

                    loginUser = userbl.CkeckUserIfAlreadyRegistered(user);
                    if (loginUser.Status == AppConstant.ERROR)
                    {
                        //Div_ErrorMessage.Style["display"] = "block";
                        string querystring = string.Empty;
                        //Div_ErrorMessage.InnerHtml = DisplayMessage.USER_ALREADY_REGISTERED;
                        string msg  = DisplayMessage.USER_ALREADY_REGISTERED;
                        Session["Message"] = msg;
                        string url = Request.RawUrl;
                        Page.Response.Redirect("~" + url, true);  
                    }
                    else
                    {
                        addUserDeatilModel = userbl.RegisterUser(user);
                        if (addUserDeatilModel.Status == AppConstant.SUCCESS)
                        {
                            SendMailModel mailDetail = new SendMailModel();
                            mailDetail.Subject = "Notification";
                            mailDetail.EmailID = user.EmailID;
                            mailDetail.Message = "You are registered...";
                            userbl.SendMail(mailDetail);
                            Session["userID"] = user.UserID;
                            Session["userEmail"] = user.EmailID;
                            Session["userTypeID"] = user.UserTypeID;
                            Session["userFirstName"] = user.FirstName;
                            Session["userLastName"] = user.LastName;
                            Session["userMobileNo"] = user.MobileNo;
                            string url = Request.RawUrl;
                            Page.Response.Redirect("~" + url, true);

                        }
                        else
                        {
                            //Div_ErrorMessage.Style["display"] = "block";
                            //Div_ErrorMessage.InnerHtml = DisplayMessage.USER_NOT_REGISTERED;
                            string url = Request.RawUrl;
                            Page.Response.Redirect("~" + url, true);
                         
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> ModalSigninSignup.cs, Function->Button_SignUp_Click1  : \n " + ex);
            }
        }
        protected void Button_SignIn_Click(object sender, EventArgs e)
        {
            string EmailSignIn, PasswordSignIn;
            EmailSignIn = TextBox_SignInEmail.Text.ToString();
            PasswordSignIn = TextBox_SignInPassword.Text.ToString();
            validatdestatus = validator.ValidateUserLoginDetails(EmailSignIn, PasswordSignIn);
            try
            {
                if (validatdestatus.Equals(DisplayMessage.VALIDATION_SUCCESS))
                {
                    user.EmailID = EmailSignIn;
                    user.Password = userbl.EncryptPassword(PasswordSignIn);
                    loginUser = userbl.LoginUser(user);
                    if (loginUser.Status == AppConstant.SUCCESS)
                    {
                        Session["userID"] = loginUser.UserID;
                        Session["userEmail"] = loginUser.EmailID;
                        Session["userTypeID"] = loginUser.UserTypeID;
                        Session["userFirstName"] = loginUser.FirstName;
                        Session["userLastName"] = loginUser.LastName;
                        Session["userMobileNo"] = loginUser.MobileNo;

                        if (loginUser.UserTypeID == AppConstant.USER_TYPE_ID[0])//Redirect to user page
                        {
                            //string url1 = Page.Request.Url.ToString();
                            string url = Request.RawUrl;
                            Page.Response.Redirect("~" + url, true);

                            //string str2 = Page.Request.Url.ToString();
                        }
                        else if (loginUser.UserTypeID == AppConstant.USER_TYPE_ID[1])// Redirect to admin page
                        {
                            Response.Redirect("~/Admin/admin.aspx", false);
                        }
                    }
                    else
                    {
                        loginUser = userbl.CkeckUserIfAlreadyRegistered(user);
                        if (loginUser.Status == AppConstant.ERROR)
                        {
                            //Div_ErrorMessage.Style["display"] = "block";
                            //Div_ErrorMessage.InnerHtml = DisplayMessage.INVALID_USERID_PASSWORD;
                            string msg = DisplayMessage.INVALID_USERID_PASSWORD;
                            Session["Message"] = msg;  
                            string url = Request.RawUrl;                                                       
                            Page.Response.Redirect("~" + url, true);  
                        }
                        else
                        {
                            //Div_ErrorMessage.Style["display"] = "block";
                            //Div_ErrorMessage.InnerHtml = DisplayMessage.INVALID_LOGIN_DETAILS;
                            string msg = DisplayMessage.INVALID_LOGIN_DETAILS;
                            Session["Message"] = msg;
                            string url = Request.RawUrl;
                            Page.Response.Redirect("~" + url, true);   

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> ModalSigninSignup.cs, Function->Button_SignIn_Click  : \n " + ex);
            }
        }
    }

}
