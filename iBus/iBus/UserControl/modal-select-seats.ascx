﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="modal-select-seats.ascx.cs" Inherits="iBus.UserControl.modal_select_seats" %>
<div class="modal fade" id="Modal_SelectBusSeat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  div-select-seat">
        <div class="modal-content ">
            <div class="modal-header">
               
               <%-- <link href="../Style/seat-layout.css" rel="stylesheet" />--%>
                <link href="../Style/seat-layout-modified.css" rel="stylesheet" />
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Select seat(s)</h4>
            
            </div>
            <div class="modal-body ">
                <div class="container">
                        
                    <div class="row bus-info">
                        
                            <ul class="nav navbar-nav ">
                            <li>
                                <asp:Label ID="Label_FromToCity" runat="server" Text=""></asp:Label>

                            </li>
                            <li style="margin-left: 5px; margin-right: 5px;">|
                            </li>
                            <li>
                                <asp:Label ID="Label_DateOfJourney" runat="server" Text=""></asp:Label>
                            </li>
                            <li style="margin-left: 5px; margin-right: 5px;">|
                            </li>
                            <li>
                                <asp:Label ID="Label_BusOperator" runat="server" Text=""></asp:Label>
                            </li>
                            <li style="margin-left: 5px; margin-right: 5px;">|
                            </li>
                            <li>
                                <asp:Label ID="Label_BusType" runat="server" Text=""></asp:Label>

                            </li>
                            <li style="margin-left: 5px; margin-right: 5px;">|
                            </li>
                            <li>
                                <asp:Label ID="Label_DepartureTime" runat="server" Text=""></asp:Label>
                            </li>
                        </ul>
                        
                        
                        <asp:HiddenField ID="HiddenField_Fare" runat="server" ClientIDMode="Static"/>
                    </div>

                    <br />

                    <div id="holder" class="row">
                        <div class="col-md-9">
                            <div id="Div_SeaterLayout" class="semi-sleeper-layout">
                                <ul id="seater" class="ul-place">
                                </ul>
                            </div>
                            <div id="Div_SleeperLayout">
                                <div id="Div_SleeperLayout_Upper" class="sleeper-layout">
                                    <div>
                                        <asp:Label ID="Label1" runat="server" Text="Upper"></asp:Label>
                                    </div>
                                    <ul id="upper" class="ul-place">
                                    </ul>
                                </div>
                                <div id="Div_SleeperLayout_Lower" class="sleeper-layout">
                                    <div>
                                        <asp:Label ID="Label2" runat="server" Text="Lower"></asp:Label>
                                    </div>
                                    <ul id="lower" class="ul-place">
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3" >
                            <div style="float: left; margin-top:30px; vertical-align:central;">
                            <ul id="seatDescription">
                                <li style="background: url('../Image/available_seat_img.gif') no-repeat;">Available Seat</li>
                                <li style="background: url('../Image/booked_seat_img.gif') no-repeat scroll 0 0 transparent;">Booked Seat</li>
                                <li style="background: url('../Image/selected_seat_img.gif') no-repeat scroll 0 0 transparent;">Selected Seat</li>
                            </ul>
                        </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            
                            <asp:Label ID="Label_SelectedSeatsTitle" runat="server" Text="Selected seat(s) : "></asp:Label>
                            <asp:HiddenField ID="HiddenField_SeatsSelected" runat="server" ClientIDMode="Static" />
                        
                            <asp:Label ID="Label_SelectedSeats" runat="server"  ClientIDMode="Static"></asp:Label>
                       
                        </div>
                        <div class="col-md-6">
                            <asp:Label ID="Label_TotalFareTitle" runat="server" Text="Total Fare : "></asp:Label>
                            <asp:Label ID="Label_TotalFare" runat="server" Text="0" ClientIDMode="Static"></asp:Label>
                       
                        </div>
                        
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Label ID="Label_BoardingPoint" runat="server" Text="Select boarding point : "></asp:Label>
                            <asp:DropDownList ID="DropDownList_BoardingPoint" runat="server"></asp:DropDownList>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="Button_Continue" runat="server" Text="Continue >>" OnClientClick="return saveSeatsToHiddenField();" OnClick ="Button_Continue_Click" CssClass="button-blue" ClientIDMode="Static"/>
               <%-- <asp:Button ID="Button_Continue" runat="server" Text="Continue >>" CssClass="btn-primary" OnClientClick="return saveSeatsToHiddenField();"  ClientIDMode="Static" Enabled="False" />
               --%>
                
            </div>

        </div>
    </div>
</div>
