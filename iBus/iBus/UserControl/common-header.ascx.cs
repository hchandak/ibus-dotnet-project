﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using log4net;
namespace iBus.UserControl
{
    public partial class CommonHeader : System.Web.UI.UserControl
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"] != null && Session["userID"].ToString().Trim().Length > 0)
            {
                Div_CustomerHeader.Style["display"] = "block";
                Div_DefaultHeader.Style["display"] = "none";
                Label_UserEmail.Text = Session["userEmail"].ToString();
            }
            else
            {
                Div_CustomerHeader.Style["display"] = "none";
                Div_DefaultHeader.Style["display"] = "block";
            }
        }

        protected void LinkButton_CustomerSignOut_Click(object sender, EventArgs e)
        {
            try
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                cookie.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(cookie);
                Response.Redirect("~/ibus.aspx", false);
            
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> CommonHeader.cs, Function->LinkButton_CustomerSignOut_Click  : \n " + ex);
            }
        }
    }
}