﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="modal-signin-signup.ascx.cs" Inherits="iBus.UserControl.ModalSigninSignup" %>
<!--SignIn Modal Popup Start-->
<asp:ScriptManagerProxy ID="ScriptManagerProxy2" runat="server"></asp:ScriptManagerProxy>
     <!--SignIn Modal Popup Start-->
     <div class="modal fade  " id="Modal_SignInSignUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog large_control div-signin-signup ">
        <div class="modal-content ">
            <div class="modal-header shadow ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Please fill required info</h4>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <div class="modal-body medium_control">
                
                 <div id="Div_ErrorMessage" class="alert alert-danger" runat="server" ClientIDMode="Static"></div>                
                <div id="Div_SignUp" style="display: none">

                    <span>SingUp here</span><br />
                    <br />
                   
                    <asp:TextBox type="text" class="form-control text_box_size shadow " ID="TextBox_SignUpEmail" runat="server" ClientIDMode="Static" placeholder="Email*" /><br />
                    <asp:TextBox type="text" class="form-control text_box_size shadow" ID="TextBox_FirstName" runat="server" ClientIDMode="Static" placeholder="First Name*" /><br />
                    <asp:TextBox type="text" class="form-control text_box_size shadow" ID="TextBox_LastName" runat="server" ClientIDMode="Static" placeholder="LastName*" /><br />
                    <asp:TextBox type="password" class="form-control text_box_size shadow" ID="TextBox_SignUpPassword" runat="server" ClientIDMode="Static" placeholder="Password*" /><br />
                    <asp:TextBox type="password" class="form-control text_box_size shadow" ID="TextBox_SignUpConfirmPassword" runat="server" ClientIDMode="Static" placeholder="Confirm Password*" /><br />
                    <asp:TextBox type="text" class="form-control text_box_size shadow" ID="TextBox_SignUpContact" runat="server" ClientIDMode="Static" placeholder="Contact*" /><br />
                </div>

                <div id="Div_SignIn">
                    <span>SignIn here </span>
                    <div id="AlertWindowSignin" class="alert alert-danger" style="display: none"></div>
                    <br />
                    <br />
                    <asp:TextBox type="text" class="form-control text_box_size shadow" ID="TextBox_SignInEmail" runat="server" ClientIDMode="Static" placeholder="Email*" /><br />
                    <asp:TextBox type="password" class="form-control text_box_size shadow" ID="TextBox_SignInPassword" runat="server" ClientIDMode="Static" placeholder="Password*" /><br />
                </div>
                <a id="Link_Register"class="a-color" href="#">Are you new user?<br />
                    <br />
                </a>
                <a id="Link_SignIn" class="a-color" href="#" style="display: none">Are you allready registered?<br />
                    <br />
                </a>

            </div>
           
             </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Button_SignIn" EventName="Click"/>
                   <asp:AsyncPostBackTrigger ControlID="Button_SignUp" EventName="Click"/>
                </Triggers>
             </asp:UpdatePanel>
            <div class="modal-footer">
                <asp:Button ID="Button_SignIn" runat="server" Text="SignIn" CssClass="button-blue pull-right" ClientIDMode="Static" Height="30px" Width="90px"  OnClick="Button_SignIn_Click" AutoPostBack="true" />
                <asp:Button ID="Button_SignUp" runat="server" Text="SignUp" CssClass="button-blue pull-right" ClientIDMode="Static"  Height="30px" Width="90px"  OnClick="Button_SignUp_Click1" AutoPostBack="true"  />
                <asp:Button ID="Button_Cancel" runat="server" Text="Cancel" CssClass="button-blue pull-left" ClientIDMode="Static" Height="30px" Width="90px"  data-dismiss="modal" />
            </div>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--SignIn Modal Popup End-->
 