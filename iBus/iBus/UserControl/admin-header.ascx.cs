﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using log4net;
namespace iBus.UserControl
{
    public partial class AdminHeader : System.Web.UI.UserControl
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["userEmail"]!=null)
               Label_TravelEmail.Text = Session["userEmail"].ToString() + "...";
        }

        protected void LinkButton_AdminSignOut_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Clear();
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                cookie.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(cookie);
                Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetNoStore();
                FormsAuthentication.SignOut();
                Response.Redirect("~/ibus.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AdminHeader.cs, Function->LinkButton_AdminSignOut_Click  : \n " + ex);
            }
        }

        
    }
}