﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="admin-header.ascx.cs" Inherits="iBus.UserControl.AdminHeader" %>

<nav class="navbar div-banner" role="navigation">
          <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav">
                    <li ><img src="/Image/ibus-logo.png" /></li> 
               </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li class="text-right" style="margin-top:10px"><a class="a-color" style="cursor:pointer" onclick="RedirectToAdminAccount()"><asp:Label runat="server" ID="Label_TravelEmail" ></asp:Label></a></li>
              </ul>
              
          </div>
 
          <div class="collapse navbar-collapse navbar-ex1-collapse div-header">
               <ul class="nav navbar-nav ">
                    <li ><a href="../Admin/admin.aspx">My Account</a></li>
                    <li ><a  style="cursor:pointer"  href="../Admin/travel-detail.aspx">Travels</a></li>
                    <li ><a style="cursor:pointer" href="../Admin/route-detail.aspx" >Routes</a></li>
                    <li ><a style="cursor:pointer" href="../Admin/bus-detail.aspx">Bus</a></li>
                    <li ><a style="cursor:pointer" href="../Admin/trip-detail.aspx">Trips</a></li>
                    <li><a href="../Admin/about-us.aspx">About Us</a></li>
                    <li><a href="../Admin/contact-us.aspx">Contact</a></li>
                </ul>
               <ul class="nav navbar-nav navbar-right">
                    
                    <li><asp:LinkButton runat="server" ID="LinkButton_AdminSignOut" OnClick="LinkButton_AdminSignOut_Click" >SignOut</asp:LinkButton></li>
               </ul>
          </div>
 </nav><!-- Nav End -->
