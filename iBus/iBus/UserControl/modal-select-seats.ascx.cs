﻿using BussinessModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessLogic;

namespace iBus.UserControl
{
    public partial class modal_select_seats : System.Web.UI.UserControl
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void LoadPopUP()
        {

            BookSeatBL bookSeatObj = new BookSeatBL();
            BusSearchModel busSearchModelObj = (BusSearchModel)Session["busSearchDetails"];
            //BusSearchModel busSearchModelObj = new BusSearchModel();
            //busSearchModelObj.FromCity = "Surat";
            //busSearchModelObj.ToCity = "Mumbai";
            //busSearchModelObj.DateOfJourney = "10/13/2013";
            //Reservation_PassengerBL resPassObj = new Reservation_PassengerBL();
            ShowBusListModelModified selectedBusDetail = (ShowBusListModelModified)Session["selectedBusDetail"];

            Label_FromToCity.Text = busSearchModelObj.FromCity + " to " + busSearchModelObj.ToCity;
            Label_DateOfJourney.Text = busSearchModelObj.DateOfJourney;
            Label_BusOperator.Text = selectedBusDetail.TravelName;
            Label_BusType.Text = selectedBusDetail.BusType;
            Label_DepartureTime.Text = "Departure : " + selectedBusDetail.DepartureTime.ToString();
            HiddenField_Fare.Value = selectedBusDetail.Fare.ToString();

            DataAccessObject.TripDAO tripDataObject = new DataAccessObject.TripDAO();
            DropDownList_BoardingPoint.DataSource = tripDataObject.GetBoradingPointsDepartureTime(selectedBusDetail.TripID);
            //DropDownList_BoardingPoint.DataTextField = ds.Tables[0].Columns[1].ToString();
            //myDropDown.DataValueField = ds.Tables[0].Columns[0].ToString();
            DropDownList_BoardingPoint.DataBind();

        }

        protected void Button_Continue_Click(object sender, EventArgs e)
        {
            //Session["SelectedNoOfSeats"] = DropDownList_SelectNoOfSeats.SelectedValue;

            try { 
                ShowBusListModelModified selectedBusDetail = (ShowBusListModelModified)Session["selectedBusDetail"];
            
            string str_SeatIDs = HiddenField_SeatsSelected.Value;
            
            Session["SeatIDs"] = str_SeatIDs;
            Session["DailyTripID"] = selectedBusDetail.DailyTripID;
            Response.Redirect("add-passenger-details.aspx");    
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Exception message->" + ex.Message + "\n  Exception source->" + ex.Source + "\n  Exception stack trace->" + ex.StackTrace + "\n  Exception TargetSite->" + ex.TargetSite);
            }
            finally{
            }
            



        }
    }
}