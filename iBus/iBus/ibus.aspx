﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ibus.aspx.cs" Inherits="iBus.ibus"
    MasterPageFile="~/MasterPage/Default.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Script/registration.js"></script>
    <script src="Script/jquery-ui-1.10.3.custom.js"></script>
    <link href="Style/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="/Script/js-image-slider.js"></script>
    <link href="/Style/js-image-slider.css" rel="stylesheet" />
   <%--  <script type="text/javascript">
        window.history.forward(1);
    </script>--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <iBus:Control_ModalSignInSignUp ID="Control_ModalSignInSignUp" runat="server" />

    <div class="container">
         <div id="Div_ErrorMessageLoginRegister" class="alert alert-danger" runat="server" clientidmode="Static" style="display: none"></div>  
        <div class="alert alert-danger" id="Div_ErrorMessageBusSearch" clientidmode="Static" style="display: none" runat="server"></div>
     
        <div class="row">
            <asp:Label ID="Label_heading" runat="server"  CssClass="inset-text"  Text="Online bus ticket booking "></asp:Label>
        </div>
      
        <br />
      
        <div class="row" >
            <div class="col-md-2">
          
                    <div class=" form-group">

                        <asp:Label ID="Label_From" runat="server" Text="From"></asp:Label>
                        <asp:TextBox ID="TextBox_FromCity" runat="server" CssClass="form-control capitalize shadow-light " ClientIDMode="Static"></asp:TextBox>

                    </div>
                    <div class=" form-group">

                        <asp:Label ID="Label_To" runat="server" Text="To"></asp:Label>
                        <asp:TextBox ID="TextBox_ToCity" runat="server" CssClass="form-control capitalize shadow-light" ClientIDMode="Static"></asp:TextBox>

                    </div>
              
                    <div class=" form-group">
                        <asp:Label ID="Label_DateOfJourney" runat="server" Text="Date of journey"></asp:Label>

                        <asp:TextBox ID="TextBox_DateOfJourney" runat="server" CssClass="form-control shadow-light" placeholder="dd-mm-yyyy" ClientIDMode="Static"></asp:TextBox><br />
                        <br />
                          <asp:Button ID="Button_Search" runat="server" Text="Search" class="searchButton" OnClientClick="return ValidateAll();" OnClick="Button_Search_Click" />
                          
                    </div>
                   <%-- <img src="/Image/animated-bus.jpg" id="img_bus_red"  /> --%>
            </div>
           
           <div class="col-md-5 col-md-offset-2" >
               <iBus:Control_AdvertisementTravel runat="server" ID="Control_AdvertisementTravel" />
                        
          </div>
            
        </div>
        <asp:HiddenField ID="HiddenField_Cities" runat="server" ClientIDMode="Static" />
    </div>

   

    <script src="Script/bus-search.js"></script>
</asp:Content>
