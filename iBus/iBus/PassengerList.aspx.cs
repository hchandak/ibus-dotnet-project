﻿using BussinessLogic;
using BussinessModel;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iBus
{
    public partial class PassengerList : System.Web.UI.Page
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        UserBL userbl = new UserBL();
        List<PassengerDetailModel> passengerlist = new List<PassengerDetailModel>();

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// This is reading values from session & putting it into reservationID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["PNR"] != null && Session["PNR"].ToString().Length > 0)
                {
                    int reservationID = Convert.ToInt32(Session["PNR"]);
                    passengerlist = userbl.GetPassengerList(reservationID);
                    PassengerDetail.DataSource = passengerlist;
                    PassengerDetail.DataBind();
                }
                else
                {
                    Response.Redirect("ibus.aspx", false);
                }
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// This function fires event to cancel reservation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_CancelReservation_Click(object sender, EventArgs e)
        {
            UpdatePassengerDetailModel response = new UpdatePassengerDetailModel();
            int ticketCancelled = 0;
            try
            {
                foreach (RepeaterItem i in PassengerDetail.Items)
                {
                    //Retrieve the state of the CheckBox
                    CheckBox cb = (CheckBox)i.FindControl("chkbox_Cancel_Ticket");
                    if (cb.Checked)
                    {
                        //Retrieve the value associated with that CheckBox
                        HiddenField passengerIDHidden = (HiddenField)i.FindControl("hiddenPassengerID");
                        int passengerID = Convert.ToInt32(passengerIDHidden.Value);
                        response = userbl.CancelPassengerReservation(passengerID);
                        ticketCancelled++;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> PassengerList.cs, Function->Button_CancelReservation_Click  : \n " + ex);
            }

            Session["ticketCancelled"] = ticketCancelled;
            Response.Redirect("profile-page.aspx",false);
        }

        protected void Button_Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("profile-page.aspx", false);
        }
    }
}