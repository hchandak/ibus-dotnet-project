﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace iBus.MasterPage
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if (Session["userID"] != null && Session["userID"].ToString().Trim().Length > 0)
           {
           }
           else
           {
               Session.Clear();
               HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
               cookie.Expires = DateTime.Now.AddYears(-1);
               Response.Cookies.Add(cookie);
               Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
               //Response.Cache.SetCacheability(HttpCacheability.NoCache);
               //Response.Cache.SetNoStore();
               //Response.Cache.SetNoStore();
               //Response.Cache.SetCacheability(HttpCacheability.NoCache);
               //Response.AppendHeader("pragma", "no-cache");
               //Response.Buffer = true;
               //Response.CacheControl = "no-cache";
               //Response.AddHeader("Pragma", "no-cache");
               //Response.Expires = -1441;
               FormsAuthentication.SignOut();
               Response.Redirect("~/ibus.aspx", false);
           }
        }
    }
}