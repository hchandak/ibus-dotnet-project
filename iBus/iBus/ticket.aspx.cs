﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessModel;
using BussinessLogic;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Net.Mail;
using System.Net;
using log4net.Repository.Hierarchy;



namespace iBus
{
    public partial class ticket : System.Web.UI.Page
    {
        UserBL userbl = new UserBL();
        TicketModel ticketModel = new TicketModel();
        List<PassengerDetailModel> passengerList = new List<PassengerDetailModel>();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"] != null && Session["userID"].ToString().Length > 0)
            {

            }
            else
            {
                Response.Redirect("ibus.aspx", false);
            }

            Div_SucessMessage.Visible = true;
            Div_SucessMessage.InnerHtml = DisplayMessage.TICKET_BOOK_SUCCESS;
            if (Session["ticketModel"] != null && Session["ticketModel"].ToString().Trim().Length > 0)
            {
                ticketModel = (TicketModel)Session["ticketModel"];
            }

            if (Session["passengerList"] != null && Session["passengerList"].ToString().Trim().Length > 0)
            {   //taking passenger list for icket from session
                passengerList = (List<PassengerDetailModel>)Session["passengerList"];
            }

            if (ticketModel != null && passengerList != null)
            {
                PopulateTicket(ticketModel, passengerList);
            }

        }
        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  Generates Pdf
        /// </summary>  
        protected void Button_Download_Click(object sender, EventArgs e)
        {
            try
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=iBusTicket.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);    
                //this.Page.RenderControl(hw);
                Panel_Ticket.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> ticket.cs, Function->Button_Download_Click  : \n " + ex);
            }

        }
        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  Send Mail with attachment
        /// </summary>  
        protected void Button_GenerateTicketviaEmail_Click(object sender, EventArgs e)
        {
            try
            {
                string mailTo = null;
                if (Session["PassengerEmailId"] != null && Session["PassengerEmailId"].ToString().Trim().Length > 0)
                {
                    mailTo = Session["PassengerEmailId"].ToString();
                }
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    {
                        Panel_Ticket.RenderControl(hw);
                        StringReader sr = new StringReader(sw.ToString());
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            PdfWriter.GetInstance(pdfDoc, memoryStream);
                            pdfDoc.Open();
                            htmlparser.Parse(sr);
                            pdfDoc.Close();
                            byte[] bytes = memoryStream.ToArray();
                            memoryStream.Close();

                            MailMessage mm = new MailMessage("iBusTicketing@gmail.com", mailTo);
                            mm.Subject = "iBusTicket";
                            mm.Body = "You have successfully booked your ticket";
                            mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "iBusPDF.pdf"));
                            mm.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                            NetworkCred.UserName = "iBusTicketing@gmail.com";
                            NetworkCred.Password = "ibus12345";
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                    }
                }
                Div_SucessMessage.Visible = true;
                Div_SucessMessage.InnerHtml = "";
                Div_SucessMessage.Visible = true;
                Div_SucessMessage.InnerHtml = DisplayMessage.TICKET_MAIL_SENT;
                Session["PassengerEmailId"] = null;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> ticket.cs, Function->Button_GenerateTicketviaEmail_Click  : \n " + ex);
            }
        }
        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  PopulateTicket: binds data with the table and repeater on the ui as output
        /// </summary>          
        public void PopulateTicket(TicketModel ticketModel, List<PassengerDetailModel> passengerList)
        {
            try
            {
                //Populate DataTable
                DataTable dt = new DataTable();
                dt.Columns.Add("PNR");
                dt.Columns.Add("TripCode");
                dt.Columns.Add("NoofSeats");
                dt.Columns.Add("StartPlace");
                dt.Columns.Add("DepartureTime");
                dt.Columns.Add("EndPlace");
                dt.Columns.Add("DOR");
                dt.Columns.Add("DOJ");
                dt.Columns.Add("BoardingPoint");
                dt.Columns.Add("TotalFare");
                dt.Rows.Add();
                dt.Rows[0]["PNR"] = ticketModel.PNR;
                dt.Rows[0]["TripCode"] = ticketModel.TripCode;
                dt.Rows[0]["NoofSeats"] = ticketModel.NumberofSeats;
                dt.Rows[0]["StartPlace"] = ticketModel.Source;
                dt.Rows[0]["DepartureTime"] = string.Format("{0:00}:{1:00}", ticketModel.DepartureTime.Hours, ticketModel.DepartureTime.Minutes);
                dt.Rows[0]["EndPlace"] = ticketModel.Destination;
                dt.Rows[0]["DOR"] = ticketModel.DOR.ToShortDateString();
                dt.Rows[0]["DOJ"] = ticketModel.DOJ.ToShortDateString();
                dt.Rows[0]["BoardingPoint"] = ticketModel.BoardingPoint;
                dt.Rows[0]["TotalFare"] = ticketModel.Fare;

                //Bind Datatable to Labels
                lblPNR.Text = dt.Rows[0]["PNR"].ToString();
                lblTripCode.Text = dt.Rows[0]["TripCode"].ToString();
                lblNoofSeats.Text = dt.Rows[0]["NoofSeats"].ToString();
                lblStartPlace.Text = dt.Rows[0]["StartPlace"].ToString();
                lblDepartureTime.Text = dt.Rows[0]["DepartureTime"].ToString();
                lblEndPlace.Text = dt.Rows[0]["EndPlace"].ToString();
                lblDateOfReservation.Text = dt.Rows[0]["DOR"].ToString();
                lblDateOfJourney.Text = dt.Rows[0]["DOJ"].ToString();
                lblTotalFare.Text = dt.Rows[0]["TotalFare"].ToString();
                //lblBoardingPoint.Text = dt.Rows[0]["BoardingPoint"].ToString();

                PassengerDetail.DataSource = passengerList;
                PassengerDetail.DataBind();
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> ticket.cs, Function->PopulateTicket  : \n " + ex);
            }
        }
    }
}