﻿using BussinessLogic;
using BussinessModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iBus
{
    public partial class DemoFilter : System.Web.UI.Page
    {
        static List<ShowBusListModel> listShowBusListOrigin = new List<ShowBusListModel>();
        static List<ShowBusListModel> listShowBusListWorking = new List<ShowBusListModel>();
        static List<ShowBusListModel> noDataBusList = new List<ShowBusListModel>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                listShowBusListOrigin.Clear();
                listShowBusListWorking.Clear();
                BusSearchBL busSearchBLObj = new BusSearchBL();
                //BusSearchModel busSearchModelObj = (BusSearchModel)Session["busSearchDetails"];
                BusSearchModel busSearchModelObj = new BusSearchModel();
                busSearchModelObj.FromCity = "Surat";
                busSearchModelObj.ToCity = "Mumbai";
                busSearchModelObj.DateOfJourney = "10/13/2013";


                listShowBusListOrigin = busSearchBLObj.GetShowBusList(busSearchModelObj);
                listShowBusListWorking = listShowBusListOrigin;

                List<string> FilterListNames = busSearchBLObj.GetTravelNamesList();
                CheckBoxList_BusOperator.DataSource = FilterListNames;
                CheckBoxList_BusOperator.DataBind();
                ShowBusListModel noDataBusObj = new ShowBusListModel();
                


                //ListItem lst;
                //string text;
                //for (int i = 0; i < 10; i++)
                //{
                //    text = i.ToString() + " Checkbox in DDL";
                //    lst = new ListItem(text, i.ToString());
                //    chkList.Items.Add(lst);

                //}

                //List<CityDetailModel> listCityDetail = new List<CityDetailModel>();
                //listCityDetail = busSearchBLObj.GetCityDetail();
                //Repeater_Demo.DataSource = listCityDetail;
                //Repeater_Demo.DataBind();
            }
            Repeater_Demo.DataSource = listShowBusListWorking;
            Repeater_Demo.DataBind();
        }

        protected void CheckBoxList_BusOperator_SelectedIndexChanged(object sender, EventArgs e)
        {
            Repeater_Demo.Visible = true;
            bool flag = false;
            List<ShowBusListModel> busOperatorFilteredList = new List<ShowBusListModel>();
            List<ShowBusListModel> busTypeFilteredList = new List<ShowBusListModel>();
            listShowBusListWorking = listShowBusListOrigin;
            //showbusList = listShowBusListWorking;
            foreach (ListItem item in CheckBoxList_BusOperator.Items)
            {

                if (item.Selected)
                {
                    flag = true;
                    busOperatorFilteredList = busOperatorFilteredList.Concat((from bus in listShowBusListWorking where bus.TravelName == item.Text select bus).ToList()).ToList();
                }
            }
            if (flag)
                listShowBusListWorking = busOperatorFilteredList;


            //busTypeFilteredList = busOperatorFilteredList;
            //flag = false;
            //foreach (ListItem item in CheckBoxList_BusType.Items)
            //{

            //    if (item.Selected)
            //    {
            //        flag = true;
            //        busTypeFilteredList = busTypeFilteredList.Concat((from bus in listShowBusListWorking where bus.BustType == item.Text select bus).ToList()).ToList();
            //    }
            //}
            //if (flag)
            //    listShowBusListWorking = busTypeFilteredList;

            if (listShowBusListWorking.Count != 0)
            {
                Repeater_Demo.DataSource = listShowBusListWorking;
                Repeater_Demo.DataBind();
            }
            else {
                Repeater_Demo.Visible = false;
            }

            //GVDisplay.DataSource = listShowBusListWorking;
            //GVDisplay.DataBind();
        }
    }
}