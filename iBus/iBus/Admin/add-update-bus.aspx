﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-update-bus.aspx.cs" Inherits="iBus.Admin.UpdateBus" 
    MasterPageFile="~/MasterPage/admin.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <script src="../Script/bootstrap-tooltip.js"></script>
   <script src="../Script/admin-bus.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="container">
    <div id="Div_AddBus" runat="server"   class="col-md-8 div_form_layout" >
         <div class="alert alert-success" id="Div_SucessMessageAdmin" ClientIDMode="Static" style="display:none"   runat="server"></div>
         <div class="alert alert-danger" id="Div_ErrorMessageAdmin" ClientIDMode="Static" style="display:none"  runat="server"></div>
         <div >
            <div class="col-md-6" runat="server" id="Div_BusHeader">
                <h4 >Fill Bus Detail</h4>
            </div>
            <div class="pull-right"><br />
                <asp:Button id="Button_GoToAddNewTrip" style="display:none" Text="Add New Trip" CssClass="button-blue" runat="server" OnClick="Button_GoToAddNewTrip_Click"></asp:Button>
                <asp:Button id="Button_GoToBusDetail" CssClass="button-blue" Text="Go To Bus List" runat="server" OnClick="Button_GoToBusDetail_Click" ></asp:Button>
              
            </div>
        </div><br /><br /><br /><br />               
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_BusType" Text="Bus type*" runat="server"></asp:Label>
             </div>
             <div class="col-md-8">
                 <asp:DropDownList ID="DropDownList_BusType" CssClass="medium_control " runat="server" ClientIDMode="Static" ></asp:DropDownList><br /><br />
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_BusCapacity" Text="Bus capacity*" runat="server"></asp:Label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control small_control"  id="TextBox_BusCapacity" runat="server" ClientIDMode="Static"  Enabled="false"/><br />
             </div>
         </div>
         
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_BusTravelName" Text="Travel name*" runat="server"></asp:Label>
             </div>
             <div class="col-md-8">
                 <asp:DropDownList ID="DropDownList_BusTravelName"  CssClass="medium_control " runat="server" ClientIDMode="Static" ></asp:DropDownList><br /><br />
             </div>
         </div>

         <div class="row" runat="server" id="Div_Amenities">
             
             <div class="col-md-3">
                 <asp:Label ID="Label_Amenities" Text="Amenities" runat="server"></asp:Label>
             </div>
             <div class="col-md-9">
             <div class="col-md-6">
                 <asp:CheckBox Checked="true" ID="CheckBox_WaterBottle"  runat="server" ClientIDMode="Static" />
                 <asp:Label ID="Label_WaterBottle" Text="WaterBottle" runat="server"></asp:Label>
             </div>
             <div class="col-md-6">
                 <asp:CheckBox  ID="CheckBox_Movie"  runat="server" ClientIDMode="Static" />
                 <asp:Label ID="Label_Movie" Text="Movie" runat="server"></asp:Label>
             </div>
             <div class="col-md-6">
                 <asp:CheckBox  ID="CheckBox_Blanket"  runat="server" ClientIDMode="Static" />
                 <asp:Label ID="Label_Blanket" Text="Blanket" runat="server"></asp:Label>
             </div>
             <div class="col-md-6">
                 <asp:CheckBox  ID="CheckBox_CharginPoint"  runat="server" ClientIDMode="Static" />
                 <asp:Label ID="Label_CharginPoint" Text="CharginPoint" runat="server"></asp:Label>
             </div>
             <div class="col-md-6">
                 <asp:CheckBox   ID="CheckBox_ReadingLight"  runat="server" ClientIDMode="Static" />
                 <asp:Label ID="Label_ReadingLight" Text="ReadingLight" runat="server"></asp:Label>
             </div>
            </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_BusIsActive" Text="IsActive?" runat="server"></asp:Label>
             </div>
             <div class="col-md-4 ">
                 <asp:CheckBox  ID="CheckBox_BusIsActive" runat="server" ClientIDMode="Static" /><br /><br />
             </div>
         </div>
        <div class="pull-right">
           <asp:Button Text="Update Bus" ID="Button_UpdateBusDetail" runat="server" OnClientClick="return ValidateBusDetail();" OnClick="Button_UpdateBusDetail_Click" ClientIDMode="Static" CssClass="button-blue" />
           <asp:Button Text="Add Bus" ID="Button_SaveBusDetail" runat="server" OnClientClick="return ValidateBusDetail();" OnClick="Button_SaveBusDetail_Click" ClientIDMode="Static" CssClass="button-blue"/>
           
            <br /><br />
        </div>
        <br /> <br /> 
    </div> 
</div>
</asp:Content>

