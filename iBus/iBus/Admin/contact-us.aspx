﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contact-us.aspx.cs" Inherits="iBus.Admin.ContactUs"
    MasterPageFile="~/MasterPage/admin.Master" %>
<asp:Content ID="contactUs" ContentPlaceHolderID="head" runat="server">
    <link href="Style/common.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="contactUsBody" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <iBus:Control_ModalSignInSignUp ID="Control_ModalSignInSignUp" runat="server" />

  
    
    <div class="row">
        <div class="col-md-6">
            <b>Contact us at</b>
            <br />
            <br />
            <p>EmailID:   iBusTicketing@gmail.com </p>
            <p>Contact No:  080-123-45678</p>
            <p>Address:  Neev Technologies, Bangalore</p>
                                  
        </div>
        <div class="col-md-6">
            <div class="alert alert-success" id="Div_SucessMessage" ClientIDMode="Static" style="display:none"   runat="server"></div>
            <div class="alert alert-danger" id="Div_ErrorMessage" ClientIDMode="Static" style="display:none"  runat="server"></div>
            <b>Send Email to us</b><br /> <br />
                              
         <div class="row">
             <div class="col-md-3">
                  <label id="Label_YourEmail"  runat="server">Your Email<sup class="required">*</sup></label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control large_control " id="TextBox_YourEmail" runat="server" ClientIDMode="Static" placeholder="Email" /><br /> 
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <label id="Label_YourSubject"  runat="server">Subject</label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control large_control " id="TextBox_YourSubject" runat="server" ClientIDMode="Static" placeholder="Subject" /><br /> 
             </div>
         </div>
          <div class="row">
             <div class="col-md-3">
                 <label id="lbl_YourMessage"  runat="server">Message<sup class="required">*</sup></label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" TextMode="MultiLine" Rows="7" CssClass="form-control large_control " id="TextBox_YourMessage" runat="server" ClientIDMode="Static" placeholder="Message" /> <br />
             </div>
         </div>
        <div class="col-md-offset-3">
            <asp:Button Text="Send Mail To Us" ID="Button_SendYourMail" runat="server"   ClientIDMode="Static" OnClick="Button_SendYourMail_Click"
                 CssClass="button-blue extra_large_control" /><br /><br />
        </div> 
        </div>
    </div>

</asp:Content>
