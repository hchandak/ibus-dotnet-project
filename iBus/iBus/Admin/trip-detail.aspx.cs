﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessLogic;
using BussinessModel;
using DataAccessObject;
using System.Web.Security;
using log4net;
using System.Text;
using System.Web.Services;
using System.Web.UI.HtmlControls;
namespace iBus.Admin
{
    public partial class TripDetail : System.Web.UI.Page
    {
        static string modifiedBy = string.Empty;
        TripBL tripBL = new TripBL();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load trip detail in form of table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {                 
            try
            {
                modifiedBy = Session["userEmail"].ToString();
                if (!IsPostBack)
                {
                    InitializeTripDetail();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripDetail.cs, Function->Page_Load  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Bind trip detail tabel
        /// </summary>
        protected void InitializeTripDetail()
        {
            GetIDAndStatusRequest tripDetail = new GetIDAndStatusRequest();
            List<TripDetailModel> response = new List<TripDetailModel>();
            try
            {
                response = tripBL.SelectAllTripDetail(tripDetail);
                if (response.Count > AppConstant.RECOURD_COUNT_ZERO)
                {
                    Repeater_TripList.DataSource = response;
                    Repeater_TripList.DataBind();
                }
                else
                {
                    Repeater_TripList.DataSource = null;
                    Repeater_TripList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripDetail.cs, Function->InitializeTripDetail  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to add-update-trip page for add new trip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_AddNewTrip_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("add-update-trip.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripDetail.cs, Function->Button_AddNewTrip_Click  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Delete existing trip
        /// </summary>
        /// <param name="tripID"></param>
        /// <returns></returns>
        [WebMethod]
        public static int DeleteTripDetail(int tripID)
        {
            try
            {
                UpdateStatusDAO DAO = new UpdateStatusDAO();
                if (DAO.UpdateTripStatus(tripID, Convert.ToInt32(AppConstant.STATUS.DELETED), modifiedBy))
                {
                    return AppConstant.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripDetail.cs, Function->DeleteTripDetail  : \n " + ex);
            }
            return AppConstant.ERROR;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Bind Is Active field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Repeater_TripList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    string strIsActive = string.Empty;
                    HtmlGenericControl labelIsActive = (HtmlGenericControl)e.Item.FindControl("Label_IsActive");
                    strIsActive = labelIsActive.InnerText;
                   
                    if (strIsActive == "Active")
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_InActive"));
                        image.Style["display"] = "none";
                    }
                    else if (strIsActive == "Not Active")
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_Active"));
                        image.Style["display"] = "none";
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripDetail.cs, Function->Repeater_TripList_ItemDataBound  : \n " + ex);
            }
        }
    }
}