﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="about-us.aspx.cs" Inherits="iBus.Admin.AboutUs"  MasterPageFile="~/MasterPage/admin.Master" %>

<asp:Content ID ="AboutUs" runat="server" ContentPlaceHolderID="head">
    <link href="Style/common.css" rel="stylesheet" />
    <script src="Script/registration.js"></script>
</asp:Content>

<asp:Content ID="AboutUsBody" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="row">
<div class="col-md-2"><img id="img_bus_red" src="../Image/animated-bus.jpg" /></div>

<div class="col-lg-offset-2 col-md-8">
 <p style="text-align:justify">The inspiration for iBus Booking system came from redBus.com while planning for training project at Neev Technologies, Bangalore.</p>
 <p style="text-align:justify">The i-Bus booking system is the website which facilitates users to book bus tickets online. The application will enable user to search for buses for a given date of journey and filter the buses on the basis of various criteria such as bus type, ticket price, etc. The application automatically sends ticket information to the passenger's email address.</p>

<p style="text-align:justify">The goal of the application is  to simplify the bus ticket booking system and to reduce over-time pay for the users.</p>

<p style="text-align:justify">Our team members agreed on implementing this idea. We wanted to implement Online Bus booking system using ASP.NET</p>
 </div>
        </div>
                  
    <div class="row">
        <div class="col-lg-offset-3 col-md-3">
            <img src="../Image/navneet.jpg" class="img_profile" /><br /><br />
             Navneet Rajpal
        </div>
         <div class="col-md-3">
            <img src="../Image/maneesh.jpg" class="img_profile"/><br /><br />
             Maneesh Dhakad
         </div>
         <div class="col-lg-offset-1">
            <img src="../Image/hemant.jpg" class="img_profile" /><br /><br />
             Hemant Chandak
         </div>
    </div>





</asp:Content>