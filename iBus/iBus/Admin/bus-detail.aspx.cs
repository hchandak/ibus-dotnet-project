﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessLogic;
using BussinessModel;
using DataAccessObject;
using System.Web.Security;
using log4net;
using System.Text;
using System.Web.Services;
using System.Web.UI.HtmlControls;
namespace iBus.Admin
{
    public partial class BusDetail : System.Web.UI.Page
    {
        static string modifiedBy = string.Empty;
        BusBL busBL = new BusBL();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load bus detail in form of table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                modifiedBy = Session["userEmail"].ToString();
                if (!IsPostBack)
                {
                    InitializeBusDetail();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusDetail.cs, Function->Page_Load  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// bind bus detail table with data source
        /// </summary>               
        
        protected void InitializeBusDetail()
        {
            GetIDAndStatusRequest busDetail = new GetIDAndStatusRequest();
            List<BusDetailModel> response = new List<BusDetailModel>();
            try
            {
                response = busBL.SelectAllBusDetail(busDetail);
                if (response.Count > AppConstant.RECOURD_COUNT_ZERO)
                {
                    Repeater_BusList.DataSource = response;
                    Repeater_BusList.DataBind();
                }
                else
                {
                    Repeater_BusList.DataSource = null;
                    Repeater_BusList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusDetail.cs, Function->InitializeBusDetail  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to add-update-bus page for add new bus 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_AddNewBus_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("add-update-bus.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusDetail.cs, Function->Button_AddNewBus_Click  : \n " + ex);
            }

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Delete existing bus 
        /// </summary>
        /// <param name="busID"></param>
        /// <returns></returns>
        [WebMethod]
        public static int DeleteBusDetail(int busID)
        {
            try
            {
                UpdateStatusDAO DAO = new UpdateStatusDAO();
                if (DAO.UpdateBusStatus(busID, Convert.ToInt32(AppConstant.STATUS.DELETED), modifiedBy))
                {
                    BusDetail obj = new BusDetail();
                    return AppConstant.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusDetail.cs, Function->DeleteBusDetail  : \n " + ex);
            }
            return AppConstant.ERROR;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Bind Amenities and Is Active field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Repeater_BusList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    int amenitiesCount = 5;
                    string[] strAmenities;
                    string[] stringSeparators = new string[] { ", " };
                    string amenities = string.Empty;
                    string strIsActive = string.Empty;
                    HtmlGenericControl labelAmenities = (HtmlGenericControl)e.Item.FindControl("Label_Amenities");
                    HtmlGenericControl labelIsActive = (HtmlGenericControl)e.Item.FindControl("Label_IsActive");
                    amenities = labelAmenities.InnerText;
                    strIsActive = labelIsActive.InnerText;
                    strAmenities = amenities.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    if (!strAmenities.Contains(AppConstant.AMENITIES[0]))
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_WaterBottle"));
                        image.Style["display"] = "none";
                        amenitiesCount--;

                    }
                    if (!strAmenities.Contains(AppConstant.AMENITIES[1]))
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_Movies"));
                        image.Style["display"] = "none";
                        amenitiesCount--;
                    }
                    if (!strAmenities.Contains(AppConstant.AMENITIES[2]))
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_Blancket"));
                        image.Style["display"] = "none";
                        amenitiesCount--;
                    }
                    if (!strAmenities.Contains(AppConstant.AMENITIES[3]))
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_ReadingLight"));
                        image.Style["display"] = "none";
                        amenitiesCount--;

                    }
                    if (!strAmenities.Contains(AppConstant.AMENITIES[4]))
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_CharginPoint"));
                        image.Style["display"] = "none";
                        amenitiesCount--;

                    }
                    if (amenitiesCount == 0)
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_NoAmenities"));
                        image.Style["display"] = "block";
                    }
                    if (strIsActive == "Active")
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_InActive"));
                        image.Style["display"] = "none";
                    }
                    else if (strIsActive == "Not Active")
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_Active"));
                        image.Style["display"] = "none";
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusDetail.cs, Function->Repeater_BusList_ItemDataBound  : \n " + ex);
            }
        }

       
    }
}