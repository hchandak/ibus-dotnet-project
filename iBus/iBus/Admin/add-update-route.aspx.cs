﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataValidation;
using BussinessLogic;
using BussinessModel;
using log4net;
using System.Text;
namespace iBus.Admin
{
    public partial class AddUpdateRoute : System.Web.UI.Page
    {
        RouteBL routeBL = new RouteBL();
        AdminValidation validation = new AdminValidation();
        BusSearchBL busSearchBL = new BusSearchBL();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>            
        ///<author>Maneesh</author>
        /// <summary>
        /// Load route detail form base on query string 
        /// Where Admin can give detail for add or update route
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["routeID"] != null && Request.QueryString["routeID"].ToString().Trim().Length > 0)
                    {
                        LoadRouteDetail(Convert.ToInt32(Request.QueryString["routeID"].ToString().Trim()));
                    }
                    else
                    {
                        InitializeRouteDetail();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateRoute.cs, Function->Page_Load  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Initialize route detail form
        /// </summary>
        protected void InitializeRouteDetail()
        {
            try
            {
                string strCities = string.Empty;
                Button_UpdateRouteDetail.Style["display"] = "none";
                strCities = busSearchBL.GetCityListString();
                HiddenField_Cities.Value = strCities;
                TextBox_FromCity.Text = "";
                TextBox_ToCity.Text = "";
                CheckBox_RouteIsActive.Checked = true;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateRoute.cs, Function->InitializeRouteDetail  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Save route detail
        /// It will go to validation layer to check all given inputs
        /// Then call bussiness logic layer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_SaveRouteDetail_Click(object sender, EventArgs e)
        {
            string strMessage = DisplayMessage.INVALID_VALUES; ;
            RouteDetailModel routeDetail = new RouteDetailModel();
            try
            {
                routeDetail.ToCity = TextBox_ToCity.Text;
                routeDetail.FromCity = TextBox_FromCity.Text;
                routeDetail.Status = CheckBox_RouteIsActive.Checked ? Convert.ToInt32(AppConstant.STATUS.ACTIVE) : Convert.ToInt32(AppConstant.STATUS.IN_ACTIVE);
                routeDetail.CreatedBy = Session["userEmail"].ToString();
                routeDetail.CreatedDate = System.DateTime.Today.ToShortDateString();
                strMessage = validation.ValidateRouteDetail(routeDetail);
                if (strMessage == DisplayMessage.VALIDATION_SUCCESS)
                {
                    AddRouteDetailModel response = new AddRouteDetailModel();
                    response = routeBL.AddRouteDetail(routeDetail);
                    if (response.Status == AppConstant.SUCCESS)
                    {

                        Div_SucessMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.Style["display"] = "none";
                        Div_SucessMessageAdmin.InnerHtml = DisplayMessage.SAVE_ROUTE_DETAILS_SUCCESS;
                        Button_GoToAddNewTrip.Style["display"] = "block";
                        Session["route1ID"] = response.Route1ID;
                        Session["route2ID"] = response.Route2ID;
                        Button_SaveRouteDetail.Style["display"] = "none";
                        LoadRouteDetail(response.Route1ID);
                    }

                    else if (response.Status == AppConstant.RECORD_EXIST)
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.ROUTE_EXIST;
                    }
                    else
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.SAVE_ROUTE_DETAILS_FAIL;
                    }
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = strMessage;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateRoute.cs, Function->Button_SaveRouteDetail_Click  : \n " + ex);
                Div_SucessMessageAdmin.Style["display"] = "none";
                Div_ErrorMessageAdmin.Style["display"] = "block";
                Div_ErrorMessageAdmin.InnerHtml = strMessage;
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load route detail for update route
        /// </summary>
        /// <param name="routeID"></param>
        protected void LoadRouteDetail(int routeID)
        {
            try
            {
                if (Button_SaveRouteDetail.Style["display"] == "none")
                {
                    Button_SaveRouteDetail.Style["display"] = "block";
                }
                else
                {
                    Button_SaveRouteDetail.Style["display"] = "none";
                }
                TextBox_FromCity.Attributes["disabled"] = "disabled";
                TextBox_ToCity.Attributes["disabled"] = "disabled";
                GetIDAndStatusRequest request = new GetIDAndStatusRequest();
                RouteDetailModel response = new RouteDetailModel();
                request.ID = routeID;
                request.Status = Convert.ToInt32(AppConstant.STATUS.ACTIVE);
                response = routeBL.LoadRouteDetail(request);
                if (response != null)
                {
                    this.TextBox_FromCity.Text = response.FromCity;
                    this.TextBox_ToCity.Text = response.ToCity;
                    this.CheckBox_RouteIsActive.Checked = response.Status == Convert.ToInt32(AppConstant.STATUS.ACTIVE) ? true : false;
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.RECORD_NOT_FOUND;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateRoute.cs, Function->LoadRouteDetail  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Update route detail
        /// It will go to validation layer to check all given inputs
        /// Then call bussiness logic layer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_UpdateRouteDetail_Click(object sender, EventArgs e)
        {
            string strMessage = DisplayMessage.INVALID_VALUES; ;
            RouteDetailModel routeDetail = new RouteDetailModel();
            try
            {
                if (Request.QueryString["routeID"] != null && Convert.ToInt32(Request.QueryString["routeID"].ToString().Trim()) > AppConstant.ERROR)
                {
                    routeDetail.RouteID = Convert.ToInt32(Request.QueryString["routeID"].ToString().Trim());
                    routeDetail.ToCity = TextBox_ToCity.Text;
                    routeDetail.FromCity = TextBox_FromCity.Text;
                    routeDetail.Status = CheckBox_RouteIsActive.Checked ? Convert.ToInt32(AppConstant.STATUS.ACTIVE) : Convert.ToInt32(AppConstant.STATUS.IN_ACTIVE);
                    routeDetail.ModifiedBy = Session["userEmail"].ToString();
                    routeDetail.ModifiedDate = System.DateTime.Today.ToShortDateString();
                    UpdateRouteDetailModel response = new UpdateRouteDetailModel();
                    response = routeBL.UpdateRouteDetail(routeDetail);
                    if (response.Status == AppConstant.SUCCESS)
                    {
                        Div_SucessMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.Style["display"] = "none";
                        Div_SucessMessageAdmin.InnerHtml = DisplayMessage.UPDATE_ROUTE_DETAILS_SUCCESS;
                        Button_SaveRouteDetail.Style["display"] = "block";
                        LoadRouteDetail(routeDetail.RouteID);
                    }
                    else
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.UPDATE_ROUTE_DETAILS_FAIL;
                    }
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.INVALID_URL;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateRoute.cs, Function->Button_UpdateRouteDetail_Click  : \n " + ex);
                Div_SucessMessageAdmin.Style["display"] = "none";
                Div_ErrorMessageAdmin.Style["display"] = "block";
                Div_ErrorMessageAdmin.InnerHtml = strMessage;
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to route detail page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_GoToRouteDetail_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("route-detail.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateRoute.cs, Function->Button_GoToRouteDetail_Click  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to add new trip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_GoToAddNewTrip_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("add-update-trip.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateRoute.cs, Function->Button_GoToAddNewTrip_Click  : \n " + ex);
            }
        }
    }
}