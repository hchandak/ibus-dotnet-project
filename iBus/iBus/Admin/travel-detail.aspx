﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="travel-detail.aspx.cs" Inherits="iBus.Admin.TravelDetail" 
    MasterPageFile="~/MasterPage/admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Script/admin-travel.js"></script>
    <link href="../DataTables/css/demo_table.css" rel="stylesheet" />
    <script src="../DataTables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tblTravelList').dataTable({
                "sPaginationType": "full_numbers"
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="Div_TravelDetail" runat="server">
        <div class="alert alert-success" id="Div_SucessMessageAdmin" clientidmode="Static" style="display: none" runat="server"></div>
        <div class="alert alert-danger" id="Div_ErrorMessageAdmin" clientidmode="Static" style="display: none" runat="server"></div>
        <div class="row">
            <div class="col-md-10" runat="server" id="Div_TravelListHeader">
               <h4 >Travels List :</h4>
            </div>
            <div class="col-md-2">
               <asp:Button ID="Button_AddNewTravel" CssClass="button-blue" Text="Add New Travel" runat="server" OnClick="Button_AddNewTravel_Click"></asp:Button>
            </div>
        </div>
        <br />
        <div id="Div_RepeaterTravelList" class="table-background">
            <asp:Repeater ID="Repeater_TravelList" runat="server" OnItemDataBound="Repeater_TravelList_ItemDataBound" >             
                <HeaderTemplate >
                    <table id="tblTravelList" class="table table-bordered" >
                       <thead >
                           <tr>
                              <th class="small_control">Travel Code</th>
                              <th class="medium_control">Travel Name</th>
                              <th class="medium_control">Contact Address</th>
                              <th class="medium_control">Travel Email</th>
                              <th class="small_control">Contact</th>
                              <th class="small_control">Is Active?</th>
                              <th class="small_control">Action</th>
                           </tr>
                       </thead>
                       <tbody>
                  </HeaderTemplate>
                  <ItemTemplate >
                      <label id="Label_IsActive" runat="server"  style="display:none" ><%# Eval("IsActive") %></label>
                      <tr id="trTravelDetailRow<%# Eval("TravelID") %>">
                         <td class="small_control" style="text-align:center"><%# Eval("TravelID") %> </td>
                         <td class="small_control"> <%# Eval("TravelName") %></td>
                         <td class="medium_control"><%# Eval("ContactAddress") %></td>
                         <td class="medium_control"><%# Eval("TravelEmail") %> </td>
                         <td class="small_control"><%# Eval("ContactNo") %></td>
                         <td class="small_control" style="text-align:center">
                           <asp:Image id="Image_Active" runat="server" CssClass="margin" style="cursor:pointer;" src="../Image/active.png" title="Active" />
                           <asp:Image id="Image_InActive" runat="server" CssClass="margin" style="cursor:pointer;" src="../Image/in-active.jpg" title="InActive" />
                        </td>
                         <td class="small_control" style="text-align:center">
                             <a id="Link_UpdateTravel" style="cursor:pointer;"   onclick="RedirectToUpdateTravels('<%# Eval("TravelID") %>')" ><img " src="/Image/edit.png" title="Update travel"/></a>
                             <a id='Link_DeleteTravel' style="cursor:pointer;"   data-toggle="modal" onclick="ConfirmDeleteTravelDetail('<%# Eval("TravelID") %>')" ><img src="/Image/delete.png" title="Delete travel" /></a>
                         </td>
                      </tr>
                  </ItemTemplate>
                  <FooterTemplate>
                      </tbody> </table>
                  </FooterTemplate>
              </asp:Repeater>
          </div>    
         <iBus:Control_DisplayMessage ID="Control_DispalyMessage" runat="server" />
        <br /> <br /> 
      </div>      
</asp:Content>


