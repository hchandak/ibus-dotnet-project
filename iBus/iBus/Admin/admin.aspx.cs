﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataValidation;
using BussinessLogic;
using BussinessModel;
using System.Web.Security;
using log4net;
using System.Text;
namespace iBus.Admin
{
    public partial class Admin : System.Web.UI.Page
    {
        AdminValidation validation = new AdminValidation();
        TravelsBL travelBL = new TravelsBL();
        RouteBL routeBL = new RouteBL();
        BusBL busBL = new BusBL();
        TripBL tripBL = new TripBL();
       
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Display admin profile                         
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {               
                if (!IsPostBack)
                {
                    Div_AdminDetail.InnerHtml = Session["userFirstName"].ToString() + " " + Session["userLastName"].ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> Admin.cs, Function->Page_Load  : \n " + ex);
            }
        }
       
        
    }
}