﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataValidation;
using BussinessLogic;
using BussinessModel;
using log4net;
using System.Text;
namespace iBus.Admin
{
    public partial class ContactUs : System.Web.UI.Page
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>                      
        /// Send user mail to iBus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_SendYourMail_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = string.Empty;
                SendMailModel mailDetail = new SendMailModel();
                UserValidation validation = new UserValidation();
                UserBL userBL = new UserBL();
                mailDetail.Subject = TextBox_YourSubject.Text + " From " + TextBox_YourEmail.Text;
                mailDetail.EmailID = TextBox_YourEmail.Text;
                mailDetail.Message = TextBox_YourMessage.Text;
                strMessage = validation.SendMailValidation(mailDetail);
                if (strMessage == DisplayMessage.VALIDATION_SUCCESS)
                {
                    mailDetail.EmailID = AppConstant.IBUS_EMAIL;
                    userBL.SendMail(mailDetail);
                    Div_SucessMessage.Style["display"] = "block";
                    Div_ErrorMessage.Style["display"] = "none";
                    Div_SucessMessage.InnerHtml = DisplayMessage.MAIL_SEND_SUCCESSFULLY;
                    TextBox_YourSubject.Text = "";
                    TextBox_YourEmail.Text = "";
                    TextBox_YourMessage.Text = "";
                }
                else
                {
                    Div_SucessMessage.Style["display"] = "none";
                    Div_ErrorMessage.Style["display"] = "block";
                    Div_ErrorMessage.InnerHtml = strMessage;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> ContactUs.cs, Function->Button_SendYourMail_Click  : \n " + ex);
            }
        }
    }
}