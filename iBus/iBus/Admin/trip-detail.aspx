﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="trip-detail.aspx.cs" Inherits="iBus.Admin.TripDetail" 
    MasterPageFile="~/MasterPage/admin.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Script/admin-trip.js"></script>
    <link href="../DataTables/css/demo_table.css" rel="stylesheet" />
    <script src="../DataTables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tblTripList').dataTable({
                "sPaginationType": "full_numbers"
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div id="Div_TripDetail" runat="server">
          <div class="alert alert-success" id="Div_SucessMessageAdmin" clientidmode="Static" style="display: none" runat="server"></div>
          <div class="alert alert-danger" id="Div_ErrorMessageAdmin" clientidmode="Static" style="display: none" runat="server"></div>
          <div class="row">
             <div class="col-md-10" runat="server" id="Div_TripListHeader">
                 <h4 >Trip List :</h4>
             </div>
             <div class="col-md-2 ">
                 <asp:Button ID="Button_AddNewTrip" CssClass="button-blue" Text="Add New Trip" runat="server" OnClick="Button_AddNewTrip_Click"></asp:Button>
             </div>
          </div>
          <br /> 
          <div id="Div_RepeaterTripList" class="table-background" >
              <asp:Repeater ID="Repeater_TripList" runat="server" OnItemDataBound="Repeater_TripList_ItemDataBound">
                 <HeaderTemplate  >                      
                    <table id="tblTripList" class="table table-bordered altrowstable"  >
                        <thead >
                           <tr>
                              <th class="tiny_control">Trip Code</th>
                              <th class="small_control">Travel Name</th>
                              <th class="medium_control">Bus Type</th>
                              <th class="small_control">Route</th>
                              <th class="small_control">Departure</th>
                              <th class="small_control">Journey Time</th>
                              <th class="tiny_control">Fare</th>
                              <th class="tiny_control">Is Active?</th>
                              <th class="small_control">Action</th>
                           </tr>
                        </thead>
                        <tbody>
                  </HeaderTemplate>
                  <ItemTemplate >
                      <label id="Label_IsActive" runat="server"  style="display:none" ><%# Eval("IsActive") %></label>
                     <tr id="trTripDetailRow<%# Eval("TripID") %>">
                        <td class="tiny_control" style="text-align:center"> <%# Eval("TripID") %></td>
                        <td class="small_control"><%# Eval("TravelName") %></td>
                        <td class="medium_control"><%# Eval("BusType") %></td>
                        <td class="small_control"><%# Eval("Route") %></td>
                        <td class="small_control" style="text-align:center"><%# Eval("Departure") %></td>
                        <td class="small_control" style="text-align:center"><%# Eval("JourneyDuration") %></td>
                        <td class="tiny_control" style="text-align:center"><%# Eval("Fare") %></td>
                        <td class="tiny_control" style="text-align:center">
                           <asp:Image id="Image_Active" runat="server" CssClass="margin" style="cursor:pointer;" src="../Image/active.png" title="Active" />
                           <asp:Image id="Image_InActive" runat="server" CssClass="margin" style="cursor:pointer;" src="../Image/in-active.jpg" title="InActive" />
                       </td>
                        <td class="small_control" style="text-align:center">
                            <a id="Link_UpdateTrip" style="cursor:pointer;"  onclick="RedirectToUpdateTrip('<%# Eval("TripID") %>')" ><img " src="/Image/edit.png" title="Update trip"/></a>
                            <a id='Link_DeleteTrip' style="cursor:pointer;" class="margin"  data-toggle="modal" onclick="ConfirmDeleteTripDetail('<%# Eval("TripID") %>')" ><img src="/Image/delete.png" title="Delete trip" /></a>
                        </td>
                     
                     </tr>
                  </ItemTemplate>
                  <FooterTemplate>
                     </tbody> </table>
                  </FooterTemplate>
              </asp:Repeater>
          </div>
          <iBus:Control_DisplayMessage ID="Control_DispalyMessage" runat="server" />
          <br /> <br />           
      </div>
</asp:Content>
