﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataValidation;
using BussinessLogic;
using BussinessModel;
using log4net;
using System.Text;
namespace iBus.Admin
{
    public partial class AddUpdateTravel : System.Web.UI.Page
    {
        AdminValidation validation = new AdminValidation();                
        TravelsBL travelBL = new TravelsBL();
        UserBL userBL = new UserBL();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>          
        /// <summary>
        /// Load travel detail form base on query string               
        /// Where Admin can give detail for add or update travel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["travelID"] != null && Request.QueryString["travelID"].ToString().Trim().Length > 0)
                    {
                        LoadTravelDetail(Convert.ToInt32(Request.QueryString["travelID"].ToString().Trim()));
                    }
                    else
                    {
                        InitializeTravelDetail();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTravel.cs, Function->Page_Load  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Initialize travel detail form
        /// </summary>
        protected void InitializeTravelDetail()
        {
            try
            {
                Button_UpdateTravelDetail.Style["display"] = "none";
                this.TextBox_TravelName.Text = "";
                this.TextBox_TravelAddress.Text = "";
                this.TextBox_TravelEmail.Text = "";
                this.TextBox_TravelContact.Text = "";
                CheckBox_TravelIsActive.Checked = true;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTravel.cs, Function->InitializeTravelDetail  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Save travel detail
        /// It will go to valiation layer to check all given inputs
        /// Then call bussiness logic layer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_SaveTravelDetail_Click(object sender, EventArgs e)
        {
            string strMessage = DisplayMessage.INVALID_VALUES; ;
            TravelDetailModel travelDetail = new TravelDetailModel();
            try
            {
                travelDetail.TravelName = TextBox_TravelName.Text;
                travelDetail.ContactAddress = TextBox_TravelAddress.Text;
                travelDetail.TravelEmail = TextBox_TravelEmail.Text;
                travelDetail.ContactNo = TextBox_TravelContact.Text;
                travelDetail.Status = CheckBox_TravelIsActive.Checked ? Convert.ToInt32(AppConstant.STATUS.ACTIVE) : Convert.ToInt32(AppConstant.STATUS.IN_ACTIVE);
                travelDetail.CreatedBy = Session["userEmail"].ToString();
                travelDetail.CreatedDate = System.DateTime.Today.ToShortDateString();
                strMessage = validation.ValidateTravelDetail(travelDetail);
                if (strMessage == DisplayMessage.VALIDATION_SUCCESS)
                {
                    AddTravelDetailModel response = new AddTravelDetailModel();
                    response = travelBL.AddTravelDetail(travelDetail);
                    if (response.Status == AppConstant.SUCCESS)
                    {
                        Div_SucessMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.Style["display"] = "none";
                        Div_SucessMessageAdmin.InnerHtml = DisplayMessage.SAVE_TRAVEL_DETAILS_SUCCESS;
                        SendMailModel mailDetail = new SendMailModel();
                        mailDetail.EmailID = travelDetail.TravelEmail;
                        mailDetail.Subject = "Notification";
                        mailDetail.Message = travelDetail.TravelName + " is successfully registered, We hope that you will enjoy with our services.";
                        userBL.SendMail(mailDetail);
                        Button_GoToAddNewBus.Style["display"] = "block";
                        Session["travelID"] = response.TravelID;
                        Button_SaveTravelDetail.Style["display"] = "none";
                        LoadTravelDetail(response.TravelID);
                    }

                    else if (response.Status == AppConstant.RECORD_EXIST)
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.TRAVEL_NAME_OR_EMAIL_EXIST;
                    }
                    else
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.SAVE_TRAVEL_DETAILS_FAIL;
                    }

                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = strMessage;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTravel.cs, Function->Button_SaveTravelDetail_Click  : \n " + ex);
                Div_SucessMessageAdmin.Style["display"] = "none";
                Div_ErrorMessageAdmin.Style["display"] = "block";
                Div_ErrorMessageAdmin.InnerHtml = strMessage;
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load travel detail from for update travel
        /// </summary>
        /// <param name="travelID"></param>
        protected void LoadTravelDetail(int travelID)
        {
            try
            {
                if (Button_SaveTravelDetail.Style["display"] == "none")
                {
                    Button_SaveTravelDetail.Style["display"] = "block";
                }
                else
                {
                    Button_SaveTravelDetail.Style["display"] = "none";
                }
                GetIDAndStatusRequest request = new GetIDAndStatusRequest();
                TravelDetailModel response = new TravelDetailModel();
                request.ID = travelID;
                request.Status = Convert.ToInt32(AppConstant.STATUS.ACTIVE);
                response = travelBL.LoadTravelDetail(request);
                if (response != null)
                {
                    this.TextBox_TravelName.Text = response.TravelName;
                    this.TextBox_TravelAddress.Text = response.ContactAddress;
                    this.TextBox_TravelEmail.Text = response.TravelEmail;
                    this.TextBox_TravelContact.Text = response.ContactNo;
                    CheckBox_TravelIsActive.Checked = (response.Status) == Convert.ToInt32(AppConstant.STATUS.ACTIVE) ? true : false;
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.RECORD_NOT_FOUND;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTravel.cs, Function->LoadTravelDetail  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Update travel detail
        /// It will go to validation layer to check all given inputs
        /// Then call bussiness logic layer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_UpdateTravelDetail_Click(object sender, EventArgs e)
        {
            string strMessage = DisplayMessage.INVALID_VALUES; ;
            TravelDetailModel travelDetail = new TravelDetailModel();
            try
            {
                if (Request.QueryString["travelID"] != null && Convert.ToInt32(Request.QueryString["travelID"].ToString().Trim()) > AppConstant.ERROR)
                {
                    travelDetail.TravelID = Convert.ToInt32(Request.QueryString["travelID"].ToString().Trim());
                    travelDetail.TravelName = TextBox_TravelName.Text;
                    travelDetail.ContactAddress = TextBox_TravelAddress.Text;
                    travelDetail.TravelEmail = TextBox_TravelEmail.Text;
                    travelDetail.ContactNo = TextBox_TravelContact.Text;
                    travelDetail.Status = CheckBox_TravelIsActive.Checked ? Convert.ToInt32(AppConstant.STATUS.ACTIVE) : Convert.ToInt32(AppConstant.STATUS.IN_ACTIVE);
                    travelDetail.ModifiedBy = Session["userEmail"].ToString();
                    travelDetail.ModifiedDate = System.DateTime.Today.ToShortDateString();
                    strMessage = validation.ValidateTravelDetail(travelDetail);
                    if (strMessage == DisplayMessage.VALIDATION_SUCCESS)
                    {
                        UpdateTravelDetailModel response = new UpdateTravelDetailModel();
                        response = travelBL.UpdateTravelDetail(travelDetail);
                        if (response.Status == AppConstant.SUCCESS)
                        {
                            Div_SucessMessageAdmin.Style["display"] = "block";
                            Div_ErrorMessageAdmin.Style["display"] = "none";
                            Div_SucessMessageAdmin.InnerHtml = DisplayMessage.UPDATE_TRAVEL_DETAILS_SUCCESS;
                            SendMailModel mailDetail = new SendMailModel();
                            mailDetail.EmailID = travelDetail.TravelEmail;
                            mailDetail.Subject = "Notification";
                            mailDetail.Message ="\""+ travelDetail.TravelName + "\""+" detail updated successfully, We hope that you will enjoy our services.";
                            userBL.SendMail(mailDetail);
                            Button_SaveTravelDetail.Style["display"] = "block";
                            LoadTravelDetail(travelDetail.TravelID);

                        }
                        else if (response.Status == AppConstant.RECORD_EXIST)
                        {
                            Div_SucessMessageAdmin.Style["display"] = "none";
                            Div_ErrorMessageAdmin.Style["display"] = "block";
                            Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.TRAVEL_NAME_OR_EMAIL_EXIST;
                        }
                        else
                        {
                            Div_SucessMessageAdmin.Style["display"] = "none";
                            Div_ErrorMessageAdmin.Style["display"] = "block";
                            Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.UPDATE_TRAVEL_DETAILS_FAIL;
                        }
                    }
                    else
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = strMessage;
                    }
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.INVALID_URL;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTravel.cs, Function->Button_UpdateTravelDetail_Click  : \n " + ex);
                Div_SucessMessageAdmin.Style["display"] = "none";
                Div_ErrorMessageAdmin.Style["display"] = "block";
                Div_ErrorMessageAdmin.InnerHtml = strMessage;
            }

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to travel detail page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_GoToTravelDetail_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("travel-detail.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTravel.cs, Function->Button_GoToTravelDetail_Click  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to add-update-bus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_GoToAddNewBus_Click(object sender, EventArgs e)
        {
            try
            {
                 Response.Redirect("add-update-bus.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTravel.cs, Function->Button_GoToAddNewBus_Click  : \n " + ex);
            }
        }

    }
}