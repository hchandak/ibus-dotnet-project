﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-update-route.aspx.cs" Inherits="iBus.Admin.AddUpdateRoute"
    MasterPageFile="~/MasterPage/admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Script/bootstrap-tooltip.js"></script>
    <script src="../Script/admin-route.js"></script>
    <script src="../Script/bus-search.js"></script>
    <script src="../Script/jquery-ui-1.10.3.custom.js"></script>
    <link href="../Style/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="container">
    <div id="Div_AddRoute" runat="server"   class="col-md-8 div_form_layout"  >
         <div class="alert alert-success" id="Div_SucessMessageAdmin" ClientIDMode="Static" style="display:none"   runat="server"></div>
         <div class="alert alert-danger" id="Div_ErrorMessageAdmin" ClientIDMode="Static" style="display:none"  runat="server"></div>
         <div >
            <div class="col-md-6" runat="server" id="Div_RouteHeader">
                <h4 >Fill Route Detail</h4>
            </div>
            <div class="pull-right"><br />
                 <asp:Button id="Button_GoToAddNewTrip" style="display:none" Text="Add New Trip" CssClass="button-blue" runat="server" OnClick="Button_GoToAddNewTrip_Click"></asp:Button>
                 <asp:Button id="Button_GoToRouteDetail" CssClass="button-blue" Text="Go To Route List" runat="server" OnClick="Button_GoToRouteDetail_Click" ></asp:Button>
               
            </div>
        </div><br /><br /><br /><br />
         <div class="row">                         
             <div class="col-md-3">
                 <asp:Label ID="Label_RouteFromCity" Text="From city*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:TextBox ID="TextBox_FromCity"  CssClass="form-control medium_control" placeholder="From city" runat="server" ClientIDMode="Static" ></asp:TextBox><br /><br />
             </div>
         </div>
                  
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_RouteToCity" Text="To city*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:TextBox ID="TextBox_ToCity"  CssClass="form-control medium_control" placeholder="To city" runat="server" ClientIDMode="Static" ></asp:TextBox><br /><br />
             </div>
         </div>
         
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_RouteIsActive" Text="IsActive?" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:CheckBox  ID="CheckBox_RouteIsActive" runat="server" ClientIDMode="Static" /><br /><br />
             </div>
         </div>
        <div class="pull-right">
           <asp:Button Text="Update Route" ID="Button_UpdateRouteDetail" runat="server" OnClick="Button_UpdateRouteDetail_Click" ClientIDMode="Static" CssClass="button-blue" />
           <asp:Button Text="Add Route" ID="Button_SaveRouteDetail" runat="server" OnClientClick="return ValidateAll('routeSearch')" OnClick="Button_SaveRouteDetail_Click" ClientIDMode="Static" CssClass="button-blue"/><br /><br />
        </div>
        <asp:HiddenField ID="HiddenField_Cities" ClientIDMode="Static" runat="server" />    
        <br /> <br /> 
    </div> 
</div>
</asp:Content>