﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataValidation;
using BussinessLogic;
using BussinessModel;
using log4net;
using System.Text;
namespace iBus.Admin
{
    public partial class UpdateBus : System.Web.UI.Page
    {
        BusBL busBL = new BusBL();
        TravelsBL travelBL = new TravelsBL();
        AdminValidation validation = new AdminValidation();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load bus detail form base on query string 
        /// Where Admin can give detail for add or update bus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {                 
            try
            {
               // InitializeBusDetail();
                if (!IsPostBack)
                {
                    if (Request.QueryString["busID"] != null && Request.QueryString["busID"].ToString().Trim().Length > 0) // Not handled
                    {
                        LoadBusDetail(Convert.ToInt32(Request.QueryString["busID"].ToString().Trim()));
                    }
                    else
                    {
                        InitializeBusDetail();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateBus.cs, Function->Page_Load  : \n " + ex);
            }
        }


        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Initialize bus detail form
        /// </summary>
        protected void InitializeBusDetail()
        {
            int busTypeCount = 0;
            try
            {
                Button_UpdateBusDetail.Style["display"] = "none";
                GetIDAndStatusRequest busTypeDetail = new GetIDAndStatusRequest();
                List<BusTypeDetailModel> response = new List<BusTypeDetailModel>();
                response = busBL.SelectAllBusType(busTypeDetail);
                DropDownList_BusType.Items.Clear();
                DropDownList_BusType.Items.Add(new ListItem("Select Bus Type", busTypeCount.ToString()));
                foreach (string busType in AppConstant.BUS_TYPE)
                {
                    busTypeCount++;
                    DropDownList_BusType.Items.Add(new ListItem(busType, busTypeCount.ToString()));
                }
              
                GetIDAndStatusRequest travelDetail = new GetIDAndStatusRequest();
                List<TravelDetailModel> response1 = new List<TravelDetailModel>();
                response1 = travelBL.SelectAllTravelDetail(travelDetail);
                DropDownList_BusTravelName.Items.Clear();
                DropDownList_BusTravelName.Items.Add(new ListItem("Select Travel", "0"));
                foreach (TravelDetailModel list in response1)
                {
                    DropDownList_BusTravelName.Items.Add(new ListItem(list.TravelName, list.TravelID.ToString()));
                }

                if (Session["travelID"] != null) // Session coming from  add-update-travel page 
                {
                    DropDownList_BusTravelName.SelectedValue = Session["travelID"].ToString();
                }
                if (Convert.ToInt32(DropDownList_BusType.SelectedValue) == 1 || Convert.ToInt32(DropDownList_BusType.SelectedValue) == 3)
                {
                    TextBox_BusCapacity.Text = Convert.ToInt32(AppConstant.BUS_CAPACITY.SLEEPER).ToString();
                }
                else if (Convert.ToInt32(DropDownList_BusType.SelectedValue) == 2 || Convert.ToInt32(DropDownList_BusType.SelectedValue) == 4)
                {
                    TextBox_BusCapacity.Text = Convert.ToInt32(AppConstant.BUS_CAPACITY.SEMI_SLEEPER).ToString();
                }

                CheckBox_BusIsActive.Checked = true;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateBus.cs, Function->InitializeBusDetail  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Save bus detail
        /// It will go to validation layer to check all given inputs
        /// Then call bussiness logic layer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_SaveBusDetail_Click(object sender, EventArgs e)
        {
            string strMessage = DisplayMessage.INVALID_VALUES;
            BusDetailModel busDetail = new BusDetailModel();
            try
            {
                busDetail.BusType = DropDownList_BusType.SelectedItem.Text;
                busDetail.BusTypeID = Convert.ToByte(DropDownList_BusType.SelectedValue);
                busDetail.TravelID = Convert.ToByte(DropDownList_BusTravelName.SelectedValue);
                busDetail.TravelName = DropDownList_BusTravelName.SelectedItem.Text;
                if (Convert.ToInt32(DropDownList_BusType.SelectedValue) == 1 || Convert.ToInt32(DropDownList_BusType.SelectedValue) == 3)
                {
                    busDetail.Capacity = Convert.ToInt32(AppConstant.BUS_CAPACITY.SLEEPER);
                }
                else if (Convert.ToInt32(DropDownList_BusType.SelectedValue) == 2 || Convert.ToInt32(DropDownList_BusType.SelectedValue) == 4)
                {
                    busDetail.Capacity = Convert.ToInt32(AppConstant.BUS_CAPACITY.SEMI_SLEEPER);
                }
                busDetail.WaterBottle = CheckBox_WaterBottle.Checked;
                busDetail.Movie = CheckBox_Movie.Checked;
                busDetail.Blanket = CheckBox_Blanket.Checked;
                busDetail.ChargingPoint = CheckBox_CharginPoint.Checked;
                busDetail.ReadingLight = CheckBox_ReadingLight.Checked;

                busDetail.Status = CheckBox_BusIsActive.Checked ? Convert.ToInt32(AppConstant.STATUS.ACTIVE) : Convert.ToInt32(AppConstant.STATUS.IN_ACTIVE);
                busDetail.CreatedBy = Session["userEmail"].ToString();
                busDetail.CreatedDate = System.DateTime.Today.ToShortDateString();
                strMessage = validation.ValidateBusDetail(busDetail, AppConstant.IS_SAVE);
                if (strMessage == DisplayMessage.VALIDATION_SUCCESS)
                {
                    AddBusDetailModel response = new AddBusDetailModel();
                    response = busBL.AddBusDetail(busDetail);
                    if (response.Status == AppConstant.SUCCESS)
                    {
                        Div_SucessMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.Style["display"] = "none";
                        Div_SucessMessageAdmin.InnerHtml = DisplayMessage.SAVE_BUS_DETAILS_SUCCESS;
                        Button_GoToAddNewTrip.Style["display"] = "block"; 
                        Session["busID"] = response.BusID;
                        Session["travelID"] = response.TravelID;
                        Button_SaveBusDetail.Style["display"] = "none";
                        LoadBusDetail(response.BusID);
                    }
                    else if (response.Status == AppConstant.RECORD_EXIST)
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.BUS_SAME_TYPE_AND_AMENITIES_EXIST;
                    }
                    else
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.SAVE_BUS_DETAILS_FAIL;
                    }
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = strMessage;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateBus.cs, Function->Button_SaveBusDetail_Click  : \n " + ex);
                Div_SucessMessageAdmin.Style["display"] = "none";
                Div_ErrorMessageAdmin.Style["display"] = "block";
                Div_ErrorMessageAdmin.InnerHtml = strMessage;
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load bus detai form for update bus
        /// </summary>
        /// <param name="busID"></param>
        protected void LoadBusDetail(int busID)
        {
            try
            {
                if (Button_SaveBusDetail.Style["display"] == "none")
                {
                    Button_SaveBusDetail.Style["display"] = "block";
                }
                else
                {
                    Button_SaveBusDetail.Style["display"] = "none";
                }
                string[] strAmenities;
                string[] stringSeparators = new string[] { ", " };
               
                DropDownList_BusType.Attributes["disabled"] = "disabled";
                DropDownList_BusTravelName.Attributes["disabled"] = "disabled";
                Div_Amenities.Attributes["disabled"] = "disabled";
                CheckBox_WaterBottle.Enabled = false;
                CheckBox_Movie.Enabled = false;
                CheckBox_Blanket.Enabled = false;
                CheckBox_ReadingLight.Enabled = false;
                CheckBox_CharginPoint.Enabled = false;
                GetIDAndStatusRequest request = new GetIDAndStatusRequest();
                BusDetailModel response = new BusDetailModel();
                request.ID = busID;
                request.Status = Convert.ToInt32(AppConstant.STATUS.ACTIVE);
                response = busBL.LoadBusDetail(request);
                DropDownList_BusType.Items.Clear();
                DropDownList_BusTravelName.Items.Clear();

                if (response != null)
                {
                                        
                    DropDownList_BusType.Items.Add(new ListItem(response.BusType, response.BusTypeID.ToString()));
                    DropDownList_BusTravelName.Items.Add(new ListItem(response.TravelName, response.TravelID.ToString()));
                    strAmenities = response.Amenities.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    CheckBox_WaterBottle.Checked = strAmenities.Contains(AppConstant.AMENITIES[0]);
                    CheckBox_Movie.Checked = strAmenities.Contains(AppConstant.AMENITIES[1]);
                    CheckBox_Blanket.Checked = strAmenities.Contains(AppConstant.AMENITIES[2]);
                    CheckBox_ReadingLight.Checked = strAmenities.Contains(AppConstant.AMENITIES[3]);
                    CheckBox_CharginPoint.Checked = strAmenities.Contains(AppConstant.AMENITIES[4]);
                    TextBox_BusCapacity.Text = response.Capacity.ToString();
                    CheckBox_BusIsActive.Checked = (response.Status) == Convert.ToInt32(AppConstant.STATUS.ACTIVE) ? true : false;
                   
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.RECORD_NOT_FOUND;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateBus.cs, Function->LoadBusDetail  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Update bus detail
        /// It will go to validation layer to check all given input
        /// Then call bussiness logic layer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_UpdateBusDetail_Click(object sender, EventArgs e)
        {
            string strMessage = DisplayMessage.INVALID_VALUES;
            BusDetailModel busDetail = new BusDetailModel();
            try
            {
                if (Request.QueryString["busID"] != null && Convert.ToInt32(Request.QueryString["busID"].ToString().Trim()) > AppConstant.ERROR)
                {
                    busDetail.BusID = Convert.ToInt32(Request.QueryString["busID"].ToString().Trim());// Not handled
                    busDetail.BusType = DropDownList_BusType.SelectedItem.Text;
                    busDetail.BusTypeID = Convert.ToByte(DropDownList_BusType.SelectedValue);
                    busDetail.TravelID = Convert.ToByte(DropDownList_BusTravelName.SelectedValue);
                    busDetail.TravelName = DropDownList_BusTravelName.SelectedItem.Text;
                    busDetail.Capacity = Convert.ToByte(TextBox_BusCapacity.Text);
                    busDetail.WaterBottle = CheckBox_WaterBottle.Checked;
                    busDetail.Movie = CheckBox_Movie.Checked;
                    busDetail.Blanket = CheckBox_Blanket.Checked;
                    busDetail.ChargingPoint = CheckBox_CharginPoint.Checked;
                    busDetail.ReadingLight = CheckBox_ReadingLight.Checked;

                    busDetail.Status = CheckBox_BusIsActive.Checked ? Convert.ToInt32(AppConstant.STATUS.ACTIVE) : Convert.ToInt32(AppConstant.STATUS.IN_ACTIVE);
                    busDetail.CreatedBy = Session["userEmail"].ToString();
                    busDetail.CreatedDate = System.DateTime.Today.ToShortDateString();
                    strMessage = validation.ValidateBusDetail(busDetail, AppConstant.IS_UPDATE);
                    if (strMessage == DisplayMessage.VALIDATION_SUCCESS)
                    {
                        UpdateBusDetailModel response = new UpdateBusDetailModel();
                        response = busBL.UpdateBusDetail(busDetail);
                        if (response.Status == AppConstant.SUCCESS)
                        {
                            Div_SucessMessageAdmin.Style["display"] = "block";
                            Div_ErrorMessageAdmin.Style["display"] = "none";
                            Div_SucessMessageAdmin.InnerHtml = DisplayMessage.UPDATE_BUS_DETAILS_SUCCESS;
                            Button_SaveBusDetail.Style["display"] = "block";
                            LoadBusDetail(busDetail.BusID);
                        }
                        else if (response.Status == AppConstant.DEPENDENCY)
                        {
                            Div_SucessMessageAdmin.Style["display"] = "none";
                            Div_ErrorMessageAdmin.Style["display"] = "block";
                            Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.UPDATE_BUS_DEPENDENCY;
                        }
                        else
                        {
                            Div_SucessMessageAdmin.Style["display"] = "none";
                            Div_ErrorMessageAdmin.Style["display"] = "block";
                            Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.UPDATE_BUS_DETAILS_FAIL;
                        }
                    }
                    else
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = strMessage;
                    }
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.INVALID_URL;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateBus.cs, Function->Button_UpdateBusDetail_Click  : \n " + ex);
                Div_SucessMessageAdmin.Style["display"] = "none";
                Div_ErrorMessageAdmin.Style["display"] = "block";
                Div_ErrorMessageAdmin.InnerHtml = strMessage;
            }

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to bus detail page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_GoToBusDetail_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("bus-detail.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateBus.cs, Function->Button_GoToBusDetail_Click  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to add-update-trip page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_GoToAddNewTrip_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("add-update-trip.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateBus.cs, Function->Button_GoToAddNewTrip_Click  : \n " + ex);
            }
        }

      
    }
}