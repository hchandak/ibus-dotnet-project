﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="route-detail.aspx.cs" Inherits="iBus.Admin.RouteDetail"  
    MasterPageFile="~/MasterPage/admin.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Script/admin-route.js"></script>
    <link href="../DataTables/css/demo_table.css" rel="stylesheet" />
    <script src="../DataTables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tblRouteList').dataTable({
                "sPaginationType": "full_numbers"
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div id="Div_RouteDetail"  runat="server">
         <div class="alert alert-success" id="Div_SucessMessageAdmin" clientidmode="Static" style="display: none" runat="server"></div>
         <div class="alert alert-danger" id="Div_ErrorMessageAdmin" clientidmode="Static" style="display: none" runat="server"></div>
         <div class="row">
             <div class="col-md-10" runat="server" id="Div_RouteListHeader">
                  <h4 >Route List :</h4>                 
             </div>
             <div class="col-md-2 ">
                  <asp:Button ID="Button_AddNewRoute" CssClass="button-blue" Text="Add New Route" runat="server" OnClick="Button_AddNewRoute_Click"></asp:Button>
             </div>
         </div>
         <br /> 
         <div id="Div_RepeaterRouteList" class="table-background">
            <asp:Repeater ID="Repeater_RouteList" runat="server" OnItemDataBound="Repeater_RouteList_ItemDataBound">
               <HeaderTemplate >
                  <table id="tblRouteList" class="table table-bordered" >
                      <thead >
                        <tr>
                           <th class="small_control">Route Code</th>
                           <th class="small_control">From City</th>
                           <th class="small_control">To City</th>
                           <th class="small_control">Is Active?</th>
                           <th class="tiny_control">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                </HeaderTemplate>
                <ItemTemplate >
                    <label id="Label_IsActive" runat="server"  style="display:none" ><%# Eval("IsActive") %></label>
                   <tr id="trRouteDetailRow<%# Eval("RouteID") %>">
                      <td class="small_control" style="text-align:center"><%# Eval("RouteID") %></td>
                      <td class="small_control"><%# Eval("FromCity") %></td>
                      <td class="small_control"><%# Eval("ToCity") %></td>
                      <td class="small_control" style="text-align:center">
                           <asp:Image id="Image_Active" runat="server" CssClass="margin" style="cursor:pointer;" src="../Image/active.png" title="Active" />
                           <asp:Image id="Image_InActive" runat="server" CssClass="margin" style="cursor:pointer;" src="../Image/in-active.jpg" title="InActive" />
                       </td>
                      <td class="tiny_control"  style="text-align:center">
                           <a id="Link_UpdateRoute" style="cursor:pointer;"  onclick="RedirectToUpdateRoute('<%# Eval("RouteID") %>')" ><img " src="/Image/edit.png" title="Update route"/></a>
                           <a id='Link_DeleteRoute' style="cursor:pointer;" class="margin"  data-toggle="modal" onclick="ConfirmDeleteRouteDetail('<%# Eval("RouteID") %>')" ><img src="/Image/delete.png" title="Delete route" /></a>
                      </td>
                   </tr>
                 </ItemTemplate>
                 <FooterTemplate>
                    </tbody> </table>
                 </FooterTemplate>
             </asp:Repeater>    
         </div>
         <iBus:Control_DisplayMessage ID="Control_DispalyMessage" runat="server" />
         <br /> <br /> 
     </div>
</asp:Content>