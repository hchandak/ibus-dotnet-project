﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessLogic;
using BussinessModel;
using DataAccessObject;
using System.Web.Security;
using log4net;
using System.Text;
using System.Web.Services;
using System.Web.UI.HtmlControls;
namespace iBus.Admin
{
    public partial class TravelDetail : System.Web.UI.Page
    {
        static string modifiedBy = string.Empty;
        TravelsBL travelBL = new TravelsBL();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load travel detail in form of table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)                    
        {
            try
            {
                modifiedBy = Session["userEmail"].ToString();
                if (!IsPostBack)
                {
                    InitializeTravelDetail();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelDetail.cs, Function->Page_Load  : \n " + ex);
            }
            
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Bind travel detail table
        /// </summary>
        protected void InitializeTravelDetail()
        {
            GetIDAndStatusRequest travelDetail = new GetIDAndStatusRequest();
            List<TravelDetailModel> response = new List<TravelDetailModel>();
            try
            {
                response = travelBL.SelectAllTravelDetail(travelDetail);
                if (response.Count > AppConstant.RECOURD_COUNT_ZERO)
                {
                    Repeater_TravelList.DataSource = response;
                    Repeater_TravelList.DataBind();
                }
                else
                {
                    Repeater_TravelList.DataSource = null;
                    Repeater_TravelList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelDetail.cs, Function->InitializeTravelDetail  : \n " + ex);
            }

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to add-update-travel page for add new travel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_AddNewTravel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("add-update-travel.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelDetail.cs, Function->Button_AddNewTravel_Click  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Delete existing travel
        /// </summary>
        /// <param name="travelID"></param>
        /// <returns></returns>
        [WebMethod]
        public static int DeleteTravelDetail(int travelID)
        {
            try
            {
                UpdateStatusDAO DAO = new UpdateStatusDAO();
                if (DAO.UpdateTravelStatus(travelID, Convert.ToInt32(AppConstant.STATUS.DELETED), modifiedBy))
                {
                   return AppConstant.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelDetail.cs, Function->DeleteTravelDetail  : \n " + ex);
            }

            return AppConstant.ERROR;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Bind Is Active field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Repeater_TravelList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    string strIsActive = string.Empty;
                    HtmlGenericControl labelIsActive = (HtmlGenericControl)e.Item.FindControl("Label_IsActive");
                    strIsActive = labelIsActive.InnerText;

                    if (strIsActive == "Active")
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_InActive"));
                        image.Style["display"] = "none";
                    }
                    else if (strIsActive == "Not Active")
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_Active"));
                        image.Style["display"] = "none";
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelDetail.cs, Function->Repeater_TravelList_ItemDataBound  : \n " + ex);
            }
        }

       
    }
}