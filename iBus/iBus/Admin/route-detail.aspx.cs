﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessLogic;
using BussinessModel;
using DataAccessObject;
using System.Web.Security;
using log4net;
using System.Text;
using System.Web.Services;
using System.Web.UI.HtmlControls;
namespace iBus.Admin
{
    public partial class RouteDetail : System.Web.UI.Page
    {
        static string modifiedBy = string.Empty;
        RouteBL routeBL = new RouteBL();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load route detail in form of table             
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                modifiedBy = Session["userEmail"].ToString();
                if (!IsPostBack)
                {
                    InitializeRouteDetail();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDetail.cs, Function->Page_Load  : \n " + ex);
            }
           
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Bind route detail table
        /// </summary>
        protected void InitializeRouteDetail()
        {
            GetIDAndStatusRequest routeDetail = new GetIDAndStatusRequest();
            List<RouteDetailModel> response = new List<RouteDetailModel>();
            try
            {
                response = routeBL.SelectAllRouteDetail(routeDetail);
                if (response.Count > AppConstant.RECOURD_COUNT_ZERO)
                {
                    Repeater_RouteList.DataSource = response;
                    Repeater_RouteList.DataBind();
                }
                else
                {
                    Repeater_RouteList.DataSource = null;
                    Repeater_RouteList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDetail.cs, Function->InitializeRouteDetail  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to add-update-route page for add new route
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_AddNewRoute_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("add-update-route.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDetail.cs, Function->Button_AddNewRoute_Click  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Delete existing route
        /// </summary>
        /// <param name="routeID"></param>
        /// <returns></returns>
        [WebMethod]
        public static int DeleteRouteDetail(int routeID)
        {
            try
            {
                UpdateStatusDAO DAO = new UpdateStatusDAO();
                if (DAO.UpdateRouteStatus(routeID, Convert.ToInt32(AppConstant.STATUS.DELETED), modifiedBy))
                {
                    return AppConstant.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDetail.cs, Function->DeleteRouteDetail  : \n " + ex);
            }
            return AppConstant.ERROR;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Bind Is Active field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Repeater_RouteList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    string strIsActive = string.Empty;
                    HtmlGenericControl labelIsActive = (HtmlGenericControl)e.Item.FindControl("Label_IsActive");
                    strIsActive = labelIsActive.InnerText;

                    if (strIsActive == "Active")
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_InActive"));
                        image.Style["display"] = "none";
                    }
                    else if (strIsActive == "Not Active")
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_Active"));
                        image.Style["display"] = "none";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDetail.cs, Function->Repeater_RouteList_ItemDataBound  : \n " + ex);
            }
        }


    }
}