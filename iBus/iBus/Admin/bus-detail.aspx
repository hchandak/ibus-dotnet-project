﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bus-detail.aspx.cs" Inherits="iBus.Admin.BusDetail"
    MasterPageFile="~/MasterPage/admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Script/admin-bus.js"></script>
    <link href="../DataTables/css/demo_table.css" rel="stylesheet" />
    <script src="../DataTables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tblBusList').dataTable({
                "sPaginationType": "full_numbers"
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Div_BusDetail" runat="server">
        <div class="alert alert-success" id="Div_SucessMessageAdmin" clientidmode="Static" style="display: none" runat="server"></div>
        <div class="alert alert-danger" id="Div_ErrorMessageAdmin" clientidmode="Static" style="display: none" runat="server"></div>
        <div class="row">
            <div class="col-md-10 " runat="server" id="Div_BusListHeader">
                <h4 >Bus List :</h4>
             </div>
             <div class="col-md-2 ">
                <asp:Button ID="Button_AddNewBus" CssClass="button-blue" Text="Add New Bus" runat="server" OnClick="Button_AddNewBus_Click"></asp:Button>
             </div>
         </div>                      
        <br />
        <div id="Div_RepeaterBusList" class="table-background">
             <asp:Repeater ID="Repeater_BusList" runat="server" OnItemDataBound="Repeater_BusList_ItemDataBound">
                <HeaderTemplate >
                   <table id="tblBusList" class="table table-bordered" >
                       <thead >
                          <tr>
                             <th class="small_control">Bus Code</th>
                             <th class="small_control">Travel Name</th>
                             <th class="medium_control">Bus Type</th>
                             <th class="tiny_control">Capacity</th>
                             <th class="medium_control">Amenities</th>
                             <th class="small_control">Is Active?</th>
                             <th class="small_control">Action</th>
                          </tr>
                       </thead>
                       <tbody>
                </HeaderTemplate>
                <ItemTemplate >
                    <label id="Label_Amenities" runat="server"  style="display:none" ><%# Eval("Amenities") %></label>
                    <label id="Label_IsActive" runat="server"  style="display:none" ><%# Eval("IsActive") %></label>
                   <tr id="trBusDetailRow<%# Eval("BusID") %>">
                       
                       <td class="small_control" style="text-align:center"><%# Eval("BusID") %></td>
                       <td class="small_control"><%# Eval("TravelName") %></td>
                       <td class="medium_control"><%# Eval("BusType") %></td>
                       <td class="tiny_control" style="text-align:center"><%# Eval("Capacity") %></td>
                       <td class="medium_control" >
                           <asp:Image id="Image_NoAmenities" CssClass="margin" runat="server" style="cursor:pointer;display:none" src="../Image/Amenities/no-amenities.jpg" title="No Amenities" />
                           <asp:Image id="Image_WaterBottle" CssClass="margin" runat="server" style="cursor:pointer;" src="../Image/Amenities/water-bottle.png" title="Water Bottle" />
                           <asp:Image id="Image_Blancket" CssClass="margin" runat="server" style="cursor:pointer;" src="../Image/Amenities/blancket1.png" title="Blanket" />
                           <asp:Image id="Image_Movies" CssClass="margin" runat="server" style="cursor:pointer;" src="../Image/Amenities/movies.png" title="Movies" />
                           <asp:Image id="Image_CharginPoint" CssClass="margin" runat="server" style="cursor:pointer;" src="../Image/Amenities/charging-point.jpg" title="Chargin Point" />
                           <asp:Image id="Image_ReadingLight" runat="server" class="margin" style="cursor:pointer;" src="../Image/Amenities/readinglight.png" title="Reading Light" />
                           
                       </td>
                       <td class="small_control" style="text-align:center">
                           <asp:Image id="Image_Active" runat="server" CssClass="margin" style="cursor:pointer;" src="../Image/active.png" title="Active" />
                           <asp:Image id="Image_InActive" runat="server" CssClass="margin" style="cursor:pointer;" src="../Image/in-active.jpg" title="InActive" />
                       </td>
                       <td class="small_control" style="text-align:center">
                           <a id="Link_UpdateBus" style="cursor:pointer;"  onclick="RedirectToUpdateBus('<%# Eval("BusID") %>')" ><img " src="/Image/edit.png" title="Update bus"/></a>
                           <a id='Link_DeleteBus' style="cursor:pointer;" class="margin" data-toggle="modal" onclick="ConfirmDeleteBusDetail('<%# Eval("BusID") %>')" ><img src="/Image/delete.png" title="Delete bus" /></a>
                       </td>
                               
                   </tr>
               </ItemTemplate>
               <FooterTemplate>
                   </tbody> </table>
               </FooterTemplate>
            </asp:Repeater>
         </div> 
        <iBus:Control_DisplayMessage ID="Control_DispalyMessage" runat="server" />
        <br /> <br /> 
     </div>
</asp:Content>