﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="iBus.Admin.Admin"
    MasterPageFile="~/MasterPage/admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/js-image-slider.css" rel="stylesheet" />
    <script src="../Script/js-image-slider.js"></script>       
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container " >
        
        <div class="alert alert-success" id="Div_SucessMessageAdmin" clientidmode="Static" style="display: none" runat="server"></div>
        <div class="alert alert-danger" id="Div_ErrorMessageAdmin" clientidmode="Static" style="display: none" runat="server"></div>
        
        <div class="row">
            <div class="col-md-12">

                <div class="row" id="Div_AdminMyAccount" runat="server">
                    <div class="col-md-3" runat="server" id="Div_AdminDetail"></div>
                    <div class="col-md-9">
                        <iBus:Control_AdvertisementTravel runat="server" ID="Control_AdvertisementTravel" />
                       
                    </div>
                </div>

            </div>              
         
        </div>
    </div><br /><br />
</asp:Content>


