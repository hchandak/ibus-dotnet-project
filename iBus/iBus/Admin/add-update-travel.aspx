﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-update-travel.aspx.cs" Inherits="iBus.Admin.AddUpdateTravel" 
    MasterPageFile="~/MasterPage/admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <script src="../Script/bootstrap-tooltip.js"></script>
   <script src="../Script/admin-travel.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="container">
    <div id="Div_AddTravel"  runat="server"  class="col-md-8 div_form_layout"  >
        <div class="alert alert-success" id="Div_SucessMessageAdmin" ClientIDMode="Static" style="display:none"   runat="server"></div>
        <div class="alert alert-danger" id="Div_ErrorMessageAdmin" ClientIDMode="Static" style="display:none"  runat="server"></div>
         <div >
            <div class="col-md-6" runat="server" id="Div_TravelHeader">
                <h4>Fill Travel Detail</h4>
            </div>
            <div class="pull-right"><br />
                <asp:Button id="Button_GoToAddNewBus" style="display:none" Text="Add New Bus" CssClass="button-blue" runat="server" OnClick="Button_GoToAddNewBus_Click"></asp:Button>
                <asp:Button id="Button_GoToTravelDetail" Text="Go To Travel List" CssClass="button-blue" runat="server" OnClick="Button_GoToTravelDetail_Click" ></asp:Button>
            
            </div>
        </div><br /><br /><br /><br />
         <div class="row">          
             <div class="col-md-3">                  
                 <label id="lbl_TravelName"  runat="server">Travel name<sup class="required">*</sup></label>
             </div>
             <div class="col-md-8" >
                 <asp:TextBox type="text " CssClass="form-control medium_control"  id="TextBox_TravelName" runat="server" ClientIDMode="Static" placeholder="Name" /><br />
             </div>
         </div>
         
         <div class="row">
             <div class="col-md-3">
                 <label id="lbl_TravelAddress"  runat="server">Address<sup class="required">*</sup></label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" TextMode="MultiLine" Rows="4" CssClass="form-control medium_control " id="TextBox_TravelAddress" runat="server" ClientIDMode="Static" placeholder="Address" /> <br />
             </div>
         </div>
               
         <div class="row">
             <div class="col-md-3">
                  <label id="Label_TravelEmail"  runat="server">Travel email<sup class="required">*</sup></label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control medium_control " id="TextBox_TravelEmail" runat="server" ClientIDMode="Static" placeholder="Email" /><br /> 
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <label id="Label_TravelContact"  runat="server">Travel contact<sup class="required">*</sup></label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control medium_control " id="TextBox_TravelContact" runat="server" ClientIDMode="Static" placeholder="Contact" /><br /> 
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TravelIsActive" Text="IsActive?" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:CheckBox   ID="CheckBox_TravelIsActive" runat="server" ClientIDMode="Static" />
             </div>
         </div>
         <div class="pull-right">
            <asp:Button Text="Update Travel" ID="Button_UpdateTravelDetail" runat="server" OnClientClick="return ValidateTravelDetail();" OnClick="Button_UpdateTravelDetail_Click" ClientIDMode="Static" CssClass="button-blue"  />
            <asp:Button Text="Add Travel" ID="Button_SaveTravelDetail" runat="server" OnClientClick="return ValidateTravelDetail();" OnClick="Button_SaveTravelDetail_Click" ClientIDMode="Static" CssClass="button-blue" /><br /><br />
        </div>  
       <br /> <br /> 
   </div> 
</div>
</asp:Content>

