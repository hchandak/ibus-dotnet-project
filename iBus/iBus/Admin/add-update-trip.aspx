﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-update-trip.aspx.cs" Inherits="iBus.Admin.AddUpdateTrip" 
    MasterPageFile="~/MasterPage/admin.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/Script/bootstrap-tooltip.js"></script>
    <script src="../Script/admin-trip.js"></script>
    <link href="../DataTables/css/demo_table.css" rel="stylesheet" />
    <script src="../DataTables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tblBusTypeAndAmenitiesList').dataTable({
                "sPaginationType": "full_numbers"
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="container">
   
    <div id="Div_AddTrip" runat="server"   class="col-md-11 div_form_layout" >
        <div class="alert alert-success" id="Div_SucessMessageAdmin" ClientIDMode="Static" style="display:none"   runat="server"></div>
        <div class="alert alert-danger" id="Div_ErrorMessageAdmin" ClientIDMode="Static" style="display:none"  runat="server"></div>
         <div >
            <div class="col-md-3" runat="server" id="Div_TripHeader">                    
                <h4 >Fill Trip Detail</h4>
            </div>
            <div class=" pull-right"><br />
                 <asp:Button id="Button_GoToAddNewTravel" Text="Add New Travel" CssClass="button-blue" runat="server" OnClick="Button_GoToAddNewTravel_Click"></asp:Button>
                 <asp:Button id="Button_GoToAddNewBus" Text="Add New Bus" CssClass="button-blue" runat="server" OnClick="Button_GoToAddNewBus_Click"></asp:Button>
                 <asp:Button id="Button_GoToAddNewRoute" Text="Add New Route" CssClass="button-blue" runat="server" OnClick="Button_GoToAddNewRoute_Click"></asp:Button>
                 <asp:Button id="Button_GoToTripDetail" CssClass="button-blue"  Text="Go To Trip List" runat="server" OnClick="Button_GoToTripDetail_Click"></asp:Button>
            </div>
         </div><br /><br /><br /><br />
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TripTravelName" Text="Travel name*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4 ">
                 <asp:DropDownList ID="DropDownList_TripTravelName"  CssClass="medium_control " OnSelectedIndexChanged="DropDownList_TripTravelName_SelectedIndexChanged" runat="server" ClientIDMode="Static" AutoPostBack="true" ></asp:DropDownList><br />
             </div>
         </div><br />
        
         
         <div class="row" id="Div_BusTypeAndAmenitiesList" style="display:none"  runat="server" >
            
            <div class="col-md-3">
               <asp:Label ID="Label_TripBusType"  runat="server"></asp:Label>
            </div>
           <%-- <asp:ScriptManagerProxy ID="ScriptManagerProxy_AdminBusTypeAndAmenitiesList" runat="server"></asp:ScriptManagerProxy>
            <asp:UpdatePanel ID="UpdatePanel_AdminBusTypeAndAmenitiesList" runat="server">
                <ContentTemplate>--%>
                    <div id="Div_RepeaterBusTypeAndAmenitiesList" class="col-md-9 " runat="server">
                        <asp:Label ID="Label_BusTypeAndAmenities"  runat="server"></asp:Label>
                        <div id="Div_RepeaterBusTypeAndAmenities" class="table-background">
                          <asp:Repeater ID="Repeater_BusTypeAndAmenitiesList" runat="server" >
                            <HeaderTemplate >
                            <table id="tblBusTypeAndAmenitiesList" class="table table-bordered" >
                                <thead >
                                    <tr>
                                        <th >Select</th>
                                        <th >BusType</th>
                                        <th >Amenities</th>
                                    </tr>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate >
                               <tr>
                                    <td style="text-align:center"><input name="radioBusTypeAndAmenities" type="radio"  value='<%# Eval("BusID") %>' /></td>
                                    <td ><%# Eval("BusType") %></td>
                                    <td ><%# Eval("Amenities") %></td>
                               </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> </table>
                            </FooterTemplate>
                          </asp:Repeater>
                        </div>
                    </div>
               <%-- </ContentTemplate>
                <Triggers>
                   <asp:AsyncPostBackTrigger ControlID="DropDownList_TripTravelName" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
              --%>
         </div><br />
         
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TripRoute" Text="Route*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:DropDownList ID="DropDownList_TripRoute"  CssClass="large_control " runat="server" ClientIDMode="Static" ></asp:DropDownList><br /><br />
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TripFare" Text="Fare*" runat="server"></asp:Label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control small_control "  id="TextBox_TripFare" runat="server" ClientIDMode="Static" placeholder='Fare' /><br />
             </div>
         </div>
        <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_Frequency" Text="Frequency*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4 ">
                 <asp:DropDownList ID="DropDownList_Frequency"  CssClass="medium_control "  runat="server" ClientIDMode="Static" ></asp:DropDownList><br /><br />
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TripDepartureTime" Text="Departure time*" runat="server"></asp:Label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control small_control"  id="TextBox_DepartureTime" runat="server" ClientIDMode="Static" placeholder='HH:MM' /><br />
             </div>
             
         </div>
        
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_JourneyDuration" Text="Journey duration*" runat="server"></asp:Label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control small_control"  id="TextBox_JourneyDuration" runat="server" ClientIDMode="Static" placeholder='HH:MM' /><br />
             </div>
     
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TripIsActive" Text="IsActive?" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:CheckBox  ID="CheckBox_TripIsActive" runat="server" ClientIDMode="Static" />
             </div>
         </div><br />
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_BoardingPoint" Text="Boarding points*" runat="server"></asp:Label>
             </div>
             <asp:ScriptManagerProxy ID="ScriptManagerProxy_AdminBusTypeAndAmenitiesList" runat="server"></asp:ScriptManagerProxy>
             <asp:UpdatePanel ID="UpdatePanel_BoardingPoint" runat="server">
                 <ContentTemplate>
                     <div class="col-md-9">
                            <asp:GridView ID="GridView_BoardingPoint" ShowFooter="true" ClientIDMode="Static"  ShowHeader="true" ShowHeaderWhenEmpty="true" CssClass="table table-bordered"
                                AutoGenerateColumns="false"  runat="server"  OnRowCommand="GridView_BoardingPoint_RowCommand" OnRowUpdating="GridView_BoardingPoint_RowUpdating" OnRowDeleting="GridView_BoardingPoint_RowDeleting" >
                                    <Columns>   

                                        <asp:TemplateField    HeaderText="Boarding point*">
                                            <ItemStyle  Width="200px" ></ItemStyle>
                                            <FooterStyle HorizontalAlign="Center"  Width="200px" />
                                            <ItemTemplate >
                                                <label id='lblSBoardingPointName'  runat="server"><%# Eval("PointName") %></label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox id="TextBox_BoardingPointName" ClientIDMode="Static"  runat="server" placeholder="Boarding point"  ></asp:TextBox>
                                            </FooterTemplate> 
                                        </asp:TemplateField>
                                        <asp:TemplateField    HeaderText="Address*">
                                            <ItemStyle  Width="300px"></ItemStyle>
                                            <FooterStyle HorizontalAlign="Center"  Width="300px" />
                                            <ItemTemplate >
                                                <label id='lblSBoardingPointAddress' runat="server"><%# Eval("PointAddress") %></label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox id="TextBox_BoardingPointAddress"  ClientIDMode="Static" runat="server" placeholder="Address"  ></asp:TextBox>
                                            </FooterTemplate> 
                                        </asp:TemplateField>

                                        <asp:TemplateField    HeaderText="Departure*">
                                            <ItemStyle  Width="80px" ></ItemStyle>
                                            <FooterStyle HorizontalAlign="Center"  Width="80px" />
                                            <ItemTemplate >
                                                <label id='lblSBoardingPointTime' runat="server"><%# Eval("Departure") %></label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox id="TextBox_BoardingPointTime" ClientIDMode="Static"  runat="server" placeholder="HH:MM"  ></asp:TextBox>
                                            </FooterTemplate> 
                                        </asp:TemplateField>
             
                                        <asp:TemplateField   HeaderText="Option">
                                            <ItemStyle   HorizontalAlign="Center" Width="80px"></ItemStyle>
                                            <FooterStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemTemplate >
                                                <asp:LinkButton id='LinkButton_DeleteBoardingPoint' runat="server" CommandName="Delete"  CommandArgument='<%# Container.DataItemIndex %>' ><img src="/Image/delete.png" title="Delete boarding point" /></asp:LinkButton>                 
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:LinkButton ID="LinkButton_InsertBoardingPoint" runat="server" CommandName="Insert" OnClientClick="return ValidateBoardingPoint();" CommandArgument='<%# Container.DataItemIndex %>' ><img src="/Image/edit.png" title="Insert boarding point" /></asp:LinkButton> 
                                            </FooterTemplate>
                                        </asp:TemplateField> 
                                     
                                    </Columns>
                                   
                                    <HeaderStyle  HorizontalAlign="Center" VerticalAlign="Middle" Font-Italic="False" Font-Bold="True"> </HeaderStyle>
                                    <FooterStyle HorizontalAlign="Center"  BorderStyle="None"></FooterStyle>
                            </asp:GridView>
                     </div>
                  </ContentTemplate>
                 <Triggers>
                     <asp:AsyncPostBackTrigger ControlID="GridView_BoardingPoint" EventName="RowCommand" />
                 </Triggers>
             </asp:UpdatePanel>
          </div>
         <div class="pull-right">
           <asp:Button Text="Update Trip" ID="Button_UpdateTripDetail" runat="server" OnClientClick="return ValidateTripDetailForUpdate();" OnClick="Button_UpdateTripDetail_Click" ClientIDMode="Static" CssClass="button-blue" />
           <asp:Button Text="Add Trip" ID="Button_SaveTripDetail" runat="server" OnClientClick="return ValidateTripDetailForSave();"  OnClick="Button_SaveTripDetail_Click" ClientIDMode="Static" CssClass="button-blue" /><br /><br />
        </div>
        <br /> <br /> 
    </div>
  </div>
</asp:Content>

