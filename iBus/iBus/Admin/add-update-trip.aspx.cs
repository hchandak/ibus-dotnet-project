﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataValidation;
using BussinessLogic;
using BussinessModel;
using log4net;
using System.Text;
using System.Data;
using System.Web.UI.HtmlControls;
namespace iBus.Admin
{
    public partial class AddUpdateTrip : System.Web.UI.Page
    {
        AdminValidation validation = new AdminValidation();
        TripBL tripBL = new TripBL();
        TravelsBL travelBL = new TravelsBL();
        RouteBL routeBL = new RouteBL();              
        BusBL busBL = new BusBL();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load trip detail form base on query string 
        /// Where Admin can give detail for add or update trip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["tripID"] != null && Request.QueryString["tripID"].ToString().Trim().Length > 0)
                    {
                        LoadTripDetail(Convert.ToInt32(Request.QueryString["tripID"].ToString().Trim()));
                    }
                    else
                    {
                        InitializeTripDetail();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->Page_Load  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Initialize trip detail form
        /// </summary>
        protected void InitializeTripDetail()
        {
            int index = 0;
            try
            {
                Button_UpdateTripDetail.Style["display"] = "none";
                DropDownList_TripTravelName.Items.Clear();
                DropDownList_TripRoute.Items.Clear();
                DropDownList_Frequency.Items.Clear();
                TextBox_TripFare.Text = "";
                TextBox_DepartureTime.Text = "";
                TextBox_JourneyDuration.Text = "";
                DropDownList_TripTravelName.Items.Add(new ListItem("Select Travel", index.ToString()));
                DropDownList_TripRoute.Items.Add(new ListItem("Select Route", index.ToString()));
                DropDownList_Frequency.Items.Add(new ListItem("Select Freq", index.ToString()));
                               
                GetIDAndStatusRequest travelDetail = new GetIDAndStatusRequest();
                List<TravelDetailModel> response1 = new List<TravelDetailModel>();
                response1 = travelBL.SelectAllTravelDetail(travelDetail);
                foreach (TravelDetailModel travel in response1)
                {
                    DropDownList_TripTravelName.Items.Add(new ListItem(travel.TravelName, travel.TravelID.ToString()));
                }

                if (Session["travelID"] != null && Session["busID"] != null)
                {
                    DropDownList_TripTravelName.SelectedValue = Session["travelID"].ToString();
                    GetIDAndStatusRequest request = new GetIDAndStatusRequest();
                    BusDetailModel response = new BusDetailModel();
                    request.ID = Convert.ToInt32(Session["busID"]);
                    request.Status = Convert.ToInt32(AppConstant.STATUS.ACTIVE);
                    response = busBL.LoadBusDetail(request);
                    Div_BusTypeAndAmenitiesList.Style["display"] = "block";
                    Label_TripBusType.Text = "BusType and Amenities*";
                    Label_BusTypeAndAmenities.Text = response.BusType + " , " + response.Amenities;

                }

                GetIDAndStatusRequest routeDetail = new GetIDAndStatusRequest();
                List<RouteDetailModel> response2 = new List<RouteDetailModel>();
                response2 = routeBL.SelectAllRouteDetail(routeDetail);
                foreach (RouteDetailModel route in response2)
                {
                    DropDownList_TripRoute.Items.Add(new ListItem(route.FromCity + " To " + route.ToCity, route.RouteID.ToString()));
                }

                if (Session["route1ID"] != null)
                {
                    DropDownList_TripRoute.SelectedValue = Session["route1ID"].ToString();
                }

                GetIDAndStatusRequest frequencyDetail = new GetIDAndStatusRequest();
                List<FreqDetailModel> response3 = new List<FreqDetailModel>();
                response3 = tripBL.SelectAllFrequencyType(frequencyDetail);
                foreach (FreqDetailModel frequency in response3)
                {
                    DropDownList_Frequency.Items.Add(new ListItem(frequency.FreqType, frequency.FreqID.ToString()));
                }

                CheckBox_TripIsActive.Checked = true;
                InitializeBoardingPointGrid();
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->InitializeTripDetail  : \n " + ex);
            }

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Save trip detail
        /// It will go to validation layer to check all given input
        /// Then call bussiness layer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_SaveTripDetail_Click(object sender, EventArgs e)
        {
            string strMessage = DisplayMessage.INVALID_VALUES;
            TripDetailModel tripDetail = new TripDetailModel();
            try
            {
                if (Session["travelID"] != null && Session["busID"] != null)
                {
                    if (DropDownList_TripTravelName.SelectedValue == Session["travelID"].ToString())
                    {
                        tripDetail.BusID = Convert.ToInt32(Session["busID"]);
                    }
                    else
                    {
                        tripDetail.BusID = Convert.ToInt32(Request.Form["radioBusTypeAndAmenities"]);
                    }
                }
                else
                {
                    tripDetail.BusID = Convert.ToInt32(Request.Form["radioBusTypeAndAmenities"]);
                }
                tripDetail.TravelID = Convert.ToInt32(DropDownList_TripTravelName.SelectedValue);
                tripDetail.RouteID = Convert.ToInt32(DropDownList_TripRoute.SelectedValue);
                tripDetail.FreqID = Convert.ToByte(DropDownList_Frequency.SelectedValue);
                tripDetail.TravelName = DropDownList_TripTravelName.SelectedItem.Text;
                tripDetail.Fare = Convert.ToInt32(TextBox_TripFare.Text.ToString().Trim());
                tripDetail.DepartureTime = TimeSpan.Parse(TextBox_DepartureTime.Text.ToString());
                string[] strTime;
                strTime = TextBox_JourneyDuration.Text.ToString().Split(':');
                tripDetail.JourneyDuration = TextBox_JourneyDuration.Text.ToString();
                tripDetail.Totaltime = Convert.ToInt32(strTime[0]) * 60 + Convert.ToInt32(strTime[1]);
                tripDetail.Status = CheckBox_TripIsActive.Checked ? Convert.ToInt32(AppConstant.STATUS.ACTIVE) : Convert.ToInt32(AppConstant.STATUS.IN_ACTIVE);
                tripDetail.CreatedBy = Session["userEmail"].ToString();
                tripDetail.CreatedDate = System.DateTime.Today.ToShortDateString();


                //Get all boarding points
                HtmlGenericControl lblBoardingPointName, lblBoardingPointAddress, lblBoardingPointTime;
                List<BoardingPointDetailModel> listBoardingPoint = new List<BoardingPointDetailModel>();
                BoardingPointDetailModel boardingPoint = new BoardingPointDetailModel();
                int gridRowCount = GridView_BoardingPoint.Rows.Count;
                int rowIndex = 0;
                while (gridRowCount >= 1)
                {
                    GridViewRow Itemrow = GridView_BoardingPoint.Rows[rowIndex];

                    lblBoardingPointName = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointName");
                    lblBoardingPointAddress = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointAddress");
                    lblBoardingPointTime = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointTime");

                    boardingPoint.PointName = lblBoardingPointName.InnerText.Trim();
                    boardingPoint.PointAddress = lblBoardingPointAddress.InnerText.Trim();
                    boardingPoint.Departure = lblBoardingPointTime.InnerText.Trim();
                    boardingPoint.DepartureTime = TimeSpan.Parse(lblBoardingPointTime.InnerText.Trim());
                    listBoardingPoint.Add(new BoardingPointDetailModel(boardingPoint));
                    gridRowCount--;
                    rowIndex++;
                }


                strMessage = validation.ValidateTripDetail(tripDetail, AppConstant.IS_SAVE);

                if (strMessage == DisplayMessage.VALIDATION_SUCCESS)
                {
                    AddTripDetailModel response = new AddTripDetailModel();
                    response = tripBL.AddTripDetail(tripDetail, listBoardingPoint);
                    if (response.Status == AppConstant.SUCCESS)
                    {
                        Div_SucessMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.Style["display"] = "none";
                        Div_SucessMessageAdmin.InnerHtml = DisplayMessage.SAVE_TRIP_DETAILS_SUCCESS;
                        Button_GoToAddNewRoute.Style["display"] = "block";
                        Button_SaveTripDetail.Style["display"] = "none";
                        LoadTripDetail(response.TripID);
                    }
                    else if (response.Status == AppConstant.RECORD_EXIST)
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.TRIP_EXIST;
                    }
                    else
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.SAVE_TRIP_DETAILS_FAIL;
                    }
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = strMessage;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->Button_SaveTripDetail_Click  : \n " + ex);
                Div_SucessMessageAdmin.Style["display"] = "none";
                Div_ErrorMessageAdmin.Style["display"] = "block";
                Div_ErrorMessageAdmin.InnerHtml = strMessage;
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load trip detail form for update trip
        /// </summary>
        /// <param name="tripID"></param>
        protected void LoadTripDetail(int tripID)
        {
            int index = 0;
            try
            {
                if (Button_SaveTripDetail.Style["display"] == "none")
                {
                    Button_SaveTripDetail.Style["display"] = "block";
                }
                else
                {
                    Button_SaveTripDetail.Style["display"] = "none";
                }
                DropDownList_TripTravelName.Items.Clear();
                DropDownList_TripRoute.Items.Clear();
                DropDownList_Frequency.Items.Clear();
                DropDownList_TripTravelName.Attributes["disabled"] = "disabled";
                DropDownList_TripRoute.Attributes["disabled"] = "disabled";

                DropDownList_Frequency.Items.Add(new ListItem("Select Freq", index.ToString()));
                                
                GetIDAndStatusRequest request = new GetIDAndStatusRequest();
                TripDetailModel response = new TripDetailModel();
                request.ID = tripID;
                request.Status = Convert.ToInt32(AppConstant.STATUS.ACTIVE);
                response = tripBL.LoadTripDetail(request);
                LoadBoardingPointGrid(tripID); //Load boarding point grid
                if (response != null)
                {

                    this.TextBox_TripFare.Text = response.Fare.ToString();
                    this.DropDownList_TripTravelName.Items.Add(new ListItem(response.TravelName, response.TravelID.ToString()));
                    Div_BusTypeAndAmenitiesList.Style["display"] = "block";
                    Label_TripBusType.Text = "BusType and Amenities*";
                    Label_BusTypeAndAmenities.Text = response.BusType + " , " + response.Amenities;

                    this.DropDownList_TripRoute.Items.Add(new ListItem(response.Route, response.RouteID.ToString()));

                    GetIDAndStatusRequest frequencyDetail = new GetIDAndStatusRequest();
                    List<FreqDetailModel> response1 = new List<FreqDetailModel>();
                    response1 = tripBL.SelectAllFrequencyType(frequencyDetail);
                    foreach (FreqDetailModel frequency in response1)
                    {
                        DropDownList_Frequency.Items.Add(new ListItem(frequency.FreqType, frequency.FreqID.ToString()));
                    }
                    DropDownList_Frequency.SelectedValue = response.FreqID.ToString();

                    TextBox_DepartureTime.Text = response.Departure.Substring(0,5);
                    
                    TextBox_JourneyDuration.Text = (response.Totaltime / 60).ToString();
                    if (TextBox_JourneyDuration.Text.Length == 1)
                        TextBox_JourneyDuration.Text = "0" + TextBox_JourneyDuration.Text;
                    TextBox_JourneyDuration.Text  =TextBox_JourneyDuration.Text+":" + (response.Totaltime % 60).ToString();//Need to update DataBase
                    if (TextBox_JourneyDuration.Text.Length == 4)
                        TextBox_JourneyDuration.Text += "0";
                    CheckBox_TripIsActive.Checked = (response.Status) == Convert.ToInt32(AppConstant.STATUS.ACTIVE) ? true : false;
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.RECORD_NOT_FOUND;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->LoadTripDetail  : \n " + ex);
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Update trip detail
        /// It will go to validation layer to check all given input
        /// Then call bussiness layer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_UpdateTripDetail_Click(object sender, EventArgs e)
        {
            string strMessage = DisplayMessage.INVALID_VALUES;
            TripDetailModel tripDetail = new TripDetailModel();
            try
            {
                if (Request.QueryString["tripID"] != null && Convert.ToInt32(Request.QueryString["tripID"].ToString().Trim()) > AppConstant.ERROR)
                {
                    tripDetail.TripID = Convert.ToInt32(Request.QueryString["tripID"].ToString().Trim());
                    tripDetail.FreqID = Convert.ToByte(DropDownList_Frequency.SelectedValue);
                    tripDetail.Fare = Convert.ToInt32(TextBox_TripFare.Text);
                    tripDetail.DepartureTime = TimeSpan.Parse(TextBox_DepartureTime.Text);
                    string[] strTime;
                    strTime = TextBox_JourneyDuration.Text.ToString().Split(':');
                    tripDetail.JourneyDuration = TextBox_JourneyDuration.Text.ToString();
                    tripDetail.Totaltime = Convert.ToInt32(strTime[0]) * 60 + Convert.ToInt32(strTime[1]);
                    tripDetail.Status = CheckBox_TripIsActive.Checked ? Convert.ToInt32(AppConstant.STATUS.ACTIVE) : Convert.ToInt32(AppConstant.STATUS.IN_ACTIVE);
                    tripDetail.ModifiedBy = Session["userEmail"].ToString();
                    tripDetail.ModifiedDate = System.DateTime.Today.ToShortDateString();

                    //Get all boarding points
                    HtmlGenericControl lblBoardingPointName, lblBoardingPointAddress, lblBoardingPointTime;
                    List<BoardingPointDetailModel> listBoardingPoint = new List<BoardingPointDetailModel>();
                    BoardingPointDetailModel boardingPoint = new BoardingPointDetailModel();
                    int gridRowCount = GridView_BoardingPoint.Rows.Count;
                    int rowIndex = 0;
                    while (gridRowCount >= 1)
                    {
                        GridViewRow Itemrow = GridView_BoardingPoint.Rows[rowIndex];

                        lblBoardingPointName = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointName");
                        lblBoardingPointAddress = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointAddress");
                        lblBoardingPointTime = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointTime");

                        boardingPoint.PointName = lblBoardingPointName.InnerText.Trim();
                        boardingPoint.PointAddress = lblBoardingPointAddress.InnerText.Trim();
                        boardingPoint.Departure = lblBoardingPointTime.InnerText.Trim();
                        boardingPoint.DepartureTime = TimeSpan.Parse(lblBoardingPointTime.InnerText.Trim());
                        listBoardingPoint.Add(new BoardingPointDetailModel(boardingPoint));
                        gridRowCount--;
                        rowIndex++;
                    }

                    strMessage = validation.ValidateTripDetail(tripDetail, AppConstant.IS_UPDATE);
                    if (strMessage == DisplayMessage.VALIDATION_SUCCESS)
                    {
                        UpdateTripDetailModel response = new UpdateTripDetailModel();
                        response = tripBL.UpdateTripDetail(tripDetail, listBoardingPoint);
                        if (response.Status == AppConstant.SUCCESS)
                        {
                            Div_SucessMessageAdmin.Style["display"] = "block";
                            Div_ErrorMessageAdmin.Style["display"] = "none";
                            Div_SucessMessageAdmin.InnerHtml = DisplayMessage.UPDATE_TRIP_DETAILS_SUCCESS;
                            Button_SaveTripDetail.Style["display"] = "block";
                            LoadTripDetail(tripDetail.TripID);
                        }
                        else if (response.Status == AppConstant.RECORD_EXIST)
                        {
                            Div_SucessMessageAdmin.Style["display"] = "none";
                            Div_ErrorMessageAdmin.Style["display"] = "block";
                            Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.TRIP_EXIST;
                        }
                        else if (response.Status == AppConstant.DEPENDENCY)
                        {
                            Div_SucessMessageAdmin.Style["display"] = "none";
                            Div_ErrorMessageAdmin.Style["display"] = "block";
                            Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.UPDATE_TRIP_DEPENDENCY;
                        }
                        else
                        {
                            Div_SucessMessageAdmin.Style["display"] = "none";
                            Div_ErrorMessageAdmin.Style["display"] = "block";
                            Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.UPDATE_TRIP_DETAILS_FAIL;
                        }
                    }
                    else
                    {
                        Div_SucessMessageAdmin.Style["display"] = "none";
                        Div_ErrorMessageAdmin.Style["display"] = "block";
                        Div_ErrorMessageAdmin.InnerHtml = strMessage;
                    }
                }
                else
                {
                    Div_SucessMessageAdmin.Style["display"] = "none";
                    Div_ErrorMessageAdmin.Style["display"] = "block";
                    Div_ErrorMessageAdmin.InnerHtml = DisplayMessage.INVALID_URL;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->Button_UpdateTripDetail_Click  : \n " + ex);
                Div_SucessMessageAdmin.Style["display"] = "none";
                Div_ErrorMessageAdmin.Style["display"] = "block";
                Div_ErrorMessageAdmin.InnerHtml = strMessage;
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Display bus type and amenities in table once select travel name from dropdown list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownList_TripTravelName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strBusTypeAndAmenities = string.Empty;
            try
            {
                List<BusTypeAndAmenitiesModel> response = new List<BusTypeAndAmenitiesModel>();
                GetIDAndStatusRequest request = new GetIDAndStatusRequest();
                request.ID = Convert.ToInt32(DropDownList_TripTravelName.SelectedValue);
                response = tripBL.SelectAllBusTypeAndAmenitiesModel(request);
                Div_BusTypeAndAmenitiesList.Style["display"] = "block";
                Label_TripBusType.Text = "BusType and Amenities*";
                if (response != null && response.Count > AppConstant.RECOURD_COUNT_ZERO)
                {
                    Label_BusTypeAndAmenities.Text = DisplayMessage.SELECT_BUS_TYPE;
                    InitializeBusTypeAndAmenities();

                }
                else
                {
                    Label_BusTypeAndAmenities.Text = DisplayMessage.REPEATER_RECORD_NOT_FOUND;
                    Repeater_BusTypeAndAmenitiesList.DataSource = null;
                    Repeater_BusTypeAndAmenitiesList.DataBind();

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->DropDownList_TripTravelName_SelectedIndexChanged  : \n " + ex);
           
            }

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Initialize bus type and amenities in form of table
        /// </summary>
        public void InitializeBusTypeAndAmenities()
        {
            List<BusTypeAndAmenitiesModel> response = new List<BusTypeAndAmenitiesModel>();
            GetIDAndStatusRequest request = new GetIDAndStatusRequest();
            try
            {
                request.ID = Convert.ToInt32(DropDownList_TripTravelName.SelectedValue);
                response = tripBL.SelectAllBusTypeAndAmenitiesModel(request);

                if (response.Count > AppConstant.RECOURD_COUNT_ZERO)
                {
                    Repeater_BusTypeAndAmenitiesList.DataSource = response;
                    Repeater_BusTypeAndAmenitiesList.DataBind();
                }
                else
                {
                    Label_BusTypeAndAmenities.Text = DisplayMessage.REPEATER_RECORD_NOT_FOUND;
                    Repeater_BusTypeAndAmenitiesList.DataSource = null;
                    Repeater_BusTypeAndAmenitiesList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->InitializeBusTypeAndAmenities  : \n " + ex);

            }

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to trip detail page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_GoToTripDetail_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("trip-detail.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->Button_GoToTripDetail_Click  : \n " + ex);

            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to add-update-route page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_GoToAddNewRoute_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("add-update-route.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->Button_GoToAddNewRoute_Click  : \n " + ex);

            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to add-update-bus page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_GoToAddNewBus_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("add-update-bus.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->Button_GoToAddNewBus_Click  : \n " + ex);

            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Redirect to add-update-travel page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_GoToAddNewTravel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("add-update-travel.aspx", false);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AddUpdateTrip.cs, Function->Button_GoToAddNewTravel_Click  : \n " + ex);

            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Initialize grid to add new boarding point
        /// </summary>
        public void InitializeBoardingPointGrid()
        {

            List<BoardingPointDetailModel> listBoardingPoint = new List<BoardingPointDetailModel>();
            BoardingPointDetailModel boardingPoint = new BoardingPointDetailModel();
            boardingPoint.PointName = "No Boarding Points Available";
            listBoardingPoint.Add(new BoardingPointDetailModel(boardingPoint));
            GridView_BoardingPoint.DataSource = listBoardingPoint;
            GridView_BoardingPoint.DataBind();
            int columnsCount = GridView_BoardingPoint.Columns.Count;
            GridView_BoardingPoint.Rows[0].Cells[0].ColumnSpan = columnsCount;
            GridView_BoardingPoint.Rows[0].Cells[1].Visible = false;
            GridView_BoardingPoint.Rows[0].Cells[2].Visible = false;
            GridView_BoardingPoint.Rows[0].Cells[3].Visible = false;
            
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Load grid to update boarding point detail
        /// </summary>
        public void LoadBoardingPointGrid(int tripID)
        {
            
            List<BoardingPointDetailModel> boardingPointDetail = new List<BoardingPointDetailModel>();
            boardingPointDetail=tripBL.SelectAllBoardingPoint(tripID);
            if (boardingPointDetail.Count > 0)
            {
                GridView_BoardingPoint.DataSource = boardingPointDetail;
                GridView_BoardingPoint.DataBind();
            }
            else
            {
                List<BoardingPointDetailModel> listBoardingPoint = new List<BoardingPointDetailModel>();
                BoardingPointDetailModel boardingPoint = new BoardingPointDetailModel();
                boardingPoint.PointName = "No Boarding Points Available";
                listBoardingPoint.Add(new BoardingPointDetailModel(boardingPoint));
                GridView_BoardingPoint.DataSource = listBoardingPoint;
                GridView_BoardingPoint.DataBind();
                GridView_BoardingPoint.Rows[0].Cells[1].Visible = false;
                GridView_BoardingPoint.Rows[0].Cells[2].Visible = false;
                GridView_BoardingPoint.Rows[0].Cells[3].Visible = false;
                int columnsCount = GridView_BoardingPoint.Columns.Count;
                GridView_BoardingPoint.Rows[0].Cells[0].ColumnSpan = columnsCount;
            }
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Update and delete boarding points
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView_BoardingPoint_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                
                TextBox tbBoardingPointName, tbBoardingPointAddress, tbBoardingPointTime;
                HtmlGenericControl lblBoardingPointName, lblBoardingPointAddress, lblBoardingPointTime;

                if (e.CommandName == "Insert")
                {
                    GridViewRow footerRow = GridView_BoardingPoint.FooterRow;

                    tbBoardingPointName = (TextBox)footerRow.FindControl("TextBox_BoardingPointName");
                    tbBoardingPointAddress = (TextBox)footerRow.FindControl("TextBox_BoardingPointAddress");
                    tbBoardingPointTime = (TextBox)footerRow.FindControl("TextBox_BoardingPointTime");

                    List<BoardingPointDetailModel> listBoardingPoint = new List<BoardingPointDetailModel>();
                    BoardingPointDetailModel newBoardingPoint = new BoardingPointDetailModel();
                    int gridRowCount = GridView_BoardingPoint.Rows.Count;
                    int rowIndex = 0;
                   
                    while (gridRowCount >= 1)
                    {
                        GridViewRow Itemrow = GridView_BoardingPoint.Rows[rowIndex];
                        BoardingPointDetailModel PreviousBoardingPoint = new BoardingPointDetailModel();
                        lblBoardingPointName = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointName");
                        lblBoardingPointAddress = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointAddress");
                        lblBoardingPointTime = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointTime");

                        PreviousBoardingPoint.PointName = lblBoardingPointName.InnerText.Trim();
                        PreviousBoardingPoint.PointAddress = lblBoardingPointAddress.InnerText.Trim();
                        PreviousBoardingPoint.Departure = lblBoardingPointTime.InnerText.Trim();
                        if (PreviousBoardingPoint.Departure.Length > 0 && PreviousBoardingPoint.PointAddress.Length>0)
                            listBoardingPoint.Add(new BoardingPointDetailModel(PreviousBoardingPoint));
                        gridRowCount--;
                        rowIndex++;
                    }

                    newBoardingPoint.PointName = tbBoardingPointName.Text.Trim();
                    newBoardingPoint.PointAddress = tbBoardingPointAddress.Text.Trim();
                    newBoardingPoint.Departure = tbBoardingPointTime.Text.Trim();
                    listBoardingPoint.Add(new BoardingPointDetailModel(newBoardingPoint));
                    GridView_BoardingPoint.DataSource = listBoardingPoint;
                    GridView_BoardingPoint.DataBind();
                                      
                }
                else if (e.CommandName == "Delete")
                {
                    List<BoardingPointDetailModel> listBoardingPoint = new List<BoardingPointDetailModel>();
                    BoardingPointDetailModel newBoardingPoint = new BoardingPointDetailModel();
                    int gridRowCount = GridView_BoardingPoint.Rows.Count;
                    if (gridRowCount == 1)
                    {
                         InitializeBoardingPointGrid();
                    }
                    else
                    {
                        int rowIndex = 0;
                        int argRowIndex = Convert.ToInt32(e.CommandArgument);
                        while (gridRowCount >= 1)
                        {
                            GridViewRow Itemrow = GridView_BoardingPoint.Rows[rowIndex];
                            BoardingPointDetailModel PreviousBoardingPoint = new BoardingPointDetailModel();
                            lblBoardingPointName = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointName");
                            lblBoardingPointAddress = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointAddress");
                            lblBoardingPointTime = (HtmlGenericControl)GridView_BoardingPoint.Rows[Itemrow.RowIndex].FindControl("lblSBoardingPointTime");

                            PreviousBoardingPoint.PointName = lblBoardingPointName.InnerText.Trim();
                            PreviousBoardingPoint.PointAddress = lblBoardingPointAddress.InnerText.Trim();
                            PreviousBoardingPoint.Departure = lblBoardingPointTime.InnerText.Trim();
                            if (argRowIndex != rowIndex)
                                listBoardingPoint.Add(new BoardingPointDetailModel(PreviousBoardingPoint));
                            gridRowCount--;
                            rowIndex++;
                        }
                        GridView_BoardingPoint.DataSource = listBoardingPoint;
                        GridView_BoardingPoint.DataBind();
                    }
                   
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void GridView_BoardingPoint_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridView_BoardingPoint.DataBind();
        }

        protected void GridView_BoardingPoint_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
             GridView_BoardingPoint.DataBind();
        }

    }
}