﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PassengerList.aspx.cs" Inherits="iBus.PassengerList" MasterPageFile="~/MasterPage/default.Master" %>

<asp:Content id="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="/Style/customer.css" rel="stylesheet" />
    <script src="Script/PassengerList.js"></script>
       <script src="DataTables/js/jquery.dataTables.min.js"></script>
    <link href="DataTables/css/demo_table.css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <br />
    <br />


<div class="col-lg-offset-2 col-md-8">
     <asp:Repeater ID="PassengerDetail" runat="server">

            <HeaderTemplate>
                <table border="1" class="table-background">
                    <tr>
                        <th><b>PassengerID</b></th>
                        <th><b>First Name</b></th>
                        <th><b>Last Name</b></th>
                        <th><b>Gender</b></th>
                        <th><b>Age</b></th>
                        <th><b>Seat No.</b></th>
                        <th><b>Select</b></th>
                    </tr>
            </HeaderTemplate>

            <ItemTemplate>
                <tr>                    
                    <td style="padding:20px"><%# Eval("PDID") %> </td>
                    <td style="padding:20px"><%# Eval("FirstName") %> </td>
                    <td style="padding:20px"><%# Eval("LastName") %> </td>
                    <td style="padding:20px"><%# Eval("Gender") %></td>
                    <td style="padding:20px"><%# Eval("Age") %></td>
                    <td style="padding:20px"><%# Eval("SeatID") %></td>
                    <td style="padding:20px"><asp:CheckBox runat="server" ID="chkbox_Cancel_Ticket" /></td>
                    <asp:HiddenField ID="hiddenPassengerID" Value ='<%# Eval("PDID") %>'  runat="server"/>
                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>

        </asp:Repeater>
    <br />
    <br />
    <br />
    <br />


     <asp:Button ID="Button_CancelReservation" runat="server" Text="Submit" CssClass="button-blue" ClientIDMode="Static"  OnClick="Button_CancelReservation_Click" />
    <asp:Button ID="Button_Cancel" runat="server" Text="Cancel" CssClass="button-blue" ClientIDMode="Static" OnClick="Button_Cancel_Click"  />
</div>

</asp:Content>
