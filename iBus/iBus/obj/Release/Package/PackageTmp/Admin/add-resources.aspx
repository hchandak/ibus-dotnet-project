﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-resources.aspx.cs" Inherits="iBus.Admin.AddResources" 
  MasterPageFile="~/MasterPage/admin.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Style/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="/Script/add-resources.js"></script>
    <script src="/Script/admin.js"></script>
    <script src="/Script/bus-search.js"></script>
    <script src="/Script/jquery-ui-1.10.3.custom.js"></script>
    <link href="/Style/admin.css" rel="stylesheet" />
    
    <script src="/Script/bootstrap-tooltip.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="container">
    <br />
    <div class="alert alert-success" id="Div_SucessMessageAdmin" ClientIDMode="Static" style="display:none"   runat="server"></div>
    <div class="alert alert-danger" id="Div_ErrorMessageAdmin" ClientIDMode="Static" style="display:none"  runat="server"></div>

    <div id="Div_AddTravel"  runat="server"  class="col-md-8 div_form_layout" style="display:none" >
         <div class="row">
            <div class="col-md-8" runat="server" id="Div_TravelHeader">
                <h4>Fill Travel Detail</h4>
            </div>
            <div class="col-md-2 "><br /><br />
                <asp:Button id="Button_GoToTravelDetail" Text="Go To Travel List" CssClass="button-blue" runat="server" OnClientClick="return RedirectToTravelDetail();" ></asp:Button>
            </div>
        </div><br />
         <div class="row"> 
             <div class="col-md-3">
                 <label id="lbl_TravelName"  runat="server">Travel name<sup class="required">*</sup></label>
             </div>
             <div class="col-md-8" >
                 <asp:TextBox type="text " CssClass="form-control medium_control"  id="TextBox_TravelName" runat="server" ClientIDMode="Static" placeholder="Name" /><br />
             </div>
         </div>
         
         <div class="row">
             <div class="col-md-3">
                 <label id="lbl_TravelAddress"  runat="server">Address<sup class="required">*</sup></label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" TextMode="MultiLine" Rows="4" CssClass="form-control medium_control " id="TextBox_TravelAddress" runat="server" ClientIDMode="Static" placeholder="Address" /> <br />
             </div>
         </div>
               
         <div class="row">
             <div class="col-md-3">
                  <label id="Label_TravelEmail"  runat="server">Travel email<sup class="required">*</sup></label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control medium_control " id="TextBox_TravelEmail" runat="server" ClientIDMode="Static" placeholder="Email" /><br /> 
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <label id="Label_TravelContact"  runat="server">Travel contact<sup class="required">*</sup></label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control medium_control " id="TextBox_TravelContact" runat="server" ClientIDMode="Static" placeholder="Contact" /><br /> 
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TravelIsActive" Text="IsActive?" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:CheckBox Checked="true"  ID="CheckBox_TravelIsActive" runat="server" ClientIDMode="Static" />
             </div>
         </div>
         <div class="col-md-3 col-md-offset-8">
            <asp:Button Text="Update Travel" ID="Button_UpdateTravelDetail" runat="server" OnClientClick="return ValidateTravelDetail();" OnClick="Button_UpdateTravelDetail_Click" ClientIDMode="Static" CssClass="button-blue"  />
            <asp:Button Text="Add Travel" ID="Button_SaveTravelDetail" runat="server" OnClientClick="return ValidateTravelDetail();" OnClick="Button_SaveTravelDetail_Click" ClientIDMode="Static" CssClass="button-blue" /><br /><br />
        </div>  
       
   </div>            

    <div id="Div_AddBus" runat="server"   class="col-md-8 div_form_layout" style="display:none" >
         <div class="row">
            <div class="col-md-8" runat="server" id="Div_BusHeader">
                <h4 >Fill Bus Detail</h4>
            </div>
            <div class="col-md-2"><br /><br />
                <asp:Button id="Button_GoToBusDetail" CssClass="button-blue" Text="Go To Bus List" runat="server" OnClientClick="return RedirectToBusDetail()" ></asp:Button>
            </div><br /><br />
        </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_BusType" Text="Bus type*" runat="server"></asp:Label>
             </div>
             <div class="col-md-8">
                 <asp:DropDownList ID="DropDownList_BusType" CssClass="medium_control " runat="server" ClientIDMode="Static" ></asp:DropDownList><br /><br />
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_BusCapacity" Text="Bus capacity*" runat="server"></asp:Label>
             </div>
             <div class="col-md-8">
                 <asp:TextBox type="text" CssClass="form-control small_control"  id="TextBox_BusCapacity" runat="server" ClientIDMode="Static" Text="40" Enabled="false"/><br />
             </div>
         </div>
         
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_BusTravelName" Text="Travel name*" runat="server"></asp:Label>
             </div>
             <div class="col-md-8">
                 <asp:DropDownList ID="DropDownList_BusTravelName"  CssClass="medium_control " runat="server" ClientIDMode="Static" ></asp:DropDownList><br /><br />
             </div>
         </div>

         <div class="row" runat="server" id="Div_Amenities">
             
             <div class="col-md-3">
                 <asp:Label ID="Label_Amenities" Text="Amenities" runat="server"></asp:Label>
             </div>
             <div class="col-md-9">
             <div class="col-md-6">
                 <asp:CheckBox Checked="true" ID="CheckBox_WaterBottle"  runat="server" ClientIDMode="Static" />
                 <asp:Label ID="Label_WaterBottle" Text="WaterBottle" runat="server"></asp:Label>
             </div>
             <div class="col-md-6">
                 <asp:CheckBox  ID="CheckBox_Movie"  runat="server" ClientIDMode="Static" />
                 <asp:Label ID="Label_Movie" Text="Movie" runat="server"></asp:Label>
             </div>
             <div class="col-md-6">
                 <asp:CheckBox  ID="CheckBox_Blanket"  runat="server" ClientIDMode="Static" />
                 <asp:Label ID="Label_Blanket" Text="Blanket" runat="server"></asp:Label>
             </div>
             <div class="col-md-6">
                 <asp:CheckBox  ID="CheckBox_CharginPoint"  runat="server" ClientIDMode="Static" />
                 <asp:Label ID="Label_CharginPoint" Text="CharginPoint" runat="server"></asp:Label>
             </div>
             <div class="col-md-6">
                 <asp:CheckBox   ID="CheckBox_ReadingLight"  runat="server" ClientIDMode="Static" />
                 <asp:Label ID="Label_ReadingLight" Text="ReadingLight" runat="server"></asp:Label>
             </div>
            </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_BusIsActive" Text="IsActive?" runat="server"></asp:Label>
             </div>
             <div class="col-md-4 ">
                 <asp:CheckBox Checked="true"  ID="CheckBox_BusIsActive" runat="server" ClientIDMode="Static" /><br /><br />
             </div>
         </div>
        <div class="col-md-3 col-md-offset-8">
           <asp:Button Text="Update Bus" ID="Button_UpdateBusDetail" runat="server" OnClientClick="return ValidateBusDetail();" OnClick="Button_UpdateBusDetail_Click" ClientIDMode="Static" CssClass="button-blue" />
           <asp:Button Text="Add Bus" ID="Button_SaveBusDetail" runat="server" OnClientClick="return ValidateBusDetail();" OnClick="Button_SaveBusDetail_Click" ClientIDMode="Static" CssClass="button-blue"/><br /><br />
        </div>
    </div>

    <div id="Div_AddRoute" runat="server"   class="col-md-8 div_form_layout" style="display:none" >
         <div class="row">
            <div class="col-md-8" runat="server" id="Div_RouteHeader">
                <h4 >Fill Route Detail</h4>
            </div>
            <div class="col-md-2"><br /><br />
                <asp:Button id="Button_GoToRouteDetail" CssClass="button-blue" Text="Go To Route List" runat="server" OnClientClick="return RedirectToRouteDetail()" ></asp:Button>
            </div>
        </div><br />
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_RouteFromCity" Text="From city*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:TextBox ID="TextBox_FromCity"  CssClass="control medium_control" placeholder="From city" runat="server" ClientIDMode="Static" ></asp:TextBox><br /><br />
             </div>
         </div>
                  
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_RouteToCity" Text="To city*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:TextBox ID="TextBox_ToCity"  CssClass="control medium_control" placeholder="To city" runat="server" ClientIDMode="Static" ></asp:TextBox><br /><br />
             </div>
         </div>
         
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_RouteIsActive" Text="IsActive?" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:CheckBox Checked="true"  ID="CheckBox_RouteIsActive" runat="server" ClientIDMode="Static" /><br /><br />
             </div>
         </div>
        <div class="col-md-3 col-md-offset-8">
           <asp:Button Text="Update Route" ID="Button_UpdateRouteDetail" runat="server" OnClick="Button_UpdateRouteDetail_Click" ClientIDMode="Static" CssClass="button-blue" />
           <asp:Button Text="Add Route" ID="Button_SaveRouteDetail" runat="server" OnClientClick="return ValidateAll('routeSearch')" OnClick="Button_SaveRouteDetail_Click" ClientIDMode="Static" CssClass="button-blue"/><br /><br />
        </div>
    </div>

    <div id="Div_AddTrip" runat="server"   class="col-md-11 div_form_layout" style="display:none" >
         <div class="row">
            <div class="col-md-8" runat="server" id="Div_TripHeader">
                <h4 >Fill Trip Detail</h4>
            </div>
            <div class="col-md-2 text-right"><br /><br />
                <asp:Button id="Button_GoToTripDetail" CssClass="button-blue"  Text="Go To Trip List" runat="server" OnClientClick="return RedirectToTripDetail()"></asp:Button>
            </div>
        </div><br />
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TripTravelName" Text="Travel name*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4 ">
                 <asp:DropDownList ID="DropDownList_TripTravelName"  CssClass="medium_control " OnSelectedIndexChanged="DropDownList_TripTravelName_SelectedIndexChanged" runat="server" ClientIDMode="Static" AutoPostBack="true" ></asp:DropDownList><br />
             </div>
         </div>
         
         <div class="row" >
             <asp:ScriptManagerProxy ID="ScriptManagerProxy_BusTypeAndAmenites" runat="server"></asp:ScriptManagerProxy>
             <asp:updatepanel ID="Updatepanel_Div_BusTypeAndAmenites" runat="server">
                <ContentTemplate>
                       <div class="col-md-3">
                              <asp:Label ID="Label_TripBusType"  runat="server"></asp:Label>
                       </div>
                       <div class="col-md-9 " runat="server">
                          <asp:Label runat="server" id="Label_BusTypeAndAmenities" ></asp:Label><br />
                         
                          <asp:GridView ID="Grid_BusTypeAndAmenitiesDetail" ClientIDMode="Static"  AutoGenerateColumns="False" CssClass="table table-bordered " PageSize="6"
                              AlternatingRowStyle-BackColor = "#C2D69B"  runat="server"  AllowPaging="true" OnPageIndexChanging="Grid_BusTypeAndAmenitiesDetail_PageIndexChanging">
                           <Columns>   
                                <asp:TemplateField   HeaderText="Select">
                                    <ItemStyle HorizontalAlign="Center"  CssClass="tiny_control"></ItemStyle>
                                    <ItemTemplate>
                                        <input name="radioBusTypeAndAmenities" type="radio"  value='<%# Eval("BusID") %>' />
                                        <%--<asp:RadioButton ID="RadioButton_BusTypeAndAmenities" runat="server" ClientIDMode="Static" />--%>     
                                    </ItemTemplate> 
                                </asp:TemplateField>
                                <asp:TemplateField   HeaderText="Bus Type">
                                    <ItemStyle   CssClass="medium_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblTripBusType" runat="server"  ><%# Eval("BusType") %></label>     
                                    </ItemTemplate> 
                                </asp:TemplateField>
                                <asp:TemplateField    HeaderText="Amenities">
                                    <ItemStyle  CssClass="extra_large_control"></ItemStyle>
                                    <ItemTemplate >
                                    <label id="lblTripAmenities" runat="server"   ><%# Eval("Amenities") %></label>    
                                </ItemTemplate>
                               </asp:TemplateField>
                          </Columns>
                         <PagerStyle HorizontalAlign="Left"  BackColor="#507CD1"  ></PagerStyle>
                          <%--<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />--%>
                          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />
                     </asp:GridView>
                    <div id="Div_BusTypeAndAmenitiesDetailSummary" runat="server" ></div><br />
                   
                   </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="DropDownList_TripTravelName" EventName="SelectedIndexChanged" />
                </Triggers>
             </asp:updatepanel>
             
         </div>
         
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TripRoute" Text="Route*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:DropDownList ID="DropDownList_TripRoute"  CssClass="large_control " runat="server" ClientIDMode="Static" ></asp:DropDownList><br /><br />
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TripFare" Text="Fare*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:TextBox type="text" CssClass="form-control small_control "  id="TextBox_TripFare" runat="server" ClientIDMode="Static" placeholder='Fare' /><br />
             </div>
         </div>
        <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_Frequency" Text="Frequency*" runat="server"></asp:Label>
             </div>
             <div class="col-md-4 ">
                 <asp:DropDownList ID="DropDownList_Frequency"  CssClass="medium_control "  runat="server" ClientIDMode="Static" ></asp:DropDownList><br /><br />
             </div>
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TripDepartureTime" Text="Departure time*" runat="server"></asp:Label>
             </div>
             <div class="col-md-1">
                <asp:DropDownList ID="DropDownList_TripDepartureHour"  CssClass="small_control " runat="server" ClientIDMode="Static" ></asp:DropDownList><br />
             </div>
             <div class="col-md-1">
                <asp:DropDownList ID="DropDownList_TripDepartureMinut"  CssClass="small_control " runat="server" ClientIDMode="Static" ></asp:DropDownList><br /><br />
             </div>
             
         </div>
        
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_ArrivalTime" Text="Arrival time*" runat="server"></asp:Label>
                 
             </div>
             <div class="col-md-1">
                <asp:DropDownList ID="DropDownList_TripArrivalHoure"  CssClass="small_control " runat="server" ClientIDMode="Static" ></asp:DropDownList><br />
             </div>
             <div class="col-md-1">
                <asp:DropDownList ID="DropDownList_TripArrivalMinut"  CssClass="small_control " runat="server" ClientIDMode="Static" ></asp:DropDownList><br /><br />
             </div>
             
         </div>
         <div class="row">
             <div class="col-md-3">
                 <asp:Label ID="Label_TripIsActive" Text="IsActive?" runat="server"></asp:Label>
             </div>
             <div class="col-md-4">
                 <asp:CheckBox Checked="true"  ID="CheckBox_TripIsActive" runat="server" ClientIDMode="Static" />
             </div>
         </div>
        <div class="col-md-3 col-md-offset-8">
           <asp:Button Text="Update Trip" ID="Button_UpdateTripDetail" runat="server" OnClientClick="return ValidateTripDetailForUpdate();" OnClick="Button_UpdateTripDetail_Click" ClientIDMode="Static" CssClass="button-blue" />
           <asp:Button Text="Add Trip" ID="Button_SaveTripDetail" runat="server" OnClientClick="return ValidateTripDetailForSave();" OnClick="Button_SaveTripDetail_Click" ClientIDMode="Static" CssClass="button-blue" /><br /><br />
        </div>
    </div>

    <asp:HiddenField ID="HiddenField_Cities" ClientIDMode="Static" runat="server" />      
</div>   <br /><br />
</asp:Content>

