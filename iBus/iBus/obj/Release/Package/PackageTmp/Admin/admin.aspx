﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="iBus.Admin.Admin"
    MasterPageFile="~/MasterPage/admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/Script/admin.js"></script>
    <link href="/Style/admin.css" rel="stylesheet" />
    <script src="/Script/js-image-slider.js"></script>
    <script src="/Script/js-image-slider.js"></script>
    <link href="/Style/js-image-slider.css" rel="stylesheet" />
    <script src="/Script/jquery.tablesorter.min.js"></script>
    <%--<link href="/Style/common.css" rel="stylesheet" />--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container " >
        
        <div class="alert alert-success" id="Div_SucessMessage" clientidmode="Static" style="display: none" runat="server"></div>
        <div class="alert alert-danger" id="Div_ErrorMessage" clientidmode="Static" style="display: none" runat="server"></div>
        
        <div class="row">
            <div class="col-md-12">

                <div class="row" id="Div_AdminMyAccount" runat="server">
                    <div class="col-md-3" runat="server" id="Div_AdminDetail"></div>
                    <div class="col-md-9">
                        <iBus:Control_AdvertisementTravel runat="server" ID="Control_AdvertisementTravel" />
                        
                    </div>
                </div>

                <div id="Div_TravelDetail" runat="server">
                    <div class="row">
                        <div class="col-md-10" runat="server" id="Div_TravelListHeader">
                            <h4 class="-h4">Travels List :</h4>
                        </div>
                        <div class="col-md-2">
                            <asp:Button ID="Button_AddNewTravel" CssClass="button-blue" Text="Add New Travel" runat="server" OnClick="Button_AddNewTravel_Click"></asp:Button>
                        </div>
                    </div>

                    <br />
                    <asp:updatepanel ID="Updatepanel_TravelDetail" runat="server">
                     <ContentTemplate>
                        <asp:GridView ID="Grid_TravelDetail" ClientIDMode="Static" CssClass="table table-bordered " OnRowCommand="Grid_TravelDetailRowCommand" 
                            OnRowUpdating="Grid_TravelDetailRowUpdating"  AllowPaging="true" OnPageIndexChanging="Grid_TravelDetail_PageIndexChanging" PageSize="6"
                            OnRowDeleting="Grid_TravelDetailRowDeleting" AutoGenerateColumns="False" runat="server" AlternatingRowStyle-BackColor = "#C2D69B">
                            <Columns>

                                <asp:TemplateField HeaderText="Travels Name">
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblTravelID" runat="server" style="display: none"><%# Eval("TravelID") %></label>
                                        <label id="lblTravelName" runat="server"><%# Eval("TravelName") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Travels Address">
                                    <ItemStyle CssClass="medium_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblTravelAddress"><%# Eval("ContactAddress") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Travels Email">
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblTravelEmail"><%# Eval("TravelEmail") %></label>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Travel Contact">
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblTravelContact"><%# Eval("ContactNo") %></label>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Option">
                                    <ItemStyle CssClass="tiny_control"></ItemStyle>
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-md-5 col-md-offset-1">

                                                <asp:LinkButton ID="Link_UpdateTravel"  runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Update"><img style="height:25px;width:25px" src="/Image/update.ico" title="Update travel"/></asp:LinkButton>
                                            </div>
                                            <div class="col-md-5 ">
                                                <asp:LinkButton ID='Link_DeleteTravel'  runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Delete" ><img src="/Image/delete.png" title="Delete travel" /></asp:LinkButton>
                                            </div>
                                        </div>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>
                            <%--<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  CssClass="grid_fix_footer"/>--%>
                            <PagerStyle HorizontalAlign="Left"  BackColor="#507CD1"   ></PagerStyle>
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />

                        </asp:GridView>
                        <div id="Div_TravelListSummary" runat="server" ></div>
                     </ContentTemplate>
                  </asp:updatepanel>
                </div>

                <div id="Div_RouteDetail"  runat="server">
                    <div class="row">
                        <div class="col-md-10" runat="server" id="Div_RouteListHeader">
                            <h4 class="-h4">Route List :</h4>
                        </div>
                        <div class="col-md-2 ">
                            <asp:Button ID="Button_AddNewRoute" CssClass="button-blue" Text="Add New Route" runat="server" OnClick="Button_AddNewRoute_Click"></asp:Button>
                        </div>
                    </div>
                    <br />
                  <%-- <asp:ScriptManagerProxy ID="ScriptManagerProxy_RouteDetail" runat="server"></asp:ScriptManagerProxy>--%>
                   <asp:updatepanel ID="Updatepanel_Div_RouteDetail" runat="server">
                     <ContentTemplate>
                       <asp:GridView ID="Grid_RouteDetail" ClientIDMode="Static" CssClass="table table-bordered"  OnRowCommand="Grid_RouteDetail_RowCommand" OnRowUpdating="Grid_RouteDetail_RowUpdating" PageSize="6" 
                            OnRowDeleting="Grid_RouteDetail_RowDeleting" AutoGenerateColumns="False" runat="server" AlternatingRowStyle-BackColor = "#C2D69B" AllowPaging="true" OnPageIndexChanging="Grid_RouteDetail_PageIndexChanging" >
                            <Columns>

                                <asp:TemplateField HeaderText="From City">
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblRouteID" runat="server" style="display: none"><%# Eval("RouteID") %></label>
                                        <label id="lblRouteRouteFromCity" runat="server"><%# Eval("FromCity") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="To City" >
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblRouteToCity"><%# Eval("ToCity") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IsActive" >
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblRouteCreatedBy"><%# Eval("IsActive") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Option" >
                                    <ItemStyle CssClass="tiny_control"></ItemStyle>
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-md-5 col-md-offset-1">

                                                <asp:LinkButton ID="Link_UpdateRoute"  runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Update"><img style="height:25px;width:25px" src="/Image/update.ico" title="Update route"/></asp:LinkButton>
                                            </div>
                                            <div class="col-md-5 ">
                                                <asp:LinkButton ID='Link_DeleteRoute'  runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Delete"><img src="/Image/delete.png" title="Delete route" /></asp:LinkButton>
                                            </div>
                                        </div>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>
                            <%--<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />--%>
                            <PagerStyle HorizontalAlign="Left"  BackColor="#507CD1"  ></PagerStyle>
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />
                        </asp:GridView>
                       <div id="Div_RouteListSummary" runat="server" ></div>
                    </ContentTemplate>
                  </asp:updatepanel>
                </div>
                
                <div id="Div_BusDetail" runat="server">
                    <div class="row">
                        <div class="col-md-10 " runat="server" id="Div_BusListHeader">
                            <h4 class="-h4">Bus List :</h4>
                        </div>
                        <div class="col-md-2 ">
                            <asp:Button ID="Button_AddNewBus" CssClass="button-blue" Text="Add New Bus" runat="server" OnClick="Button_AddNewBus_Click"></asp:Button>
                        </div>
                    </div>
                    <br />
                    <asp:updatepanel ID="Updatepanel_BusDetail" runat="server">
                     <ContentTemplate>
                        <asp:GridView ID="Grid_BusDetail" ClientIDMode="Static" CssClass="table table-bordered " OnRowCommand="Grid_BusDetail_RowCommand" PageSize="6"
                            OnRowUpdating="Grid_BusDetail_RowUpdating"   AllowPaging="true" OnPageIndexChanging="Grid_BusDetail_PageIndexChanging"
                            OnRowDeleting="Grid_BusDetail_RowDeleting" AutoGenerateColumns="False" runat="server" AlternatingRowStyle-BackColor = "#C2D69B">
                            <Columns>

                                <asp:TemplateField HeaderText="Travel Name">
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblBusID" runat="server" style="display: none"><%# Eval("BusID") %></label>
                                        <label id="lblBusTravelName" runat="server"><%# Eval("TravelName") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Bus Type">
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblBusType"><%# Eval("BusType") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Capacity">
                                    <ItemStyle CssClass="tiny_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblBusCapacity"><%# Eval("Capacity") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amenities">
                                    <ItemStyle CssClass="large_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblBusCreatedBy"><%# Eval("Amenities") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Option">
                                    <ItemStyle CssClass="tiny_control"></ItemStyle>
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-md-5 col-md-offset-1">

                                                <asp:LinkButton ID="Link_UpdateBus"  runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Update"><img style="height:25px;width:25px" src="/Image/update.ico" title="Update bus"/></asp:LinkButton>
                                            </div>
                                            <div class="col-md-5 ">
                                                <asp:LinkButton ID='Link_DeleteBus'  runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Delete"><img src="/Image/delete.png" title="Delete bus" /></asp:LinkButton>
                                            </div>
                                        </div>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>
                            <%--<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  CssClass="grid_fix_footer"/>--%>
                            <PagerStyle HorizontalAlign="Left"  BackColor="#507CD1" ></PagerStyle>
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />
                        </asp:GridView>
                        <div id="Div_BusListSummary" runat="server" ></div>
                      </ContentTemplate>
                  
                  </asp:updatepanel>
                </div>
               
                <div id="Div_TripDetail" runat="server">
                    <div class="row">
                        <div class="col-md-10" runat="server" id="Div_TripListHeader">
                            <h4 class="-h4">Trip List :</h4>
                        </div>
                        <div class="col-md-2 ">
                            <asp:Button ID="Button_AddNewTrip" CssClass="button-blue" Text="Add New Trip" runat="server" OnClick="Button_AddNewTrip_Click"></asp:Button>
                        </div>
                    </div>
                    <br />
                    <asp:updatepanel ID="Updatepanel1" runat="server">
                     <ContentTemplate>
                        <asp:GridView ID="Grid_TripDetail" ClientIDMode="Static" CssClass="table table-bordered " OnRowCommand="Grid_TripDetail_RowCommand" 
                            OnRowUpdating="Grid_TripDetail_RowUpdating"   AllowPaging="true" OnPageIndexChanging="Grid_TripDetail_PageIndexChanging" PageSize="6"
                            OnRowDeleting="Grid_TripDetail_RowDeleting" AutoGenerateColumns="False" runat="server" AlternatingRowStyle-BackColor = "#C2D69B">
                            <Columns>

                                <asp:TemplateField HeaderText="Travel Name">
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblTripID" runat="server" style="display: none"><%# Eval("TripID") %></label>
                                        <label id="lblTripTravelName" runat="server"><%# Eval("TravelName") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Bus Type">
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblTripBusType"><%# Eval("BusType") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Route ">
                                    <ItemStyle CssClass="medium_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblTripRoutFromToCity" runat="server"><%# Eval("Route") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Departure Time">
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblBusTripDepartureTime"><%# Eval("DepartureTime") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Arrival Time">
                                    <ItemStyle CssClass="small_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblBusTripArrivalTime"><%# Eval("ArrivalTime") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Fare">
                                    <ItemStyle CssClass="tiny_control"></ItemStyle>
                                    <ItemTemplate>
                                        <label id="lblBusTripFare"><%# Eval("Fare") %></label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Option">
                                    <ItemStyle CssClass="tiny_control"></ItemStyle>
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-md-5 col-md-offset-1">

                                                <asp:LinkButton ID="Link_UpdateTrip"  runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Update"><img style="height:25px;width:25px" src="/Image/update.ico" title="Update trip"/></asp:LinkButton>
                                            </div>
                                            <div class="col-md-5 ">
                                                <asp:LinkButton ID='Link_DeleteTrip'  runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Delete"><img src="/Image/delete.png" title="Delete trip" /></asp:LinkButton>
                                            </div>
                                        </div>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>
                            <%--<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  CssClass="grid_fix_footer"/>--%>
                            <PagerStyle HorizontalAlign="Left"  BackColor="#507CD1"  ></PagerStyle>
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />
                        </asp:GridView>
                        <div id="Div_TripListSummary" runat="server" ></div>
                         </ContentTemplate>
                  </asp:updatepanel>
                     
                </div>

            </div>
                
              
            <%--<div class="col-md-1">
              <%--<iBus:Control_AdvertisementBusFacility runat="server" ID="Control_AdvertisementBusFacility" />--%>
              <%--<iBus:Control_AdvertisementRoute runat="server" ID="Control_AdvertisementRoute" />
            </div>--%>
        </div>
    </div><br /><br />
</asp:Content>


