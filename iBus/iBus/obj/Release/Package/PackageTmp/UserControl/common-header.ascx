﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="common-header.ascx.cs" Inherits="iBus.UserControl.CommonHeader" %>

<nav class="navbar " role="navigation" runat="server" >
          <div class="collapse navbar-collapse navbar-ex1-collapse div-banner">
               <ul class="nav navbar-nav">
                    <li ><img src="/Image/ibus-logo.png" /></li>
               </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li class="text-right" style="margin-top:10px"><asp:Label runat="server" ID="Label_UserEmail" ></asp:Label></li>
              </ul>
              
          </div>
           
          <div class="div-header" runat="server" id="Div_DefaultHeader" style="display:none">
               <ul id="headerList" class="nav navbar-nav">
                    <li class="active"><a href="../ibus.aspx">Home</a></li>
                    <li><a href="#" >About Us</a></li>
                    <li><a href="#">Contact</a></li>
                    
               </ul>
               <ul class="nav navbar-nav navbar-right">
                    <li><a href="#Modal_SignInSignUp" onclick="ShowModalSignInSignUp();" data-toggle="modal">SignIn/SignUp</a></li>
                    
               </ul>
          </div>

          <div class=" div-header" runat="server" id="Div_CustomerHeader" style="display:none">
               <ul class="nav navbar-nav ">
                   <li ><a href="../ibus.aspx" >Home</a></li> 
                   <li ><a href="../profile-page.aspx" >My Account</a></li>
                    
                    <li><a href="#" >About Us</a></li>
                    <li><a href="#" >Contact</a></li>
                    
                </ul>
               <ul class="nav navbar-nav navbar-right">
                    <li><asp:LinkButton runat="server" ID="LinkButton_CustomerSignOut" OnClick="LinkButton_CustomerSignOut_Click" >SignOut</asp:LinkButton></li>
           
               </ul>
          </div>
 </nav><!-- Nav End -->
