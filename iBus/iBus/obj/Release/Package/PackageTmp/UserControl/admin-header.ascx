﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="admin-header.ascx.cs" Inherits="iBus.UserControl.AdminHeader" %>

<nav class="navbar div-banner" role="navigation">
          <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav">
                    <li ><img src="/Image/ibus-logo.png" /></li> 
               </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li class="text-right" style="margin-top:10px"><a class="a-color" style="cursor:pointer" onclick="RedirectToAdminAccount()"><asp:Label runat="server" ID="Label_TravelEmail" ></asp:Label></a></li>
              </ul>
              
          </div>
 
          <div class="collapse navbar-collapse navbar-ex1-collapse div-header">
               <ul class="nav navbar-nav ">
                    <li ><a href="../Admin/admin.aspx">My Account</a></li>
                    <li ><a  style="cursor:pointer"  href="../Admin/admin.aspx?resource=travel">Travels</a></li>
                    <li ><a style="cursor:pointer" href="../Admin/admin.aspx?resource=route" >Routes</a></li>
                    <li ><a style="cursor:pointer" href="../Admin/admin.aspx?resource=bus">Bus</a></li>
                    <li ><a style="cursor:pointer" href="../Admin/admin.aspx?resource=trip">Trips</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
               <ul class="nav navbar-nav navbar-right">
                    
                    <li><asp:LinkButton runat="server" ID="LinkButton_AdminSignOut" OnClick="LinkButton_AdminSignOut_Click" >SignOut</asp:LinkButton></li>
               </ul>
          </div>
 </nav><!-- Nav End -->
