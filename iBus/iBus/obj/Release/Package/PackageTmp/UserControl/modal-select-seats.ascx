﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="modal-select-seats.ascx.cs" Inherits="iBus.UserControl.modal_select_seats" %>
<div class="modal fade" id="Modal_SelectBusSeat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  div-select-seat">
        <div class="modal-content ">
            <div class="modal-header">
               
                <link href="../Style/seat-layout.css" rel="stylesheet" />
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2>Select seat(s)</h2>
            
            </div>
            <div class="modal-body ">
                <div class="container">
                        
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav navbar-nav ">
                            <li>
                                <asp:Label ID="Label_FromToCity" runat="server" Text=""></asp:Label>

                            </li>
                            <li style="margin-left: 5px; margin-right: 5px;">|
                            </li>
                            <li>
                                <asp:Label ID="Label_DateOfJourney" runat="server" Text=""></asp:Label>
                            </li>
                            <li style="margin-left: 5px; margin-right: 5px;">|
                            </li>
                            <li>
                                <asp:Label ID="Label_BusOperator" runat="server" Text=""></asp:Label>
                            </li>
                            <li style="margin-left: 5px; margin-right: 5px;">|
                            </li>
                            <li>
                                <asp:Label ID="Label_BusType" runat="server" Text=""></asp:Label>

                            </li>
                            <li style="margin-left: 5px; margin-right: 5px;">|
                            </li>
                            <li>
                                <asp:Label ID="Label_DepartureTime" runat="server" Text=""></asp:Label>
                            </li>
                        </ul>
                        </div>
                        
                        <asp:HiddenField ID="HiddenField_Fare" runat="server" ClientIDMode="Static"/>
                    </div>

                    <br />

                    <div id="holder" class="row">
                        <div  class="col-md-8">
                           <%-- <div id="holder">--%>
                                <ul id="place">
                                </ul>
                            <%--</div>--%>
                        </div>
                        <div class="col-md-4" >
                            <div style="float: left; margin-top:30px;">
                            <ul id="seatDescription">
                                <li style="background: url('../Image/available_seat_img.gif') no-repeat;">Available Seat</li>
                                <li style="background: url('../Image/booked_seat_img.gif') no-repeat scroll 0 0 transparent;">Booked Seat</li>
                                <li style="background: url('../Image/selected_seat_img.gif') no-repeat scroll 0 0 transparent;">Selected Seat</li>
                            </ul>
                        </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            
                            <asp:Label ID="Label_SelectedSeatsTitle" runat="server" Text="Selected seat(s) : "></asp:Label>
                            <asp:HiddenField ID="HiddenField_SeatsSelected" runat="server" ClientIDMode="Static" />
                        
                            <asp:Label ID="Label_SelectedSeats" runat="server"  ClientIDMode="Static"></asp:Label>
                       
                        </div>
                        <div class="col-md-6">
                            <asp:Label ID="Label_TotalFareTitle" runat="server" Text="Total Fare : "></asp:Label>
                            <asp:Label ID="Label_TotalFare" runat="server" Text="0" ClientIDMode="Static"></asp:Label>
                       
                        </div>
                        
                    </div>

                    <div class="row">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="Button_Continue" runat="server" Text="Continue >>" OnClientClick="return saveSeatsToHiddenField();" OnClick ="Button_Continue_Click" CssClass="button-blue" ClientIDMode="Static"/>
               <%-- <asp:Button ID="Button_Continue" runat="server" Text="Continue >>" CssClass="btn-primary" OnClientClick="return saveSeatsToHiddenField();"  ClientIDMode="Static" Enabled="False" />
               --%>
                
            </div>

        </div>
    </div>
</div>
