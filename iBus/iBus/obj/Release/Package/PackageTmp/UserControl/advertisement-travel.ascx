﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="advertisement-travel.ascx.cs" Inherits="iBus.UserControl.advertisement_travel" %>
<div id="sliderFrame">
    <div id="slider">
       <a class="lazyImage" href="/Image/Travels/bus5.jpg" title="Super deluxe semi sleeper bus">Super deluxe semi sleeper bus</a>
       <a class="lazyImage " href="/Image/Travels/bus6.jpg" title="Super deluxe semi sleeper bus">Super deluxe semi sleeper bus</a>
       <a class="lazyImage " href="/Image/Travels/bus7.jpg" title="Super deluxe semi sleeper bus">Super deluxe semi sleeper bus</a>
       <%--<a class="lazyImage " href="/Image/Travels/ksrtc1.png" title="KSRTC semi sleeper bus">KSRTC semi sleeper bus</a>--%>
       <a class="lazyImage " href="/Image/Travels/neeta1.jpg" title="NEETA Travels semi sleeper bus">NEETA Travels semi sleeper bus</a>
       <a class="lazyImage " href="/Image/Travels/srs1.jpg" title="SRS Travels semi sleeper bus">SRS Travels semi sleeper bus</a>
       <a class="lazyImage " href="/Image/Travels/srs2.jpg" title="SRS Travels sleeper bus" >SRS Travels sleeper bus</a>
       <a class="lazyImage " href="/Image/Travels/starbus1.jpg" title="Starbus ac semi sleeper bus" >Starbus ac semi sleeper bus</a>
     </div>
     <!--thumbnails-->
     <div id="thumbs">
         <div class="thumb"><img src="/Image/Travels/bus5.jpg"  /></div>
         <div class="thumb"><img src="/Image/Travels/bus6.jpg" /></div>
         <div class="thumb"><img src="/Image/Travels/bus7.jpg" /></div>
         <%--<div class="thumb"><img src="/Image/Travels/ksrtc1.png" /></div>--%>
         <div class="thumb"><img src="/Image/Travels/neeta1.jpg" /></div>
         <div class="thumb"><img src="/Image/Travels/srs1.jpg" /></div>
         <div class="thumb"><img src="/Image/Travels/srs2.jpg" /></div>
         <div class="thumb"><img src="/Image/Travels/starbus1.jpg" /></div>
     </div>
 </div>