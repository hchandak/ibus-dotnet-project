﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="customer-header.ascx.cs" Inherits="iBus.UserControl.CustomerHeader" %>
<nav class="navbar navbar-inverse" role="navigation">
          <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav">
                    <li class="active"><a href="#">My Account</a></li>
                    <li ><a href="#">Profile</a></li>
                    <li><a href="#">History</a></li>
                    
               </ul>
               <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" data-toggle="modal">SignOut</a></li>
                  
               </ul>
          </div>
 </nav><!-- Nav End -->