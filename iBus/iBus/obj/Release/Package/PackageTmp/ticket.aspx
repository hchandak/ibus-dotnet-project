﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ticket.aspx.cs" Inherits="iBus.ticket"
    MasterPageFile="~/MasterPage/default.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/Script/admin.js"></script>
    <link href="Style/customer.css" rel="stylesheet" />
    <script src="Script/customer.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="alert alert-success" id="Div_SucessMessage" clientidmode="Static" runat="server" visible="false"></div>
    <div runat="server" id="Div_TicketDetail"></div>

    <asp:Panel ID="Panel_Ticket" runat="server">
        <h1>i-Bus</h1>
        Online Booking System.
         
        <br />
        <br />
        <asp:Label ID="lbl_Payment_Note"><b>Note: Payment has to be done at Boarding point.</b></asp:Label>
        <table id="table_ticket">

            <tr>
                <td colspan="2" style="background-color: #18B5F0; height: 18px; color: White; border: 1px solid white">
                    <b>Ticket Details</b>
                </td>
            </tr>
            <tr>
                <td><b>PNR No.:</b></td>
              
                <td>
                    <asp:Label ID="lblPNR" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Trip Code:</b></td>
                <td>
                    <asp:Label ID="lblTripCode" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><b>No. of Seats:</b></td>
                <td>
                    <asp:Label ID="lblNoofSeats" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Passenger Start Palce:</b></td>
                <td>
                    <asp:Label ID="lblStartPlace" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Departure Time:</b></td>
                <td>
                    <asp:Label ID="lblDepartureTime" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Passenger End Palce:</b></td>
                <td>
                    <asp:Label ID="lblEndPlace" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Date of Reservation:</b></td>
                <td>
                    <asp:Label ID="lblDateOfReservation" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Date of Journey:</b></td>
                <td>
                    <asp:Label ID="lblDateOfJourney" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Boarding Point:</b></td>
                <td>
                    <asp:Label ID="lblBoardingPoint" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Total Fare:</b></td>
                <td>
                    <asp:Label ID="lblTotalFare" runat="server"></asp:Label></td>
            </tr>
        </table>

        <br />
        <br />

        <asp:Repeater ID="PassengerDetail" runat="server">

            <HeaderTemplate>
                <table border="1">
                    <tr>
                        <th><b>First Name</b></th>
                        <th><b>Last Name</b></th>
                        <th><b>Gender</b></th>
                        <th><b>Age</b></th>
                        <th><b>Seat No.</b></th>
                    </tr>
            </HeaderTemplate>

            <ItemTemplate>
                <tr>
                    <td><%# Eval("FirstName") %> </td>
                    <td><%# Eval("LastName") %> </td>
                    <td><%# Eval("Gender") %></td>
                    <td><%# Eval("Age") %></td>
                    <td><%# Eval("SeatID") %></td>
                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>

        </asp:Repeater>

        <br />
        <br />

        <p style="color: black"><b>Terms and Conditions:</b></p>
        <br />
        <p style="color: black">
            1.iBus is ONLY a bus ticket agent. It does not operate bus services of its own. In order to provide a comprehensive choice of bus operators, departure times and prices to customers, it has tied up with many bus operators.
            iBus advice to customers is to choose bus operators they are aware of and whose service they are comfortable with.
        </p>
        <p style="color: black">
            2.Passengers are required to furnish the following at the time of boarding the bus:<br />
            (1) A copy of the ticket (A print out of the ticket or the print out of the ticket e-mail).<br />
            (2) A valid identity proof(eg.Driving Licence or PAN Card)<br />
            Failing to do so, they may not be allowed to board the bus.
        </p>

    </asp:Panel>
    <br />
    <br />
    <br />
    <asp:Button ID="Button_GenerateTicketviaEmail" runat="server" Text="Email Ticket" CssClass="button-blue" ClientIDMode="Static" OnClick="Button_GenerateTicketviaEmail_Click" />
    <asp:Button ID="Button_Download" runat="server" Text="Download Ticket" CssClass="button-blue" ClientIDMode="Static" OnClick="Button_Download_Click" />

</asp:Content>
