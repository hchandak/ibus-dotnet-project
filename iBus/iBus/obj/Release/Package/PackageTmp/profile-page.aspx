﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="profile-page.aspx.cs" Inherits="iBus.profile_page" MasterPageFile="~/MasterPage/default.Master" %>

<asp:Content ID="ProfilePageHead" ContentPlaceHolderID="head" runat="server">
    <link href="/Style/customer.css" rel="stylesheet" />
    <script src="/Script/customer.js"></script>
</asp:Content>

<asp:Content ID="ProfilePageBody" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                Welcome  <asp:Label ID="lbl_firstName"  runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lbl_lastName"  runat="server"></asp:Label>
                 <br />
                <asp:Label ID="lbl_userName"  runat="server"></asp:Label>  
                <br /> 
                <asp:Label ID="lbl_emailId" runat="server"></asp:Label><br />
                <asp:Label ID="lbl_mobileNo" T runat="server"></asp:Label><br />
            </div>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12">
                <asp:Button ID="Button_ShowHistory" runat="server" Text="Show History" CssClass="btn btn-small btn-primary" ClientIDMode="Static" OnClick="Button_ShowHistory_Click" /><br />
                <br />
                <div style="overflow: scroll; height: 300px; width: 1000px">
                    <asp:Repeater ID="PassengerHistory" runat="server" OnItemCommand="PassengerHistory_ItemCommand">

                        <HeaderTemplate>
                            <table border="1">
                                <tr>
                                    <th><b>PNR No.:</b></th>
                                    <th><b>TripCode</b></th>
                                    <th><b>No.of Seats Booked</b></th>
                                    <th><b>Source</b></th>
                                    <th><b>Destination</b></th>
                                    <th><b>Departure Time</b></th>
                                    <th><b>Date of Reservation</b></th>
                                    <th><b>Date of Journey</b></th>
                                    <th><b>Total Fare</b></th>
                                    <%--<th><b>Generate Ticket</b></th>--%>
                                </tr>
                        </HeaderTemplate>

                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("PNR") %> </td>
                                <td><%# Eval("TripCode") %> </td>
                                <td><%# Eval("NumberofSeats") %></td>
                                <td><%# Eval("Source") %></td>
                                <td><%# Eval("Destination") %></td>
                                <td><%# Eval("DepartureTime") %></td>
                                <td><%# Eval("DOR") %></td>
                                <td><%# Eval("DOJ") %></td>
                                <td><%# Eval("Fare") %></td>
                                <%--<td>
                                    <asp:Button ID="Button_ViewTicket" runat="server" Text="View" CssClass="btn btn-small btn-primary small_control" OnClientClick="return GoToTicketPage('1')" /></td>--%>
                            </tr>
                        </ItemTemplate>

                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
