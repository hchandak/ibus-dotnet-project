﻿var settings = {
    rows: 5,
    cols: 15,
    rowCssPrefix: 'row-',
    colCssPrefix: 'col-',
    seatWidth: 35,
    seatHeight: 35,
    seatCss: 'seat',
    selectedSeatCss: 'selectedSeat',
    selectingSeatCss: 'selectingSeat'
};

//function pageLoad(sender, args) {
    
//    $(document).ready(function () {
//        var bookedSeats = [5, 10, 25];
//        init(bookedSeats);
//    });
//}
//$(document).ready(function () {
//    var bookedSeats = [5, 10, 25];
//    init(bookedSeats);
//});

//function hi() {
//    alert("Hii");
//}

var init = function (reservedSeat) {
    alert("Inside");
    reservedSeat = [5, 10, 25];
    var str = [], seatNo, className;
    for (i = 0; i < settings.rows; i++) {
        for (j = 0; j < settings.cols; j++) {
            seatNo = (i + j * settings.rows + 1);
            className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString();
            if ($.isArray(reservedSeat) && $.inArray(seatNo, reservedSeat) != -1) {
                className += ' ' + settings.selectedSeatCss;
            }
            str.push('<li class="' + className + '"' +
            'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px">' +
            '<a title="' + seatNo + '">' + seatNo + '</a>' +
            '</li>');
        }
    }
    $('#place').html(str.join(''));
    bindClickEvent();
    return false;
};
//case I: Show from starting
//init();

//Case II: If already booked
//var bookedSeats = [5, 10, 25];
//init(bookedSeats);

function bindClickEvent() {
    $('.' + settings.seatCss).click(function () {
        if ($(this).hasClass(settings.selectedSeatCss)) {
            alert('This seat is already reserved');
        }
        else {
            if ($('.' + settings.selectingSeatCss).length < 5) {
                $(this).toggleClass(settings.selectingSeatCss);
            }
            else {
                alert("You cannot select more than 5 seats");
            }
            
        }
    });

}
//$('.' + settings.seatCss).click(function () {
//    if ($(this).hasClass(settings.selectedSeatCss)) {
//        alert('This seat is already reserved');
//    }
//    else {
//        $(this).toggleClass(settings.selectingSeatCss);
//    }
//});

$('#btnShow').click(function () {
    var str = [];
    $.each($('#place li.' + settings.selectedSeatCss + ' a, #place li.' + settings.selectingSeatCss + ' a'), function (index, value) {
        str.push($(this).attr('title'));
    });
    alert(str.join(','));
})

$('#btnShowNew').click(function () {
    var str = [], item;
    $.each($('#place li.' + settings.selectingSeatCss + ' a'), function (index, value) {
        item = $(this).attr('title');
        str.push(item);
    });
    alert(str.join(','));
})