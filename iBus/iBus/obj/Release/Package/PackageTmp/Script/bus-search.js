﻿
var cityList = new Array();

$(document).ready(function () {

    if ($('#Div_ErrorMessageLoginRegister').css('display') != 'none')
    {
        $('#Div_ErrorMessageLoginRegister').fadeOut(2000);
    }


    var cities = $("#HiddenField_Cities").val();
    cityList = cities.split(';');
    $("#TextBox_FromCity").autocomplete({
        source: cityList,
        autoFocus: true
    }).focusin(function () {

        $(this).removeClass("errorControl");
    });

    $("#TextBox_ToCity").autocomplete({
        source: cityList,
        autoFocus: true
    }).focusin(function () {

        $(this).removeClass("errorControl");

    });

    $("#TextBox_DateOfJourney").keydown(function (event) {
        event.preventDefault();
    }).datepicker({ minDate: 0, maxDate: "+2M" }).focusin(function () {
        $(this).removeClass("errorControl");

    });


});


function ValidateAll(action) {

    $("#Div_ErrorMessageBusSearch").text("").hide();
    $("#Div_ErrorMessageAdmin").text("").hide();

    var flag = false;

    // FROM city validation
    var fromStation = $("#TextBox_FromCity").val().toString().trim().toLowerCase();
    if (fromStation != "") {

        for (var i = 1; i < cityList.length; i++) {
            if (fromStation == cityList[i].toLowerCase()) {
                flag = true;
                $("#TextBox_FromCity").val(cityList[i]);
            }
        }

    }

    if (flag == false) {
        $("#TextBox_FromCity").val("").addClass("errorControl");
        $("#Div_ErrorMessageBusSearch").text("Enter a valid FromCity. ").show();
        $("#Div_ErrorMessageAdmin").text("Enter a valid FromCity. ").show();
        return false;
    }

    //TO city validation
    var toStation = $("#TextBox_ToCity").val().toString().trim().toLowerCase();
    flag = false;
    if (toStation != "") {
        for (var i = 1; i < cityList.length; i++) {
            if (toStation == cityList[i].toLowerCase()) {
                $("#TextBox_ToCity").val(cityList[i]);
                flag = true;
            }
        }
    }
    if (flag == false) {
        $("#TextBox_ToCity").val("").addClass("errorControl");
        $("#Div_ErrorMessageBusSearch").text("Info! Please enter a valid ToCity. ").show();
        $("#Div_ErrorMessageAdmin").text("Info! Please enter a valid ToCity. ").show();
        return false;
    }
    if (action != "routeSearch") {
        //DateOfJourney validation
        if ($("#TextBox_DateOfJourney").val().toString().trim() == "") {
            $("#TextBox_DateOfJourney").addClass("errorControl")
            $("#Div_ErrorMessageBusSearch").text("Info! Please select date of journey.").show();
            
            return false;
        }
    }

    if ($("#TextBox_ToCity").val() == $("#TextBox_FromCity").val()) {
        $("#Div_ErrorMessageBusSearch").text("Info! Source and Destination cannot be same.").show();
        $("#Div_ErrorMessageAdmin").text("Info! Source and Destination cannot be same.").show();
        return false;
    }

    return true;
}