﻿var validlName = /^[A-Za-z]+$/;
var str_msg = "";
var age;
var validEmail = /[a-z0-9!#$%&'*+/=?^{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)\b/;




$(document).ready(function () {

  





    //Validating PassengerEmailId  
    $('#TextBox_UserEmailId').change(function () {
        if (!validEmail.test(this.value)) {
            $('#TextBox_UserEmailId').val('');
            $('#TextBox_UserEmailId').addClass("errorControl");
            $('#TextBox_UserEmailId').focus();            
        }
        else { $('#TextBox_UserEmailId').removeClass("errorControl"); }
    
    });


    //Validating Passenger1MobileNuber  
    $('#TextBox_UserMobileNo').change(function () {
        if ((isNaN($('#TextBox_UserMobileNo').val()) || $('#TextBox_UserMobileNo').val().length > 13)) {
            $('#TextBox_UserMobileNo').val('');
            $('#TextBox_UserMobileNo').addClass("errorControl");
            $('#TextBox_UserMobileNo').focus();
            
        }
        else { $('#TextBox_UserMobileNo').removeClass("errorControl"); }
    });


    //Validating Passenger1FirstName  
    $('#TextBox_Passenger1FirstName').change(function () {
        if (!validlName.test(this.value)) {
            $('#TextBox_Passenger1FirstName').val('');
            $('#TextBox_Passenger1FirstName').addClass("errorControl");
            $('#TextBox_Passenger1FirstName').focus();
            
        }
        else { $('#TextBox_Passenger1FirstName').removeClass("errorControl"); }
    });

    //Validating Passenger1LastName  
    $('#TextBox_Passenger1LastName').change(function () {
        if (!validlName.test(this.value)) {
            $('#TextBox_Passenger1LastName').val('');
            $('#TextBox_Passenger1LastName').addClass("errorControl");
            $('#TextBox_Passenger1LastName').focus();
            
        }
        else { $('#TextBox_Passenger1LastName').removeClass("errorControl"); }
    });

    //Validating Passenger1Age  
    $('#TextBox_Passenger1Age').change(function () {
        if (isNaN(this.value) || (this.value) < 1 || (this.value) > 110) {
            $('#TextBox_Passenger1Age').val('');
            $('#TextBox_Passenger1Age').addClass("errorControl");
            $('#TextBox_Passenger1Age').focus();
            
        }
        else { $('#TextBox_Passenger1Age').removeClass("errorControl"); }
    });


    //  Validating Passenger2FirstName
    $('#TextBox_Passenger2FirstName').change(function () {

        if (!validlName.test(this.value)) {
            $('#TextBox_Passenger2FirstName').val('');
            $('#TextBox_Passenger2FirstName').addClass("errorControl");
            $('#TextBox_Passenger2FirstName').focus();
            
        }
        else { $('#TextBox_Passenger2FirstName').removeClass("errorControl"); }
    });

    //  Validating Passenger2LastName  
    $('#TextBox_Passenger2LastName').change(function () {
        if (!validlName.test(this.value)) {
            $('#TextBox_Passenger2LastName').val('');
            $('#TextBox_Passenger2LastName').addClass("errorControl");
            $('#TextBox_Passenger2LastName').focus();
        }
        else { $('#TextBox_Passenger2LastName').removeClass("errorControl"); }
    });

    //   Validating Passenger2Age  
    $('#TextBox_Passenger2Age').change(function () {
        if (isNaN(this.value) || (this.value) < 1 || (this.value) > 110) {
            $('#TextBox_Passenger2Age').val('');
            $('#TextBox_Passenger2Age').addClass("errorControl");
            $('#TextBox_Passenger2Age').focus();
            
        }
        else { $('#TextBox_Passenger2Age').removeClass("errorControl"); }
    });


    //Validating Passenger3FirstName
    $('#TextBox_Passenger3FirstName').change(function () {
        if (!validlName.test(this.value)) {
            $('#TextBox_Passenger3FirstName').val('');
            $('#TextBox_Passenger3FirstName').addClass("errorControl");
            $('#TextBox_Passenger3FirstName').focus();
            
        }
        else { $('#TextBox_Passenger3FirstName').removeClass("errorControl"); }
    });

    //Validating Passenger3LastName  
    $('#TextBox_Passenger3LastName').change(function () {
        if (!validlName.test(this.value)) {
            $('#TextBox_Passenger3LastName').val('');
            $('#TextBox_Passenger3LastName').addClass("errorControl");
            $('#TextBox_Passenger3LastName').focus();
            
        }
        else { $('#TextBox_Passenger3LastName').removeClass("errorControl"); }
    });

    //Validating Passenger3Age  
    $('#TextBox_Passenger3Age').change(function () {
        if (isNaN(this.value) || (this.value) < 1 || (this.value) > 110) {
            $('#TextBox_Passenger3Age').val('');
            $('#TextBox_Passenger3Age').addClass("errorControl");
            $('#TextBox_Passenger3Age').focus();
            
        }
        else { $('#TextBox_Passenger3Age').removeClass("errorControl"); }
    });


    //Validating Passenger4FirstName
    $('#TextBox_Passenger4FirstName').change(function () {
        if (!validlName.test(this.value)) {
            $('#TextBox_Passenger4FirstName').val('');
            $('#TextBox_Passenger4FirstName').addClass("errorControl");
            $('#TextBox_Passenger4FirstName').focus();
            
        }
        else { $('#TextBox_Passenger4FirstName').removeClass("errorControl"); }
    });

    //Validating Passenger4LastName  
    $('#TextBox_Passenger4LastName').change(function () {
        if (!validlName.test(this.value)) {
            $('#TextBox_Passenger4LastName').val('');
            $('#TextBox_Passenger4LastName').addClass("errorControl");
            $('#TextBox_Passenger4LastName').focus();
            
        }
        else { $('#TextBox_Passenger4LastName').removeClass("errorControl"); }
    });

    //Validating Passenger4Age  
    $('#TextBox_Passenger4Age').change(function () {
        if (isNaN(this.value) || (this.value) < 1 || (this.value) > 110) {
            $('#TextBox_Passenger4Age').val('');
            $('#TextBox_Passenger4Age').addClass("errorControl");
            $('#TextBox_Passenger4Age').focus();
            
        }
        else { $('#TextBox_Passenger4Age').removeClass("errorControl"); }
    });


    //Validating Passenger5FirstName
    $('#TextBox_Passenger5FirstName').change(function () {
        if (!validlName.test(this.value)) {
            $('#TextBox_Passenger5FirstName').val('');
            $('#TextBox_Passenger5FirstName').addClass("errorControl");
            $('#TextBox_Passenger5FirstName').focus();
            
        }
        else { $('#TextBox_Passenger5FirstName').removeClass("errorControl"); }
    });

    //Validating Passenger5LastName  
    $('#TextBox_Passenger5LastName').change(function () {
        if (!validlName.test(this.value)) {
            $('#TextBox_Passenger5LastName').val('');
            $('#TextBox_Passenger5LastName').addClass("errorControl");
            $('#TextBox_Passenger5LastName').focus();            
        }
        else { $('#TextBox_Passenger5LastName').removeClass("errorControl"); }
    });

    //Validating Passenger5Age  
    $('#TextBox_Passenger5Age').change(function () {

        if (isNaN(this.value) || (this.value) < 1 || (this.value) > 110) {
            $('#TextBox_Passenger5Age').val('');
            $('#TextBox_Passenger5Age').addClass("errorControl");
            $('#TextBox_Passenger5Age').focus();
            
        }
        else { $('#TextBox_Passenger5Age').removeClass("errorControl"); }
    });



    $("#Button_SubmitPassengersDetail").click(function (event) {


        if ($('#Div_Passenger1').css('display') != 'none') {

            //Validating Passenger1FirstName
            if ($('#TextBox_Passenger1FirstName').val() == null || $('#TextBox_Passenger1FirstName').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger1FirstName*" + "<br/>";
            }

            //Validating Passenger1LastName
            if ($('#TextBox_Passenger1LastName').val() == null || $('#TextBox_Passenger1LastName').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger1LastName*" + "<br/>";
            }

            //Validating Passenger1Age
            if ($('#TextBox_Passenger1Age').val() == null || $('#TextBox_Passenger1Age').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger1Age*" + "<br/>";
            }
        }



        if ($('#Div_Passenger2').css('display') != 'none') {
            //Validating Passenger2FirstName
            if ($('#TextBox_Passenger2FirstName').val() == null || $('#TextBox_Passenger2FirstName').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger2FirstName*" + "<br/>";
            }

            //Validating Passenger2LastName
            if ($('#TextBox_Passenger2LastName').val() == null || $('#TextBox_Passenger2LastName').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger2LastName*" + "<br/>";
            }

            //Validating Passenger2Age
            if ($('#TextBox_Passenger2Age').val() == null || $('#TextBox_Passenger2Age').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger2Age*" + "<br/>";
            }


        }


        if ($('#Div_Passenger3').css('display') != 'none') {

            //Validating Passenger3FirstName
            if ($('#TextBox_Passenger3FirstName').val() == null || $('#TextBox_Passenger3FirstName').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger3FirstName*" + "<br/>";
            }

            //Validating Passenger3LastName
            if ($('#TextBox_Passenger3LastName').val() == null || $('#TextBox_Passenger3LastName').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger3LastName*" + "<br/>";
            }

            //Validating Passenger3Age
            if ($('#TextBox_Passenger3Age').val() == null || $('#TextBox_Passenger3Age').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger3Age*" + "<br/>";
            }
        }

        if ($('#Div_Passenger4').css('display') != 'none') {

            //Validating Passenger4FirstName
            if ($('#TextBox_Passenger4FirstName').val() == null || $('#TextBox_Passenger4FirstName').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger4FirstName*" + "<br/>";
            }

            //Validating Passenger4LastName
            if ($('#TextBox_Passenger4LastName').val() == null || $('#TextBox_Passenger4LastName').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger4LastName*" + "<br/>";
            }

            //Validating Passenger4Age
            if ($('#TextBox_Passenger4Age').val() == null || $('#TextBox_Passenger4Age').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger4Age*" + "<br/>";
            }
        }


        if ($('#Div_Passenger5').css('display') != 'none') {

            //Validating Passenger5FirstName
            if ($('#TextBox_Passenger5FirstName').val() == null || $('#TextBox_Passenger5FirstName').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger5FirstName*" + "<br/>";
            }

            //Validating Passenger5LastName
            if ($('#TextBox_Passenger5LastName').val() == null || $('#TextBox_Passenger5LastName').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger5LastName*" + "<br/>";
            }

            //Validating Passenger5Age
            if ($('#TextBox_Passenger5Age').val() == null || $('#TextBox_Passenger5Age').val().length == 0) {
                event.preventDefault();
                str_msg += "Passenger5Age*" + "<br/>";
            }
        }



        if (str_msg.length > 0) {
            $('#Div_ErrorMessage').css('display', 'block');
            $('#Div_ErrorMessage').html('Info! All (*) field(s) are required. Please fill all required detail before you proceed.');
            // $('#Div_ErrorMessage').html(str_msg);
            $('#Div_ErrorMessage').fadeOut(8000);
        }
    });

    $('#Div_SucessMessage').fadeOut(8000);

});

function GoToTicketPage(resID) {
    event.preventDefault();
    windows.location.href = "ticket.aspx?resID=" + resID;
}