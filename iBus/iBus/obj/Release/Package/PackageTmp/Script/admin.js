﻿$(document).ready(function () {
    setInterval(function () {
        if ($('#Div_ErrorMessage').css('display') != 'none') {
            $('#Div_ErrorMessage').fadeOut(5000);
        }
        if ($('#Div_SucessMessage').css('display') != 'none') {
            $('#Div_SucessMessage').fadeOut(5000);
        }
    });

   
});

function RedirectToTravelDetail() {
    event.preventDefault();
    window.location.href = "admin.aspx?resource=travel";
}

function RedirectToBusDetail() {
    event.preventDefault();
    window.location.href = "admin.aspx?resource=bus";
}

function RedirectToRouteDetail() {
    event.preventDefault();
    window.location.href = "admin.aspx?resource=route";
}

function RedirectToTripDetail() {
    event.preventDefault();
    window.location.href = "admin.aspx?resource=trip";
}

function RedirectToAdminAccount() {
    event.preventDefault();
    window.location.href = "admin.aspx";
}

function MakeStaticRouteGridHeader(gridId, height, width, headerHeight, isFooter) {
    var tbl = document.getElementById(gridId);
    if (tbl) {
        var divRouteHeader = document.getElementById('Div_RouteHeaderRow');
        var divRouteMainContent = document.getElementById('Div_RouteMainContent');

        //*** Set divheaderRow Properties ****
        divRouteHeader.style.height = headerHeight + 'px';
        divRouteHeader.style.width = (parseInt(width) - 16) + 'px';
        divRouteHeader.style.position = 'relative';
        divRouteHeader.style.top = '0px';
        divRouteHeader.style.zIndex = '10';
        divRouteHeader.style.verticalAlign = 'top';

        //*** Set divMainContent Properties ****
        divRouteMainContent.style.width = width + 'px';
        divRouteMainContent.style.height = height + 'px';
        divRouteMainContent.style.position = 'relative';
        divRouteMainContent.style.top = -headerHeight + 'px';
        divRouteMainContent.style.zIndex = '1';

        //****Copy Header in divHeaderRow****
        divRouteHeader.appendChild(tbl.cloneNode(true));
    }

}

function MakeStaticTravelGridHeader(gridId, height, width, headerHeight, isFooter) {
    var tbl = document.getElementById(gridId);
    if (tbl) {
        var divTravelHeader = document.getElementById('Div_TravelHeaderRow');
        var divTravelMainContent = document.getElementById('Div_TravelMainContent');

        //*** Set divheaderRow Properties ****
        divTravelHeader.style.height = headerHeight + 'px';
        divTravelHeader.style.width = (parseInt(width) ) + 'px';
        divTravelHeader.style.position = 'relative';
        divTravelHeader.style.top = '0px';
        divTravelHeader.style.zIndex = '10';
        divTravelHeader.style.verticalAlign = 'top';

        //*** Set divMainContent Properties ****
        divTravelMainContent.style.width = width + 'px';
        divTravelMainContent.style.height = height + 'px';
        divTravelMainContent.style.position = 'relative';
        divTravelMainContent.style.top = -headerHeight + 'px';
        divTravelMainContent.style.zIndex = '1';

        //****Copy Header in divHeaderRow****
        divTravelHeader.appendChild(tbl.cloneNode(true));
    }

}

function MakeStaticTripGridHeader(gridId, height, width, headerHeight, isFooter) {
    var tbl = document.getElementById(gridId);
    if (tbl) {
        var divTripHeader = document.getElementById('Div_TripHeaderRow');
        var divTripMainContent = document.getElementById('Div_TripMainContent');

        //*** Set divheaderRow Properties ****
        divTripHeader.style.height = headerHeight + 'px';
        divTripHeader.style.width = (parseInt(width) - 16) + 'px';
        divTripHeader.style.position = 'relative';
        divTripHeader.style.top = '0px';
        divTripHeader.style.zIndex = '10';
        divTripHeader.style.verticalAlign = 'top';

        //*** Set divMainContent Properties ****
        divTripMainContent.style.width = width + 'px';
        divTripMainContent.style.height = height + 'px';
        divTripMainContent.style.position = 'relative';
        divTripMainContent.style.top = -headerHeight + 'px';
        divTripMainContent.style.zIndex = '1';

        //****Copy Header in divHeaderRow****
        divTripHeader.appendChild(tbl.cloneNode(true));
    }

}

function MakeStaticBusGridHeader(gridId, height, width, headerHeight, isFooter) {
    var tbl = document.getElementById(gridId);
    if (tbl) {
        var divBusHeader = document.getElementById('Div_BusHeaderRow');
        var divBusMainContent = document.getElementById('Div_BusMainContent');

        //*** Set divheaderRow Properties ****
        divBusHeader.style.height = headerHeight + 'px';
        divBusHeader.style.width = (parseInt(width) - 16) + 'px';
        divBusHeader.style.position = 'relative';
        divBusHeader.style.top = '0px';
        divBusHeader.style.zIndex = '10';
        divBusHeader.style.verticalAlign = 'top';

        //*** Set divMainContent Properties ****
        divBusMainContent.style.width = width + 'px';
        divBusMainContent.style.height = height + 'px';
        divBusMainContent.style.position = 'relative';
        divBusMainContent.style.top = -headerHeight + 'px';
        divBusMainContent.style.zIndex = '1';

        //****Copy Header in divHeaderRow****
        divBusHeader.appendChild(tbl.cloneNode(true));
    }

}
