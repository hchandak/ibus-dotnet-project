﻿var validTripFare = /^(\d*([,](?=\d{3}))?\d+)+((?!\2)[,]\d\d)?$/;
var validBusCapacit = /^([30]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/;
var validTravelName = /^[A-Za-z ]+$/;
var validEmail = /[a-z0-9!#$%&'*+/=?^{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)\b/
var validContact = /^\+?([0-9]+[ -]?){5,}[0-9]+$/;
var validAddress = /[0-9]|[A-Z/a-z]| |#|,|/;
var error = null;
$(document).ready(function () {
      
    //Travel name validation 
    $('#TextBox_TravelName').change(function () {
        if (!validTravelName.test(this.value)) {
            $('#TextBox_TravelName').val('');
            $('#TextBox_TravelName').focus();
            $('#TextBox_TravelName').popover({
                content: 'Please enter valid travel name, should not contain special characters and numbers.',
                trigger: 'manual'
            });
            $(this).popover('show');

        }
        else {
            $(this).popover('hide');
        }

    });

    //Travel address validation
    $('#TextBox_TravelAddress').change(function () {
        if (!validAddress.test(this.value)) {
            $('#TextBox_TravelAddress').val('');
            $('#TextBox_TravelAddress').focus();
            $('#TextBox_TravelAddress').popover({
                content: 'Please enter valid travel address, should not contain special characters.',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }

    });

    //Travel email validation
    $('#TextBox_TravelEmail').change(function () {
        if (!validEmail.test(this.value)) {
            $('#TextBox_TravelEmail').val('');
            $('#TextBox_TravelEmail').focus();
            $('#TextBox_TravelEmail').popover({
                content: 'Please enter valid email address.',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }

    });

    //Travel contact validation
    $('#TextBox_TravelContact').change(function () {
        if (!validContact.test(this.value)) {
            $('#TextBox_TravelContact').val('');
            $('#TextBox_TravelContact').focus();
            $('#TextBox_TravelContact').popover({
                content: 'Please enter valid contact number.',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }

    });

    //Bus capacity validation
    $('#TextBox_BusCapacity').change(function () {
        if (!validBusCapacit.test(this.value))
        {
            $('#TextBox_BusCapacity').val('');
            $('#TextBox_BusCapacity').focus();
            $('#TextBox_BusCapacity').popover({
                content: 'Bus capacity shoud be in range between (30-255).',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }
    });

    //Trip fare validation
    $('#TextBox_TripFare').change(function () {
        if (!validTripFare.test(this.value)) {
            $('#TextBox_TripFare').val('');
            $('#TextBox_TripFare').focus();
            $('#TextBox_TripFare').popover({
                content: 'Please enter valid bus fare, Should contain only numbers.',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }
    });
    
});

// Required and Regular expression validations For Add Travel..
function ValidateTravelDetail() {
    var travelName = $('#TextBox_TravelName').val();
    var travelAddress = $('#TextBox_TravelAddress').val();
    var travelEmail = $('#TextBox_TravelEmail').val();
    var travelContact = $('#TextBox_TravelContact').val();
    var strErrorMessage = "";
    if (!validTravelName.test(travelName) || travelName.length == 0) {
        strErrorMessage+="Travel name* ,"
        $('#TextBox_TravelName').val('');
        $('#TextBox_TravelName').focus();
        $('#TextBox_TravelName').popover({
            content: 'Please enter valid travel name, should not contain special characters and numbers.',
            trigger: 'manual'
        });
        $(this).popover('show');
    }
    else {
        $(this).popover('hide');
    }
    if (!validAddress.test(travelAddress) || travelAddress.length == 0) {
        strErrorMessage += "Travel Address* ,"
        $('#TextBox_TravelAddress').val('');
        $('#TextBox_TravelAddress').focus();
        $('#TextBox_TravelAddress').popover({
            content: 'Please enter valid travel address, should not contain special characters.',
            trigger: 'manual'
        });
        $(this).popover('show');
    }
    else {
        $(this).popover('hide');
    }
    if (!validEmail.test(travelEmail) || travelEmail.length == 0) {
        strErrorMessage += "Travel email* ,"
        $('#TextBox_TravelEmail').val('');
        $('#TextBox_TravelEmail').focus();
        $('#TextBox_TravelEmail').popover({
            content: 'Please enter valid email address.',
            trigger: 'manual'
        });
        $(this).popover('show');
    }
    else {
        $(this).popover('hide');
    }
    if (!validContact.test(travelContact) || travelContact.length == 0) {
        strErrorMessage += "Travel contact*."
        $('#TextBox_TravelContact').val('');
        $('#TextBox_TravelContact').focus();
        $('#TextBox_TravelContact').popover({
            content: 'Please enter valid contact number.',
            trigger: 'manual'
        });
        $(this).popover('show');
    }
    else {
        $(this).popover('hide');
    }
    if (strErrorMessage.length > 0) {
        $('#Div_ErrorMessage').css('display', 'block');
        $('#Div_ErrorMessage').html('Info! All (*) field(s) are required. Please fill all required detail before you proceed.');
        return false;
    }
    return true;
}

function ValidateBusDetail() {
    var capacity = $('#TextBox_BusCapacity').val();
    var busTypeID= $('#DropDownList_BusType').val();
    var travelID = $('#DropDownList_BusTravelName').val();
    
    var strErrorMessage = "";
    if (!$('#DropDownList_BusType').is(':disabled') && parseInt(busTypeID) <= 0) {
        strErrorMessage += "Bus type* ,";
        $('#DropDownList_BusType').focus();
       
    }
    else if (!validBusCapacit.test(capacity) || capacity <= 0) {
        strErrorMessage += "Capacity* ,";
        $('#TextBox_BusCapacity').val('');
        $('#TextBox_BusCapacity').focus();
    }

    else if (!$('#DropDownList_BusTravelName').is(':disabled') && parseInt(travelID) <= 0) {
        strErrorMessage += "Travel name* ,";
        $('#DropDownList_BusTravelName').focus();

    }
    if (strErrorMessage.length > 0) {
        $('#Div_ErrorMessage').css('display', 'block');
        $('#Div_ErrorMessage').html('Info! All (*) field(s) are required. Please fill all required detail before you proceed.');
        return false;
    }
    return true;
}

function ValidateTripDetailForSave() {
   
    var tripRoute = $('#DropDownList_TripRoute').val();
    var busTypeID = $("input[type='radio'][name='radioBusTypeAndAmenities']:checked").val();
    var travelID = $('#DropDownList_TripTravelName').val();
    var tripFare = $('#TextBox_TripFare').val();
    var tripDepartureHour=$('#DropDownList_TripDepartureHour').val();
    var tripDepartureMinut = $('#DropDownList_TripDepartureMinut').val();
    var tripArrivalHour = $('#DropDownList_TripArrivalHoure').val();
    var tripArrivalMinut = $('#DropDownList_TripArrivalMinut').val(); 
    var tripFrequency = $('#DropDownList_Frequency').val();
    var strErrorMessage = "";
    if (parseInt(travelID) <= 0) {
        strErrorMessage += "Travel Name* ,";
        $('#DropDownList_TripTravelName').focus();
    }
    if (parseInt(busTypeID) <= 0 || busTypeID==undefined) {
        strErrorMessage += "Bus Type* ,";
        $('#Grid_BusTypeAndAmenitiesDetail').focus();
    }
    else if (parseInt(tripRoute) <= 0) {
        strErrorMessage += "Route* ,";
        $('#DropDownList_TripRoute').focus();
    }
    else if (!validTripFare.test(tripFare) || tripFare.length <= 0) {
        strErrorMessage += "Fare* ,";
        $('#TextBox_TripFare').val('');
        $('#TextBox_TripFare').focus();
    }
    else if (parseInt(tripFrequency) <= 0) {
        strErrorMessage += "Frequency* ,";
        $('#DropDownList_Frequency').focus();
    }
    else if (parseInt(tripDepartureHour) <= 0) {
        strErrorMessage += "Departure Hour* ,";
        $('#DropDownList_TripDepartureHour').focus();
    }
    else if (parseInt(tripDepartureMinut) <= 0) {
        strErrorMessage += "Departure Minut* ,";
        $('#DropDownList_TripDepartureMinut').focus();
    }
    else if (parseInt(tripArrivalHour) <= 0) {
        strErrorMessage += "Arrival Hour* ,";
        $('#DropDownList_TripArrivalHoure').focus();
    }
    else if (parseInt(tripArrivalMinut) <= 0) {
        strErrorMessage += "Arrival Minut* ,";
        $('#DropDownList_TripArrivalMinut').focus();
    }
    
    if (strErrorMessage.length > 0) {
        $('#Div_ErrorMessage').css('display', 'block');
        $('#Div_ErrorMessage').html('Info! All (*) field(s) are required. Please fill all required detail before you proceed.');
        return false;
    }
    return true;
}

function ValidateTripDetailForUpdate() {

    var tripFare = $('#TextBox_TripFare').val();
    var tripDepartureHour = $('#DropDownList_TripDepartureHour').val();
    var tripDepartureMinut = $('#DropDownList_TripDepartureMinut').val();
    var tripArrivalHour = $('#DropDownList_TripArrivalHoure').val();
    var tripArrivalMinut = $('#DropDownList_TripArrivalMinut').val();
    var tripFrequency = $('#DropDownList_Frequency').val();
    var strErrorMessage = "";
    if (!validTripFare.test(tripFare) || tripFare.length <= 0) {
        strErrorMessage += "Fare* ,";
        $('#TextBox_TripFare').val('');
        $('#TextBox_TripFare').focus();
    }
    else if (parseInt(tripFrequency) <= 0) {
        strErrorMessage += "Frequency* ,";
        $('#DropDownList_Frequency').focus();
    }
    else if (parseInt(tripDepartureHour) < 0) {
        strErrorMessage += "Departure Hour* ,";
        $('#DropDownList_TripDepartureHour').focus();
    }
    else if (parseInt(tripDepartureMinut) < 0) {
        strErrorMessage += "Departure Minut* ,";
        $('#DropDownList_TripDepartureMinut').focus();
    }
    else if (parseInt(tripArrivalHour) < 0) {
        strErrorMessage += "Arrival Hour* ,";
        $('#DropDownList_TripArrivalHoure').focus();
    }
    else if (parseInt(tripArrivalMinut) <0) {
        strErrorMessage += "Arrival Minut* ,";
        $('#DropDownList_TripArrivalMinut').focus();
    }

    if (strErrorMessage.length > 0) {
        $('#Div_ErrorMessage').css('display', 'block');
        $('#Div_ErrorMessage').html('Info! All (*) field(s) are required. Please fill all required detail before you proceed.');
        return false;
    }
    return true;
}


function MakeStaticBusTypeAndAmenitiesGridHeader(gridId, height, width, headerHeight, isFooter) {
    var tbl = document.getElementById(gridId);
    if (tbl) {
        var divBusTypeAndAmenitiesHeader = document.getElementById('Div_BusTypeAndAmenitiesHeaderRow');
        var divBusTypeAndAmenitiesMainContent = document.getElementById('Div_BusTypeAndAmenitiesMainContent');

        //*** Set divheaderRow Properties ****
        divBusTypeAndAmenitiesHeader.style.height = headerHeight + 'px';
        divBusTypeAndAmenitiesHeader.style.width = (parseInt(width) - 16) + 'px';
        divBusTypeAndAmenitiesHeader.style.position = 'relative';
        divBusTypeAndAmenitiesHeader.style.top = '0px';
        divBusTypeAndAmenitiesHeader.style.zIndex = '10';
        divBusTypeAndAmenitiesHeader.style.verticalAlign = 'top';

        //*** Set divMainContent Properties ****
        divBusTypeAndAmenitiesMainContent.style.width = width + 'px';
        divBusTypeAndAmenitiesMainContent.style.height = height + 'px';
        divBusTypeAndAmenitiesMainContent.style.position = 'relative';
        divBusTypeAndAmenitiesMainContent.style.top = -headerHeight + 'px';
        divBusTypeAndAmenitiesMainContent.style.zIndex = '1';

        //****Copy Header in divHeaderRow****
        divBusTypeAndAmenitiesHeader.appendChild(tbl.cloneNode(true));
    }

}
