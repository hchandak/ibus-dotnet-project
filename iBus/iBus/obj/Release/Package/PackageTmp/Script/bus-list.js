﻿var selectedSeats = new Array();

function pageLoad(sender, args) {
    if (args.get_isPartialLoad()) {
        //Specific code for partial postbacks can go in here.
        $("#Table_BusList").tablesorter({
            // pass the headers argument and assing a object
            headers: {
                // assign the secound column (we start counting zero)
                6: {
                    // disable it by setting the property sorter to false
                    sorter: false
                }
            }
        });
        $(document).ready(function(){
            SeatHoverEffect();
            $("#Button_Continue").attr("disabled","disabled");
        });
    }
}

$(document).ready(function () {
    $("#busOperatorFilter").hide();
    $("#busTypeFilter").hide();
    $("#Div_FilterOptionsBox").hide();
    //$("#Table_BusList").tablesorter();
    $("#TextBox_BusOperator").keydown(function (event) {
        event.preventDefault();
    }).click(function () {
        $("#busOperatorFilter").toggle();
    });
    $("#TextBox_BusType").keydown(function (event) {
        event.preventDefault();
    }).click(function () {
        $("#busTypeFilter").toggle();
    });

    $("#Table_BusList").tablesorter({
        // pass the headers argument and assing a object
        headers: {
            // assign the secound column (we start counting zero)
            6: {
                // disable it by setting the property sorter to false
                sorter: false
            }
        }
    });

    $("#Div_ModifySearchBox").hide();

    $("#Div_FilterOptionsTab").click(function () {
        $("#Div_ModifySearchBox").hide();

        runFilterEffect('Div_FilterOptionsBox');
    });

    $("#Div_ModifySearchTab").click(function () {
        $("#Div_ErrorMessage").hide();
        $("#Div_FilterOptionsBox").hide();
        runFilterEffect('Div_ModifySearchBox');
    });

    SeatHoverEffect();
    
});

function SeatHoverEffect() {
    var rowCount = $('#Table_BusList tr').length - 1;

    for (i = 0; i < rowCount; i++) {
        if ($("#ContentPlaceHolder1_Repeater_BusList_HiddenField_AvailableSeats_" + i).val() != '0') {
            $("#ContentPlaceHolder1_Repeater_BusList_Button_BookSeat_" + i).hover(
                function () {
                    $(this).val("Book seat");

                }, function () {
                    var hiddenField = this.parentNode.childNodes[3];
                    var availableSeats = hiddenField.value;
                    $(this).val("Seats : " + availableSeats);
                }
            );
        }
        else {

            $("#ContentPlaceHolder1_Repeater_BusList_Button_BookSeat_" + i).attr('disabled', 'disabled').val("Sold out");
        }

    }
}

function SaveDailyTripID(bookBtn) {
    var row = bookBtn.parentNode.parentNode;
    index = row.rowIndex - 1;
    var dailyTripID = $("#ContentPlaceHolder1_Repeater_BusList_HiddenField_DailyTripID_" + index).val();
    $("#HiddenField_SelectedDailyTripID").val(dailyTripID);

    //ShowModalSeat(1);
    return true;
   
}

function ShowFilter(filterID) {
    runFilterEffect(filterID);
    return false;
}

function runFilterEffect(filterID) {
    if (!($('#'+filterID).is(":visible"))) {
        //run the effect
        $('#' + filterID).show(200);
    }
    else {
        $('#' + filterID).hide(200);
    }
}

function ShowModalSeat(reservedSeats,maxSeat) {
    LoadSeatLayout(reservedSeats);
    BindClickEvent(maxSeat);
    selectedSeats.length = 0;
    $('#Modal_SelectBusSeat').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
    
}


var settings = {
    rows: 5,
    cols: 10,
    rowCssPrefix: 'row-',
    colCssPrefix: 'col-',
    seatWidth: 35,
    seatHeight: 35,
    seatCss: 'seat',
    selectedSeatCss: 'selectedSeat',
    selectingSeatCss: 'selectingSeat'
};


function LoadSeatLayout (reservedSeat) {
    var str = [], seatNo, className;
    for (i = 0; i < settings.rows; i++) {
        for (j = 0; j < settings.cols; j++) {
            var k = i;
            if (i > 2)
                k--;
            if (i != 2) {
                seatNo = (k + j * (settings.rows-1) + 1);
                className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString();
                if ($.isArray(reservedSeat) && $.inArray(seatNo, reservedSeat) != -1) {
                    className += ' ' + settings.selectedSeatCss;
                }
                str.push('<li class="' + className + '"' +
                'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px">' +
                '<a title="' + seatNo + '">' + seatNo + '</a>' +
                '</li>');
            }
            else
                str.push('<br/>');
            
        }
    }
    $('#place').html(str.join(''));
    //bindClickEvent(maxSeat);
};
//case I: Show from starting
//init();

//Case II: If already booked
//var bookedSeats = [5, 10, 25];
//init(bookedSeats);

function BindClickEvent(maxSeat) {

    $('.' + settings.seatCss).click(function () {
        if ($(this).hasClass(settings.selectedSeatCss)) {
            alert('This seat is already reserved');
        }
        else {
            var count = selectedSeats.length;
            if ($(this).hasClass(settings.selectingSeatCss)) {
                $(this).removeClass(settings.selectingSeatCss);
                var index = selectedSeats.indexOf(this.childNodes[0].title);
                selectedSeats.splice(index,1);
                ShowSelectedSeats();
                ShowTotalFare(--count);
            }
            else {
                 
                if (count < maxSeat) {
                    $(this).addClass(settings.selectingSeatCss);
                    selectedSeats.push(this.childNodes[0].title);
                    ShowSelectedSeats();
                    
                    ShowTotalFare(++count);
                }
                else {
                    alert("You cannot select more than " + maxSeat + " seats");
                }
            }
            if (count > 0)
                $("#Button_Continue").removeAttr('disabled');
            else
            $("#Button_Continue").attr('disabled', 'disabled');
                

        }
    });

}

function ShowSelectedSeats() {
    $("#Label_SelectedSeats").text(selectedSeats.join());
    $("#HiddenField_SelectedSeats").value = $("#Label_SelectedSeats").text();
}

function ShowTotalFare(count) {
    var totalFare = count * parseInt($("#HiddenField_Fare").val());
        $("#Label_TotalFare").text(totalFare);
}

function saveSeatsToHiddenField() {
    $("#HiddenField_SeatsSelected").val($("#Label_SelectedSeats").text());
    return true;
}