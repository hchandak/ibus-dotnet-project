﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-passenger-details.aspx.cs" Inherits="iBus.add_passenger_details" MasterPageFile="~/MasterPage/Default.Master" %>

<asp:Content ID="Content1AddPassenger" ContentPlaceHolderID="head" runat="server">
    <script src="Script/registration.js"></script>
    <script src="Script/jquery-ui-1.10.3.custom.js"></script>
    <link href="Style/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <%--<link href="Style/customer.css" rel="stylesheet" />--%>
 
    <script src="Script/customer.js"></script>
</asp:Content>

<asp:Content ID="Content2AddPassenger" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <div class="alert alert-success" id="Div_SucessMessage" style="display: none" clientidmode="Static" runat="server"></div>
        <div class="alert alert-danger" id="Div_ErrorMessage" style="display: none" clientidmode="Static" runat="server"></div>
      
        <div id="Div_Passenger1" class="form-inline" clientidmode="Static" role="form" runat="server">
            Passenger1*
                <div class="form-group ">
                    <asp:TextBox type="text" class="form-control text_box_size " ID="TextBox_Passenger1FirstName" runat="server" ClientIDMode="Static" placeholder="FirstName" />
                </div>

            <div class="form-group">
                <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger1LastName" runat="server" ClientIDMode="Static" placeholder="LastName" />
            </div>

            <div class="form-group">
                <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger1Age" runat="server" ClientIDMode="Static" placeholder="Age" />
            </div>
            <div class="form-group">
                <asp:DropDownList ID="DropDownList_Passenger1Gender" class="form-control text_box_size" ClientIDMode="Static" placeholder="Gender" runat="server"></asp:DropDownList>
            </div>
            <br />
            <br />

        </div>

        <div id="Div_Passenger2" class="form-inline" clientidmode="Static" role="form" runat="server">
            Passenger2*
                <div class="form-group">
                    <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger2FirstName" runat="server" ClientIDMode="Static" placeholder="FirstName" />
                </div>

            <div class="form-group">
                <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger2LastName" runat="server" ClientIDMode="Static" placeholder="LastName" />
            </div>

            <div class="form-group">
                <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger2Age" runat="server" ClientIDMode="Static" placeholder="Age" />
            </div>
            <div class="form-group">
                <asp:DropDownList ID="DropDownList_Passenger2Gender" class="form-control text_box_size" ClientIDMode="Static" placeholder="Gender" runat="server"></asp:DropDownList>
            </div>
            <br />
            <br />
        </div>

        <div id="Div_Passenger3" class="form-inline" clientidmode="Static" role="form" runat="server">
            Passenger3*
                <div class="form-group">
                    <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger3FirstName" runat="server" ClientIDMode="Static" placeholder="FirstName" />
                </div>

            <div class="form-group">
                <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger3LastName" runat="server" ClientIDMode="Static" placeholder="LastName" />
            </div>

            <div class="form-group">
                <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger3Age" runat="server" ClientIDMode="Static" placeholder="Age" />
            </div>
            <div class="form-group ">
                <asp:DropDownList ID="DropDownList_Passenger3Gender" class="form-control text_box_size " ClientIDMode="Static" placeholder="Gender" runat="server"></asp:DropDownList>
            </div>
            <br />
            <br />
        </div>

        <div id="Div_Passenger4" class="form-inline " clientidmode="Static" role="form" runat="server">
            Passenger4*
                <div class="form-group">
                    <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger4FirstName" runat="server" ClientIDMode="Static" placeholder="FirstName" />
                </div>

            <div class="form-group">
                <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger4LastName" runat="server" ClientIDMode="Static" placeholder="LastName" />
            </div>

            <div class="form-group">
                <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger4Age" runat="server" ClientIDMode="Static" placeholder="Age" />
            </div>
            <div class="form-group">
                <asp:DropDownList ID="DropDownList_Passenger4Gender" class="form-control text_box_size " ClientIDMode="Static" placeholder="Gender" runat="server"></asp:DropDownList>
            </div>
            <br />
            <br />
        </div>

        <div id="Div_Passenger5" class="form-inline" clientidmode="Static" role="form" runat="server">
            Passenger5*
                <div class="form-group">
                    <asp:TextBox type="text" class="form-control text_box_size " ID="TextBox_Passenger5FirstName" runat="server" ClientIDMode="Static" placeholder="FirstName" />

                </div>

            <div class="form-group">
                <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger5LastName" runat="server" ClientIDMode="Static" placeholder="LastName" />

            </div>

            <div class="form-group">
                <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_Passenger5Age" runat="server" ClientIDMode="Static" placeholder="Age" />

            </div>
            <div class="form-group">
                <asp:DropDownList ID="DropDownList_Passenger5Gender" class="form-control text_box_size " ClientIDMode="Static" placeholder="Gender" runat="server"></asp:DropDownList>
            </div>
            <br />
            <br />

        </div>

        <div id="DivUserDetail" class="form-inline pull-left" role="form" runat="server">
            EmailId*
            <div class="form-group">
                
                    <asp:TextBox type="text" class="form-control text_box_size " ID="TextBox_UserEmailId" runat="server" ClientIDMode="Static" placeholder="User EmailId" />
            </div>
             Mobile No.*
            <div class="form-group">
       
                    <asp:TextBox type="text" class="form-control text_box_size" ID="TextBox_UserMobileNo" runat="server" ClientIDMode="Static" placeholder="User Mobile no." />
            </div>

             <div class="form-group">
       
            <asp:Button ID="Button_SubmitPassengersDetail" runat="server" Text="Submit" CssClass="button-blue" ClientIDMode="Static"  OnClick="Button_SubmitPassengersDetail_Click" style="margin-left:165px" />

</div>
        </div>

        <br />
    <br />
    <br />
         <asp:Label ID="lbl_Note_Mandatory" runat="server"><b>Note: All fields are mandatory.</b></asp:Label>

        <br />
        <br />






</asp:Content>
