﻿var selectedSeats = new Array();


//function pageLoad(sender, args) {
//    if (args.get_isPartialLoad()) {
//        //Specific code for partial postbacks can go in here.
//        $("#Table_BusList").tablesorter({
//            // pass the headers argument and assing a object
//            headers: {
//                // assign the secound column (we start counting zero)
//                1: {
//                    // disable it by setting the property sorter to false
//                    sorter: false
//                },
//                7: {
//                    // disable it by setting the property sorter to false
//                    sorter: false
//                }
//            }
//        });
//        $(document).ready(function(){
//            SeatHoverEffect();
//            $("#Button_Continue").attr("disabled","disabled");
//        });
//    }
//}

$(document).ready(function () {

    if ($('#Div_ErrorMessageLoginRegister').css('display') != 'none') {
        $('#Div_ErrorMessageLoginRegister').fadeOut(5000);
    }

    $("#DepartureTime").tooltip();

    $("#busOperatorFilter").hide();
    $("#busTypeFilter").hide();
    $("#Div_FilterOptionsBox").hide();
    //$("#Table_BusList").tablesorter();
    $("#TextBox_BusOperator").keydown(function (event) {
        event.preventDefault();
    }).click(function () {
        $("#busOperatorFilter").toggle();
    });
    $("#TextBox_BusType").keydown(function (event) {
        event.preventDefault();
    }).click(function () {
        $("#busTypeFilter").toggle();
    });

    $("#Table_BusList").tablesorter({
        // pass the headers argument and assing a object
        headers: {
            // assign the secound column (we start counting zero)
            2: {
                // disable it by setting the property sorter to false
                sorter: false
            },
            7: {
                // disable it by setting the property sorter to false
                sorter: false
            }
        }
    });

    $("#Div_ModifySearchBox").hide();

    $("#Div_FilterOptionsTab").click(function () {
        $("#Div_ModifySearchBox").hide();

        runFilterEffect('Div_FilterOptionsBox');
    });

    $("#Div_ModifySearchTab").click(function () {
        $("#Div_ErrorMessage").hide();
        $("#Div_FilterOptionsBox").hide();
        runFilterEffect('Div_ModifySearchBox');
    });

    SeatHoverEffect();
    
});

function LoadBoardingPoints(arr_boardingPoints, arr_boardingDepartureTime, departureTimeTagID)
{
    var str_html = '<table>\
            <tr>\
                <th>Boarding Point</th>\
                <th>DepartureTime</th>\
            </tr>';
    for (var i = 0; i < arr_boardingPoints.length; i++)
    {
        str_html.concat('<tr>\
                <th>hemant</th>\
                <th>10:00</th>\
            </tr>');
    }
    str_html.concat('</table>');

    $("#"+departureTimeTagID).tooltip({
        placement: 'right',
        title: str_html,
        html : 'true'
    });

}

function SeatHoverEffect() {
    //var rowCount = $('#Table_BusList tr').length - 1;
    var table = document.getElementById("Table_BusList");
    if (table == null)
        return;
    var rowCount = table.rows.length -1;
    for (i = 0; i < rowCount; i++) {
        var arr_boardingPoints = $("#ContentPlaceHolder1_Repeater_BusList_HiddenField_BoardingPoints_" + i).val().split(",");
        var arr_boardingDepartureTime = $("#ContentPlaceHolder1_Repeater_BusList_HiddenField_BoardingDepartureTime_" + i).val().split(",");

        var departureTimeTagID = table.rows[i + 1].cells[3].childNodes[1].id;
        LoadBoardingPoints(arr_boardingPoints, arr_boardingDepartureTime, departureTimeTagID)
        

        if ($("#ContentPlaceHolder1_Repeater_BusList_HiddenField_AvailableSeats_" + i).val() != '0') {
            $("#ContentPlaceHolder1_Repeater_BusList_Button_BookSeat_" + i).hover(
                function () {
                    $(this).val("Book seat");

                }, function () {
                    var hiddenField = this.parentNode.childNodes[3];
                    var availableSeats = hiddenField.value;
                    $(this).val("Seats : " + availableSeats);
                }
            );
        }
        else {

            $("#ContentPlaceHolder1_Repeater_BusList_Button_BookSeat_" + i).attr('disabled', 'disabled').val("Sold out");
        }

    }
}

function SaveDailyTripID(bookBtn) {
    var row = bookBtn.parentNode.parentNode;
    index = row.rowIndex - 1;
    var dailyTripID = $("#ContentPlaceHolder1_Repeater_BusList_HiddenField_DailyTripID_" + index).val();
    $("#HiddenField_SelectedDailyTripID").val(dailyTripID);

    //ShowModalSeat(1);
    return true;
   
}

function ShowFilter(filterID) {
    runFilterEffect(filterID);
    return false;
}

function runFilterEffect(filterID) {
    if (!($('#'+filterID).is(":visible"))) {
        //run the effect
        $('#' + filterID).show(200);
    }
    else {
        $('#' + filterID).hide(200);
    }
}

function ShowModalSeat(reservedSeats,maxSeat,busType) {
    if (busType == "Sleeper") {
        $("#Div_SeaterLayout").hide();
        $("#Div_SleeperLayout").show();
        LoadSleeperSeatLayout(reservedSeats);
    }
    else if (busType == "Semi-Sleeper") {
        $("#Div_SeaterLayout").show();
        $("#Div_SleeperLayout").hide();
        LoadSeaterSeatLayout(reservedSeats);
    }
    
    BindClickEvent(maxSeat);
    selectedSeats.length = 0;
    $('#Modal_SelectBusSeat').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
    
}


var settings = {
    rowCssPrefix: 'row-',
    colCssPrefix: 'col-',
    seatWidth: 35,
    seatHeight: 35,
    seatCss: 'seat',
    selectedSeatCss: 'selectedSeat',
    selectingSeatCss: 'selectingSeat'
};

var settingsSleeper = {
    rows: 4,
    cols: 11
};

var settingsSeater = {
    rows: 5,
    cols: 10
};

function LoadSleeperSeatLayout(reservedSeat) {
    var str = [], seatNo, className;
  
    //for lower seat layout
    for (i = 0; i < settingsSleeper.rows; i++) {
        var seatcol = 0;//to count seat columns
        for (j = 0; j < settingsSleeper.cols; j++) {
            var k = i;
            if (i > 2)
                k--;
            if (i != 2) {
                    if (j % 2 == 0)
                    {
                        seatNo = (k + seatcol * (settingsSleeper.rows - 1) + 1);
                        className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString();
                        if ($.isArray(reservedSeat) && $.inArray(seatNo, reservedSeat) != -1) {
                            className += ' ' + settings.selectedSeatCss;
                        }
                        seatcol++;
                        str.push('<li class="' + className + '"' +
                       'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px">' +
                       '<a title="' + seatNo + '">' + seatNo + '</a>' +
                       '</li>');
                    }
                    else {
                        str.push('<li class="blank"' +
                        'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px"></li>');
                    }
                
               
            }
            else
                str.push('<li class="blank"' +
                        'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px"></li>');

        }
    }
    $('#lower').html(str.join(''));


    //for upper seat layout
    str.length = 0;
    var baseSeatNo = seatNo;
    for (i = 0; i < settingsSleeper.rows; i++) {
        var seatcol = 0;//to count seat columns
        for (j = 0; j < settingsSleeper.cols; j++) {
            var k = i;
            if (i > 2)
                k--;
            if (i != 2) {
                if (j % 2 == 0) {
                    seatNo = (baseSeatNo+ k + seatcol * (settingsSleeper.rows - 1) + 1);
                    className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString();
                    if ($.isArray(reservedSeat) && $.inArray(seatNo, reservedSeat) != -1) {
                        className += ' ' + settings.selectedSeatCss;
                    }
                    seatcol++;
                    str.push('<li class="' + className + '"' +
                   'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px">' +
                   '<a title="' + seatNo + '">' + seatNo + '</a>' +
                   '</li>');
                }
                else {
                    str.push('<li class="blank"' +
                    'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px"></li>');
                }


            }
            else
                str.push('<li class="blank"' +
                        'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px"></li>');

        }
    }
    $('#upper').html(str.join(''));

};


function LoadSeaterSeatLayout (reservedSeat) {
    var str = [], seatNo, className;
    for (i = 0; i < settingsSeater.rows; i++) {
        for (j = 0; j < settingsSeater.cols; j++) {
            var k = i;
            if (i > 2)
                k--;
            if (i != 2) {
                seatNo = (k + j * (settingsSeater.rows-1) + 1);
                className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString();
                if ($.isArray(reservedSeat) && $.inArray(seatNo, reservedSeat) != -1) {
                    className += ' ' + settings.selectedSeatCss;
                }
                str.push('<li class="' + className + '"' +
                'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px">' +
                '<a title="' + seatNo + '">' + seatNo + '</a>' +
                '</li>');
            }
            else
                str.push('<br/>');
            
        }
    }
    $('#seater').html(str.join(''));
    //bindClickEvent(maxSeat);
};
//case I: Show from starting
//init();

//Case II: If already booked
//var bookedSeats = [5, 10, 25];
//init(bookedSeats);

function BindClickEvent(maxSeat) {

    $('.' + settings.seatCss).click(function () {
        if ($(this).hasClass(settings.selectedSeatCss)) {
            //alert('This seat is already reserved');
        }
        else {
            var count = selectedSeats.length;
            if ($(this).hasClass(settings.selectingSeatCss)) {
                $(this).removeClass(settings.selectingSeatCss);
                var index = selectedSeats.indexOf(this.childNodes[0].title);
                selectedSeats.splice(index,1);
                ShowSelectedSeats();
                ShowTotalFare(--count);
            }
            else {
                 
                if (count < maxSeat) {
                    $(this).addClass(settings.selectingSeatCss);
                    selectedSeats.push(this.childNodes[0].title);
                    ShowSelectedSeats();
                    
                    ShowTotalFare(++count);
                }
                else {
                    alert("You cannot select more than " + maxSeat + " seats");
                }
            }
            if (count > 0)
                $("#Button_Continue").removeAttr('disabled');
            else
            $("#Button_Continue").attr('disabled', 'disabled');
                

        }
    });

}

function ShowSelectedSeats() {
    $("#Label_SelectedSeats").text(selectedSeats.join());
    $("#HiddenField_SelectedSeats").value = $("#Label_SelectedSeats").text();
}

function ShowTotalFare(count) {
    var totalFare = count * parseInt($("#HiddenField_Fare").val());
        $("#Label_TotalFare").text(totalFare);
}

function saveSeatsToHiddenField() {
    $("#HiddenField_SeatsSelected").val($("#Label_SelectedSeats").text());
    return true;
}