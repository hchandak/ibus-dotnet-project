﻿var validTravelName = /^[a-zA-Z0-9 ]*[a-zA-Z ]+[a-zA-Z0-9 ]*$/;
var validEmail = /[a-z0-9!#$%&'*+/=?^{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)\b/
var validContact = /^\+?([0-9]+[ -]?){5,}[0-9]+$/;
var validAddress = /[0-9]|[A-Z/a-z]| |#|,|/;
$(document).ready(function () {

      //Travel name validation 
    $('#TextBox_TravelName').change(function () {
        if (!validTravelName.test(this.value)) {
            $('#TextBox_TravelName').val('');
            $('#TextBox_TravelName').focus();
            $('#TextBox_TravelName').popover({
                content: 'Travel name, should not contain special characters.',
                trigger: 'manual'
            });
            $(this).popover('show');

        }
        else {
            $(this).popover('hide');
        }
                           
    });

    //Travel address validation
    $('#TextBox_TravelAddress').change(function () {
        if (!validAddress.test(this.value)) {
            $('#TextBox_TravelAddress').val('');
            $('#TextBox_TravelAddress').focus();
            $('#TextBox_TravelAddress').popover({
                content: 'Please enter valid travel address.',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }

    });

    //Travel email validation
    $('#TextBox_TravelEmail').change(function () {
        if (!validEmail.test(this.value)) {
            $('#TextBox_TravelEmail').val('');
            $('#TextBox_TravelEmail').focus();
            $('#TextBox_TravelEmail').popover({
                content: 'Please enter valid email address.',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }

    });

    //Travel contact validation
    $('#TextBox_TravelContact').change(function () {
        if (!validContact.test(this.value)) {
            $('#TextBox_TravelContact').val('');
            $('#TextBox_TravelContact').focus();
            $('#TextBox_TravelContact').popover({
                content: 'Please enter valid contact number.',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }

    });

    //Bus capacity validation
    $('#TextBox_BusCapacity').change(function () {
        if (!validBusCapacit.test(this.value)) {
            $('#TextBox_BusCapacity').val('');
            $('#TextBox_BusCapacity').focus();
            $('#TextBox_BusCapacity').popover({
                content: 'Bus capacity shoud be in range between (30-255).',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }
    });

    //Trip fare validation
    $('#TextBox_TripFare').change(function () {
        if (!validTripFare.test(this.value)) {
            $('#TextBox_TripFare').val('');
            $('#TextBox_TripFare').focus();
            $('#TextBox_TripFare').popover({
                content: 'Please enter valid bus fare, Should contain only numbers.',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }
    });

});

// Required and Regular expression validations For Add Travel..
function ValidateTravelDetail() {
    var travelName = $('#TextBox_TravelName').val();
    var travelAddress = $('#TextBox_TravelAddress').val();
    var travelEmail = $('#TextBox_TravelEmail').val();
    var travelContact = $('#TextBox_TravelContact').val();
    var strErrorMessage = "";
    if (!validTravelName.test(travelName) || travelName.length == 0) {
        strErrorMessage += "Travel name* ,"
        $('#TextBox_TravelName').val('');
        $('#TextBox_TravelName').focus();
    }
    else if (!validAddress.test(travelAddress) || travelAddress.length == 0) {
        strErrorMessage += "Travel Address* ,"
        $('#TextBox_TravelAddress').val('');
        $('#TextBox_TravelAddress').focus();
    }
    else if (!validEmail.test(travelEmail) || travelEmail.length == 0) {
        strErrorMessage += "Travel email* ,"
        $('#TextBox_TravelEmail').val('');
        $('#TextBox_TravelEmail').focus();
    }
    else if (!validContact.test(travelContact) || travelContact.length == 0) {
        strErrorMessage += "Travel contact*."
        $('#TextBox_TravelContact').val('');
        $('#TextBox_TravelContact').focus();
    }
    if (strErrorMessage.length > 0) {
        $('#Div_ErrorMessageAdmin').css('display', 'block');
        $('#Div_ErrorMessageAdmin').html('Info! All (*) field(s) are required. Please fill all required detail before you proceed.');
        return false;
    }
    return true;
}

function RedirectToUpdateTravels(ID) {
    window.location.href = "add-update-travel.aspx?travelID=" + ID;
}

var travelID = null;
function ConfirmDeleteTravelDetail(ID) {
    travelID = ID;
    if ($('#Button_DeleteTravel').css('display') == 'none') {
        $('#Button_DeleteTravel').css('display', 'block');
    }
    $('#Div_MessageBody').html('Warning! Are you sure, you want to delete this travel?');
    $('#Modal_DispalyMessage').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
}

function DeleteTravelDetail() {
   
    $.ajax({
        async: true,
        type: "post",
        url: "travel-detail.aspx/DeleteTravelDetail",
        data: '{travelID: "' + parseInt(travelID) + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            
            if (data.d > 0) {
                $('#Div_SucessMessageAdmin').css('display', 'block');
                var oTable = $('#tblTravelList').dataTable();
                oTable.fnDeleteRow($('#trTravelDetailRow' + travelID)[0]);
                $('#Div_SucessMessageAdmin').html('Success! Selected travel has been deleted successfully.')
            }
            else {
                $('#Div_ErrorMessageAdmin').css('display', 'block');
                $('#Div_ErrorMessageAdmin').html('Failure! Selected travel has not been deleted, Due to some Database issue.');
            }
        },
        error: function (msg) {
            $('#Div_ErrorMessageAdmin').css('display', 'block');
            $('#Div_ErrorMessageAdmin').html('Failure! Error encountered while deleting selected travel.')
        }
    });
    return false;
    
}