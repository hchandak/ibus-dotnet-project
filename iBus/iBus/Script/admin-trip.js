﻿var validTripFare = /^(\d*([,](?=\d{3}))?\d+)+((?!\2)[,]\d\d)?$/;
var timeDuration = /^(0?[1-9]|[1-9][0-9]):[0-5][0-9]$/;
var time24Hours = /^(0?[0-9]|[0-2][0-3]):[0-5][0-9]$/;
var validName = /^[a-zA-Z0-9 ]*[a-zA-Z ]+[a-zA-Z0-9 ]*$/;
var validAddress = /[0-9]|[A-Z/a-z]| |#|,|/;
$(document).ready(function () {

      //Trip fare validation
    $('#TextBox_TripFare').change(function () {
        if (!validTripFare.test(this.value)) {
            $('#TextBox_TripFare').val('');
            $('#TextBox_TripFare').focus();
            $('#TextBox_TripFare').popover({
                content: 'Fare should contain only integer numbers.',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }
    });                   

    //Trip departure time validation
    $('#TextBox_DepartureTime').change(function () {
        if (!time24Hours.test(this.value)) {
            $('#TextBox_DepartureTime').val('');
            $('#TextBox_DepartureTime').focus();
            $('#TextBox_DepartureTime').popover({
                content: 'Departure time should be in range of (01:00) to (23:59).',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }
    });

    //Trip Journey duration validation
    $('#TextBox_JourneyDuration').change(function () {
        if (!timeDuration.test(this.value)) {
            $('#TextBox_JourneyDuration').val('');
            $('#TextBox_JourneyDuration').focus();
            $('#TextBox_JourneyDuration').popover({
                content: 'Journey duration should be in range of (01:00) to (99:59).',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }
    });

    //Boarding point name validation 
    $('#TextBox_BoardingPointName').change(function () {
        if (!validName.test(this.value)) {
            $('#TextBox_BoardingPointName').val('');
            $('#TextBox_BoardingPointName').focus();
            $('#TextBox_BoardingPointName').popover({
                content: 'Boarding point name, should not contain special characters.',
                trigger: 'manual'
            });
            $(this).popover('show');

        }
        else {
            $(this).popover('hide');
        }

    });

    //Boarding point address validation 
    $('#TextBox_BoardingPointAddress').change(function () {
        if (!validAddress.test(this.value)) {
            $('#TextBox_BoardingPointAddress').val('');
            $('#TextBox_BoardingPointAddress').focus();
            $('#TextBox_BoardingPointAddress').popover({
                content: 'Please enter valid boarding point address.',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }

    });

    //boarding point departure time validation
    $('#TextBox_BoardingPointTime').change(function () {
        if (!time24Hours.test(this.value)) {
            $('#TextBox_BoardingPointTime').val('');
            $('#TextBox_BoardingPointTime').focus();
            $('#TextBox_BoardingPointTime').popover({
                content: 'Departure time range is (01:00) to (23:59).',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }
    });


});

function ValidateTripDetailForSave() {

    var tripRoute = $('#DropDownList_TripRoute').val();
    var travelID = $('#DropDownList_TripTravelName').val();
    var tripFare = $('#TextBox_TripFare').val();
    var tripDepartureTime = $('#TextBox_DepartureTime').val();
    var journeyDuration=$('#TextBox_JourneyDuration').val();
    var tripFrequency = $('#DropDownList_Frequency').val();
    var strErrorMessage = "";
    if (parseInt(travelID) <= 0) {
        strErrorMessage += "Travel Name* ,";
        $('#DropDownList_TripTravelName').focus();
    }
    else if (parseInt(tripRoute) <= 0) {
        strErrorMessage += "Route* ,";
        $('#DropDownList_TripRoute').focus();
    }
    else if (!validTripFare.test(tripFare) || tripFare.length <= 0) {
        strErrorMessage += "Fare* ,";
        $('#TextBox_TripFare').val('');
        $('#TextBox_TripFare').focus();
    }
    else if (parseInt(tripFrequency) <= 0) {
        strErrorMessage += "Frequency* ,";
        $('#DropDownList_Frequency').focus();
    }
    else if (!time24Hours.test(tripDepartureTime) || tripDepartureTime.length <= 0) {
        strErrorMessage += "DepartureTime* ,";
        $('#TextBox_DepartureTime').val('');
        $('#TextBox_DepartureTime').focus();
    }
    else if (!timeDuration.test(journeyDuration) || tripFare.length <= 0) {
        strErrorMessage += "journeyDuration* ,";
        $('#TextBox_JourneyDuration').val('');
        $('#TextBox_JourneyDuration').focus();
    }
   
    if (strErrorMessage.length > 0) {
        $('#Div_ErrorMessageAdmin').css('display', 'block');
        $('#Div_ErrorMessageAdmin').html('Info! All (*) field(s) are required. Please fill all required detail before you proceed.');
        return false;
    }
    return true;
}

function ValidateBoardingPoint() {
    var boardingPointName = $('#TextBox_BoardingPointName').val();
    var boardingPointAddress = $('#TextBox_BoardingPointAddress').val();
    var boardingPointTime = $('#TextBox_BoardingPointTime').val();
   
    var strErrorMessage = "";
    if (!validName.test(boardingPointName) || boardingPointName.length <= 0) {
        strErrorMessage += "Name* ,";
        $('#TextBox_BoardingPointName').val('');
        $('#TextBox_BoardingPointName').focus();
    }
    else if (!validAddress.test(boardingPointAddress) || boardingPointName.length <= 0) {
        strErrorMessage += "Name* ,";
        $('#TextBox_BoardingPointAddress').val('');
        $('#TextBox_BoardingPointAddress').focus();
    }
    else if (!time24Hours.test(boardingPointTime) || boardingPointTime.length <= 0) {
        strErrorMessage += "Time* ,";
        $('#TextBox_BoardingPointTime').val('');
        $('#TextBox_BoardingPointTime').focus();
    }
    

    if (strErrorMessage.length > 0) {
        $('#Div_ErrorMessageAdmin').css('display', 'block');
        $('#Div_ErrorMessageAdmin').html('Info! All (*) field(s) are required. Please fill all required detail before you proceed.');
        return false;
    }
    return true;
}

function ValidateTripDetailForUpdate() {

    var tripFare = $('#TextBox_TripFare').val();
    var tripDepartureTime = $('#TextBox_DepartureTime').val();
    var journeyDuration = $('#TextBox_JourneyDuration').val();
    var tripFrequency = $('#DropDownList_Frequency').val();
    var strErrorMessage = "";
    if (!validTripFare.test(tripFare) || tripFare.length <= 0) {
        strErrorMessage += "Fare* ,";
        $('#TextBox_TripFare').val('');
        $('#TextBox_TripFare').focus();
    }
    else if (parseInt(tripFrequency) <= 0) {
        strErrorMessage += "Frequency* ,";
        $('#DropDownList_Frequency').focus();
    }
    else if (!time24Hours.test(tripDepartureTime) || tripDepartureTime.length <= 0) {
        strErrorMessage += "DepartureTime* ,";
        $('#TextBox_DepartureTime').val('');
        $('#TextBox_DepartureTime').focus();
    }
    else if (!timeDuration.test(journeyDuration) || tripFare.length <= 0) {
        strErrorMessage += "journeyDuration* ,";
        $('#TextBox_JourneyDuration').val('');
        $('#TextBox_JourneyDuration').focus();
    }

    if (strErrorMessage.length > 0) {
        $('#Div_ErrorMessageAdmin').css('display', 'block');
        $('#Div_ErrorMessageAdmin').html('Info! All (*) field(s) are required. Please fill all required detail before you proceed.');
        return false;
    }
    return true;
}


function RedirectToUpdateTrip(ID) {
    window.location.href = "add-update-trip.aspx?tripID=" + ID;
}

var tripID = null;
function ConfirmDeleteTripDetail(ID) {
    tripID = ID;
    if ($('#Button_DeleteTrip').css('display') == 'none') {
        $('#Button_DeleteTrip').css('display', 'block');
    }
    $('#Div_MessageBody').html('Warning! Are you sure, you want to delete this trip?');
    $('#Modal_DispalyMessage').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
}

function DeleteTripDetail() {
    $.ajax({
        async: true,
        type: "post",
        url: "trip-detail.aspx/DeleteTripDetail",
        data: '{tripID: "' + parseInt(tripID) + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d > 0) {
                $('#Div_SucessMessageAdmin').css('display', 'block');
                var oTable = $('#tblTripList').dataTable();
                oTable.fnDeleteRow($('#trTripDetailRow' + travelID)[0]);
                $('#Div_SucessMessageAdmin').html('Success! Selected trip has been deleted successfully.')
            }
            else {
                $('#Div_ErrorMessageAdmin').css('display', 'block');
                $('#Div_ErrorMessageAdmin').html('Failure! Selected trip has not been deleted, Due to some Database issue.');
            }

        },
        error: function () {
            $('#Div_ErrorMessageAdmin').css('display', 'block');
            $('#Div_ErrorMessageAdmin').html('Failure! Error encountered while deleting selected trip.')
        }
    });
    return false;

}

