﻿var validBusCapacit = /^([30]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/;

var error = null;
$(document).ready(function () {

 
    //Bus capacity validation
    $('#TextBox_BusCapacity').change(function () {
        if (!validBusCapacit.test(this.value)) {
            $('#TextBox_BusCapacity').val('');
            $('#TextBox_BusCapacity').focus();
            $('#TextBox_BusCapacity').popover({
                content: 'Bus capacity should be in range between (30-255).',
                trigger: 'manual'
            });
            $(this).popover('show');
        }
        else {
            $(this).popover('hide');
        }
    });

    //Fix bus capacity for sleeper=36 and non sleeper=40
    $('#DropDownList_BusType').change(function () {
        var busTypeID = $("#DropDownList_BusType option:selected").val();
        if (busTypeID == 1 || busTypeID == 3) {
            $('#TextBox_BusCapacity').val('36');
        }
        else if (busTypeID == 2 || busTypeID == 4) {
            $('#TextBox_BusCapacity').val('40');
        }
        else {
            $('#TextBox_BusCapacity').val('');
        }
    });
      
});

                      
function ValidateBusDetail() {
    var capacity = $('#TextBox_BusCapacity').val();
    var busTypeID = $('#DropDownList_BusType').val();
    var travelID = $('#DropDownList_BusTravelName').val();

    var strErrorMessage = "";
    if (!$('#DropDownList_BusType').is(':disabled') && parseInt(busTypeID) <= 0) {
        strErrorMessage += "Bus type* ,";
        $('#DropDownList_BusType').focus();

    }
    else if (!validBusCapacit.test(capacity) || capacity <= 0) {
        strErrorMessage += "Capacity* ,";
        $('#TextBox_BusCapacity').val('');
        $('#TextBox_BusCapacity').focus();
    }

    else if (!$('#DropDownList_BusTravelName').is(':disabled') && parseInt(travelID) <= 0) {
        strErrorMessage += "Travel name* ,";
        $('#DropDownList_BusTravelName').focus();

    }
    if (strErrorMessage.length > 0) {
        $('#Div_ErrorMessageAdmin').css('display', 'block');
        $('#Div_ErrorMessageAdmin').html('Info! All (*) field(s) are required. Please fill all required detail before you proceed.');
        return false;
    }
    return true;
}

function RedirectToUpdateBus(ID) {
    window.location.href = "add-update-bus.aspx?busID=" + ID;
}

var busID = null;
function ConfirmDeleteBusDetail(ID) {
    busID = ID;
    if ($('#Button_DeleteBus').css('display') == 'none') {
        $('#Button_DeleteBus').css('display', 'block');
    }
    $('#Div_MessageBody').html('Warning! Are you sure, you want to delete this bus?');
    $('#Modal_DispalyMessage').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
}
function DeleteBusDetail() {
    $.ajax({
        async: true,
        type: "post",
        url: "bus-detail.aspx/DeleteBusDetail",
        data: '{busID: "' + parseInt(busID) + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d > 0) {
                $('#Div_SucessMessageAdmin').css('display', 'block');
                var oTable = $('#tblBusList').dataTable();
                oTable.fnDeleteRow($('#trBusDetailRow' + travelID)[0]);
                $('#Div_SucessMessageAdmin').html('Success! Selected bus has been deleted successfully.')
            }
            else {
                $('#Div_ErrorMessageAdmin').css('display', 'block');
                $('#Div_ErrorMessageAdmin').html('Failure! Selected bus has not been deleted, Due to some Database issue.');
            }
        },
        error: function () {
            $('#Div_ErrorMessageAdmin').css('display', 'block');
            $('#Div_ErrorMessageAdmin').html('Failure! Error encountered while deleting selected bus.')

        }
    });
    return false;
}