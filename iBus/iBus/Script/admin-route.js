﻿$(document).ready(function () {
   
});

function RedirectToUpdateRoute(ID) {
    window.location.href = "add-update-route.aspx?routeID=" + ID;
}

var routeID = null;
function ConfirmDeleteRouteDetail(ID) {
    routeID = ID;
    if ($('#Button_DeleteRoute').css('display') == 'none') {
        $('#Button_DeleteRoute').css('display', 'block');
    }
    $('#Div_MessageBody').html('Warning! Are you sure, you want to delete this route?');
    $('#Modal_DispalyMessage').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });                
}
function DeleteRouteDetail() {
    $.ajax({
        async: true,
        type: "post",
        url: "route-detail.aspx/DeleteRouteDetail",
        data: '{routeID: "' + parseInt(routeID) + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d > 0) {
                $('#Div_SucessMessageAdmin').css('display', 'block');
                var oTable = $('#tblRouteList').dataTable();
                oTable.fnDeleteRow($('#trRouteDetailRow' + travelID)[0]);
                $('#Div_SucessMessageAdmin').html('Success! Selected route has been deleted successfully.')
            }
            else {
                $('#Div_ErrorMessageAdmin').css('display', 'block');
                $('#Div_ErrorMessageAdmin').html('Failure! Selected route has not been deleted, Due to some Database issue.');
            }
        },
        error: function () {
            $('#Div_ErrorMessageAdmin').css('display', 'block');
            $('#Div_ErrorMessageAdmin').html('Failure! Error encountered while deleting selected route.')

        }
    });
    return false;

}
