﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="profile-page.aspx.cs" Inherits="iBus.profile_page" MasterPageFile="~/MasterPage/default.Master" %>

<asp:Content ID="ProfilePageHead" ContentPlaceHolderID="head" runat="server">
    <link href="/Style/customer.css" rel="stylesheet" />
    <script src="Script/profile-page.js"></script>
    <script src="DataTables/js/jquery.dataTables.min.js"></script>
    <link href="DataTables/css/demo_table.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tblReservationHistory').dataTable({
                "sPaginationType": "full_numbers"
            });
        });
    </script>
</asp:Content>

<asp:Content ID="ProfilePageBody" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="alert alert-success" id="Div_PassengerList_SucessMessage" clientidmode="Static" runat="server" style="display:none"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                Welcome 
                <asp:Label ID="lbl_firstName" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lbl_lastName" runat="server"></asp:Label>
              
                <asp:Label ID="lbl_userName" runat="server"></asp:Label>
              
                <asp:Label ID="lbl_emailId" runat="server"></asp:Label>
                <asp:Label ID="lbl_mobileNo" runat="server"></asp:Label>
            </div>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12">
                <%--<asp:Button ID="Button_ShowHistory" runat="server" Text="Show History" CssClass="btn btn-small btn-primary" ClientIDMode="Static" OnClick="Button_ShowHistory_Click" /><br />--%>
                <br />

                <div class="table-background">
                    <asp:Repeater ID="PassengerHistory" runat="server">

                        <HeaderTemplate>
                            <table border="1" id="tblReservationHistory">
                                <thead >
                                <tr>
                                    <th class="tiny_control"><b>PNR</b></th>
                                    <th class="small_control"><b>Trip Code</b></th>
                                    <th class="small_control"><b>Booked Seat</b></th>
                                    <th class="medium_control"><b>Source</b></th>
                                    <th class="medium_control"><b>Destination</b></th>
                                    <th class="small_control"><b>Departure</b></th>
                                    <th class="small_control"><b>Reservation Date</b></th>
                                    <th class="medium_control"><b>Journey Date</b></th>
                                    <th class="small_control"><b>Total Fare</b></th>
                                    <th class="small_control"><b>Status</b></th>
                                    <th class="medium_control"><b>Options</b></th>
                                </tr>
                               </thead>
                               <tbody>
                        </HeaderTemplate>

                        <ItemTemplate>
                            <tr>
                                <td class="tiny_control"><%# Eval("PNR") %> </td>
                                <td class="small_control"><%# Eval("TripCode") %> </td>
                                <td class="small_control"><%# Eval("NumberofSeats") %></td>
                                <td class="medium_control"><%# Eval("Source") %></td>
                                <td class="medium_control"><%# Eval("Destination") %></td>
                                <td class="small_control"><%# Eval("str_dept_time") %></td>
                                <td class="small_control"><%# Eval("str_DOR") %></td>
                                <td class="medium_control"><%# Eval("str_DOJ") %></td>
                                <td class="small_control"><%# Eval("Fare") %></td>
                                <td class="small_control"><%# Eval("IsActive") %></td>
                                <td class="medium_control">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <asp:LinkButton ID="Link_Download_Ticket" runat="server" CommandArgument='<%# Eval("PNR") %>' OnCommand="Link_Download_Ticket_Command"><img style="height:35px;width:35px" src="Image/download-logo.png" title="Download Ticket"/></asp:LinkButton>
                                        </div>
                                        <div class="col-md-3 ">
                                            <asp:LinkButton ID='Link_Email_Ticket' runat="server" CommandArgument='<%# Eval("PNR") %>' OnCommand="Link_Email_Ticket_Command"><img style="height:35px;width:35px" src="Image/email.png" title="Email Ticket" /></asp:LinkButton>
                                        </div>
                                        <div class="col-md-3 ">
                                           <%-- <asp:LinkButton ID='Link_Cancel_Ticket' runat="server" CommandArgument='<%# Eval("PNR") %>' OnCommand="Link_Cancel_Ticket_Command"><img style="height:35px;width:35px" src="Image/cancel-ticket.png" title="Cancel Ticket" /></asp:LinkButton>--%>
                                            <asp:LinkButton ID='Link_Cancel_Ticket_Passenger_Wise' runat="server"  CommandArgument='<%# Eval("PNR") %>' OnCommand="Link_Cancel_Ticket_Passenger_Wise_Command"><img style="height:35px;width:35px" src="Image/cancel-ticket.png" title="Cancel Ticket" /></asp:LinkButton>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>

                        <FooterTemplate>
                   </tbody> </table>
               </FooterTemplate>
                    </asp:Repeater>
                </div>
              

                <div id="Div_Ticket_Model" style="display: none">
                    <asp:Panel ID="Panel_Ticket" runat="server">
                        <h1>i-Bus</h1>
                        Online Booking System.
         
        <br />
                        <br />
                        <asp:Label ID="lbl_Payment_Note"><b>Note: Payment has to be done at Boarding point.</b></asp:Label>
                        <table id="table_ticket">

                            <tr>
                                <td colspan="2" style="background-color: #18B5F0; height: 18px; color: White; border: 1px solid white">
                                    <b>Ticket Details</b>
                                </td>
                            </tr>
                            <tr>
                                <td><b>PNR No.:</b></td>

                                <td>
                                    <asp:Label ID="lblPNR" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td><b>Trip Code:</b></td>
                                <td>
                                    <asp:Label ID="lblTripCode" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td><b>No. of Seats:</b></td>
                                <td>
                                    <asp:Label ID="lblNoofSeats" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td><b>Passenger Start Palce:</b></td>
                                <td>
                                    <asp:Label ID="lblStartPlace" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td><b>Departure Time:</b></td>
                                <td>
                                    <asp:Label ID="lblDepartureTime" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td><b>Passenger End Palce:</b></td>
                                <td>
                                    <asp:Label ID="lblEndPlace" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td><b>Date of Reservation:</b></td>
                                <td>
                                    <asp:Label ID="lblDateOfReservation" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td><b>Date of Journey:</b></td>
                                <td>
                                    <asp:Label ID="lblDateOfJourney" runat="server"></asp:Label></td>
                            </tr>
                            <%--<tr>
                                <td><b>Boarding Point:</b></td>
                                <td>
                                    <asp:Label ID="lblBoardingPoint" runat="server"></asp:Label></td>
                            </tr>--%>
                            <tr>
                                <td><b>Total Fare:</b></td>
                                <td>
                                    <asp:Label ID="lblTotalFare" runat="server"></asp:Label></td>
                            </tr>
                        </table>

                        <br />
                        <br />

                        <asp:Repeater ID="PassengerDetail" runat="server">

                            <HeaderTemplate>
                                <table border="1">
                                    <tr>
                                        <th><b>First Name</b></th>
                                        <th><b>Last Name</b></th>
                                        <th><b>Gender</b></th>
                                        <th><b>Age</b></th>
                                        <th><b>Seat No.</b></th>
                                    </tr>
                            </HeaderTemplate>

                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("FirstName") %> </td>
                                    <td><%# Eval("LastName") %> </td>
                                    <td><%# Eval("Gender") %></td>
                                    <td><%# Eval("Age") %></td>
                                    <td><%# Eval("SeatID") %></td>
                                </tr>
                            </ItemTemplate>

                            <FooterTemplate>
                                </table>
                            </FooterTemplate>

                        </asp:Repeater>


                        <br />
                        <br />

                        <p style="color: black"><b>Terms and Conditions:</b></p>
                        <br />
                        <p style="color: black">
                            1.iBus is ONLY a bus ticket agent. It does not operate bus services of its own. In order to provide a comprehensive choice of bus operators, departure times and prices to customers, it has tied up with many bus operators.
            iBus advice to customers is to choose bus operators they are aware of and whose service they are comfortable with.
                        </p>
                        <p style="color: black">
                            2.Passengers are required to furnish the following at the time of boarding the bus:<br />
                            (1) A copy of the ticket (A print out of the ticket or the print out of the ticket e-mail).<br />
                            (2) A valid identity proof(eg.Driving Licence or PAN Card)<br />
                            Failing to do so, they may not be allowed to board the bus.
                        </p>

                    </asp:Panel>

                </div>

       
            </div>
        </div>
       
    </div>
</asp:Content>
