﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessModel;
using BussinessLogic;
using System.Data;
using System.ComponentModel;
using DataValidation;
using System.Web.UI.HtmlControls;

namespace iBus
{
    public partial class bus_list : System.Web.UI.Page
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static List<ShowBusListModelModified> listShowBusListOrigin = new List<ShowBusListModelModified>();
        static List<ShowBusListModelModified> listShowBusListWorking = new List<ShowBusListModelModified>();
        static string strCities;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Message"] != null)
            {
                Div_ErrorMessageLoginRegister.Style["display"] = "block";
                Div_ErrorMessageLoginRegister.InnerHtml = Session["Message"].ToString();
                Session["Message"] = null;

            }

            if (!IsPostBack)
            {
                try
                {
                    string strSeatPopUp = string.Empty;
                    if (listShowBusListOrigin != null)
                        listShowBusListOrigin.Clear();
                    if (listShowBusListOrigin != null)
                        listShowBusListWorking.Clear();
                    BusSearchBL busSearchBLObj = new BusSearchBL();
                    BusSearchModel busSearchModelObj = (BusSearchModel)Session["busSearchDetails"];

                    //BusSearchModel busSearchModelObj = new BusSearchModel();
                    //busSearchModelObj.FromCity = "Surat";
                    //busSearchModelObj.ToCity = "Mumbai";
                    //busSearchModelObj.DateOfJourney = "10/13/2013";

                    LoadTitleTabs(busSearchModelObj);

                    listShowBusListOrigin = busSearchBLObj.GetShowBusList(busSearchModelObj);
                    listShowBusListWorking = listShowBusListOrigin;


                    Display_BusList();
                    

                    List<string> FilterListNames = busSearchBLObj.GetTravelNamesList();
                    CheckBoxList_BusOperator.DataSource = FilterListNames;
                    CheckBoxList_BusOperator.DataBind();

                    FilterListNames = busSearchBLObj.GetBusTypeNameList();
                    CheckBoxList_BusType.DataSource = FilterListNames;
                    CheckBoxList_BusType.DataBind();


                    strCities = busSearchBLObj.GetCityListString();
                    HiddenField_Cities.Value = strCities;
                }
                catch (Exception ex)
                {
                    Logger.Error("\n\nError Exception message->" + ex.Message + "\n  Exception source->" + ex.Source + "\n  Exception stack trace->" + ex.StackTrace + "\n  Exception TargetSite->" + ex.TargetSite);
                }
                finally
                {

                }

                
                
            }
                
        }

        protected void LoadTitleTabs(BusSearchModel busSearchModelObj)
        {
            try
            {
                Label_FromCitySelected.Text = busSearchModelObj.FromCity;
                Label_ToCitySelected.Text = busSearchModelObj.ToCity;
                Label_JourneyDateSelected.Text = busSearchModelObj.DateOfJourney;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Exception message->" + ex.Message + "\n  Exception source->" + ex.Source + "\n  Exception stack trace->" + ex.StackTrace + "\n  Exception TargetSite->" + ex.TargetSite);
            }
            finally
            {

            }
            
            
        }
        protected void Button_BookSeat_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["userID"] != null && Session["userID"].ToString().Trim().Length > 0)
                {
                    int dailyTripID = Convert.ToInt32(HiddenField_SelectedDailyTripID.Value);
                    ShowBusListModelModified selectedBusDetail = listShowBusListWorking.SingleOrDefault(x => x.DailyTripID == dailyTripID);


                    Session["selectedBusDetail"] = selectedBusDetail;


                    modalselectseats.LoadPopUP();

                    string str_SemiSleeper = "Semi-Sleeper";
                    string str_BusType = "Sleeper";
                    if (selectedBusDetail.BusType.Contains(str_SemiSleeper))
                        str_BusType = str_SemiSleeper;
                    
                       

                    //Reservation_PassengerBL resPassObj = new Reservation_PassengerBL();
                    //string str_ReservedSeat = resPassObj.GetReservedSeats(selectedBusDetail.DailyTripID);
                    BookSeatBL bookSeatObj = new BookSeatBL();
                    int maxSeat = bookSeatObj.GetMaxNoOfSeats(selectedBusDetail.AvailableSeats);
                    string str_ReservedSeat = bookSeatObj.GetReservedSeats(dailyTripID);

                    //str_BusType = "Semi-Sleeper";
                    //str_ReservedSeat = "1,5,17";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "ShowModalSeat([" + str_ReservedSeat + "]," + maxSeat + ",'"+str_BusType+"');", true);

                  
                }
                else
                    ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "ShowModalSignInSignUp();", true);
        
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Exception message->" + ex.Message + "\n  Exception source->" + ex.Source + "\n  Exception stack trace->" + ex.StackTrace + "\n  Exception TargetSite->" + ex.TargetSite);
            }
            finally
            {

            }

           
            
            
        }

        protected void Display_BusList() {
            if (listShowBusListWorking!=null && listShowBusListWorking.Count != 0)
            {
                Repeater_BusList.Visible = true;
                Div_NoRecordsFound.Style["display"] = "none";
                Repeater_BusList.DataSource = listShowBusListWorking;
                Repeater_BusList.DataBind();
            }
            else
            {
                Repeater_BusList.Visible = false;
                Div_NoRecordsFound.Style["display"] = "block";
              
            }
        }
       

        protected void Filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Repeater_BusList.Visible = true;
                bool flag = false;
                List<ShowBusListModelModified> busOperatorFilteredList = new List<ShowBusListModelModified>();
                List<ShowBusListModelModified> busTypeFilteredList = new List<ShowBusListModelModified>();
                listShowBusListWorking = listShowBusListOrigin;
                //showbusList = listShowBusListWorking;
                foreach (ListItem item in CheckBoxList_BusOperator.Items)
                {

                    if (item.Selected)
                    {
                        flag = true;
                        busOperatorFilteredList = busOperatorFilteredList.Concat((from bus in listShowBusListWorking where bus.TravelName == item.Text select bus).ToList()).ToList();
                    }
                }
                if (flag)
                    listShowBusListWorking = busOperatorFilteredList;


                //busTypeFilteredList = busOperatorFilteredList;
                flag = false;
                foreach (ListItem item in CheckBoxList_BusType.Items)
                {

                    if (item.Selected)
                    {
                        flag = true;
                        busTypeFilteredList = busTypeFilteredList.Concat((from bus in listShowBusListWorking where bus.BusType == item.Text select bus).ToList()).ToList();
                    }
                }
                if (flag)
                    listShowBusListWorking = busTypeFilteredList;

                Display_BusList();
                //GVDisplay.DataSource = listShowBusListWorking;
                //GVDisplay.DataBind();
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Exception message->" + ex.Message + "\n  Exception source->" + ex.Source + "\n  Exception stack trace->" + ex.StackTrace + "\n  Exception TargetSite->" + ex.TargetSite);
            }
            finally
            {

            }

            
        }

        protected void Button_SearchBus_Click(object sender, EventArgs e)
        {
            BusSearchModel busSearchDetailsObj = new BusSearchModel();
            busSearchDetailsObj.FromCity = TextBox_FromCity.Text.Trim().ToLower();
            busSearchDetailsObj.ToCity = TextBox_ToCity.Text.Trim().ToLower();
            busSearchDetailsObj.DateOfJourney = TextBox_DateOfJourney.Text.Trim().ToLower();
            UserValidation userValidationObj = new UserValidation();
            try
            {
                string strValidationResult = userValidationObj.ValidateBusSearch(ref busSearchDetailsObj, strCities);
                if (strValidationResult == AppConstant.SUCCESS.ToString())
                {
                    Session["busSearchDetails"] = busSearchDetailsObj;
                    Response.Redirect("bus-list.aspx",false);
                }
                else
                {
                    Div_ErrorMessageBusSearch.Style["display"] = "block";
                    Div_ErrorMessageBusSearch.InnerHtml = strValidationResult;

                }
            }

            catch (Exception ex)
            {
                Logger.Error("\n\nError Exception message->" + ex.Message + "\n  Exception source->" + ex.Source + "\n  Exception stack trace->" + ex.StackTrace + "\n  Exception TargetSite->" + ex.TargetSite);
            }
        }

        protected void Repeater_BusList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    int amenitiesCount = 5;
                    string[] strAmenities;
                    string[] stringSeparators = new string[] { ", " };
                    string amenities = string.Empty;
                    string strIsActive = string.Empty;
                    HtmlGenericControl labelAmenities = (HtmlGenericControl)e.Item.FindControl("Label_Amenities");
                    HtmlGenericControl labelIsActive = (HtmlGenericControl)e.Item.FindControl("Label_IsActive");
                    amenities = labelAmenities.InnerText;
                    strIsActive = labelIsActive.InnerText;
                    strAmenities = amenities.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    if (!strAmenities.Contains(AppConstant.AMENITIES[0]))
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_WaterBottle"));
                        image.Style["display"] = "none";
                        amenitiesCount--;

                    }
                    if (!strAmenities.Contains(AppConstant.AMENITIES[1]))
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_Movies"));
                        image.Style["display"] = "none";
                        amenitiesCount--;
                    }
                    if (!strAmenities.Contains(AppConstant.AMENITIES[2]))
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_Blancket"));
                        image.Style["display"] = "none";
                        amenitiesCount--;
                    }
                    if (!strAmenities.Contains(AppConstant.AMENITIES[3]))
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_ReadingLight"));
                        image.Style["display"] = "none";
                        amenitiesCount--;

                    }
                    if (!strAmenities.Contains(AppConstant.AMENITIES[4]))
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_CharginPoint"));
                        image.Style["display"] = "none";
                        amenitiesCount--;

                    }
                    if (amenitiesCount == 0)
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_NoAmenities"));
                        image.Style["display"] = "block";
                    }
                    if (strIsActive == "Active")
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_InActive"));
                        image.Style["display"] = "none";
                    }
                    else if (strIsActive == "Not Active")
                    {
                        Image image = new Image();
                        image = (Image)(e.Item.FindControl("Image_Active"));
                        image.Style["display"] = "none";
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusDetail.cs, Function->Repeater_BusList_ItemDataBound  : \n " + ex);
            }
        }

        //protected string ExtractTime(string str_time)
        //{
        //    TimeSpan time =TimeSpan.Parse(str_time);
        //    string formatedTime = string.Format("{0:00}:{1:00}", time.Hours, time.Minutes);
        //    return formatedTime;
        //}

    }
}