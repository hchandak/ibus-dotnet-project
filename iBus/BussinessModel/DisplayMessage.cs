﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessModel
{
    public class DisplayMessage
    {
        #region Common

        public const string REQUIRED_FIELDS = "Info! All (*) filds are required, Please fill all required fields before you proceed.";
        public const string VALIDATION_SUCCESS = "Success! All required fileds contain valid values.";
        public const string REPEATER_RECORD_NOT_FOUND = "Info! Records not found.";
        public const string RECORD_NOT_FOUND = "Info! Selected record not found.";
        public const string INVALID_URL = "Info! Page can not be redirect , invalid URL selection.";
        public const string INVALID_VALUES = "Info! Values are not in valid formate , Please currect all vaues before you proceed.";
        
        #endregion

        #region Admin
        public const string SELECT_BUS_TYPE = "Please select bus type and amenities";
        public const string INVALID_TRAVEL_NAME = "Info! Travels name field contain invalid value, Should not contain special characters and numbers.";
        public const string INVALID_TRAVEL_ADDRESS = "Info! Travels address field  contain invalid value, Please fill valid Travels address before you proceed.";
        public const string INVALID_TRAVEL_EMAIL = "Info! Travels email field contain invalid value, Please fill valid Travels email before you proceed.";
        public const string INVALID_TRAVEL_CONTACT = "Info! Travels contact field contain invalid value, Please fill valid Travels contact before you proceed.";
        public const string INVALID_FROM_CITY = "Info! From city field contain invalid value, Please select valid From city before you proceed.";
        public const string INVALID_TO_CITY = "Info! To city field contain invalid value, Please select valid To city before you proceed.";
        public const string INVALID_BUT_TYPE = "Info! Bus type field contain invalid value, Please select valid Bus type before you proceed.";
        public const string INVALID_BUS_CAPACITY = "Info! Bus capacity field contain invalid value, Should be in range between (30-255).";
        public const string INVALID_DEPARTURE_TIME = "Info! Departure time field contain invalid value. Please enter valid Departure time in (HH:MM) should be in range (01:00) to (23:59) before you proceed.";
        public const string INVALID_JOURNEY_DURATION = "Info! JourneyDuration time field contain invalid value. Please enter valid Journey duration in (HH:MM) should be in range (01:00) to (99:59) before you proceed.";
        public const string INVALID_ROUTE = "Info! Route field contain invalid value, Please select valid Route before you proceed.";
        public const string INVALID_TRIP_FARE = "Info! Fare field contain invalid value, Should contain only numbers, but not in decimal.";


        public const string DB_RECORD_NOT_MATCH = "Info! Your request can not be proceed with given input, Because Database is not updated.";
        public const string SAVE_TRAVEL_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string SAVE_BUS_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string SAVE_ROUTE_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string SAVE_TRIP_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string SAVE_TRAVEL_DETAILS_SUCCESS = "Success! New Travels has been created successfully.";
        public const string SAVE_BUS_DETAILS_SUCCESS = "Success! New Bus has been created successfully.";
        public const string SAVE_ROUTE_DETAILS_SUCCESS = "Success! New Route has been created successfully.";
        public const string SAVE_TRIP_DETAILS_SUCCESS = "Success! New Trip has been created successfully.";

        public const string UPDATE_BUS_DEPENDENCY = "Failure! Your request can not be proceed with given input, Because associative travel is not active";
        public const string UPDATE_TRIP_DEPENDENCY = "Failure! Your request can not be proceed with given input, Because associative travel or bus is not active";
        public const string UPDATE_TRAVEL_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string UPDATE_BUS_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string UPDATE_ROUTE_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string UPDATE_TRIP_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string UPDATE_TRAVEL_DETAILS_SUCCESS = "Success! Travels detail has been updated successfully.";
        public const string UPDATE_BUS_DETAILS_SUCCESS = "Success! Bus detail has been updated successfully.";
        public const string UPDATE_ROUTE_DETAILS_SUCCESS = "Success! Route detail has been updated successfully.";
        public const string UPDATE_TRIP_DETAILS_SUCCESS = "Success! Trip detail has been updated successfully.";

        public const string DELETE_TRAVEL_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string DELETE_BUS_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string DELETE_ROUTE_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string DELETE_TRIP_DETAILS_FAIL = "Failure! Your request can not be proceed with given input, Because Database is not updated.";
        public const string DELETE_TRAVEL_DETAILS_SUCCESS = "Success! Selected Travels has been deleted successfully.";
        public const string DELETE_BUS_DETAILS_SUCCESS = "Success! Selected Bus has been deleted successfully.";
        public const string DELETE_ROUTE_DETAILS_SUCCESS = "Success! Selected Route has been deleted successfully.";
        public const string DELETE_TRIP_DETAILS_SUCCESS = "Success! Selected Trip has been deleted successfully.";

        public const string TRAVEL_NAME_OR_EMAIL_EXIST = "Info! Travel name or email allready exist. Please try with some other travel name or email.";
        public const string ROUTE_EXIST = "Info! Selected route allready exist. Please try with some other route.";
        public const string BUS_SAME_TYPE_AND_AMENITIES_EXIST = "Info! This type of bus with same amenities allready exist. Please try with some other bustype or amenities.";
        public const string TRIP_EXIST = "Info! Given trip detail allready exist with same (Bus, Route and Departure time). Please try with some other details.";

        public const string TRAVEL_IS_IN_USE = "Info! This travel allready in use. you can not delete it.";
        public const string RESOURCE_IS_IN_USE = "Info! This resource allready in use. you can not delete it.";

        #endregion

        #region User
        public const string MAIL_SEND_SUCCESSFULLY = "Success! We have receive your mail. Thanks for your feedback, We will make sure that you will get answer for all your queries.";
        public const string INVALID_USER_EMAIL = "Info! Email field contain invalid value, Please fill valid email before you proceed.";
        public const string INVALID_DATEOFJOURNEY = "Info! Date of journey field contain invalid value, Please select valid date before you proceed.";
        public const string INVALID_FROM_TO_CITY = "Info! Source and Destination cannot be same, Please select valid city before you proceed.";

        public const string USER_ALREADY_REGISTERED = "Registration failed ! You are already registered. ";
        public static string USER_NOT_REGISTERED = "Registration failed ! Fatal error occured. ";
        public const string INVALID_USERID_PASSWORD = "Info! userID password does not match.";
        public const string INVALID_LOGIN_DETAILS = "Info! user does not exist. You should first register. ";
        public const string INVALID_REGISTRATION_EMAIL = "Info! Registration email field contain invalid value, Please fill valid email before you proceed.";
        public const string INVALID_REGISTRATION_FIRST_NAME = "Info! First name field contain invalid value, Please fill travels name before you proceed.";
        public const string INVALID_REGISTRATION_LAST_NAME = "Info! Last name field contain invalid value, Please fill valid name before you proceed.";
        public const string INVALID_REGISTRATION_CONTACT = "Info! Registration contact field contain invalid value, Please fill valid contact before you proceed.";
        public const string INVALID_REGISTRATION_PASSWORD_CONFIRMPASSWORD_DOESNT_MATCH = "Info! Registration password and confirm password does not match, Please fill same password before you proceed.";
        public const string INVALID_PASSENGER_FIRST_NAME = "Info! First name field contain invalid value, Please fill valid name before you proceed.";
        public const string INVALID_PASSENGER_LAST_NAME = "Info! Last name field contain invalid value, Please fill valid name before you proceed.";
        public const string INVALID_PASSENGER_AGE = "Info! Age field contain invalid value, Please fill valid age before you proceed.";
        public const string INVALID_PASSENGER_GENDER = "Info! Gender field contain invalid value, Please fill valid gender before you proceed.";
        public const string INVALID_PASSENGER_EMAILID = "Info! Email field contain invalid value, Please fill valid EmailId before you proceed.";

        public const string TICKET_BOOK_SUCCESS = "Congratulaions ! You have successfully booked your ticket. ";
        public const string TICKET_BOOK_FAILED = "Sorry!!! Failed in adding passenger details. ";
        public const string TICKET_MAIL_SENT = "Congratulaions ! Mail has been sucessfully sent to you. ";
        public const string TICKET_CANCELLED = "Congratulaions ! Ticket has been cancelled sucessfully. ";
        public const string TICKET_ALREADY_CANCELLED = "Your ticket has been alredy cancelled.!!! ";
        public const string TICKET_DOWNLOADED = "Your ticket has been downloaded sucessfully.!!!";
        #endregion
    }
}
