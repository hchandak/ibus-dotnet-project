﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace BussinessModel
{
    public class AppConstant
    {
        #region Regular Expressions
        public static Regex REGEX_EMAIL = new Regex(@"[a-z0-9!#$%&'*+/=?^{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)\b");
        public static Regex REGEX_NAME = new Regex(@"^[a-zA-Z0-9 ]*[a-zA-Z ]+[a-zA-Z0-9 ]*$");
        public static Regex REGEX_ADDRESS = new Regex(@"^[a-zA-Z0-9\s,'-]*$");
        public static Regex REGEX_CONTACT = new Regex(@"^\+?([0-9]+[ -]?){5,}[0-9]+$");
        public static Regex REGEX_DATE = new Regex(@"^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$");
        public static Regex REGEX_BUS_CAPACITY = new Regex(@"^([1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$"); //1-255
        public static Regex REGEX_TRIP_FARE = new Regex(@"^(\d*([,](?=\d{3}))?\d+)+((?!\2)[,]\d\d)?$");
        public static Regex REGEX_AGE = new Regex(@"^[1-9]?[0-9]{1}$|^100$");
        public static Regex REGEX_TIME_DURATION = new Regex(@"^(0?[1-9]|[1-9][0-9]):[0-5][0-9]$");
        public static Regex REGEX_TIME_24_HOURS = new Regex(@"^(0?[0-9]|[0-2][0-3]):[0-5][0-9]$");
        #endregion

        #region Common contants
        public static string IBUS_EMAIL = "iBusTicketing@gmail.com";
        public static int CANCELLED = 3;
        public static int BOOKED = 4;
        public static int MAXSEAT = 5;
        public static int RECOURD_COUNT_ZERO = 0;
        public static int SUCCESS = 1;
        public static int DEPENDENCY = -2;
        public static int ERROR = 0;
        public static int RECORD_EXIST = -2;
        public static int IS_UPDATE = 2;
        public static int IS_SAVE = 1;
        public enum BUS_CAPACITY { SLEEPER = 36, SEMI_SLEEPER = 40 };
        public enum STATUS { IN_ACTIVE=0,ACTIVE=1, DELETED=2};
        public static List<string> ADMIN_MODULE = new List<string>() { "travel", "route", "bus", "trip" };
        public static List<string> MODULE_OPERATION = new List<string>() { "add", "update", "delete" };
        public static List<int> USER_TYPE_ID = new List<int>() { 1, 2 };//1-Customer , 2-Admin
        public static List<string> HOUR = new List<string>() { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11",
                                                               "12", "13", "14", "15", "16", "17", "18", "19", "20", "21","22","23"};
        public static List<string> MINUT = new List<string>() { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", 
                                                        "11" ,"12", "13", "14", "15", "16", "17", "18", "19", "20", 
                                                        "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
                                                        "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
                                                        "41", "42", "43", "44", "45", "46", "47", "48", "49", "50",
                                                        "51", "52", "53", "54", "55", "56", "57", "58", "59"
                                                      };
        public static List<string> AMENITIES = new List<string>() { "WaterBottle", "Movie", "Blanket", "ReadingLight", "ChargingPoint" };
        public static List<string> BUS_TYPE = new List<string>() { "AC Sleeper", "AC Semi-Sleeper", "Non-AC Sleeper", "Non-AC Semi-Sleeper" };
        #endregion

    }
}
