﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessModel
{
    public class GetIDAndStatusRequest
    {
        public int ID { get; set; }
        public int Status { get; set; }
    }

    public class UpdateCityDetailModel
    {

        public int Status { get; set; }
    }
    public class DeletCityDetailModel
    {
        public int Status { get; set; }
    }
    public class AddCityDetailModel
    {
        public int Status { get; set; }
    }
    public class CityDetailModel
    {
        public int CityID { get; set; }
        public string City { get; set; }
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

    }
    public class UpdateAmenitiesDetailModel
    {
        public int Status { get; set; }
    }
    public class DeletAmenitiesDetailModel
    {
        public int Status { get; set; }
    }
    public class AddAmenitiesDetailModel
    {
        public int Status { get; set; }
    }
    public class AmenitiesDetailModel
    {
        public int CityID { get; set; }
        public string City { get; set; }
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }



    public class UpdateBusDetailModel
    {
        public int Status { get; set; }
    }
    public class DeleteBusDetailModel
    {
        public int Status { get; set; }
    }
    public class AddBusDetailModel
    {
        public int TravelID { get; set; }
        public int BusID { get; set; }
        public int Status { get; set; }
    }
    public class BusDetailModel
    {
        public int BusID { get; set; }
        public int BusTypeID { get; set; }
        public string BusType { get; set; }
        public string Amenities { get; set; }
        public bool WaterBottle { get; set; }
        public bool Movie { get; set; }
        public bool Blanket { get; set; }
        public bool ChargingPoint { get; set; }
        public bool ReadingLight { get; set; }
        public int Capacity { get; set; }
        public int TravelID { get; set; }
        public string TravelName { get; set; }
        public int Status { get; set; }
        public string IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }


    public class UpdateBusTypeDetailModel
    {
        public int Status { get; set; }
    }
    public class DeletBusTypeDetailModel
    {
        public int Status { get; set; }
    }
    public class AddBusTypeDetailModel
    {
        public int Status { get; set; }
    }
    public class BusTypeDetailModel
    {
        public int BusTypeID { get; set; }
        public string BusType { get; set; }
        public Nullable<int> AmenitiesID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }

    public class UpdateDailyTripDetailModel                               
    {
        public int Status { get; set; }
    }
    public class DeleteDailyTripDetailModel
    {
        public int Status { get; set; }
    }
    public class AddDailyTripDetailModel
    {
        public int Status { get; set; }
    }
    public class DailyTripDetailModel
    {
        public int DailyTripID { get; set; }
        public int TripID { get; set; }
        public string TripDate { get; set; }
        public int AvailableSeats { get; set; }
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }

    public class UpdateFreqDetailModel
    {
        public int Status { get; set; }
    }
    public class DeleteFreqDetailModel
    {
        public int Status { get; set; }
    }
    public class AddFreqDetailModel
    {
        public int Status { get; set; }
    }
    public class FreqDetailModel
    {
        public int FreqID { get; set; }
        public string FreqType { get; set; }
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }


    public class UpdatePassengerDetailModel
    {
        public int Status { get; set; }
    }
    public class DeletePassengerDetailModel
    {
        public int Status { get; set; }
    }
    public class AddPassengerDetailModel
    {
        public int Status { get; set; }
    }
    public class PassengerDetailModel
    {
        public int PDID { get; set; }
        public int ResID { get; set; }
        public int? SeatID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public Nullable<int> Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }

    public class UpdateReservationDetailModel
    {
        public int Status { get; set; }
    }
    public class DeleteReservationDetailModel
    {
        public int Status { get; set; }
    }
    public class AddReservationDetailModel
    {
        public int Status { get; set; }
    }
    public class ReservationDetailModel
    {
        public int ResID { get; set; }
        public int UserID { get; set; }
        public int DailyTripID { get; set; }
        public string ResDate { get; set; }
        public int ResNoOfSeat { get; set; }
        public string ResEmailID { get; set; }
        public string ResMobileNo { get; set; }
        public int BillAmount { get; set; }
        public Nullable<int> Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }



    public class UpdateRouteDetailModel
    {
        public int Status { get; set; }
    }
    public class DeleteRouteDetailModel
    {
        public int Status { get; set; }
    }
    public class AddRouteDetailModel
    {
        public int Route1ID { get; set; }
        public int Route2ID { get; set; }
        public int Status { get; set; }
    }
    public class RouteDetailModel
    {
        public int RouteID { get; set; }
        public int FromCityID { get; set; }
        public string FromCity { get; set; }
        public int ToCityID { get; set; }
        public string ToCity { get; set; }
        public int Status { get; set; }
        public string IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }

    }

    public class UpdateTravelDetailModel
    {
        public int Status { get; set; }
    }
    public class DeleteTravelDetailModel
    {
        public int Status { get; set; }
    }
    public class AddTravelDetailModel
    {
        public int TravelID { get; set; }
        public int Status { get; set; }
    }
    public class TravelDetailModel
    {
        public int TravelID { get; set; }
        public string TravelName { get; set; }
        public string ContactNo { get; set; }
        public string ContactAddress { get; set; }
        public int Status { get; set; }
        public string IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string TravelEmail { get; set; }
    }



    public class UpdateTripDetailModel
    {
        public int Status { get; set; }
    }
    public class DeleteTripDetailModel
    {
        public int Status { get; set; }
    }
    public class AddTripDetailModel
    {
        public int TripID { get; set; }
        public int Status { get; set; }
    }
    public class TripDetailModel
    {

        public int TripID { get; set; }
        public int TravelID { get; set; }
        public int BusTypeID { get; set; }
        public int BusID { get; set; }
        public int RouteID { get; set; }
        public int FreqID { get; set; }
        public string BusType { get; set; }
        public string Route { get; set; }
        public string Amenities { get; set; }
        public string TravelName { get; set; }
        public System.TimeSpan DepartureTime { get; set; }
        public int Totaltime { get; set; }
        public string Departure { get; set; }
        public string JourneyDuration { get; set; }
        public int Fare { get; set; }
        public int Status { get; set; }
        public string IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }

    public class UpdateUserDetailModel
    {
        public int Status { get; set; }
    }
    public class DeleteUserDetailModel
    {
        public int Status { get; set; }
    }
    public class AddUserDetailModel
    {
        public int Status { get; set; }
    }
    public class UserDetailModel
    {
        public int UserID { get; set; }
        public int UserTypeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string Password { get; set; }
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }

    public class UpdateUsertTypeDetailModel
    {
        public int Status { get; set; }
    }
    public class DeleteUserTypeDetailModel
    {
        public int Status { get; set; }
    }
    public class AddUserTypeDetailModel
    {
        public int Status { get; set; }
    }
    public class UserTypeDetailModel
    {
        public int UserTypeID { get; set; }
        public string UserType { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }

    public class BusTypeAndAmenitiesModel
    {
        public int BusID { get; set; }
        public int BusTypeID { get; set; }
        public int TravelID { get; set; }
        public string BusType { get; set; }
        public string Amenities { get; set; }
    }
    public class TicketModel
    {
        public int PNR { get; set; }
        public int TripCode { get; set; }
        public int NumberofSeats { get; set; }
        public String Source { get; set; }
        public TimeSpan DepartureTime { get; set; }
        public String Destination { get; set; }
        public DateTime DOR { get; set; }
        public DateTime DOJ { get; set; }
        public string BoardingPoint { get; set; }
        public int Fare { get; set; }
        public Nullable<int> Status { get; set; }
        public String IsActive { get; set; }
        public string str_DOJ { get; set; }
        public string str_DOR { get; set; }
        public string str_dept_time { get; set; }
    }
    
    public class TicketModelPassengerList
    {
        public String Name { get; set; }
        public int Age { get; set; }
        public int Gender { get; set; }
    }
    public class LoginUserDetailModel
    {
        public int Status { get; set; }
        public int UserID { get; set; }
        public int UserTypeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
    }



    public class ShowBusListModel
    {
        public int DailyTripID { get; set; }
        public int TripID { get; set; }
        public string TravelName { get; set; }
        public string BusType { get; set; }
        public TimeSpan DepartureTime { get; set; }
        public TimeSpan ArrivalTime { get; set; }
        public int JouneyTime { get; set; }
        public int AvailableSeats { get; set; }
        public int Fare { get; set; }
        public int busTypeID { get; set; }
    }

    public class ShowBusListModelModified
    {
        public int DailyTripID { get; set; }
        public int TripID { get; set; }
        public string TravelName { get; set; }
        public string BusType { get; set; }
        public string DepartureTime { get; set; }
        public string ArrivalTime { get; set; }
        public string Duration { get; set; }
        public string BoardingPoints { get; set; }
        public string BoardingDepartureTime { get; set; }
        public int AvailableSeats { get; set; }
        public int Fare { get; set; }
        public string Amenities { get; set; }
    }

    public class BusSearchModel
    {
        public string FromCity { get; set; }
        public string ToCity { get; set; }
        public string DateOfJourney { get; set; }
    }

    public class UpdateTableStatusModel
    {
        public int TableID { get; set; }
        public int Status { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }

    public class SendMailModel
    {
       public string EmailID { get; set; }
       public string Subject { get; set; }
       public string Message { get; set; }
       
    }

    public class SubRouteDetailModel 
    {
        public int SubRouteID { get; set; }
        public int RouteID { get; set; }
        public int StopCityID { get; set; }
        public int StopNo { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string viaCities { get; set; }
    }

    public class BoardingPointDetailModel
    {
        public BoardingPointDetailModel() { }
        public BoardingPointDetailModel(BoardingPointDetailModel point)
        {
            PointID = point.PointID;
            TripID = point.TripID;
            PointName = point.PointName;
            PointAddress = point.PointAddress;
            DepartureTime = point.DepartureTime;
            Departure = point.Departure;
        }
        public int PointID { get; set; }
        public int TripID { get; set; }
        public string PointName { get; set; }
        public string PointAddress { get; set; }
        public System.TimeSpan DepartureTime { get; set; }
        public string Departure { get; set; }
    }

    public class DisplayBoardingPoint
    {
        public string BoardingPoints { get; set; }
    }
}
