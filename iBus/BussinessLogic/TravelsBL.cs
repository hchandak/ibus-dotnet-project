﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface;
using BussinessModel;
using DataAccessObject;
namespace BussinessLogic
{
    public class TravelsBL : ITravels
    {
        TravelsDAO DAO = new TravelsDAO();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to AddTravelDetail() in DataAccessObject layer
        /// TravelDetailModel is argument type which contain information about travel which be want to add
        /// AddTravelDetailModel is return type which return status of this operation (Success/Fail) 
        /// </summary>
        /// <param name="travelDetail"></param>   
        /// <returns>AddTravelDetailModel</returns>
        public AddTravelDetailModel AddTravelDetail(TravelDetailModel travelDetail)
        {
            AddTravelDetailModel response = new AddTravelDetailModel();
            try
            {
                response = DAO.AddTravelDetail(travelDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsBL.cs, Function->AddTravelDetail  : \n " + ex);
            }
            
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to LoadTravelDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain travelID to load travel detail
        /// TravelDetailModel is return type which return travel detail
        /// </summary>
        /// <param name="travelDetail"></param>
        /// <returns>TravelDetailModel</returns>
        public TravelDetailModel LoadTravelDetail(GetIDAndStatusRequest travelDetail)
        {
            TravelDetailModel response = new TravelDetailModel();
            try
            {
                response = DAO.LoadTravelDetail(travelDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsBL.cs, Function->LoadTravelDetail  : \n " + ex);
            }
            
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to UpdateTravelDetail() in DataAccessObject layer
        /// TravelDetailModel is argument type which contain information about travel which be want to update 
        /// UpdateTravelDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="travelDetail"></param>
        /// <returns>UpdateTravelDetailModel</returns>
        public UpdateTravelDetailModel UpdateTravelDetail(TravelDetailModel travelDetail)
        {
            UpdateTravelDetailModel response = new UpdateTravelDetailModel();
            try
            {
                response = DAO.UpdateTravelDetail(travelDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsBL.cs, Function->UpdateTravelDetail  : \n " + ex);
            }
            
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to DeleteTravelDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain travelID to load travel detail
        /// DeleteTravelDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="travelDetail"></param>
        /// <returns>DeleteTravelDetailModel</returns>
        public DeleteTravelDetailModel DeleteTravelDetail(GetIDAndStatusRequest travelDetail)
        {
            DeleteTravelDetailModel response = new DeleteTravelDetailModel();
            try
            {
                response = DAO.DeleteTravelDetail(travelDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsBL.cs, Function->DeleteTravelDetail  : \n " + ex);
            }
           
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to SelectAllTravelDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain travelIsActive 
        /// List<TravelDetailModel> is return type which return travel detail list
        /// </summary>
        /// <param name="travelDetail"></param>
        /// <returns>List<TravelDetailModel></returns>
        public List<TravelDetailModel> SelectAllTravelDetail(GetIDAndStatusRequest travelDetail)
        {
            List<TravelDetailModel> response = new List<TravelDetailModel>();
            try
            {
                response = DAO.SelectAllTravelDetail(travelDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsBL.cs, Function->SelectAllTravelDetail  : \n " + ex);
            }
            
            return response;
        }
    }
}
