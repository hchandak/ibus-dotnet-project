﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface;
using BussinessModel;
using DataAccessObject;
namespace BussinessLogic
{
    public class BusBL : IBus
    {    
        BusDAO DAO = new BusDAO();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to AddBusDetail() in DataAccessObject layer
        /// BusDetailModel is argument type which contain information about bus which be want to add
        /// AddBusDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>AddBusDetailModel</returns>
        public AddBusDetailModel AddBusDetail(BusDetailModel busDetail)
        {
            AddBusDetailModel response = new AddBusDetailModel();
            try
            {
                response = DAO.AddBusDetail(busDetail);    
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusBL.cs, Function->AddBusDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to LoadBusDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain busID to load bus detail
        /// BusDetailModel is return type which return bus detail
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>BusDetailModel</returns>
        public BusDetailModel LoadBusDetail(GetIDAndStatusRequest busDetail)
        {
            BusDetailModel response = new BusDetailModel();
            try
            {
                response = DAO.LoadBusDetail(busDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusBL.cs, Function->LoadBusDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to UpdateBusDetail() in DataAccessObject layer
        /// BusDetailModel is argument type which contain information about bus which be want to update 
        /// UpdateBusDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>UpdateBusDetailModel</returns>
        public UpdateBusDetailModel UpdateBusDetail(BusDetailModel busDetail)
        {
            UpdateBusDetailModel response = new UpdateBusDetailModel();
            try
            {
                response = DAO.UpdateBusDetail(busDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusBL.cs, Function->UpdateBusDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to DeleteBusDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain busID to load bus detail
        /// DeleteBusDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>DeleteBusDetailModel</returns>
        public DeleteBusDetailModel DeleteBusDetail(GetIDAndStatusRequest busDetail)
        {
            DeleteBusDetailModel response = new DeleteBusDetailModel();
            try
            {
                response = DAO.DeleteBusDetail(busDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusBL.cs, Function->DeleteBusDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to SelectAllBusDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain busIsActive 
        /// List<BusDetailModel> is return type which return bus detail list
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>List<BusDetailModel></returns>
        public List<BusDetailModel> SelectAllBusDetail(GetIDAndStatusRequest busDetail)
        {
            List<BusDetailModel> response = new List<BusDetailModel>();
            try
            {
                response = DAO.SelectAllBusDetail(busDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusBL.cs, Function->SelectAllBusDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to SelectAllBusType() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain busTypeIsActive 
        /// List<BusTypeDetailModel> is return type which return bus type detail list 
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>List<BusTypeDetailModel></returns>
        public List<BusTypeDetailModel> SelectAllBusType(GetIDAndStatusRequest busDetail)
        {
            List<BusTypeDetailModel> response = new List<BusTypeDetailModel>();
            try
            {
                response = DAO.SelectAllBusType(busDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BusBL.cs, Function->SelectAllBusType  : \n " + ex);
            }

            return response;
        }
    }
}
