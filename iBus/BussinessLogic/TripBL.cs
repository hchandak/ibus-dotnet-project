﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface;
using BussinessModel;
using DataAccessObject;
using log4net;
namespace BussinessLogic
{
    public class TripBL : ITrip
    {
        TripDAO DAO = new TripDAO();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to AddTripDetail() in DataAccessObject layer
        /// TripDetailModel is argument type which contain information about trip which be want to add
        /// AddTripDetailModel is return type which return status of this operation (Success/Fail) 
        /// </summary>
        /// <param name="tripDetail"></param>    
        /// <returns>AddTripDetailModel</returns>
        public AddTripDetailModel AddTripDetail(TripDetailModel tripDetail)
        {
            AddTripDetailModel response = new AddTripDetailModel();
            try
            {
                response = DAO.AddTripDetail(tripDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripBL.cs, Function->AddTripDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to LoadTripDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain tripID to load trip detail
        /// TripDetailModel is return type which return trip detail
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>TripDetailModel</returns>
        public TripDetailModel LoadTripDetail(GetIDAndStatusRequest tripDetail)
        {
            TripDetailModel response = new TripDetailModel();
            try
            {
                response = DAO.LoadTripDetail(tripDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripBL.cs, Function->LoadTripDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to UpdateTripDetail() in DataAccessObject layer
        /// TripDetailModel is argument type which contain information about trip which be want to update 
        /// UpdateTripDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>UpdateTripDetailModel</returns>
        public UpdateTripDetailModel UpdateTripDetail(TripDetailModel tripDetail)
        {
            UpdateTripDetailModel response = new UpdateTripDetailModel();
            try
            {
                response = DAO.UpdateTripDetail(tripDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripBL.cs, Function->UpdateTripDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to DeleteTripDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain tripID to load trip detail
        /// DeleteTripDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>DeleteTripDetailModel</returns>
        public DeleteTripDetailModel DeleteTripDetail(GetIDAndStatusRequest tripDetail)
        {
            DeleteTripDetailModel response = new DeleteTripDetailModel();
            try
            {
                response = DAO.DeleteTripDetail(tripDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripBL.cs, Function->DeleteTripDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to SelectAllTripDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain tripIsActive 
        /// List<TripDetailModel> is return type which return trip detail list
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>List<TripDetailModel></returns>
        public List<TripDetailModel> SelectAllTripDetail(GetIDAndStatusRequest tripDetail)
        {
            List<TripDetailModel> response = new List<TripDetailModel>();
            try
            {
                response = DAO.SelectAllTripDetail(tripDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripBL.cs, Function->SelectAllTripDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to SelectAllBusTypeAndAmenitiesModel() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain busTypeIsActive and amenitiesIsActive 
        /// List<BusTypeAndAmenitiesModel> is return type which return trip detail list
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>List<BusTypeAndAmenitiesModel></returns>
        public List<BusTypeAndAmenitiesModel> SelectAllBusTypeAndAmenitiesModel(GetIDAndStatusRequest tripDetail)
        {
            List<BusTypeAndAmenitiesModel> response = new List<BusTypeAndAmenitiesModel>();
            try
            {
                response = DAO.SelectAllBusTypeAndAmenitiesModel(tripDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripBL.cs, Function->SelectAllBusTypeAndAmenitiesModel  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to SelectAllFrequencyType() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain freqIsActive 
        /// List<FreqDetailModel> is return type which return frequency detail list
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>List<FreqDetailModel></returns>
        public List<FreqDetailModel> SelectAllFrequencyType(GetIDAndStatusRequest tripDetail)
        {
            List<FreqDetailModel> response = new List<FreqDetailModel>();
            try
            {
                response = DAO.SelectAllFrequencyType(tripDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripBL.cs, Function->SelectAllFrequencyType  : \n " + ex);
            }

            return response;
        }
    }
}
