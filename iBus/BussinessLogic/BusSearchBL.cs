﻿using BussinessModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface;
using DataAccessObject;

namespace BussinessLogic
{
    public class BusSearchBL : ICity
    {
        TravelsDAO travelsDaoObject = new TravelsDAO();
        CityDAO cityDataObject = new CityDAO();
        RouteDAO routeDataObject = new RouteDAO();
        TripDAO tripDataObject = new TripDAO();
        BusDAO busDataObject = new BusDAO();

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        ///     
        /// </summary>
        /// <returns></returns>
        public List<CityDetailModel> GetCityDetail()
        {
            List<CityDetailModel> cityDetailList = new List<CityDetailModel>();

            cityDetailList = cityDataObject.GetCityDetail();
            return cityDetailList;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCityName"></param>
        /// <returns></returns>
        public int GetCityID(string strCityName)
        {
            return cityDataObject.GetCityID(strCityName);
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromCityID"></param>
        /// <param name="toCityID"></param>
        /// <returns></returns>
        public int GetRouteID(int fromCityID, int toCityID)
        {
            return routeDataObject.GetRouteID(fromCityID, toCityID);
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String GetCityListString()
        {
            string strCities = "";
            List<CityDetailModel> cityDetails = GetCityDetail();
            foreach (var city in cityDetails)
            {
                strCities += ";" + city.City;
            }
            return strCities;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="busSearchModelObj"></param>
        /// <returns></returns>
        public List<ShowBusListModelModified> GetShowBusList(BusSearchModel busSearchModelObj)
        {
            List<ShowBusListModel> listShowBusListModel = new List<ShowBusListModel>();
            List<ShowBusListModelModified> listShowBusListModelModified = new List<ShowBusListModelModified>();
            int toCityID = GetCityID(busSearchModelObj.ToCity);
            int fromCityID = GetCityID(busSearchModelObj.FromCity);

            //NEW CODE AFTER VIA CITY INCLUSION
            //List<SubRouteDetailModel> subRouteDetailList = routeDataObject.GetSubRouteList(fromCityID,toCityID);
            //if (subRouteDetailList.Count != 0)
            //{ 
            //    listShowBusListModel = tripDataObject.GetShowBusList(busSearchModelObj.DateOfJourney, subRouteDetailList);
               
            //    listShowBusListModel = listShowBusListModel.Select(x =>
            //    {
            //        x.ArrivalTime = x.DepartureTime.Add(TimeSpan.FromMinutes(x.JouneyTime));
            //        return x;
            //    }).ToList();

            //    BusDAO busDataObject = new BusDAO();
            //    listShowBusListModelModified = (from businfo in listShowBusListModel
            //                                    select new ShowBusListModelModified
            //                                    {
            //                                        DailyTripID = businfo.DailyTripID,
            //                                        TripID = businfo.TripID,
            //                                        TravelName = businfo.TravelName,
            //                                        BusType = businfo.BusType,
            //                                        DepartureTime = string.Format("{0:00}:{1:00}", businfo.DepartureTime.Hours, businfo.DepartureTime.Minutes),
            //                                        ArrivalTime = string.Format("{0:00}:{1:00}", businfo.ArrivalTime.Hours, businfo.ArrivalTime.Minutes),
            //                                        Duration = GetDurationFromMinutes(businfo.JouneyTime),
            //                                        AvailableSeats = businfo.AvailableSeats,
            //                                        Fare = businfo.Fare,
            //                                        Amenities = busDataObject.GetAmenitiesByBusTypeID(businfo.busTypeID),
                                                    
            //                                    }).ToList();

            //}
            //else
            //    listShowBusListModelModified = null;

            int routeID = GetRouteID(fromCityID, toCityID);
            if (routeID != -1)
            {
                listShowBusListModel = tripDataObject.GetShowBusList(busSearchModelObj.DateOfJourney, routeID);
                listShowBusListModel = listShowBusListModel.Select(x =>
                {
                    x.ArrivalTime = x.DepartureTime.Add(TimeSpan.FromMinutes(x.JouneyTime));
                    return x;
                }).ToList();

                BusDAO busDataObject = new BusDAO();
                listShowBusListModelModified = (from businfo in listShowBusListModel
                                                select new ShowBusListModelModified
                                                {
                                                    DailyTripID = businfo.DailyTripID,
                                                    TripID = businfo.TripID,
                                                    TravelName = businfo.TravelName,
                                                    BusType = businfo.BusType,
                                                    DepartureTime = string.Format("{0:00}:{1:00}", businfo.DepartureTime.Hours, businfo.DepartureTime.Minutes),
                                                    ArrivalTime = string.Format("{0:00}:{1:00}", businfo.ArrivalTime.Hours, businfo.ArrivalTime.Minutes),
                                                    Duration = GetDurationFromMinutes(businfo.JouneyTime),
                                                    AvailableSeats = businfo.AvailableSeats,
                                                    Fare = businfo.Fare,
                                                    Amenities = busDataObject.GetAmenitiesByBusTypeID(businfo.busTypeID),
                                                    BoardingPoints = tripDataObject.GetBoradingPoints(businfo.TripID),
                                                    BoardingDepartureTime = tripDataObject.GetBoradingPointsDepartureTime(businfo.TripID)
                                                   
                                                }).ToList();

                //listShowBusListModel = listShowBusListModel.Select(x => { x.Duration = (TimeSpan.Compare(x.Duration,TimeSpan.FromSeconds(0)) == -1  ? (x.Duration.Add(TimeSpan.FromHours(24))) : x.Duration); return x; }).ToList();
            }
            else
                listShowBusListModelModified = null;
            return listShowBusListModelModified;
        }

        public string GetDurationFromMinutes(int minutes) 
        {
            int durationHours = minutes / 60;
            int durationMinutes = minutes-durationHours*60;
            return durationMinutes == 0 ? durationHours + "h" : durationHours + "h " + durationMinutes + "m";
        }


        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<string> GetTravelNamesList()
        {
            List<string> listTravelNames = travelsDaoObject.GetTravelNames();
            return listTravelNames;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<string> GetBusTypeNameList()
        {
            List<string> listBusTypeNames = busDataObject.GetBusTypeNameList();
            return listBusTypeNames;
        }
    }
}
