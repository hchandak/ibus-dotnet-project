﻿using DataAccessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogic
{
    public class BookSeatBL
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="availability"></param>
        /// <returns></returns>
        public int GetMaxNoOfSeats(int availability)
        {
            int maxSeat = BussinessModel.AppConstant.MAXSEAT;
             

            try
            {
                if (maxSeat > availability)
                    maxSeat = availability;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BookSeatBL.cs, Function->GetMaxNoOfSeats  : \n " + ex);
            }
            return maxSeat;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// </summary>
        /// <param name="dailyTripID"></param>
        /// <returns></returns>
        public string GetReservedSeats(int dailyTripID)
        {
            Reservation_PassengerDAO resPassObj = new Reservation_PassengerDAO();
            string str_ReservedSeat = string.Empty;
            try
            {
                int?[] reservedSeat = resPassObj.GetReservedSeat(dailyTripID);
                str_ReservedSeat = String.Join(",", reservedSeat);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BookSeatBL.cs, Function->GetReservedSeats  : \n " + ex);
            }
            return str_ReservedSeat;
        }
    }
}