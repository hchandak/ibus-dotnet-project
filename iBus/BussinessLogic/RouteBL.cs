﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface;
using BussinessModel;
using DataAccessObject;
namespace BussinessLogic
{
    public class RouteBL : IRoute
    {
        RouteDAO DAO = new RouteDAO();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to AddRouteDetail() in DataAccessObject layer
        /// RouteDetailModel is argument type which contain information about route which be want to add
        /// AddRouteDetailModel is return type which return status of this operation (Success/Fail) 
        /// </summary>
        /// <param name="routeDetail"></param>   
        /// <returns>AddRouteDetailModel</returns>
        public AddRouteDetailModel AddRouteDetail(RouteDetailModel routeDetail)
        {
            AddRouteDetailModel response = new AddRouteDetailModel();
            try
            {
                response = DAO.AddRouteDetail(routeDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteBL.cs, Function->AddRouteDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to LoadRouteDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain routeID to load route detail
        /// RouteDetailModel is return type which return route detail
        /// </summary>
        /// <param name="routeDetail"></param>
        /// <returns>RouteDetailModel</returns>
        public RouteDetailModel LoadRouteDetail(GetIDAndStatusRequest routeDetail)
        {
            RouteDetailModel response = new RouteDetailModel();
            try
            {
                response = DAO.LoadRouteDetail(routeDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteBL.cs, Function->LoadRouteDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to UpdateRouteDetail() in DataAccessObject layer
        /// RouteDetailModel is argument type which contain information about route which be want to update 
        /// UpdateRouteDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="routeDetail"></param>
        /// <returns>UpdateRouteDetailModel</returns>
        public UpdateRouteDetailModel UpdateRouteDetail(RouteDetailModel routeDetail)
        {
            UpdateRouteDetailModel response = new UpdateRouteDetailModel();
            try
            {
                response = DAO.UpdateRouteDetail(routeDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteBL.cs, Function->UpdateRouteDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to DeleteRouteDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain routeID to load route detail
        /// DeleteRouteDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="routeDetail"></param>
        /// <returns>DeleteRouteDetailModel</returns>
        public DeleteRouteDetailModel DeleteRouteDetail(GetIDAndStatusRequest routeDetail)
        {
            DeleteRouteDetailModel response = new DeleteRouteDetailModel();
            try
            {
                response = DAO.DeleteRouteDetail(routeDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteBL.cs, Function->DeleteRouteDetail  : \n " + ex);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Make call to SelectAllRouteDetail() in DataAccessObject layer
        /// GetIDAndStatusRequest is argument type which contain routeIsActive 
        /// List<RouteDetailModel> is return type which return route detail list
        /// </summary>
        /// <param name="routeDetail"></param>
        /// <returns>List<RouteDetailModel></returns>
        public List<RouteDetailModel> SelectAllRouteDetail(GetIDAndStatusRequest routeDetail)
        {
            List<RouteDetailModel> response = new List<RouteDetailModel>();
            try
            {
                response = DAO.SelectAllRouteDetail(routeDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteBL.cs, Function->SelectAllRouteDetail  : \n " + ex);
            }

            return response;
        }
    }
}
