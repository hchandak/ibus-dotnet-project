﻿using BussinessModel;
using DataAccessObject;
using Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogic
{
    public class UserBL : IUser
    {
        UserDAO DAO = new UserDAO();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  RegisterUser: it passes values of user to be registered to the DAO layer
        /// </summary>    
        /// <returns>AddUserDetailModel: status oa 0 or 1</returns>
        public AddUserDetailModel RegisterUser(UserDetailModel user)
        {
            AddUserDetailModel response = new AddUserDetailModel();
            try
            {
                response = DAO.RegisterUser(user);

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->RegisterUser  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///   LoginUser takes value from code behind and calls function of DAO layer to find that user exist in data base or naot
        /// </summary>   
        /// <returns>LoginUserDetailModel which contains status which will be either 0 or 1</returns>
        public LoginUserDetailModel LoginUser(UserDetailModel user)
        {
            LoginUserDetailModel response = new LoginUserDetailModel();
            try
            {
                response = DAO.LoginUser(user);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->LoginUser  : \n " + ex);
            }
            return response;

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// CkeckUserIfAlreadyRegistered: used to check during registration of new user, if it already exist in data base
        /// Futher calls CkeckUserIfAlreadyRegistered of DAO layer
        /// </summary>     
        /// <returns>LoginUserDetailModel contains status as 0 or 1</returns>
        public LoginUserDetailModel CkeckUserIfAlreadyRegistered(UserDetailModel user)
        {
            LoginUserDetailModel response = new LoginUserDetailModel();

            try
            {
                response = DAO.CkeckUserIfAlreadyRegistered(user);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->CkeckUserIfAlreadyRegistered  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  SendMail: Sends mail to the registered user's mail id
        /// </summary> 
        public void SendMail(SendMailModel mailDetail)
        {
            try
            {
                var fromAddress = new MailAddress("iBusTicketing@gmail.com", "From i-Bus Admin");
                MailAddress toAddress;
                const string fromPassword = "ibus12345";
                string subject =mailDetail.Subject; //"Notification";
                toAddress = new MailAddress(mailDetail.EmailID);

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    Timeout = 20000
                };

                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = mailDetail.Message 
                }) 
                {
                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->SendMail  : \n " + ex);
            }

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  AddPassengerDetails: add calls AddPassengerDetails of DAO which inserts passenger details in database
        /// </summary>
        /// <returns>AddPassengerDetailModel: contains status as 0 or 1</returns>
        public AddPassengerDetailModel AddPassengerDetails(PassengerDetailModel passengerDetail)
        {
            AddPassengerDetailModel response = new AddPassengerDetailModel();

            try
            {
                response = DAO.AddPassengerDetails(passengerDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->AddPassengerDetails  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// GetResDateFromDailyTripID: Gives reservation date from DailyTripDetails table
        /// </summary>
        /// <returns>string: returns ResId</returns>
        public string GetResDateFromDailyTripID(int DailyTripID)
        {
            string ResDate = "";
            try
            {
                ResDate = DAO.GetResDateFromDailyTripID(DailyTripID);

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->GetResDateFromDailyTripID  : \n " + ex);
            }
            return ResDate;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// SaveReservationDetails: Passes details for reservation to DAO
        /// </summary>
        /// <returns>AddReservationDetailModel: contains status as 0 or 1</returns>
        public AddReservationDetailModel SaveReservationDetails(ReservationDetailModel reservationDetail)
        {
            AddReservationDetailModel response = new AddReservationDetailModel();
            try
            {
                response = DAO.SaveReservationDetails(reservationDetail);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->SaveReservationDetails  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///GetTicketDetails: takes Reservationid as  argument and calls the GetTicketDetails method of DAO
        ///it is used to get ticket details of passed reservationId
        /// </summary>
        /// <returns>TicketModel: conatains all details of ticket</returns>
        public TicketModel GetTicketDetails(int ReservationId)
        {
            TicketModel ticketModel = new TicketModel();
            try
            {
                ticketModel = DAO.GetTicketDetails(ReservationId);
            }
            catch (Exception ex)
            {

                Logger.Error("\n\nError Class-> UserBL.cs, Function->GetTicketDetails  : \n " + ex);
            }

            return ticketModel;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  GetPassengerList: calls GetPassengerList of DAO to get passenger list with resid passed
        /// </summary>
        /// <returns>List<PassengerDetailModel>: contains list of passenger who are going to travel</returns>
        public List<PassengerDetailModel> GetPassengerList(int ReservationId)
        {
            List<PassengerDetailModel> passengerList = new List<PassengerDetailModel>();
            try
            {
                passengerList = DAO.GetPassengerList(ReservationId);
            }
            catch (Exception ex)
            {

                Logger.Error("\n\nError Class-> UserBL.cs, Function->GetPassengerList  : \n " + ex);
            }
            return passengerList;

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///   GetReservationHistory: gives all history of given user Id
        /// </summary>
        /// <param name="userId"> used to fetch all details fro database</param>
        /// <returns>List<TicketModel>: Gives list of details</returns>
        public List<TicketModel> GetReservationHistory(int userId)
        {
            List<TicketModel> resultList = new List<TicketModel>();           
            try
            {
                resultList = DAO.GetReservationHistory(userId);
                for (int i = 0; i < resultList.Count; i++)
                {
                    resultList[i].str_DOJ = resultList[i].DOJ.ToShortDateString();
                    resultList[i].str_DOR = resultList[i].DOR.ToShortDateString();
                    resultList[i].str_dept_time = string.Format("{0:00}:{1:00}", resultList[i].DepartureTime.Hours, resultList[i].DepartureTime.Minutes);
                }
                
            }
            catch (Exception ex)
            {

                Logger.Error("\n\nError Class-> UserBL.cs, Function->GetReservationHistory  : \n " + ex);
            }
            return resultList;

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DailyTripID"></param>
        /// <returns></returns>
        public TripDetailModel GetFare(int DailyTripID)
        {
            TripDetailModel response = new TripDetailModel(); ;
            try
            {
                response = DAO.GetFare(DailyTripID);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->GetFare  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="no_of_passenger"></param>
        /// <param name="DailyTripID"></param>
        /// <returns></returns>
        public DailyTripDetailModel DecreaseSeatAvailability(int no_of_passenger, int DailyTripID)
        {
            DailyTripDetailModel response = new DailyTripDetailModel();
            try
            {
                response = DAO.DecreaseSeatAvailability(no_of_passenger, DailyTripID);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->DecreaseSeatAvailability  : \n " + ex);
                
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// Gives EmailId from reservationID
        /// </summary>
        /// <param name="reservationID"></param>        
        public ReservationDetailModel GetEmailID(int reservationID)
        {
            ReservationDetailModel response = new ReservationDetailModel();
            try
            {
                response = DAO.GetEmailID(reservationID);
            }
            catch (Exception ex)
            {

                Logger.Error("\n\nError Class-> UserBL.cs, Function->GetEmailID  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// Cancels reservation for whole ticket    
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        public UpdateDailyTripDetailModel CancelReservation(ReservationDetailModel reservation)
        {
            UpdateDailyTripDetailModel response = new UpdateDailyTripDetailModel();
            try
            {
                response = DAO.CancelReservation(reservation);
            }
            catch (Exception ex)
            {

                Logger.Error("\n\nError Class-> UserBL.cs, Function->CancelReservation  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// It checks status of ticket 
        /// </summary>
        /// <param name="reservation">Gives status as booked or cancelled</param>
        /// <returns></returns>
        public UpdateDailyTripDetailModel CheckTicketStatus(ReservationDetailModel reservation)
        {
            UpdateDailyTripDetailModel response = new UpdateDailyTripDetailModel();
            try
            {
                response = DAO.CheckTicketStatus(reservation);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->CheckTicketStatus  : \n " + ex); throw;
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  Cancels reservation passenger wise
        /// </summary>
        /// <param name="passengerID">Contains passenger ID</param>
        /// <returns></returns>
        public UpdatePassengerDetailModel CancelPassengerReservation(int passengerID)
        {
            UpdatePassengerDetailModel response = new UpdatePassengerDetailModel();
            try
            {
                response = DAO.CancelPassengerReservation(passengerID);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserBL.cs, Function->CancelPassengerReservation  : \n " + ex);
            }
            return response;
        }

        //public string Encryptdata(string password)
        //{
        //    string strmsg = string.Empty;
        //    byte[] encode = new byte[password.Length];
        //    encode = System.Text.Encoding.UTF8.GetBytes(password);
        //    strmsg = Convert.ToBase64String(encode);
        //    return strmsg;
        //}



        //public string Decryptdata(string encryptpwd)
        //{
        //    string decryptpwd = string.Empty;
        //    System.Text.UTF8Encoding encodepwd = new System.Text.UTF8Encoding();
        //    System.Text.Decoder Decode = encodepwd.GetDecoder();
        //    byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        //    int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        //    char[] decoded_char = new char[charCount];
        //    Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        //    decryptpwd = new String(decoded_char);
        //    return decryptpwd;
        //}

        /// <summary>
        /// To encrypt the input password
        /// </summary>
        /// <param name="textPassword"></param>
        /// <returns>It returns encrypted code</returns>
        public string EncryptPassword(string textPassword)
        {
            //Input          
            byte[] passBytes = System.Text.Encoding.Unicode.GetBytes(textPassword);
            string encryptPass = Convert.ToBase64String(passBytes);
            return encryptPass;
        }

        // <summary>
        /// To Decode the encrypted password
        /// </summary>
        /// <param name="encryptedPassword"></param>
        /// <returns>It returns plain password</returns>
        public string DecryptPassword(string encryptedPassword)
        {
            //output
            byte[] passByteData = Convert.FromBase64String(encryptedPassword);
            string originalPassword = System.Text.Encoding.Unicode.GetString(passByteData);
            return originalPassword;
        }

    }

}
