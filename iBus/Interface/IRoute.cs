﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
namespace Interface
{
    public interface IRoute
    {
        AddRouteDetailModel AddRouteDetail(RouteDetailModel routeDetail);
        RouteDetailModel LoadRouteDetail(GetIDAndStatusRequest routeDetail);
        UpdateRouteDetailModel UpdateRouteDetail(RouteDetailModel routeDetail);
        DeleteRouteDetailModel DeleteRouteDetail(GetIDAndStatusRequest routeDetail);
        List<RouteDetailModel> SelectAllRouteDetail(GetIDAndStatusRequest routeDetail);
    }
}
