﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
namespace Interface
{
    public interface IUser
    {
        AddUserDetailModel RegisterUser(UserDetailModel user);
        LoginUserDetailModel LoginUser(UserDetailModel user);
        LoginUserDetailModel CkeckUserIfAlreadyRegistered(UserDetailModel user);
        AddPassengerDetailModel AddPassengerDetails(PassengerDetailModel passengerDetail);
        string GetResDateFromDailyTripID(int DailyTripID);
        AddReservationDetailModel SaveReservationDetails(ReservationDetailModel reservationDetail);
        TicketModel GetTicketDetails(int ReservationId);
        TripDetailModel GetFare(int DailyTripID);
        UpdatePassengerDetailModel CancelPassengerReservation(int passengerID);
        UpdateDailyTripDetailModel CheckTicketStatus(ReservationDetailModel reservation);
        UpdateDailyTripDetailModel CancelReservation(ReservationDetailModel reservation);
        ReservationDetailModel GetEmailID(int reservationID);
        DailyTripDetailModel DecreaseSeatAvailability(int no_of_passenger, int DailyTripID);    
        List<TicketModel> GetReservationHistory(int userId);
        List<PassengerDetailModel> GetPassengerList(int ReservationId);
  
    }
}
