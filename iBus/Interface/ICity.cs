﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;

namespace Interface
{
    public interface ICity
    {
        List<CityDetailModel> GetCityDetail();
        int GetCityID(string strCityName);
    }
}
