﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
namespace Interface
{
    public interface ITravels
    {
        AddTravelDetailModel AddTravelDetail(TravelDetailModel travelDetail);
        TravelDetailModel LoadTravelDetail(GetIDAndStatusRequest travelDetail);
        UpdateTravelDetailModel UpdateTravelDetail(TravelDetailModel travelDetail);
        DeleteTravelDetailModel DeleteTravelDetail(GetIDAndStatusRequest travelDetail);
        List<TravelDetailModel> SelectAllTravelDetail(GetIDAndStatusRequest travelDetail);
    }
}
