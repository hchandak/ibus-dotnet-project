﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
namespace Interface
{
    public interface IBus
    {
        AddBusDetailModel AddBusDetail(BusDetailModel busDetail);
        BusDetailModel LoadBusDetail(GetIDAndStatusRequest busDetail);
        UpdateBusDetailModel UpdateBusDetail(BusDetailModel busDetail);
        DeleteBusDetailModel DeleteBusDetail(GetIDAndStatusRequest busDetail);
        List<BusDetailModel> SelectAllBusDetail(GetIDAndStatusRequest busDetail);
        List<BusTypeDetailModel> SelectAllBusType(GetIDAndStatusRequest busDetail);
    }
}
