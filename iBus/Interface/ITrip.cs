﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
namespace Interface
{
    public interface ITrip
    {
        AddTripDetailModel AddTripDetail(TripDetailModel tripDetail);
        TripDetailModel LoadTripDetail(GetIDAndStatusRequest tripDetail);
        UpdateTripDetailModel UpdateTripDetail(TripDetailModel tripDetail);
        DeleteTripDetailModel DeleteTripDetail(GetIDAndStatusRequest tripDetail);
        List<TripDetailModel> SelectAllTripDetail(GetIDAndStatusRequest tripDetail);
        List<BusTypeAndAmenitiesModel> SelectAllBusTypeAndAmenitiesModel(GetIDAndStatusRequest tripDetail);
        List<FreqDetailModel> SelectAllFrequencyType(GetIDAndStatusRequest tripDetail);
    }
}
