﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
using System.Text.RegularExpressions;
namespace DataValidation
{
    public class UserValidation
    {

        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary> ValidateUserRegistrationDetails    
        /// Validates the arguments passed during registration
        /// <returns>string message that contains status i.e. Suceeded or failed</returns>          
        public string ValidateUserRegistrationDetails(string EmailSignUp, string FirstNameSingnUp, string LastNameSignUp, string PassswordSignUp, string ConfirmPasswordSignUp, string ContactSignUp)
        {
            try
            {
                Match matchEmailSignUp = AppConstant.REGEX_EMAIL.Match(EmailSignUp);
                Match matchFirstNameSingnUp = AppConstant.REGEX_NAME.Match(FirstNameSingnUp);
                Match matchLastNameSignUp = AppConstant.REGEX_NAME.Match(LastNameSignUp);
                Match matchContactSignUp = AppConstant.REGEX_CONTACT.Match(ContactSignUp);

                //Required field validation
                if (!(EmailSignUp.Length > 0) || !(FirstNameSingnUp.Length > 0) || !(LastNameSignUp.Length > 0) || !(PassswordSignUp.Length > 0) || !(ConfirmPasswordSignUp.Length > 0) || !(ContactSignUp.Length > 0))
                    return DisplayMessage.REQUIRED_FIELDS;

                //Field specific validation
                if (!(matchEmailSignUp.Success))
                    return DisplayMessage.INVALID_REGISTRATION_EMAIL;
                if (!(matchFirstNameSingnUp.Success))
                    return DisplayMessage.INVALID_REGISTRATION_FIRST_NAME;
                if (!(matchLastNameSignUp.Success))
                    return DisplayMessage.INVALID_REGISTRATION_LAST_NAME;
                if (!(matchContactSignUp.Success))
                    return DisplayMessage.INVALID_REGISTRATION_CONTACT;
                if (!(PassswordSignUp == ConfirmPasswordSignUp))
                    return DisplayMessage.INVALID_REGISTRATION_PASSWORD_CONFIRMPASSWORD_DOESNT_MATCH;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserValidation.cs, Function->ValidateUserRegistrationDetails  : \n " + ex);
            }

            return DisplayMessage.VALIDATION_SUCCESS;
        }

        /// <summary>
        ///  ValidateUserLoginDetails validates values given by user or admin while logging in
        /// </summary>
        /// <param name="EmailSignIn"></param>
        /// <param name="PasswordSignIn"></param>
        /// <returns>string message that contains status i.e. Suceeded or failed </returns>
        public string ValidateUserLoginDetails(string EmailSignIn, string PasswordSignIn)
        {
            try
            {
                if (!(EmailSignIn.Length > 0) || !(PasswordSignIn.Length > 0))
                    return DisplayMessage.REQUIRED_FIELDS;
                else
                    return DisplayMessage.VALIDATION_SUCCESS;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserValidation.cs, Function->ValidateUserLoginDetails  : \n " + ex);
                return DisplayMessage.REQUIRED_FIELDS;
            }

        }
        /// <summary>
        ///     ValidatePassengerDetails: Validates passenger details passed
        /// </summary>

        /// <returns>string: Sucess or failure message</returns>
        public string ValidatePassengerDetails(string PassengerFirstName, string PassengerLastName, int PassengerAge, char PassengerGender, string UserName)
        {
            try
            {
                Match matchFirstName = AppConstant.REGEX_NAME.Match(PassengerFirstName);
                Match matchLastName = AppConstant.REGEX_NAME.Match(PassengerLastName);
                Match matchUserName = AppConstant.REGEX_EMAIL.Match(UserName);



                //Required field validation
                if (!(PassengerFirstName.Length > 0) )
                    return DisplayMessage.REQUIRED_FIELDS;

                //Field specific validation
                if (!(matchFirstName.Success))
                    return DisplayMessage.INVALID_PASSENGER_FIRST_NAME;
                //if (!(matchLastName.Success))
                //    return DisplayMessage.INVALID_PASSENGER_LAST_NAME;
                if (!(matchUserName.Success))
                    return DisplayMessage.INVALID_PASSENGER_EMAILID;
                //if (!(PassengerAge==0 || PassengerAge>110))
                //    return DisplayMessage.INVALID_PASSENGER_AGE;
                //if (!(PassengerGender==null))
                //    return DisplayMessage.INVALID_PASSENGER_GENDER;

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserValidation.cs, Function->ValidatePassengerDetails  : \n " + ex);
            }

            return DisplayMessage.VALIDATION_SUCCESS;
        }

        /// <summary>
        /// To Validate User input in bus search request
        /// </summary>
        /// <param name="busSearchDetailsObj"></param>
        /// <param name="strCities"></param>
        /// <returns></returns>
        public string ValidateBusSearch(ref BusSearchModel busSearchDetailsObj, String strCities)
        {
            try
            {


                String strDate = busSearchDetailsObj.DateOfJourney;
                String[] arrayCities = strCities.Split(';');
                bool boolTo = false;
                bool boolFrom = false;
                bool boolDateOfJourney = false;

                // FROM and TO City validation
                for (int i = 1; i < arrayCities.Length; i++)
                {
                    if (busSearchDetailsObj.FromCity == arrayCities[i].ToLower())
                    {
                        busSearchDetailsObj.FromCity = arrayCities[i];
                        boolFrom = true;
                        break;
                    }
                }

                if (!boolFrom)
                {
                    return DisplayMessage.INVALID_FROM_CITY;
                }

                for (int i = 1; i < arrayCities.Length; i++)
                {
                    if (busSearchDetailsObj.ToCity == arrayCities[i].ToLower())
                    {
                        busSearchDetailsObj.ToCity = arrayCities[i];
                        boolTo = true;
                        break;
                    }
                }

                if (!boolTo)
                {
                    return DisplayMessage.INVALID_TO_CITY;
                }

                //Date of journey validation
                if (strDate.Length > 0)
                {
                    if (AppConstant.REGEX_DATE.Match(strDate).Success)
                    {
                        boolDateOfJourney = true;
                    }
                }

                if (!boolDateOfJourney)
                {
                    return DisplayMessage.INVALID_DATEOFJOURNEY;
                }

                if (busSearchDetailsObj.FromCity == busSearchDetailsObj.ToCity)
                {
                    return DisplayMessage.INVALID_FROM_TO_CITY;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return AppConstant.SUCCESS.ToString();
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Validation of sending mail for contact us 
        /// </summary>
        /// <param name="mailDetail"></param>
        /// <returns></returns>
        public string SendMailValidation(SendMailModel mailDetail)
        {
            try
            {
                Match matchEmail = AppConstant.REGEX_EMAIL.Match(mailDetail.EmailID);

                //Required field validation
                if (!(mailDetail.EmailID.Length > 0) || !(mailDetail.Message.Length > 0))
                    return DisplayMessage.REQUIRED_FIELDS;

                //Field specific validation
                if (!(matchEmail.Success))
                    return DisplayMessage.INVALID_USER_EMAIL;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserValidation.cs, Function->SendMailValidation  : \n " + ex);
            }
            return DisplayMessage.VALIDATION_SUCCESS;
        }
    }
}
