﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
using System.Text.RegularExpressions;
namespace DataValidation
{
    public class AdminValidation
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Validate travel detail before Add new travel  and update travel detail
        /// This include required field validation as well as field specific validation
        /// Return Success/Failure message
        /// </summary>
        /// <param name="travelDetail"></param>
        /// <param name="isUpdate"></param>
        /// <returns>Success/Failure message</returns>
        public string ValidateTravelDetail(TravelDetailModel travelDetail)
        {
            try
            {
                Match matchTravelName = AppConstant.REGEX_NAME.Match(travelDetail.TravelName);
                Match matchTravelAddress = AppConstant.REGEX_ADDRESS.Match(travelDetail.ContactAddress);
                Match matchTravelEmail = AppConstant.REGEX_EMAIL.Match(travelDetail.TravelEmail);
                Match matchTravelContact = AppConstant.REGEX_CONTACT.Match(travelDetail.ContactNo);


                //Required field validation
                if (!(travelDetail.TravelName.Length > 0) || !(travelDetail.ContactAddress.Length > 0) || !(travelDetail.TravelEmail.Length > 0) || !(travelDetail.ContactNo.Length > 0))
                    return DisplayMessage.REQUIRED_FIELDS;

                //Field specific validation
                if (!(matchTravelName.Success))
                    return DisplayMessage.INVALID_TRAVEL_NAME;
                if (!(matchTravelAddress.Success))
                    return DisplayMessage.INVALID_TRAVEL_ADDRESS;
                if (!(matchTravelEmail.Success))
                    return DisplayMessage.INVALID_TRAVEL_EMAIL;
                if (!(matchTravelContact.Success))
                    return DisplayMessage.INVALID_TRAVEL_CONTACT;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AdminValidation.cs, Function->ValidateTravelDetail  : \n " + ex);
            }

            return DisplayMessage.VALIDATION_SUCCESS;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Validate bus detail before Add new bus  and update bus detail
        /// This include required field validation as well as field specific validation
        /// Return Success/Failure message
        /// </summary>
        /// <param name="busDetail"></param>
        /// <param name="isUpdate"></param>
        /// <returns>Success/Failure message</returns>
        public string ValidateBusDetail(BusDetailModel busDetail, int isUpdate)
        {
            try
            {
                Match matchBusCapacity = AppConstant.REGEX_BUS_CAPACITY.Match(busDetail.Capacity.ToString());
                if (isUpdate == AppConstant.IS_UPDATE)
                {
                    //Required field validation
                    if (busDetail.Capacity<= 0)
                        return DisplayMessage.REQUIRED_FIELDS;

                    //Field specific validation
                    if (!(matchBusCapacity.Success))
                        return DisplayMessage.INVALID_BUS_CAPACITY;
                }
                else
                {
                    //Required field validation
                    if (busDetail.BusTypeID <= 0 || busDetail.TravelID <= 0 || busDetail.Capacity <= 0)
                        return DisplayMessage.REQUIRED_FIELDS;
                    //Field specific validation
                    if (!(matchBusCapacity.Success))
                        return DisplayMessage.INVALID_BUS_CAPACITY;

                }

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AdminValidation.cs, Function->ValidateBusDetail  : \n " + ex);
            }
            return DisplayMessage.VALIDATION_SUCCESS;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Validate route detail before Add new route
        /// This include required field validation as well as field specific validation
        /// Return Success/Failure message
        /// </summary>
        /// <param name="routeDetail"></param>
        /// <returns>Success/Failure message</returns>
        public string ValidateRouteDetail(RouteDetailModel routeDetail)
        {
            try
            {
                Match matchFromCity = AppConstant.REGEX_NAME.Match(routeDetail.FromCity);
                Match matchToCity = AppConstant.REGEX_NAME.Match(routeDetail.ToCity);

                //Required field validation
                if (!(routeDetail.ToCity.Length > 0) || !(routeDetail.FromCity.Length > 0))
                    return DisplayMessage.REQUIRED_FIELDS;

                //Field specific validation
                if (routeDetail.ToCity == routeDetail.FromCity)
                    return DisplayMessage.INVALID_FROM_TO_CITY;
                if (!(matchFromCity.Success))
                    return DisplayMessage.INVALID_FROM_CITY;
                if (!(matchToCity.Success))
                    return DisplayMessage.INVALID_TO_CITY;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AdminValidation.cs, Function->ValidateRouteDetail  : \n " + ex);
            }
            return DisplayMessage.VALIDATION_SUCCESS;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Validate trip detail before Add new trip and update trip detail
        /// This include required field validatio as well field specific validation
        /// Return Success/Failure message
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <param name="isUpdate"></param>
        /// <returns> Success/Failure message</returns>
        public string ValidateTripDetail(TripDetailModel tripDetail, int isUpdate)
        {
            try
            {
                Match matchTripFare = AppConstant.REGEX_TRIP_FARE.Match(tripDetail.Fare.ToString());
                Match matchJourneyDuration = AppConstant.REGEX_TIME.Match(tripDetail.JourneyDuration.ToString());
                if (isUpdate == AppConstant.IS_UPDATE)
                {

                    //Required field validation
                    if (tripDetail.FreqID <= 0 || tripDetail.Fare <= 0 || tripDetail.DepartureTime.ToString().Length <= 1 || tripDetail.JourneyDuration.ToString().Length <= 1)
                        return DisplayMessage.REQUIRED_FIELDS;

                    //Field specific validation
                    if (!(matchTripFare.Success))
                        return DisplayMessage.INVALID_TRIP_FARE;
                    if (tripDetail.DepartureTime.ToString().Length < 5)
                        return DisplayMessage.INVALID_DEPARTURE_TIME;
                    if (!(matchJourneyDuration.Success))
                        return DisplayMessage.INVALID_JOURNEY_DURATION;
                }
                else
                {
                    //Required field validation
                    if (tripDetail.TravelID <= 0 || tripDetail.BusID<= 0 || tripDetail.RouteID <= 0 || tripDetail.FreqID <= 0 || tripDetail.Fare.ToString().Length <= 0 || tripDetail.DepartureTime.ToString().Length <= 1 || tripDetail.JourneyDuration.ToString().Length <= 1)
                        return DisplayMessage.REQUIRED_FIELDS;

                    //Field specific validation
                    if (!(matchTripFare.Success))
                        return DisplayMessage.INVALID_TRIP_FARE;
                    if (tripDetail.DepartureTime.ToString().Length < 5)
                        return DisplayMessage.INVALID_DEPARTURE_TIME;
                    if (!(matchJourneyDuration.Success))
                        return DisplayMessage.INVALID_JOURNEY_DURATION;

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> AdminValidation.cs, Function->ValidateTripDetail  : \n " + ex);
            }
            return DisplayMessage.VALIDATION_SUCCESS;
        }
    }
}
