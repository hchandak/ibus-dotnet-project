﻿using BussinessModel;
using Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace DataAccessObject
{
    public class UserDAO : IUser
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        iBusEntities iBusDB = new iBusEntities();

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// RegisterUser: inserts values of arguments in database for registering user
        /// </summary>
        /// <returns>AddUserDetailModel: status as 0 or 1</returns>
        public AddUserDetailModel RegisterUser(UserDetailModel user)
        {
            AddUserDetailModel response = new AddUserDetailModel();
            try
            {
                User userTable = new User() { EmailID = user.EmailID, FirstName = user.FirstName, LastName = user.LastName, Password = user.Password, MobileNo = user.MobileNo, CreatedBy = user.CreatedBy, CreatedDate = Convert.ToDateTime(user.CreatedDate), Status = user.Status, UserTypeID = user.UserTypeID };

                //Add to memory
                iBusDB.Users.Add(userTable);

                //Save to database
                if (iBusDB.SaveChanges() > 0)
                    response.Status = AppConstant.SUCCESS;
                else
                    response.Status = AppConstant.ERROR;

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->RegisterUser  : \n " + ex);

            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///   LoginUser: Takes value from Business layer and checks in data base that user exist or not
        /// </summary>   
        /// <returns>LoginUserDetailModel that contains status either 0 or 1</returns>
        public LoginUserDetailModel LoginUser(UserDetailModel user)
        {
            LoginUserDetailModel response = new LoginUserDetailModel();
            UserDetailModel userExist = new UserDetailModel();
            try
            {
                var result = iBusDB.Users.First(userRow => userRow.EmailID == user.EmailID & userRow.Password == user.Password);

                userExist.EmailID = result.EmailID;
                userExist.FirstName = result.FirstName;
                userExist.LastName = result.LastName;
                userExist.Password = result.Password;
                userExist.MobileNo = result.MobileNo;
                userExist.UserID = result.UserID;
                if (userExist != null)
                {
                    response.Status = AppConstant.SUCCESS;   //User exist
                    response.UserTypeID = result.UserTypeID;
                    response.FirstName = result.FirstName;
                    response.LastName = result.LastName;
                    response.EmailID = result.EmailID;
                    response.MobileNo = result.MobileNo;
                    response.UserID = result.UserID;
                }
                else
                {
                    response.Status = AppConstant.ERROR;    // user does'nt exist   
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->LoginUser  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///        AddPassengerDetails: inserts values into the database
        /// </summary>
        /// <returns>AddPassengerDetailModel: contains status as 0 or 1</returns>
        public AddPassengerDetailModel AddPassengerDetails(PassengerDetailModel passengerDetail)
        {
            AddPassengerDetailModel response = new AddPassengerDetailModel();
            try
            {
                PassengerDetail passengerTable = new PassengerDetail() { FirstName = passengerDetail.FirstName, LastName = passengerDetail.LastName, Age = passengerDetail.Age, Gender = passengerDetail.Gender, CreatedBy = passengerDetail.CreatedBy, CreatedDate = Convert.ToDateTime(passengerDetail.CreatedDate), ResID = passengerDetail.ResID, SeatID = passengerDetail.SeatID, Status = passengerDetail.Status };

                //Add to memory
                iBusDB.PassengerDetails.Add(passengerTable);

                //Save to database
                if (iBusDB.SaveChanges() > 0)
                {
                    response.Status = AppConstant.SUCCESS;
                }
                else
                    response.Status = AppConstant.ERROR;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->SaveReservationDetails  : \n " + dbEx);
            }

            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///   CkeckUserIfAlreadyRegistered: checks in data base if passed argument values exist in database
        /// </summary>
        /// <returns>LoginUserDetailModel: status either 0 or 1</returns>
        public LoginUserDetailModel CkeckUserIfAlreadyRegistered(UserDetailModel user)
        {
            LoginUserDetailModel response = new LoginUserDetailModel();
            UserDetailModel userExist = new UserDetailModel();
            try
            {
                var result = iBusDB.Users.FirstOrDefault(userRow => userRow.EmailID == user.EmailID);

                if (result != null)
                {
                    response.Status = 0;  //user already exist
                }
                else
                {
                    response.Status = 1;   //user doesn't exisit
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->CkeckUserIfAlreadyRegistered  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// GetResDateFromDailyTripID: Gives reservation date from DailyTripDetails table
        /// </summary>
        /// <returns>string: returns ResId</returns>
        public string GetResDateFromDailyTripID(int DailyTripID)
        {
            string ResDate = "";
            try
            {
                var result = iBusDB.DailyTripDetails.First(tripRow => tripRow.DailyTripID == DailyTripID);

                ResDate = (result.TripDate).ToString();
                return ResDate;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->GetResDateFromDailyTripID  : \n " + ex);
            }
            return ResDate;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// SaveReservationDetails: inserts Reservation details in reservationDetails table
        /// </summary>
        /// <returns>AddReservationDetailModel: returns ResId or 0</returns>
        public AddReservationDetailModel SaveReservationDetails(ReservationDetailModel reservationDetail)
        {
            AddReservationDetailModel response = new AddReservationDetailModel();
            int Resid;
            try
            {
                ReservationDetail reservationTable = new ReservationDetail() { UserID = reservationDetail.UserID, DailyTripID = reservationDetail.DailyTripID, ResDate = Convert.ToDateTime(reservationDetail.ResDate), ResNoOfSeat = reservationDetail.ResNoOfSeat, ResEmailID = reservationDetail.ResEmailID, ResMobileNo = reservationDetail.ResMobileNo, BillAmount = reservationDetail.BillAmount, CreatedBy = reservationDetail.CreatedBy, CreatedDate = Convert.ToDateTime(reservationDetail.CreatedDate), Status = reservationDetail.Status };
                iBusDB.ReservationDetails.Add(reservationTable);
                if (iBusDB.SaveChanges() > 0)
                {
                    Resid = reservationTable.ResID;
                    response.Status = Resid;
                }
                else
                    response.Status = AppConstant.ERROR;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->SaveReservationDetails  : \n " + dbEx);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  GetTicketDetails: Fetches values of ticket from different tables for a particular reservation id
        /// </summary>
        /// <param name="ReservationId"></param>
        /// <returns>TicketModel: contains all ticket details</returns>
        public TicketModel GetTicketDetails(int ReservationId)
        {
            TicketModel ticketModel = new TicketModel();
            TicketModel ticketModel2 = new TicketModel();
            try
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    ticketModel = (from resdetail in iBusDB.ReservationDetails
                                   join dailytripd in iBusDB.DailyTripDetails
                                       on resdetail.DailyTripID equals dailytripd.DailyTripID
                                   join trpdtl in iBusDB.TripDetails
                                      on dailytripd.TripID equals trpdtl.TripID
                                   join rtedtl in iBusDB.RouteDetails
                                      on trpdtl.RouteID equals rtedtl.RouteID
                                   join ctydtl in iBusDB.CityDetails
                                      on rtedtl.ToCityID equals ctydtl.CityID
                                   join busdetail in iBusDB.BusDetails
                                   on trpdtl.BusID equals busdetail.BusID
                                   join traveldetail in iBusDB.TravelDetails
                                    on busdetail.TravelID equals traveldetail.TravelID
                                   where resdetail.ResID == ReservationId
                                   select new TicketModel
                                   {
                                       NumberofSeats = resdetail.ResNoOfSeat,
                                       DOR = resdetail.ResDate,
                                       Fare = resdetail.BillAmount,
                                       TripCode = dailytripd.TripID,
                                       DOJ = dailytripd.TripDate,
                                       DepartureTime = trpdtl.DepartureTime,
                                       Destination = ctydtl.City,
                                       PNR = resdetail.ResID,
                                       BoardingPoint = traveldetail.ContactAddress,
                                       Status = resdetail.Status

                                   }).FirstOrDefault();

                    ticketModel2 = (from resdetail in iBusDB.ReservationDetails
                                    join dailytripd in iBusDB.DailyTripDetails
                                        on resdetail.DailyTripID equals dailytripd.DailyTripID
                                    join trpdtl in iBusDB.TripDetails
                                       on dailytripd.TripID equals trpdtl.TripID
                                    join rtedtl in iBusDB.RouteDetails
                                       on trpdtl.RouteID equals rtedtl.RouteID
                                    join ctydtl in iBusDB.CityDetails
                                       on rtedtl.FromCityID equals ctydtl.CityID
                                    where resdetail.ResID == ReservationId
                                    select new TicketModel
                                    {
                                        Source = ctydtl.City

                                    }).FirstOrDefault();

                    ticketModel.Source = ticketModel2.Source;
             
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->GetTicketDetails  : \n " + ex);
            }
            return ticketModel;

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///    GetPassengerList: fetches records from database which contains list of passengers
        /// </summary>
        /// <returns>List<PassengerDetailModel> : Contains list of passenger</returns>
        public List<PassengerDetailModel> GetPassengerList(int ReservationId)
        {
            List<PassengerDetailModel> result = new List<PassengerDetailModel>();

            try
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    result = (from passngerdtl in iBusDB.PassengerDetails
                              where (passngerdtl.ResID == ReservationId) && (passngerdtl.Status == AppConstant.BOOKED)
                              select new PassengerDetailModel
                              {
                                  FirstName = passngerdtl.FirstName,
                                  LastName = passngerdtl.LastName,
                                  Age = passngerdtl.Age,
                                  Gender = passngerdtl.Gender,
                                  SeatID = passngerdtl.SeatID,
                                  PDID = passngerdtl.PDID

                              }).ToList();

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->GetPassengerList  : \n " + ex);
            }

            return result;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///   GetReservationHistory: gives all history of given user Id
        /// </summary>
        /// <param name="userId"> used to fetch all details fro database</param>
        /// <returns>List<TicketModel>: Gives list of details</returns>
        public List<TicketModel> GetReservationHistory(int userId)
        {
            List<TicketModel> ticketModel = new List<TicketModel>();
            List<TicketModel> ticketModel2 = new List<TicketModel>();
            try
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    ticketModel = (from resdetail in iBusDB.ReservationDetails
                                   join dailytripd in iBusDB.DailyTripDetails on resdetail.DailyTripID equals dailytripd.DailyTripID
                                   join trpdtl in iBusDB.TripDetails on dailytripd.TripID equals trpdtl.TripID
                                   join rtedtl in iBusDB.RouteDetails on trpdtl.RouteID equals rtedtl.RouteID
                                   join ctydtl in iBusDB.CityDetails on rtedtl.ToCityID equals ctydtl.CityID
                                   orderby dailytripd.TripDate
                                   where resdetail.UserID == userId
                                   select new TicketModel
                                   {
                                       NumberofSeats = resdetail.ResNoOfSeat,
                                       DOR = resdetail.ResDate,
                                       Fare = resdetail.BillAmount,
                                       TripCode = dailytripd.TripID,
                                       DOJ = dailytripd.TripDate,
                                       DepartureTime = trpdtl.DepartureTime,
                                       Destination = ctydtl.City,
                                       PNR = resdetail.ResID,
                                       Status = resdetail.Status

                                   }).ToList();

                    ticketModel2 = (from resdetail in iBusDB.ReservationDetails
                                    join dailytripd in iBusDB.DailyTripDetails on resdetail.DailyTripID equals dailytripd.DailyTripID
                                    join trpdtl in iBusDB.TripDetails on dailytripd.TripID equals trpdtl.TripID
                                    join rtedtl in iBusDB.RouteDetails on trpdtl.RouteID equals rtedtl.RouteID
                                    join ctydtl in iBusDB.CityDetails on rtedtl.FromCityID equals ctydtl.CityID
                                    orderby dailytripd.TripDate
                                    where resdetail.UserID == userId
                                    select new TicketModel
                                    {
                                        Source = ctydtl.City
                                    }).ToList();
                }

                for (int i = 0; i < ticketModel.Count; i++) // Loop through List with for
                {
                    ticketModel[i].Source = ticketModel2[i].Source;
                    if (ticketModel[i].Status == AppConstant.CANCELLED)
                    {
                        ticketModel[i].IsActive = "Cancelled";
                    }
                    else
                    {
                        ticketModel[i].IsActive = "Booked";
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->GetReservationHistory  : \n " + ex);

            }
            return ticketModel;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  Retrives fare from DB for tripID passed
        /// </summary>
        /// <param name="DailyTripID">Contains tripID</param>
        /// <returns>TripDetailModel which contains fare</returns>
        public TripDetailModel GetFare(int DailyTripID)
        {
            TripDetailModel response = new TripDetailModel();
            try
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    response = (from dailyTripDeatils in iBusDB.DailyTripDetails
                                join tripdetails in iBusDB.TripDetails
                                  on dailyTripDeatils.TripID equals tripdetails.TripID
                                where dailyTripDeatils.DailyTripID == DailyTripID
                                select new TripDetailModel
                                {
                                    Fare = tripdetails.Fare
                                }).FirstOrDefault();


                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->GetTicketDetails  : \n " + ex);
            }
            return response;

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        ///  Decreases seat availabilty  in DailyTrip Table
        /// </summary>
        /// <param name="no_of_passenger"></param>
        /// <param name="DailyTripID">Conatins tripID to which passengers are going to board</param>
        /// <returns>Status as suceeded or failed</returns>
        public DailyTripDetailModel DecreaseSeatAvailability(int no_of_passenger, int DailyTripID)
        {
            DailyTripDetailModel response = new DailyTripDetailModel();
            try
            {
                var result = iBusDB.DailyTripDetails.FirstOrDefault(TripRow => TripRow.DailyTripID == DailyTripID);
                //Add to memory
                result.AvailableSeats = result.AvailableSeats - no_of_passenger;

                //Save changes to database
                if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    response.Status = AppConstant.SUCCESS;
                else
                    response.Status = AppConstant.ERROR;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDAO.cs, Function->UpdateRouteDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>                                   
        /// <summary>
        /// Returns EmailID from particular reservationID
        /// </summary>
        /// <param name="reservationID">Conatains reservationID which is used as PNR Number </param>
        /// <returns>Passenger Email ID</returns>
        public ReservationDetailModel GetEmailID(int reservationID)
        {
            ReservationDetailModel response = new ReservationDetailModel();
            try
            {
                var result = iBusDB.ReservationDetails.First(reservationRow => reservationRow.ResID == reservationID);
                response.ResEmailID = result.ResEmailID;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->GetResDateFromDailyTripID  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// Cancels reservation for all passengers
        /// </summary>
        /// <param name="reservation">Contains reservationID</param>
        /// <returns>Status as suceeded or failure</returns>
        public UpdateDailyTripDetailModel CancelReservation(ReservationDetailModel reservation)
        {
            UpdateDailyTripDetailModel response = new UpdateDailyTripDetailModel();
            //List<PassengerDetailModel> passengerlist = new List<PassengerDetailModel>();
            try
            {
                var passengerlist = iBusDB.PassengerDetails.Where(passengerrow => passengerrow.ResID == reservation.ResID).ToList();
                for (int i = 0; i < passengerlist.Count; i++)
                { passengerlist[i].Status = AppConstant.CANCELLED; }
                var reservationresult = iBusDB.ReservationDetails.FirstOrDefault(reservationRow => reservationRow.ResID == reservation.ResID);
                var dailytripdetailresult = iBusDB.DailyTripDetails.FirstOrDefault(triprow => triprow.DailyTripID == reservationresult.DailyTripID);
                dailytripdetailresult.AvailableSeats = dailytripdetailresult.AvailableSeats + reservationresult.ResNoOfSeat;
                reservationresult.Status = AppConstant.CANCELLED;
                //Save changes to database
                if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    response.Status = AppConstant.SUCCESS;
                else
                    response.Status = AppConstant.ERROR;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->CancelReservation  : \n " + ex);
            }

            return response;

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// it checks ticket status as booked or cancelled
        /// </summary>
        /// <param name="reservation">Contains reservationID</param>
        /// <returns>Status as failed or suceeded</returns>
        public UpdateDailyTripDetailModel CheckTicketStatus(ReservationDetailModel reservation)
        {
            UpdateDailyTripDetailModel response = new UpdateDailyTripDetailModel();
            try
            {
                var result = iBusDB.ReservationDetails.FirstOrDefault(reservationRow => reservationRow.ResID == reservation.ResID && reservationRow.Status == AppConstant.CANCELLED);
                if (result != null)
                { response.Status = AppConstant.SUCCESS; }
                else
                { response.Status = AppConstant.ERROR; }

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->CkeckUserIfAlreadyRegistered  : \n " + ex);
            }
            return response;

        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Navneet</author>
        /// <summary>
        /// It cancels reservation for particular passenger
        /// </summary>
        /// <param name="passengerID">Contains passengerID</param>
        /// <returns>Status as suceeded or failed</returns>
        public UpdatePassengerDetailModel CancelPassengerReservation(int passengerID)
        {
            UpdatePassengerDetailModel response = new UpdatePassengerDetailModel();
            try
            {
                var passenger = iBusDB.PassengerDetails.FirstOrDefault(passengerrow => passengerrow.PDID == passengerID);
                passenger.Status = AppConstant.CANCELLED;
                var reservationresult = iBusDB.ReservationDetails.FirstOrDefault(reservationRow => reservationRow.ResID == passenger.ResID);
                reservationresult.ResNoOfSeat = reservationresult.ResNoOfSeat - 1;
                if (reservationresult.ResNoOfSeat == 0)
                {
                    reservationresult.Status = AppConstant.CANCELLED;
                }
                var dailytripdetailresult = iBusDB.DailyTripDetails.FirstOrDefault(triprow => triprow.DailyTripID == reservationresult.DailyTripID);
                dailytripdetailresult.AvailableSeats = dailytripdetailresult.AvailableSeats + 1;

                //Save changes to database
                if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    response.Status = AppConstant.SUCCESS;
                else
                    response.Status = AppConstant.ERROR;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> UserDAO.cs, Function->CancelReservation  : \n " + ex);
            }

            return response;
        }
    }
}

