﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
namespace DataAccessObject
{
    public class ClassToActiveStatus
    {
        public void UpdateDailyTrip() 
        {
            using (iBusEntities iBusDB = new iBusEntities())
            {
                var dailyTripDetails = (from row in iBusDB.DailyTripDetails where row.Status != (int)AppConstant.STATUS.ACTIVE select row);
                dailyTripDetails.ToList().ForEach(row => { row.Status = (int)AppConstant.STATUS.ACTIVE; });
                iBusDB.SaveChanges();
            }
        }


        public void UpdateTrip() 
        {
            using (iBusEntities iBusDB = new iBusEntities())
            {
                var tripDetails = (from row in iBusDB.TripDetails where row.Status != (int)AppConstant.STATUS.ACTIVE select row);
                tripDetails.ToList().ForEach(row => { row.Status = (int)AppConstant.STATUS.ACTIVE; });
                iBusDB.SaveChanges();
            }
        }

        public void UpdateRoute() 
        {
            using (iBusEntities iBusDB = new iBusEntities())
            {
                var routeDetails = (from row in iBusDB.RouteDetails where row.Status != (int)AppConstant.STATUS.ACTIVE select row);
                routeDetails.ToList().ForEach(row => { row.Status = (int)AppConstant.STATUS.ACTIVE; });
                iBusDB.SaveChanges();
            }
        }

        public void UpdateBus() 
        {
            using (iBusEntities iBusDB = new iBusEntities())
            {
                var busDetails = (from row in iBusDB.BusDetails where row.Status != (int)AppConstant.STATUS.ACTIVE select row);
                busDetails.ToList().ForEach(row => { row.Status = (int)AppConstant.STATUS.ACTIVE; });
                iBusDB.SaveChanges();
            }
        }

        public void UpdateTravel() 
        {
            using (iBusEntities iBusDB = new iBusEntities())
            {
                var travelDetails = (from row in iBusDB.TravelDetails where row.Status != (int)AppConstant.STATUS.ACTIVE select row);
                travelDetails.ToList().ForEach(row => { row.Status = (int)AppConstant.STATUS.ACTIVE; });
                iBusDB.SaveChanges();
            }
        }
    }
}
