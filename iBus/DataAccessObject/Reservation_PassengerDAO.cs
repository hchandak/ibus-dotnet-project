﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessObject
{
    public class Reservation_PassengerDAO
    {
        public int?[] GetReservedSeat(int dailytripID)
        {
            int?[] str_ReservedSeat;
            using (iBusEntities iBusDB = new iBusEntities())
            {    
                int? [] output = (from reservationDetails in iBusDB.ReservationDetails
                                           join passengerDetails in iBusDB.PassengerDetails
                                               on reservationDetails.ResID equals passengerDetails.ResID
                                           where reservationDetails.DailyTripID == dailytripID
                                                     select passengerDetails.SeatID).ToArray();
                str_ReservedSeat = output;
            }

            return str_ReservedSeat;
        }
    }
}
