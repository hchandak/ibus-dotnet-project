﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
using Interface;
using log4net;
using System.Data;
using System.Data.Objects;
using System.Data.Entity;
using System.Globalization;

namespace DataAccessObject
{
    public class TripDAO : ITrip
    {
        
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Add new trip in DataBase
        /// TripDetailModel is argument type which contain information about trip which be want to add
        /// AddTripDetailModel is return type which return status of this operation (Success/Fail) 
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>AddTripDetailModel</returns>
        public AddTripDetailModel AddTripDetail(TripDetailModel tripDetail, List<BoardingPointDetailModel> boardingPointDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            AddTripDetailModel response = new AddTripDetailModel();
            try
            {
                if (IsTripExist(tripDetail))
                {
                    response.Status = AppConstant.RECORD_EXIST;
                    return response;
                }
                else
                {
                    //Need to add Journey duration once Database will update
                    TripDetail tripTable = new TripDetail() { BusID = tripDetail.BusID, RouteID = tripDetail.RouteID, FreqID = tripDetail.FreqID, Fare = tripDetail.Fare, DepartureTime = tripDetail.DepartureTime, JourneyTime = tripDetail.Totaltime, Status = tripDetail.Status, CreatedBy = tripDetail.CreatedBy, CreatedDate = Convert.ToDateTime(tripDetail.CreatedDate) };
                    //Add to memory
                    iBusDB.TripDetails.Add(tripTable);
                    //Save changes to database
                    if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    {
                        response.Status = AppConstant.SUCCESS;
                        response.TripID = tripTable.TripID;
                        if(AddBoardingPoint(boardingPointDetail,response.TripID))
                            response.Status = AppConstant.SUCCESS;
                        else
                            response.Status = AppConstant.ERROR;

                    }
                    else
                        response.Status = AppConstant.ERROR;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->AddTripDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Add boarding point in DataBase
        /// </summary>
        /// <param name="boardingPointDetail"></param>
        /// <param name="tripID"></param>
        /// <returns></returns>
        public bool AddBoardingPoint(List<BoardingPointDetailModel> boardingPointDetail,int tripID)
        {
            iBusEntities iBusDB = new iBusEntities();
            try
            {
                foreach (BoardingPointDetailModel boardingPoint in boardingPointDetail)
                {
                    BoardingPointDetail boardingTable = new BoardingPointDetail() {TripID=tripID, PointName = boardingPoint.PointName, PointAddress = boardingPoint.PointAddress, DepartureTime = boardingPoint.DepartureTime };
                    iBusDB.BoardingPointDetails.Add(boardingTable);
                    if (iBusDB.SaveChanges() == 0)
                        return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->AddBoardingPoint  : \n " + ex);
            }
            return true;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Delete boarding point
        /// </summary>
        /// <param name="tripID"></param>
        /// <returns></returns>
        public bool DeleteBoardingPoint(int tripID)
        {
             iBusEntities iBusDB = new iBusEntities();
             try
             {
                 var whereCondition = iBusDB.BoardingPointDetails.Where(row=> row.TripID == tripID);
                 foreach (var row in whereCondition)
                 {
                     iBusDB.BoardingPointDetails.Attach(row);
                     iBusDB.BoardingPointDetails.Remove(row);
                 
                 }
                 //Save changes to database
                 if (iBusDB.SaveChanges() > 0)
                     return true;

             }
             catch (Exception ex)
             {
                 Logger.Error("\n\nError Class-> TripsDAO.cs, Function->DeleteBoardingPoint  : \n " + ex);
             }
             return false;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch trip detail from DataBase
        /// GetIDAndStatusRequest is argument type which contain tripID to load trip detail
        /// TripDetailModel is return type which return trip detail
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>TripDetailModel</returns>
        public TripDetailModel LoadTripDetail(GetIDAndStatusRequest tripDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            BusDAO DAO = new BusDAO();
            TripDetailModel response = new TripDetailModel();
            try
            {
                var result = iBusDB.TripDetails.FirstOrDefault(tripRow => tripRow.TripID == tripDetail.ID);
                response.TripID = result.TripID;
                response.Totaltime = result.JourneyTime;
                response.TravelName = GetTravelNameByBusID(result.BusID);
                response.BusType = GetBusTypeByBusID(result.BusID);
                response.Route = GetCityByRouteID(result.RouteID);
                response.Amenities = GetAmenitiesByBusID(result.BusID);
                response.BusID = result.BusID;
                response.RouteID = result.RouteID;
                response.FreqID = result.FreqID;
                response.Fare = result.Fare;
                response.DepartureTime = result.DepartureTime;
                response.Departure = result.DepartureTime.ToString();
                response.CreatedBy = result.CreatedBy;
                response.CreatedDate = Convert.ToString(result.CreatedDate);
                response.Status = result.Status;
                response.ModifiedBy = result.ModifiedBy;
                response.ModifiedDate = Convert.ToString(result.ModifiedDate);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->LoadTripDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Update trip detail in BataBase
        /// TripDetailModel is argument type which contain information about trip which be want to update 
        /// UpdateTripDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>UpdateTripDetailModel</returns>
        public UpdateTripDetailModel UpdateTripDetail(TripDetailModel tripDetail, List<BoardingPointDetailModel> boardingPointDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            UpdateTripDetailModel response = new UpdateTripDetailModel();
            try
            {
                bool updateStatus = false;
                //Need to add Journey duration once Database will update
                var result = iBusDB.TripDetails.FirstOrDefault(tripRow => tripRow.TripID == tripDetail.TripID);
                //Add to memory
                result.FreqID = tripDetail.FreqID;
                result.Fare = tripDetail.Fare;
                result.DepartureTime = tripDetail.DepartureTime;
                result.JourneyTime = tripDetail.Totaltime;
                result.ModifiedBy = tripDetail.ModifiedBy;
                result.ModifiedDate = Convert.ToDateTime(tripDetail.ModifiedDate);
                iBusDB.TripDetails.Attach(result);
                iBusDB.Entry(result).State = EntityState.Modified;
                //Save changes to database
                if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                {
                    response.Status = AppConstant.SUCCESS;
                    if (DeleteBoardingPoint(tripDetail.TripID))
                    {
                        if (AddBoardingPoint(boardingPointDetail, tripDetail.TripID))
                            response.Status = AppConstant.SUCCESS;
                        else
                            response.Status = AppConstant.ERROR;
                    }
                    if (result.Status != tripDetail.Status)
                        updateStatus = true;
                    if (updateStatus)
                    {
                        UpdateStatusDAO DAO = new UpdateStatusDAO();
                                              
                        if (DAO.UpdateTripStatus(tripDetail.TripID, tripDetail.Status, tripDetail.ModifiedBy))
                        {
                            response.Status = AppConstant.SUCCESS;
                        }
                        else
                        {
                            response.Status = AppConstant.DEPENDENCY;
                        }
                    }
                }
                else
                    response.Status = AppConstant.ERROR;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->UpdateTripDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Delete trip detail in DataBase
        /// GetIDAndStatusRequest is argument type which contain tripID to load trip detail
        /// DeleteTripDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>DeleteTripDetailModel</returns>
        public DeleteTripDetailModel DeleteTripDetail(GetIDAndStatusRequest tripDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            DeleteTripDetailModel response = new DeleteTripDetailModel();
            try
            {
                var tripTable = iBusDB.TripDetails.FirstOrDefault(tripRow => tripRow.TripID == tripDetail.ID);

                //Add to memory
                tripTable.Status = Convert.ToInt32(AppConstant.STATUS.DELETED);
                //Save changes to database
                if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    response.Status = AppConstant.SUCCESS;
                else
                    response.Status = AppConstant.ERROR;

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->DeleteTripDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch all trip detail list from DataBase
        /// GetIDAndStatusRequest is argument type which contain tripIsActive 
        /// List<TripDetailModel> is return type which return trip detail list
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>List<TripDetailModel></returns>
        public List<TripDetailModel> SelectAllTripDetail(GetIDAndStatusRequest tripDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            var response = new List<TripDetailModel>();
            try
            {
                tripDetail.Status = Convert.ToInt32(AppConstant.STATUS.DELETED);
                var result = from tripTable in iBusDB.TripDetails
                             where tripTable.Status != tripDetail.Status
                             select new TripDetailModel
                             {
                                 TripID = tripTable.TripID,
                                 BusID = tripTable.BusID,
                                 RouteID = tripTable.RouteID,
                                 FreqID = tripTable.FreqID,
                                 DepartureTime = tripTable.DepartureTime,
                                 Totaltime = tripTable.JourneyTime,
                                 Fare = tripTable.Fare,
                                 Status = tripTable.Status,
                                 CreatedBy = tripTable.CreatedBy,
                                 //CreatedDate = Convert.ToString(tripTable.CreatedDate),
                                 ModifiedBy = tripTable.ModifiedBy,
                                 //ModifiedDate = Convert.ToString(tripTable.ModifiedDate),
                                 //Totaltime=tripTable.JourneyTime

                             };

                response = result.ToList();
                foreach (TripDetailModel list in response)
                {
                    list.BusType = GetBusTypeByBusID(list.BusID);
                    list.TravelName = GetTravelNameByBusID(list.BusID);
                    list.Route = GetCityByRouteID(list.RouteID);
                    if (list.Status == Convert.ToInt32(AppConstant.STATUS.ACTIVE))
                        list.IsActive = "Active";
                    else
                        list.IsActive = "Not Active";
                    list.JourneyDuration = GetDurationFromMinutes(list.Totaltime);
                    list.Departure = DateTime.Today.Add(list.DepartureTime).ToString("HH:mm"); //list.DepartureTime.ToString("hh:mm tt");
                }

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->SelectAllTripDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch city by routeID from DataBase
        /// return city
        /// </summary>
        /// <param name="routeID"></param>
        /// <returns>City</returns>
        public string GetCityByRouteID(int routeID)
        {
            iBusEntities iBusDB = new iBusEntities();
            string route = string.Empty;
            try
            {
                RouteDAO DAO = new RouteDAO();
                var result = iBusDB.RouteDetails.FirstOrDefault(routeRow => routeRow.RouteID == routeID);
                route = DAO.GetCityByID(result.FromCityID) + " To " + DAO.GetCityByID(result.ToCityID);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->GetCityByRouteID  : \n " + ex);
            }
            return route;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch travelName by busID from DataBase
        /// return travel name
        /// </summary>
        /// <param name="busID"></param>
        /// <returns>TravelName</returns>
        public string GetTravelNameByBusID(int busID)
        {
            iBusEntities iBusDB = new iBusEntities();
            string travelName = string.Empty;
            try
            {
                BusDAO DAO = new BusDAO();
                var result = iBusDB.BusDetails.FirstOrDefault(busRow => busRow.BusID == busID);
                travelName = DAO.GetTravelNameByID(result.TravelID);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->GetTravelNameByBusID  : \n " + ex);
            }
            return travelName;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch bus type by busID from DataBase
        /// return bus type
        /// </summary>
        /// <param name="busID"></param>
        /// <returns>BusType</returns>
        public string GetBusTypeByBusID(int busID)
        {
            iBusEntities iBusDB = new iBusEntities();
            string busType = string.Empty;
            try
            {

                BusDAO DAO = new BusDAO();
                var result = iBusDB.BusDetails.FirstOrDefault(busRow => busRow.BusID == busID);
                busType = DAO.GetBusTypeByBusTypID(result.BusTypeID);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->GetBusTypeByBusID  : \n " + ex);
            }
            return busType;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch all bus type and amenities list from DataBase
        /// Return bus type and amenities list
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>List<BusTypeAndAmenitiesModel></returns>
        public List<BusTypeAndAmenitiesModel> SelectAllBusTypeAndAmenitiesModel(GetIDAndStatusRequest tripDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            var response = new List<BusTypeAndAmenitiesModel>();
            BusDAO DAO = new BusDAO();
            try
            {
                var result = from bus in iBusDB.BusDetails
                             where bus.TravelID == tripDetail.ID
                             select new BusTypeAndAmenitiesModel
                             {
                                 TravelID = bus.TravelID,
                                 BusID = bus.BusID,
                                 BusTypeID = bus.BusTypeID
                             };

                response = result.ToList();
                foreach (BusTypeAndAmenitiesModel list in response)
                {
                    list.BusType = DAO.GetBusTypeByBusTypID(list.BusTypeID);
                    list.Amenities = DAO.GetAmenitiesByBusTypeID(list.BusTypeID);

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->SelectAllTripDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch all Frequency type list from DataBase
        /// Return all Frequency type list
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns></returns>
        public List<FreqDetailModel> SelectAllFrequencyType(GetIDAndStatusRequest tripDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            var response = new List<FreqDetailModel>();
            try
            {
                var result = from frequency in iBusDB.FreqDetails
                             select new FreqDetailModel
                             {
                                 FreqID = frequency.FreqID,
                                 FreqType = frequency.FreqType
                             };

                response = result.ToList();

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->SelectAllFrequencyType  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Check whether IsTripExist
        /// Return boolean value (True/False)
        /// </summary>
        /// <param name="tripDetail"></param>
        /// <returns>(True/False)</returns>
        public bool IsTripExist(TripDetailModel tripDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            try
            {
                var result = iBusDB.TripDetails.FirstOrDefault(tripRow => tripRow.BusID == tripDetail.BusID && tripRow.RouteID == tripDetail.RouteID && tripRow.DepartureTime==tripDetail.DepartureTime);
                if (result == null)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->IsTripExist  : \n " + ex);
            }
            return false;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch amenities by busID from DataBase
        /// Return amenities
        /// </summary>
        /// <param name="busID"></param>
        /// <returns>Amenities</returns>
        public string GetAmenitiesByBusID(int busID)
        {
            iBusEntities iBusDB = new iBusEntities();
            string amenities = string.Empty;
            try
            {
                BusDAO DAO = new BusDAO();
                var result = iBusDB.BusDetails.FirstOrDefault(busRow => busRow.BusID == busID);
                amenities = DAO.GetAmenitiesByBusTypeID(result.BusTypeID);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->GetAmenitiesByBusID  : \n " + ex);
            }
            return amenities;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch BusList
        /// Return BusList
        /// </summary>
        /// <param name="strDateOfJourney"></param>
        /// <param name="routeID"></param>
        /// <returns></returns>
        public List<ShowBusListModel> GetShowBusList(string strDateOfJourney, int routeID)
        {
            List<ShowBusListModel> listShowBusListModel = new List<ShowBusListModel>();
            using (iBusEntities iBusDB = new iBusEntities())
            {
                DateTime date = Convert.ToDateTime(strDateOfJourney);

                listShowBusListModel = (from tripd in iBusDB.TripDetails
                                        join dailytripd in iBusDB.DailyTripDetails
                                            on tripd.TripID equals dailytripd.TripID
                                        join busd in iBusDB.BusDetails
                                             on tripd.BusID equals busd.BusID
                                        join traveld in iBusDB.TravelDetails
                                         on busd.TravelID equals traveld.TravelID
                                        join bustyped in iBusDB.BusTypeDetails
                                        on busd.BusTypeID equals bustyped.BusTypeID
                                        orderby tripd.DepartureTime
                                        where dailytripd.TripDate == date && tripd.RouteID == routeID && dailytripd.Status==(int)AppConstant.STATUS.ACTIVE
                                        select new ShowBusListModel
                                        {
                                            DailyTripID = dailytripd.DailyTripID,
                                            TripID = tripd.TripID,
                                            TravelName = traveld.TravelName,
                                            BusType = bustyped.BusType,
                                            DepartureTime = tripd.DepartureTime,
                                            JouneyTime = tripd.JourneyTime,
                                            AvailableSeats = dailytripd.AvailableSeats,
                                            Fare = tripd.Fare,
                                            busTypeID = bustyped.BusTypeID
                                        }).ToList();
            }
            return listShowBusListModel;
        }

        
        public List<ShowBusListModel> GetShowBusList(string strDateOfJourney, List<SubRouteDetailModel> subRouteList)
        {
            List<ShowBusListModel> listShowBusListModel = new List<ShowBusListModel>();
            using (iBusEntities iBusDB = new iBusEntities())
            {
                DateTime date = Convert.ToDateTime(strDateOfJourney);

                listShowBusListModel = (from tripd in iBusDB.TripDetails
                                        join subRouteRow in subRouteList
                                            on tripd.RouteID equals subRouteRow.RouteID
                                        join dailytripd in iBusDB.DailyTripDetails
                                            on tripd.TripID equals dailytripd.TripID
                                        join busd in iBusDB.BusDetails
                                             on tripd.BusID equals busd.BusID
                                        join traveld in iBusDB.TravelDetails
                                         on busd.TravelID equals traveld.TravelID
                                        join bustyped in iBusDB.BusTypeDetails
                                        on busd.BusTypeID equals bustyped.BusTypeID
                                        join fareDetailRow in iBusDB.FareDetails
                                            on tripd.TripID equals fareDetailRow.TripID
                                        orderby tripd.DepartureTime
                                        where dailytripd.TripDate == date && dailytripd.Status == (int)AppConstant.STATUS.ACTIVE && fareDetailRow.StopNo == subRouteRow.StopNo
                                        select new ShowBusListModel
                                        {
                                            DailyTripID = dailytripd.DailyTripID,
                                            TripID = tripd.TripID,
                                            TravelName = traveld.TravelName,
                                            BusType = bustyped.BusType,
                                            DepartureTime = tripd.DepartureTime,
                                            JouneyTime = tripd.JourneyTime,
                                            Fare = fareDetailRow.Fare,
                                            AvailableSeats = dailytripd.AvailableSeats,
                                            busTypeID = bustyped.BusTypeID
                                        }).ToList();
            }
            return listShowBusListModel;
        }


        


        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Return Time duration
        /// <param name="minutes"></param>
        /// <returns></returns>
        public string GetDurationFromMinutes(int minutes) 
        {
            int durationHours = minutes / 60;
            int durationMinutes = minutes-durationHours*60;
            return durationMinutes == 0 ? durationHours + "h" : durationHours + "h " + durationMinutes + "m";
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch Boarding point list from DataBase
        /// Return Boarding point list
        /// </summary>
        /// <param name="tripID"></param>
        /// <returns></returns>
        public List<BoardingPointDetailModel> SelectAllBoardingPoint(int tripID)
        {
            iBusEntities iBusDB = new iBusEntities();
            List<BoardingPointDetailModel> response = new List<BoardingPointDetailModel>();
            try
            {
                var result = from boardingPoint in iBusDB.BoardingPointDetails
                             where boardingPoint.TripID == tripID
                             select new BoardingPointDetailModel
                             {
                                 PointID = boardingPoint.ID,
                                 TripID = boardingPoint.TripID,
                                 PointName = boardingPoint.PointName,
                                 PointAddress = boardingPoint.PointAddress,
                                 DepartureTime = boardingPoint.DepartureTime
                             };

                response = result.ToList();
                foreach (BoardingPointDetailModel list in response)
                {
                    list.Departure = DateTime.Today.Add(list.DepartureTime).ToString("HH:mm");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TripsDAO.cs, Function->SelectAllBoardingPoint  : \n " + ex);
            }
            return response;
        }

        public string GetBoradingPoints(int tripID)
        {
            string[] boardingPoints;
            using (iBusEntities iBusDB = new iBusEntities())
            {
                boardingPoints = (from row in iBusDB.BoardingPointDetails where row.TripID == tripID orderby row.DepartureTime select row.PointName).ToArray();       
            }
            return string.Join(",", boardingPoints);
        }

        public string GetBoradingPointsDepartureTime(int tripID)
        {
            TimeSpan[] timeSpan_DepartureTimes;
            using (iBusEntities iBusDB = new iBusEntities())
            {
                timeSpan_DepartureTimes = (from row in iBusDB.BoardingPointDetails where row.TripID == tripID orderby row.DepartureTime select row.DepartureTime).ToArray();
            }
            string[] str_DepartureTime = timeSpan_DepartureTimes.Select(row => { return string.Format("{0:00}:{1:00}", row.Hours, row.Minutes); }).ToArray();
            return string.Join(",", str_DepartureTime);
        }
        public List<DisplayBoardingPoint> GetBoardingPointDepartureTimeList(int tripID)
        {
            List<DisplayBoardingPoint> list_BoardingPoint_DepartureTime = new List<DisplayBoardingPoint>();
            using (iBusEntities iBusDB = new iBusEntities())
            {
                list_BoardingPoint_DepartureTime = (from row in iBusDB.BoardingPointDetails where row.TripID == tripID orderby row.DepartureTime select new DisplayBoardingPoint { 
                    BoardingPoints = row.PointName +" - " + row.DepartureTime
                }).ToList();
            }
            return list_BoardingPoint_DepartureTime;
        }

    }
}
