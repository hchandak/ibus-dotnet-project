﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
using Interface;
using log4net;
using System.Data;
using System.Data.Objects;
using System.Data.Entity;
namespace DataAccessObject
{
    public class BusDAO : IBus
    {
        
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Add new bus detail in DataBase
        /// BusDetailModel is argument type which contain information about bus which be want to add
        /// Also check IsBusTypeWithSameAminitiesExist
        /// AddBusDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns></returns>
        public AddBusDetailModel AddBusDetail(BusDetailModel busDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            AddBusDetailModel response = new AddBusDetailModel();
            try
            {
                if (IsBusTypeWithSameAminitiesExist(busDetail))
                {
                    response.Status = AppConstant.RECORD_EXIST;
                    return response;
                }
                else
                {
                    int busTypeID = GetBusTypeIDByAminities(busDetail);
                    BusDetail busTable = new BusDetail() { BusTypeID = busTypeID, Capacity = busDetail.Capacity, TravelID = busDetail.TravelID, Status = busDetail.Status, CreatedBy = busDetail.CreatedBy, CreatedDate = Convert.ToDateTime(busDetail.CreatedDate) };
                    //Add to memory
                    iBusDB.BusDetails.Add(busTable);
                    //Save changes to database
                    if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    {
                        response.Status = AppConstant.SUCCESS;
                        response.BusID = busTable.BusID;
                        response.TravelID = busTable.TravelID;
                    }
                    else
                        response.Status = AppConstant.ERROR;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->AddBusDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch bus detail from DataBase
        /// GetIDAndStatusRequest is argument type which contain busID to load bus detail
        /// BusDetailModel is return type which return bus detail
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>BusDetailModel</returns>
        public BusDetailModel LoadBusDetail(GetIDAndStatusRequest busDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            BusDetailModel response = new BusDetailModel();
            try
            {
                var result = iBusDB.BusDetails.FirstOrDefault(tavelRow => tavelRow.BusID == busDetail.ID);
                response.BusID = result.BusID;
                response.BusTypeID = result.BusTypeID;
                response.BusType = GetBusTypeByBusTypID(result.BusTypeID);
                response.Capacity = result.Capacity;
                response.TravelName = GetTravelNameByID(result.TravelID);
                response.Amenities = GetAmenitiesByBusTypeID(result.BusTypeID);
                response.CreatedBy = result.CreatedBy;
                response.CreatedDate = Convert.ToString(result.CreatedDate);
                response.Status = result.Status;
                response.ModifiedBy = result.ModifiedBy;
                response.ModifiedDate = Convert.ToString(result.ModifiedDate);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->LoadBusDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Update bus detail in DataBase
        /// BusDetailModel is argument type which contain information about bus which be want to update 
        /// UpdateBusDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>UpdateBusDetailModel</returns>
        public UpdateBusDetailModel UpdateBusDetail(BusDetailModel busDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            UpdateBusDetailModel response = new UpdateBusDetailModel();
            try
            {
                bool updateStatus=false;
                var result = iBusDB.BusDetails.FirstOrDefault(busRow => busRow.BusID == busDetail.BusID);
                //Add to memory
                result.Capacity = busDetail.Capacity;
                result.ModifiedBy = busDetail.ModifiedBy;
                result.ModifiedDate = Convert.ToDateTime(busDetail.ModifiedDate);
                iBusDB.BusDetails.Attach(result);
                iBusDB.Entry(result).State = EntityState.Modified;
                //Save changes to database
                if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                {
                    response.Status = AppConstant.SUCCESS;
                    if (result.Status != busDetail.Status)
                        updateStatus = true;
                    if (updateStatus)
                    {
                        UpdateStatusDAO DAO = new UpdateStatusDAO();
                        if (DAO.UpdateBusStatus(busDetail.BusID, busDetail.Status, busDetail.ModifiedBy))
                        {
                            response.Status = AppConstant.SUCCESS;
                        }
                        else
                        {
                            response.Status = AppConstant.DEPENDENCY;
                        }
                    }
                }
                else
                    response.Status = AppConstant.ERROR;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->UpdateBusDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Delete bus detail in DataBase
        /// GetIDAndStatusRequest is argument type which contain busID to load bus detail
        /// DeleteBusDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>DeleteBusDetailModel</returns>
        public DeleteBusDetailModel DeleteBusDetail(GetIDAndStatusRequest busDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            DeleteBusDetailModel response = new DeleteBusDetailModel();
            try
            {

                var busTable = iBusDB.BusDetails.FirstOrDefault(busRow => busRow.BusID == busDetail.ID);

                //Add to memory
                busTable.Status =Convert.ToInt32(AppConstant.STATUS.DELETED);
                //Save changes to database
                if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    response.Status = AppConstant.SUCCESS;
                else
                    response.Status = AppConstant.ERROR;

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->DeleteBusDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch bus detail list from DataBase
        /// GetIDAndStatusRequest is argument type which contain busIsActive 
        /// List<BusDetailModel> is return type which return bus detail list
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>List<BusDetailModel></returns>
        public List<BusDetailModel> SelectAllBusDetail(GetIDAndStatusRequest busDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            var response = new List<BusDetailModel>();
            try
            {
                busDetail.Status = Convert.ToInt32(AppConstant.STATUS.DELETED);
                var result = from busTable in iBusDB.BusDetails
                             where busTable.Status != busDetail.Status
                             select new BusDetailModel
                             {
                                 BusID = busTable.BusID,
                                 BusTypeID = busTable.BusTypeID,
                                 TravelID = busTable.TravelID,
                                 Capacity = busTable.Capacity,
                                 Status = busTable.Status,
                                 CreatedBy = busTable.CreatedBy,
                                 //CreatedDate = Convert.ToString(busTable.CreatedDate),
                                 ModifiedBy = busTable.ModifiedBy,
                                 //ModifiedDate = Convert.ToString(busTable.ModifiedDate),
                             };

                response = result.ToList();
                foreach (BusDetailModel list in response)
                {
                    string amenities = string.Empty;
                    list.BusType = GetBusTypeByBusTypID(list.BusTypeID);
                    list.TravelName = GetTravelNameByID(list.TravelID);
                    amenities=GetAmenitiesByBusTypeID(list.BusTypeID);
                    if (amenities.Length > 0)
                        list.Amenities = amenities;
                    else
                        list.Amenities = "None";
                    if (list.Status == Convert.ToInt32(AppConstant.STATUS.ACTIVE))
                        list.IsActive = "Active";
                    else
                        list.IsActive = "Not Active";
                }

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->SelectAllBusDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch bus type from DataBase based on busTypeID
        /// Return bus type
        /// </summary>
        /// <param name="busTypeID"></param>
        /// <returns>BusType</returns>
        public string GetBusTypeByBusTypID(int busTypeID)
        {
            iBusEntities iBusDB = new iBusEntities();
            var result = iBusDB.BusTypeDetails.FirstOrDefault(busTypeRow => busTypeRow.BusTypeID == busTypeID);
            return result.BusType;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch travel name from DataBase based on travelID
        /// Return travel name
        /// </summary>
        /// <param name="travelID"></param>
        /// <returns>TravelName</returns>
        public string GetTravelNameByID(int travelID)
        {
            iBusEntities iBusDB = new iBusEntities();
            var result = iBusDB.TravelDetails.FirstOrDefault(travelRow => travelRow.TravelID == travelID);
            return result.TravelName;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch all bus type from DataBase
        /// GetIDAndStatusRequest is argument type which contain busTypeIsActive 
        /// List<BusTypeDetailModel> is return type which return bus type detail list 
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>List<BusTypeDetailModel></returns>
        public List<BusTypeDetailModel> SelectAllBusType(GetIDAndStatusRequest busDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            List<BusTypeDetailModel> response = new List<BusTypeDetailModel>();
            try
            {

                var result = iBusDB.BusTypeDetails.Distinct().Select(busTypeTable => new BusTypeDetailModel
                {
                    BusTypeID = busTypeTable.BusTypeID,
                    BusType = busTypeTable.BusType,
                    CreatedBy = busTypeTable.CreatedBy,
                    //CreatedDate = Convert.ToString(busTable.CreatedDate),
                    ModifiedBy = busTypeTable.ModifiedBy,
                    //ModifiedDate = Convert.ToString(busTable.ModifiedDate),
                }).Distinct().ToList();

                response = result;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->SelectAllBusType  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch all Amenities based on busTypeID
        /// Return Amenities
        /// </summary>
        /// <param name="busTypeID"></param>
        /// <returns>Amenities</returns>
        public string GetAmenitiesByBusTypeID(int busTypeID)
        {
            iBusEntities iBusDB = new iBusEntities();
            string strAmenities = string.Empty;
            List<string> listAmenitities = new List<string>();
            int count = 0;
            try
            {
                var result1 = iBusDB.BusTypeDetails.FirstOrDefault(busTypeRow => busTypeRow.BusTypeID == busTypeID);
                var result2 = iBusDB.AmenitiesDetails.FirstOrDefault(amenitiesRow => amenitiesRow.AmenitiesID == result1.AmenitiesID);
                if (result2.WaterBottle)
                {
                    listAmenitities.Add(AppConstant.AMENITIES[count]);
                    //strAmenities += AppConstant.AMENITIES[count] + " ";
                }
                count++;
                if (result2.Movie)
                {
                    listAmenitities.Add(AppConstant.AMENITIES[count]);
                    //strAmenities += AppConstant.AMENITIES[count] + " ";
                }
                count++;
                if (result2.Blanket)
                {
                    listAmenitities.Add(AppConstant.AMENITIES[count]);
                    //strAmenities += AppConstant.AMENITIES[count] + " ";
                }
                count++;
                if (result2.ReadingLight)
                {
                    listAmenitities.Add(AppConstant.AMENITIES[count]);
                    //strAmenities += AppConstant.AMENITIES[count] + " ";
                }
                count++;
                if (result2.ChargingPoint)
                {
                    listAmenitities.Add(AppConstant.AMENITIES[count]);
                    //strAmenities += AppConstant.AMENITIES[count];
                }

                strAmenities = string.Join(", ", listAmenitities.ToArray());
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->GetAmenitiesByBusTypeID  : \n " + ex);
            }
            return strAmenities;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Check whether bus type with same amenities exist
        /// Retrun a boolean value (True/False)
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns>True/False</returns>
        public bool IsBusTypeWithSameAminitiesExist(BusDetailModel busDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            try
            {
                var result1 = iBusDB.AmenitiesDetails.FirstOrDefault(amenitiesRow => amenitiesRow.WaterBottle == busDetail.WaterBottle && amenitiesRow.Movie == busDetail.Movie && amenitiesRow.Blanket == busDetail.Blanket && amenitiesRow.ReadingLight == busDetail.ReadingLight && amenitiesRow.ChargingPoint == busDetail.ChargingPoint);
                if (result1 == null)
                    return false;
                else
                {
                    var result2 = iBusDB.BusTypeDetails.FirstOrDefault(busTypeRow => busTypeRow.BusType == busDetail.BusType && busTypeRow.AmenitiesID == result1.AmenitiesID);
                    if (result2 == null)
                        return false;
                    else
                    {
                        var result3 = iBusDB.BusDetails.FirstOrDefault(busRow => busRow.BusTypeID == result2.BusTypeID && busRow.TravelID == busDetail.TravelID && busRow.Capacity == busDetail.Capacity);
                        if (result3 == null)
                            return false;
                        else
                            return true;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->IsBusTypeWithSameAminitiesExist  : \n " + ex);
            }
            return false;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch busTypeID based on Amenities
        /// Return busTypeID
        /// </summary>
        /// <param name="busDetail"></param>
        /// <returns></returns>
        public int GetBusTypeIDByAminities(BusDetailModel busDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            int busTypeID = AppConstant.ERROR;
            try
            {
                var result1 = iBusDB.AmenitiesDetails.FirstOrDefault(amenitiesRow => amenitiesRow.WaterBottle == busDetail.WaterBottle && amenitiesRow.Movie == busDetail.Movie && amenitiesRow.Blanket == busDetail.Blanket && amenitiesRow.ReadingLight == busDetail.ReadingLight && amenitiesRow.ChargingPoint == busDetail.ChargingPoint);
                var result2 = iBusDB.BusTypeDetails.FirstOrDefault(busTypeRow => busTypeRow.BusType == busDetail.BusType && busTypeRow.AmenitiesID == result1.AmenitiesID);
                busTypeID = result2.BusTypeID;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->GetBusTypeIDByAminities  : \n " + ex);
            }
            return busTypeID;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// Fetch Travel(Bus operator)ID of busID passed as arguement
        /// Return TravelID
        /// </summary>
        /// <param name="busID">Bus ID</param>
        /// <returns></returns>
        public int GetTravelID(int busID)
        {
            int travelID=0;
            try
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var output = (from busDetailsRow in iBusDB.BusDetails
                                  where busDetailsRow.BusID == busID
                                  select busDetailsRow.TravelID);
                    travelID = Convert.ToInt32(output);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->GetTravelID  : \n " + ex);
            }
            return travelID;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// Fetch Travel(Bus operator)ID of busID passed as arguement
        /// Return TravelID
        /// </summary>
        /// <param name="busID"></param>
        /// <returns></returns>
        public int GetBusTypeID(int busID)
        {
            int busTypeID=0;
            try
            {
                using (iBusEntities ibusDB = new iBusEntities())
                {
                    var output = (from busDetailsRow in ibusDB.BusDetails
                                  where busDetailsRow.BusID == busID
                                  select busDetailsRow.BusTypeID);
                    busTypeID = Convert.ToInt32(output);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->GetBusType  : \n " + ex);
            }
            return busTypeID;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// Fetch busType 
        /// Return busType 
        /// </summary>
        /// <param name="busTypeID"></param>
        /// <returns></returns>
        public string GetBusType(int busTypeID)
        {
            string strBusType=string.Empty;
            try
            {
                using (iBusEntities ibusDB = new iBusEntities())
                {
                    strBusType = (from busTypeDetailsRow in ibusDB.BusTypeDetails
                                  where busTypeDetailsRow.BusTypeID == busTypeID
                                  select busTypeDetailsRow.BusType).ToString();

                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->GetBusType  : \n " + ex);
            }
            return strBusType;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// Fetch BusTypeList
        /// Return BusTypeList
        /// </summary>
        /// <returns></returns>
        public List<string> GetBusTypeNameList()
        {
            List<string> busTypeNameList=null;
            try
            {
                using (iBusEntities ibusDB = new iBusEntities())
                {
                    busTypeNameList = (from busTypeDetailsRow in ibusDB.BusTypeDetails
                                       select busTypeDetailsRow.BusType).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> BussDAO.cs, Function->GetBusTypeNameList  : \n " + ex);
            }
            return busTypeNameList;
        }
    }
}