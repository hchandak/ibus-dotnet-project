﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface;
using BussinessModel;

namespace DataAccessObject
{
    public class CityDAO : ICity
    {
        public List<CityDetailModel> GetCityDetail()
        {
            List<CityDetailModel> cityDetailList = new List<CityDetailModel>();
            using (iBusEntities iBusDB = new iBusEntities())
            {     

                cityDetailList = (from cityRow in iBusDB.CityDetails orderby cityRow.City
                                  select new CityDetailModel
                                  {
                                      CityID = cityRow.CityID,
                                      City = cityRow.City,
                                      Status = cityRow.Status,
                                      CreatedBy = cityRow.CreatedBy,
                                      CreatedDate = cityRow.CreatedDate,
                                      ModifiedBy = cityRow.ModifiedBy,
                                      ModifiedDate = cityRow.ModifiedDate
                                  }).ToList();

            }

            return cityDetailList;
        }

        public int GetCityID(string strCityName)
        {
            int cityID;
            using (iBusEntities iBusDB = new iBusEntities())
            {
                var city = iBusDB.CityDetails.First(cityRow => cityRow.City == strCityName);
                cityID = city.CityID;
            }
            return cityID;
        }
    }

}
