﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
using Interface;
using log4net;
using System.Data;
using System.Data.Entity;
using System.Data.Objects;
namespace DataAccessObject
{
    public class RouteDAO : IRoute
    {
        
        int fromCityID = 0, toCityID = 0;
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Add new route in DataBase
        /// RouteDetailModel is argument type which contain information about route which be want to add
        /// AddRouteDetailModel is return type which return status of this operation (Success/Fail) 
        /// </summary>
        /// <param name="routeDetail"></param>
        /// <returns>AddRouteDetailModel</returns>
        public AddRouteDetailModel AddRouteDetail(RouteDetailModel routeDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            AddRouteDetailModel response = new AddRouteDetailModel();
            try
            {
                fromCityID = GetCityIDByName(routeDetail.FromCity);
                toCityID = GetCityIDByName(routeDetail.ToCity);
                if (IsRouteExist(fromCityID, toCityID))
                {
                    response.Status = AppConstant.RECORD_EXIST;
                }
                else
                {
                    RouteDetail route1 = new RouteDetail() { FromCityID = fromCityID, ToCityID = toCityID, Status = routeDetail.Status, CreatedBy = routeDetail.CreatedBy, CreatedDate = Convert.ToDateTime(routeDetail.CreatedDate) };
                    RouteDetail route2 = new RouteDetail() { FromCityID = toCityID, ToCityID = fromCityID, Status = routeDetail.Status, CreatedBy = routeDetail.CreatedBy, CreatedDate = Convert.ToDateTime(routeDetail.CreatedDate) };
                    //Add to memory
                    iBusDB.RouteDetails.Add(route1);
                    iBusDB.RouteDetails.Add(route2);
                    //Save changes to database
                    if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    {
                        response.Status = AppConstant.SUCCESS;
                        response.Route1ID = route1.RouteID;
                        response.Route2ID = route2.RouteID;
                    }
                    else
                        response.Status = AppConstant.ERROR;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDAO.cs, Function->AddRouteDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch route detail from DataBase
        /// GetIDAndStatusRequest is argument type which contain routeID to load route detail
        /// RouteDetailModel is return type which return route detail
        /// </summary>
        /// <param name="routeDetail"></param>
        /// <returns>RouteDetailModel</returns>
        public RouteDetailModel LoadRouteDetail(GetIDAndStatusRequest routeDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            RouteDetailModel response = new RouteDetailModel();
            try
            {
                var result = iBusDB.RouteDetails.FirstOrDefault(routeRow => routeRow.RouteID == routeDetail.ID);
                response.RouteID = result.RouteID;
                response.FromCity = GetCityByID(result.FromCityID);
                response.ToCity = GetCityByID(result.ToCityID);
                response.CreatedBy = result.CreatedBy;
                response.CreatedDate = Convert.ToString(result.CreatedDate);
                response.Status = result.Status;
                response.ModifiedBy = result.ModifiedBy;
                response.ModifiedDate = Convert.ToString(result.ModifiedDate);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDAO.cs, Function->LoadRouteDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Update route detail in DataBase
        /// RouteDetailModel is argument type which contain information about route which be want to update 
        /// UpdateRouteDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="routeDetail"></param>
        /// <returns>UpdateRouteDetailModel</returns>
        public UpdateRouteDetailModel UpdateRouteDetail(RouteDetailModel routeDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            UpdateRouteDetailModel response = new UpdateRouteDetailModel();
            try
            {
                bool updateStatus = false;
                var result = iBusDB.RouteDetails.FirstOrDefault(routeRow => routeRow.RouteID == routeDetail.RouteID);
                //Add to memory
                result.ModifiedBy = routeDetail.ModifiedBy;
                result.ModifiedDate = Convert.ToDateTime(routeDetail.ModifiedDate);
                iBusDB.RouteDetails.Attach(result);
                iBusDB.Entry(result).State = EntityState.Modified;
                //Save changes to database
                if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                {
                    response.Status = AppConstant.SUCCESS;
                    if (result.Status != routeDetail.Status)
                        updateStatus = true;
                    if (updateStatus)
                    {
                        UpdateStatusDAO DAO = new UpdateStatusDAO();
                        if (DAO.UpdateRouteStatus(routeDetail.RouteID, routeDetail.Status, routeDetail.ModifiedBy))
                        {
                            response.Status = AppConstant.SUCCESS;
                        }
                        else
                        {
                            response.Status = AppConstant.ERROR;
                        }
                    }
                }
                else
                    response.Status = AppConstant.ERROR;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDAO.cs, Function->UpdateRouteDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Delete route from DataBase
        /// GetIDAndStatusRequest is argument type which contain routeID to load route detail
        /// DeleteRouteDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="routeDetail"></param>
        /// <returns>DeleteRouteDetailModel</returns>
        public DeleteRouteDetailModel DeleteRouteDetail(GetIDAndStatusRequest routeDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            DeleteRouteDetailModel response = new DeleteRouteDetailModel();
            try
            {
                var routeTable = iBusDB.RouteDetails.FirstOrDefault(routeRow => routeRow.RouteID == routeDetail.ID);

                //Add to memory
                routeTable.Status = Convert.ToInt32(AppConstant.STATUS.DELETED);
                //Save changes to database
                if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    response.Status = AppConstant.SUCCESS;
                else
                    response.Status = AppConstant.ERROR;

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDAO.cs, Function->DeleteRouteDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch all route detail list from DataBase
        /// GetIDAndStatusRequest is argument type which contain routeIsActive 
        /// List<RouteDetailModel> is return type which return route detail list
        /// </summary>
        /// <param name="routeDetail"></param>
        /// <returns>List<RouteDetailModel></returns>
        public List<RouteDetailModel> SelectAllRouteDetail(GetIDAndStatusRequest routeDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            var response = new List<RouteDetailModel>();
            try
            {
                routeDetail.Status = Convert.ToInt32(AppConstant.STATUS.DELETED);
                var result = from routeTable in iBusDB.RouteDetails
                             where routeTable.Status != routeDetail.Status
                             select new RouteDetailModel
                             {
                                 RouteID = routeTable.RouteID,
                                 FromCityID = routeTable.FromCityID,
                                 ToCityID = routeTable.ToCityID,
                                 Status = routeTable.Status,
                                 CreatedBy = routeTable.CreatedBy,
                                 //CreatedDate = Convert.ToString(travelTable.CreatedDate),
                                 ModifiedBy = routeTable.ModifiedBy,
                                 //ModifiedDate = Convert.ToString(travelTable.ModifiedDate),

                             };
                response = result.ToList();
                foreach (RouteDetailModel list in response)
                {
                    list.FromCity = GetCityByID(list.FromCityID);
                    list.ToCity = GetCityByID(list.ToCityID);
                    if (list.Status == Convert.ToInt32(AppConstant.STATUS.ACTIVE))
                        list.IsActive = "Active";
                    else
                        list.IsActive = "Not Active";
                }


            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDAO.cs, Function->SelectAllRouteDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch city from DataBase base on cityID
        /// Return city name
        /// </summary>
        /// <param name="cityID"></param>
        /// <returns>CityName</returns>
        public string GetCityByID(int cityID)
        {
            iBusEntities iBusDB = new iBusEntities();
            string strCity = string.Empty;
            try
            {
                var result = iBusDB.CityDetails.FirstOrDefault(cityRow => cityRow.CityID == cityID);
                strCity = result.City;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDAO.cs, Function->GetCityByID  : \n " + ex);
            }
            return strCity;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch cityID from DataBase base on city
        /// Return cityID
        /// </summary>
        /// <param name="city"></param>
        /// <returns>cityID</returns>
        public int GetCityIDByName(string city)
        {
            iBusEntities iBusDB = new iBusEntities();
            int cityID = AppConstant.ERROR;
            try
            {
                var result = iBusDB.CityDetails.FirstOrDefault(cityRow => cityRow.City == city);
                cityID = result.CityID;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDAO.cs, Function->GetCityIDByName  : \n " + ex);
            }

            return cityID;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Check whether IsRouteExist 
        /// Return boolean value (True/False)
        /// </summary>
        /// <param name="fromCityID"></param>
        /// <param name="toCityID"></param>
        /// <returns>(True/False)</returns>
        public bool IsRouteExist(int fromCityID, int toCityID)
        {
            iBusEntities iBusDB = new iBusEntities();
            try
            {
                var result = iBusDB.RouteDetails.FirstOrDefault(routeRow => routeRow.FromCityID == fromCityID && routeRow.ToCityID == toCityID);
                if (result != null)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDAO.cs, Function->IsRouteExist  : \n " + ex);
            }
            return false;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch routeID 
        /// Return routeID
        /// </summary>
        /// <param name="fromCityID"></param>
        /// <param name="toCityID"></param>
        /// <returns></returns>
        public int GetRouteID(int fromCityID, int toCityID)
        {
            int routeID=0;
            try
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var route = iBusDB.RouteDetails.FirstOrDefault(routeRow => (routeRow.FromCityID == fromCityID) && (routeRow.ToCityID == toCityID) &&(routeRow.Status==(int)AppConstant.STATUS.ACTIVE));
                    if (route != null)
                        routeID = route.RouteID;
                    else
                        routeID = -1;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> RouteDAO.cs, Function->GetRouteID  : \n " + ex);
            }
            return routeID;
        }

        
        /*
        //public List<SubRouteDetailModel> GetSubRouteList(int fromCityID, int toCityID)
        //{
        //    List<SubRouteDetailModel> subRouteList = new List<SubRouteDetailModel>();
        //    try
        //    {
        //        using (iBusEntities iBusDB = new iBusEntities())
        //        {
        //            subRouteList = (from routeRow in iBusDB.RouteDetails
        //                            join subRouteRow in iBusDB.SubRouteDetails
        //                                on routeRow.RouteID equals subRouteRow.RouteID
        //                            where routeRow.FromCityID == fromCityID && subRouteRow.StopCityID == toCityID
        //                            select new SubRouteDetailModel 
        //                            {
        //                                RouteID = routeRow.RouteID,
        //                                StopNo = subRouteRow.StopNo
        //                            }
        //                            ).ToList();

        //        }
                
        //        subRouteList.ForEach(row => row.viaCities = GetViaStops(row.RouteID,row.StopNo));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("\n\nError Class-> RouteDAO.cs, Function->GetRouteID  : \n " + ex);
        //    }
        //    return subRouteList;
        //}
        */
        //public string GetViaStops(int routeID, int stopNo)
        //{
        //    string str_viaStops = string.Empty;
        //    using (iBusEntities iBusDB = new iBusEntities())
        //    {
        //        var result = (from subRouteRow in iBusDB.SubRouteDetails
        //                      join cityRow in iBusDB.CityDetails
        //                          on subRouteRow.StopCityID equals cityRow.CityID
        //                      where subRouteRow.RouteID == routeID && subRouteRow.StopNo < stopNo
        //                      select cityRow.City);
        //        str_viaStops = string.Join(",", result.ToArray());
        //    }
        //    return str_viaStops;
        //}
        */
    }
}
