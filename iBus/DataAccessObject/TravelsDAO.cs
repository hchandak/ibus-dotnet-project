﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
using Interface;
using log4net;
using System.Data;
using System.Data.Objects;
using System.Data.Entity;

namespace DataAccessObject
{
    public class TravelsDAO : ITravels
    {
        
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Add new travel in DataBase
        /// TravelDetailModel is argument type which contain information about travel which be want to add
        /// AddTravelDetailModel is return type which return status of this operation (Success/Fail) 
        /// </summary>
        /// <param name="travelDetail"></param>
        /// <returns>AddTravelDetailModel</returns>
        public AddTravelDetailModel AddTravelDetail(TravelDetailModel travelDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            AddTravelDetailModel response = new AddTravelDetailModel();
            try
            {
                if (IsTravelOrEmailExist(travelDetail.TravelName, travelDetail.TravelEmail,AppConstant.ERROR))
                {
                    response.Status = AppConstant.RECORD_EXIST;
                    return response;
                }
                else
                {
                    TravelDetail travelTable = new TravelDetail() { TravelName = travelDetail.TravelName, ContactAddress = travelDetail.ContactAddress, ContactNo = travelDetail.ContactNo, Status = travelDetail.Status, CreatedBy = travelDetail.CreatedBy, CreatedDate = Convert.ToDateTime(travelDetail.CreatedDate), TravelEmail = travelDetail.TravelEmail };
                    //Add to memory
                    iBusDB.TravelDetails.Add(travelTable);
                    //Save changes to database
                    if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    {
                        response.Status = AppConstant.SUCCESS;
                        response.TravelID = travelTable.TravelID;
                    }
                    else
                        response.Status = AppConstant.ERROR;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsDAO.cs, Function->AddTravelDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        ///Fetch travel detail from DataBase based on travelID
        /// GetIDAndStatusRequest is argument type which contain travelID to load travel detail
        /// TravelDetailModel is return type which return travel detail
        /// </summary>
        /// <param name="travelDetail"></param>
        /// <returns>TravelDetailModel</returns>
        public TravelDetailModel LoadTravelDetail(GetIDAndStatusRequest travelDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            TravelDetailModel response = new TravelDetailModel();
            try
            {
                var result = iBusDB.TravelDetails.FirstOrDefault(tavelRow => tavelRow.TravelID == travelDetail.ID);
                response.TravelID = result.TravelID;
                response.TravelName = result.TravelName;
                response.ContactAddress = result.ContactAddress;
                response.TravelEmail = result.TravelEmail;
                response.ContactNo = result.ContactNo;
                response.CreatedBy = result.CreatedBy;
                response.CreatedDate = Convert.ToString(result.CreatedDate);
                response.Status = result.Status;
                response.ModifiedBy = result.ModifiedBy;
                response.ModifiedDate = Convert.ToString(result.ModifiedDate);
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsDAO.cs, Function->LoadTravelDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Update travel detail in DataBase
        /// TravelDetailModel is argument type which contain information about travel which be want to update 
        /// UpdateTravelDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="travelDetail"></param>
        /// <returns>UpdateTravelDetailModel</returns>
        public UpdateTravelDetailModel UpdateTravelDetail(TravelDetailModel travelDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            UpdateTravelDetailModel response = new UpdateTravelDetailModel();
            try
            {
                if (IsTravelOrEmailExist(travelDetail.TravelName, travelDetail.TravelEmail, travelDetail.TravelID))
                {
                    response.Status = AppConstant.RECORD_EXIST;
                    return response;
                }
                else
                {
                    bool updateStatus = false;
                    var result = iBusDB.TravelDetails.FirstOrDefault(tavelRow => tavelRow.TravelID == travelDetail.TravelID);
                    //Add to memory
                    result.TravelName = travelDetail.TravelName;
                    result.ContactAddress = travelDetail.ContactAddress;
                    result.TravelEmail = travelDetail.TravelEmail;
                    result.ContactNo = travelDetail.ContactNo;
                    result.ModifiedBy = travelDetail.ModifiedBy;
                    result.ModifiedDate = Convert.ToDateTime(travelDetail.ModifiedDate);
                    iBusDB.TravelDetails.Attach(result);
                    iBusDB.Entry(result).State = EntityState.Modified;
                    //Save changes to database
                    if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    {
                        response.Status = AppConstant.SUCCESS;
                        if (result.Status != travelDetail.Status)
                            updateStatus = true;
                        if (updateStatus)
                        {
                            UpdateStatusDAO DAO = new UpdateStatusDAO();
                            if (DAO.UpdateTravelStatus(travelDetail.TravelID, travelDetail.Status, travelDetail.ModifiedBy))
                            {
                                response.Status = AppConstant.SUCCESS;
                            }
                            else
                            {
                                response.Status = AppConstant.ERROR;
                            }
                        }
                    }
                    else
                        response.Status = AppConstant.ERROR;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsDAO.cs, Function->UpdateTravelDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Delete travel detail from DataBase
        /// GetIDAndStatusRequest is argument type which contain travelID to load travel detail
        /// DeleteTravelDetailModel is return type which return status of this operation (Success/Fail)
        /// </summary>
        /// <param name="travelDetail"></param>
        /// <returns>DeleteTravelDetailModel</returns>
        public DeleteTravelDetailModel DeleteTravelDetail(GetIDAndStatusRequest travelDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            DeleteTravelDetailModel response = new DeleteTravelDetailModel();
            try
            {
                var travelTabel = iBusDB.TravelDetails.FirstOrDefault(tavelRow => tavelRow.TravelID == travelDetail.ID);

                //Add to memory
                travelTabel.Status = Convert.ToInt32(AppConstant.STATUS.DELETED);

                //Save changes to database
                if (iBusDB.SaveChanges() > 0)//Returns number of rows affected
                    response.Status = AppConstant.SUCCESS;
                else
                    response.Status = AppConstant.ERROR;

            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsDAO.cs, Function->DeleteTravelDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Fetch all travel detail list from DataBase
        /// GetIDAndStatusRequest is argument type which contain travelIsActive 
        /// List<TravelDetailModel> is return type which return travel detail list
        /// </summary>
        /// <param name="travelDetail"></param>
        /// <returns>List<TravelDetailModel></returns>
        public List<TravelDetailModel> SelectAllTravelDetail(GetIDAndStatusRequest travelDetail)
        {
            iBusEntities iBusDB = new iBusEntities();
            var response = new List<TravelDetailModel>();
            try
            {
                travelDetail.Status = Convert.ToInt32(AppConstant.STATUS.DELETED);
                var result = from travelTable in iBusDB.TravelDetails
                             where travelTable.Status != travelDetail.Status
                             select new TravelDetailModel
                             {
                                 TravelID = travelTable.TravelID,
                                 TravelName = travelTable.TravelName,
                                 ContactNo = travelTable.ContactNo,
                                 ContactAddress = travelTable.ContactAddress,
                                 Status = travelTable.Status,
                                 CreatedBy = travelTable.CreatedBy,
                                 //CreatedDate = Convert.ToString(travelTable.CreatedDate),
                                 ModifiedBy = travelTable.ModifiedBy,
                                 //ModifiedDate = Convert.ToString(travelTable.ModifiedDate),
                                 TravelEmail = travelTable.TravelEmail
                             };

                response = result.ToList();
                foreach (TravelDetailModel list in response)
                {
                    if (list.Status == Convert.ToInt32(AppConstant.STATUS.ACTIVE))
                        list.IsActive = "Active";
                    else
                        list.IsActive = "Not Active";
                }
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsDAO.cs, Function->SelectAllTravelDetail  : \n " + ex);
            }
            return response;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Maneesh</author>
        /// <summary>
        /// Check whether IsTravelOrEmailExist
        /// Return boolean value (True/False)
        /// </summary>
        /// <param name="travelName"></param>
        /// <param name="travelEmail"></param>
        /// <returns>(True/False)</returns>
        protected bool IsTravelOrEmailExist(string travelName, string travelEmail,int travelID)
        {
            iBusEntities iBusDB = new iBusEntities();
            try
            {
                if (travelID > AppConstant.ERROR)
                {
                    var result = iBusDB.TravelDetails.FirstOrDefault(travelRow => travelRow.TravelID != travelID && (travelRow.TravelName == travelName || travelRow.TravelEmail == travelEmail));
                    if (result != null)
                        return true;
                    else
                        return false;
                }
                else
                {
                    var result = iBusDB.TravelDetails.FirstOrDefault(travelRow => travelRow.TravelName == travelName || travelRow.TravelEmail == travelEmail);
                    if (result != null)
                        return true;
                    else
                        return false;
                }
                
            }
            catch (Exception ex)
            {
                Logger.Error("\n\nError Class-> TravelsDAO.cs, Function->IsTravelOrEmailExist  : \n " + ex);
            }
            return false;
        }

        ///<copyright>(c) Dot Net team 2013.</copyright>
        ///<author>Hemant</author>
        /// <summary>
        /// it will get TRAVEL NAME from TRAVELDETAILS table of given TRAVELID passed as parametre
        /// </summary>
        /// <param name="travelID"></param>
        /// <returns></returns>
        public List<string> GetTravelNames()
        {
           
            List<string> listTravelNames = new List<string>();
            using (iBusEntities iBusDB = new iBusEntities())
            {
                listTravelNames = (from travelDetailsRow in iBusDB.TravelDetails
                                   select travelDetailsRow.TravelName).ToList();
            }
            return listTravelNames;
        }
    }
}
