﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessModel;
using log4net;
using System.Data;
using System.Data.Entity;
using System.Data.Objects;
using BussinessModel;
namespace DataAccessObject
{
    public class UpdateStatusDAO
    {
       
        public bool UpdateTravelStatus(int updatingTravelID, int updatingStatus, string updatingModifiedBy)
        {
            bool successfullUpdate = false;
            if (updatingStatus == (int)AppConstant.STATUS.IN_ACTIVE || updatingStatus == (int)AppConstant.STATUS.DELETED)
            {
                List<int> updatingBusID = new List<int>();
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var travelDetail = (from travelDetailRow in iBusDB.TravelDetails where travelDetailRow.TravelID == updatingTravelID select travelDetailRow);
                    travelDetail.ToList().ForEach(row => { row.Status = updatingStatus; row.ModifiedBy = updatingModifiedBy; row.ModifiedDate = System.DateTime.Now; });

                    if (iBusDB.SaveChanges() <= 0)
                        return false;
                    else
                        successfullUpdate = true;
                                                       

                    updatingBusID = (from busDetailRow in iBusDB.BusDetails where busDetailRow.TravelID == updatingTravelID select busDetailRow.BusID).ToList();
                }
                if(updatingBusID.Count != 0)
                successfullUpdate = UpdateBusStatus(updatingBusID, updatingStatus, updatingModifiedBy);
               
            }
            else if (updatingStatus == (int)AppConstant.STATUS.ACTIVE)
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var travelDetail = (from travelDetailRow in iBusDB.TravelDetails where travelDetailRow.TravelID == updatingTravelID select travelDetailRow).FirstOrDefault();
                    
                    travelDetail.Status = updatingStatus;
                    travelDetail.ModifiedBy = updatingModifiedBy;
                    travelDetail.ModifiedDate = System.DateTime.Now;

                    if (iBusDB.SaveChanges() > 0)
                        successfullUpdate = true;
                }
               
            }
            return successfullUpdate;
            
        }

        public bool UpdateBusStatus(int updatingBusID, int updatingStatus, string updatingModifiedBy)
        {
            bool successfullUpdate = false;
            if (updatingStatus == (int)AppConstant.STATUS.IN_ACTIVE || updatingStatus == (int)AppConstant.STATUS.DELETED)
            {
                List<int> updatingTripID = new List<int>();
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var busDetail = (from busDetailRow in iBusDB.BusDetails where busDetailRow.BusID == updatingBusID select busDetailRow).FirstOrDefault();

                    busDetail.Status = updatingStatus;
                    busDetail.ModifiedBy = updatingModifiedBy;
                    busDetail.ModifiedDate = System.DateTime.Now;

                    if (iBusDB.SaveChanges() <= 0)
                        return false;
                    else
                        successfullUpdate = true;
                                          
                    updatingTripID = (from tripDetailRow in iBusDB.TripDetails where tripDetailRow.BusID == updatingBusID select tripDetailRow.TripID).ToList();
                }
                if (updatingTripID.Count != 0)
                successfullUpdate = UpdateTripStatus(updatingTripID, updatingStatus, updatingModifiedBy);
                
            }
            else if (updatingStatus == (int)AppConstant.STATUS.ACTIVE)
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var busDetail = (from busDetailRow in iBusDB.BusDetails where busDetailRow.BusID == updatingBusID select busDetailRow).FirstOrDefault();
                    //in travel details  table, check whether the given id status is inactive
                    if (CheckTravelDetails(busDetail.TravelID))
                    {
                        busDetail.Status = updatingStatus;
                        busDetail.ModifiedBy = updatingModifiedBy;
                        busDetail.ModifiedDate = System.DateTime.Now;

                        if (iBusDB.SaveChanges() > 0)
                            successfullUpdate = true;
                    }
                    else 
                    {
                        //message- you cannot make status active as travel status is inactive or delete
                    }

                }
              
            }
            return successfullUpdate;

            
        }

        public bool UpdateBusStatus(List<int> updatingBusID, int updatingStatus, string updatingModifiedBy)
        {
            bool successfullUpdate = false;
            List<int> updatingTripID = new List<int>();
            using (iBusEntities iBusDB = new iBusEntities())
            {
                var busDetail = (from busDetailRow in iBusDB.BusDetails where updatingBusID.Contains(busDetailRow.BusID) select busDetailRow);
                busDetail.ToList().ForEach(row => { row.Status = updatingStatus; row.ModifiedBy = updatingModifiedBy; row.ModifiedDate = System.DateTime.Now; });
                if (iBusDB.SaveChanges() <= 0)
                    return false;
                else
                    successfullUpdate = true;
                updatingTripID = (from tripDetailRow in iBusDB.TripDetails where updatingBusID.Contains(tripDetailRow.BusID) select tripDetailRow.TripID).ToList();
            }
            if (updatingTripID.Count != 0)
            successfullUpdate=UpdateTripStatus(updatingTripID, updatingStatus, updatingModifiedBy);
            return successfullUpdate;
        }


        public bool UpdateRouteStatus(int updatingRouteID, int updatingStatus, string updatingModifiedBy)
        {
            bool successfullUpdate = false;
            if (updatingStatus == (int)AppConstant.STATUS.IN_ACTIVE || updatingStatus == (int)AppConstant.STATUS.DELETED)
            {
                List<int> updatingTripID = new List<int>();
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var routeDetail = (from routeDetailRow in iBusDB.RouteDetails where routeDetailRow.RouteID == updatingRouteID select routeDetailRow).FirstOrDefault();
                    routeDetail.Status = updatingStatus;
                    routeDetail.ModifiedBy = updatingModifiedBy;
                    routeDetail.ModifiedDate = System.DateTime.Now;
                    if (iBusDB.SaveChanges() <= 0)
                        return false;
                    else
                        successfullUpdate = true;
                    updatingTripID = (from tripDetailRow in iBusDB.TripDetails where tripDetailRow.RouteID == updatingRouteID select tripDetailRow.TripID).ToList();
                }
                if (updatingTripID.Count != 0)
                successfullUpdate = UpdateTripStatus(updatingTripID, updatingStatus, updatingModifiedBy);     
            }

            else if (updatingStatus == (int)AppConstant.STATUS.ACTIVE)
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var routeDetail = (from routeDetailRow in iBusDB.RouteDetails where routeDetailRow.RouteID == updatingRouteID select routeDetailRow).FirstOrDefault();

                    routeDetail.Status = updatingStatus;
                    routeDetail.ModifiedBy = updatingModifiedBy;
                    routeDetail.ModifiedDate = System.DateTime.Now;

                    if (iBusDB.SaveChanges() > 0)
                        successfullUpdate = true;

                }
            }
            return successfullUpdate;
        }





        public bool UpdateTripStatus(int updatingTripID, int updatingStatus, string updatingModifiedBy)
        {
            bool successfullUpdate = false;
            if (updatingStatus == (int)AppConstant.STATUS.IN_ACTIVE || updatingStatus == (int)AppConstant.STATUS.DELETED)
            {
                List<int> updatingDailyTripID = new List<int>();
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var tripDetail = (from tripDetailRow in iBusDB.TripDetails where tripDetailRow.TripID == updatingTripID select tripDetailRow).FirstOrDefault();

                    tripDetail.Status = updatingStatus;
                    tripDetail.ModifiedBy = updatingModifiedBy;
                    tripDetail.ModifiedDate = System.DateTime.Now;

                    if (iBusDB.SaveChanges() <= 0)
                        return false;
                    else
                        successfullUpdate = true;

                    updatingDailyTripID = (from dailyTripDetailRow in iBusDB.DailyTripDetails where dailyTripDetailRow.TripID == updatingTripID select dailyTripDetailRow.DailyTripID).ToList();
                }
                if (updatingDailyTripID.Count != 0)
               successfullUpdate =  UpdateDailyTripStatus(updatingDailyTripID, updatingStatus, updatingModifiedBy);
            }
            else if (updatingStatus == (int)AppConstant.STATUS.ACTIVE)
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var tripDetail = (from tripDetailRow in iBusDB.TripDetails where tripDetailRow.TripID == updatingTripID select tripDetailRow).FirstOrDefault();
                    //in travel details  table, check whether the given id status is inactive
                    if (CheckBusDetails(tripDetail.BusID) && CheckRouteDetails(tripDetail.RouteID))
                    {
                        tripDetail.Status = updatingStatus;
                        tripDetail.ModifiedBy = updatingModifiedBy;
                        tripDetail.ModifiedDate = System.DateTime.Now;

                        if (iBusDB.SaveChanges() > 0)
                            successfullUpdate = true;
                    }
                    else
                    {
                        //message- you cannot make status active as either bus or route status is inactive or delete
                    }

                }
            }

            return successfullUpdate;
        }

        
        public bool UpdateTripStatus(List<int> updatingTripID, int updatingStatus, string updatingModifiedBy)
        {
            bool successfullUpdate = false;
            List<int> updatingDailyTripID = new List<int>();
            using (iBusEntities iBusDB = new iBusEntities())
            {
                var tripDetail = (from tripDetailRow in iBusDB.TripDetails where updatingTripID.Contains(tripDetailRow.TripID) select tripDetailRow);
                tripDetail.ToList().ForEach(row => { row.Status = updatingStatus; row.ModifiedBy = updatingModifiedBy; row.ModifiedDate = System.DateTime.Now;});

                if (iBusDB.SaveChanges() <= 0)
                    return false;
                else
                    successfullUpdate = true;

                updatingDailyTripID = (from dailyTripDetailRow in iBusDB.DailyTripDetails where updatingTripID.Contains(dailyTripDetailRow.TripID) select dailyTripDetailRow.DailyTripID).ToList();
            }
            if (updatingDailyTripID.Count != 0)
            successfullUpdate = UpdateDailyTripStatus(updatingDailyTripID,updatingStatus,updatingModifiedBy);
            return successfullUpdate;
        }

        public bool UpdateDailyTripStatus(int updatingDailyTripID, int updatingStatus, string updatingModifiedBy)
        {
            bool successfullUpdate = false;
            if (updatingStatus == (int)AppConstant.STATUS.IN_ACTIVE || updatingStatus == (int)AppConstant.STATUS.DELETED)
            { 
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var dailyTripDetail = (from DailyTripDetailRow in iBusDB.DailyTripDetails where DailyTripDetailRow.DailyTripID == updatingDailyTripID select DailyTripDetailRow);
                    dailyTripDetail.ToList().ForEach(row => { row.Status = updatingStatus; row.ModifiedBy = updatingModifiedBy; row.ModifiedDate = System.DateTime.Now; });
                    if (iBusDB.SaveChanges() > 0)
                        successfullUpdate = true;

                }    
            }
            else if (updatingStatus == (int)AppConstant.STATUS.ACTIVE)
            {
                using (iBusEntities iBusDB = new iBusEntities())
                {
                    var dailyTripDetail = (from dailyTripDetailRow in iBusDB.DailyTripDetails where dailyTripDetailRow.DailyTripID == updatingDailyTripID select dailyTripDetailRow).FirstOrDefault();
                    //in trip details  table, check whether the given tripid status is active
                    if (CheckTripDetails(dailyTripDetail.TripID))
                    {
                        dailyTripDetail.Status = updatingStatus;
                        dailyTripDetail.ModifiedBy = updatingModifiedBy;
                        dailyTripDetail.ModifiedDate = System.DateTime.Now;

                        if (iBusDB.SaveChanges() > 0)
                            successfullUpdate = true;
                    }
                    else
                    {
                        //message- you cannot make status active as either bus or route status is inactive or delete
                    }

                }
            }
            return successfullUpdate;

        }

        public bool UpdateDailyTripStatus(List<int> updatingDailyTripID, int updatingStatus, string updatingModifiedBy)
        {
            bool successfullUpdate = false;
            using (iBusEntities iBusDB = new iBusEntities())
            {
                var dailyTripDetail = (from DailyTripDetailRow in iBusDB.DailyTripDetails where updatingDailyTripID.Contains(DailyTripDetailRow.DailyTripID) select DailyTripDetailRow);
                dailyTripDetail.ToList().ForEach(row => { row.Status = updatingStatus; row.ModifiedBy = updatingModifiedBy; row.ModifiedDate = System.DateTime.Now; });
                if (iBusDB.SaveChanges() > 0)
                    successfullUpdate = true;

            }
            return successfullUpdate;
        }

        protected bool CheckTravelDetails(int checkTravelID)
        {
            bool isActive = false;
            int? travelID = null;
            using (iBusEntities iBusDB = new iBusEntities())
            { 
                travelID = (from travelDetailRow in iBusDB.TravelDetails where travelDetailRow.TravelID==checkTravelID && travelDetailRow.Status==(int)AppConstant.STATUS.ACTIVE select travelDetailRow.TravelID).FirstOrDefault();
            }
            if (travelID != null)
                isActive = true;

            return isActive;
        }


        protected bool CheckBusDetails(int checkBusID)
        {
            bool isActive = false;
            int? busID = null;
            using (iBusEntities iBusDB = new iBusEntities())
            {
                busID = (from busDetailRow in iBusDB.BusDetails where busDetailRow.BusID == checkBusID && busDetailRow.Status == (int)AppConstant.STATUS.ACTIVE select busDetailRow.BusID).FirstOrDefault();
            }
            if (busID != null)
                isActive = true;

            return isActive;
        }

        protected bool CheckRouteDetails(int checkBusID)
        {
            bool isActive = false;
            int? routeID = null;
            using (iBusEntities iBusDB = new iBusEntities())
            {
                routeID = (from routeDetailRow in iBusDB.RouteDetails where routeDetailRow.RouteID == checkBusID && routeDetailRow.Status == (int)AppConstant.STATUS.ACTIVE select routeDetailRow.RouteID).FirstOrDefault();
            }
            if (routeID != null)
                isActive = true;

            return isActive;
        }

        protected bool CheckTripDetails(int checkTripID)
        {
            bool isActive = false;
            int? TripID = null;
            using (iBusEntities iBusDB = new iBusEntities())
            {
                TripID = (from tripDetailRow in iBusDB.TripDetails where tripDetailRow.TripID == checkTripID && tripDetailRow.Status == (int)AppConstant.STATUS.ACTIVE select tripDetailRow.TripID).FirstOrDefault();
            }
            if (TripID != null)
                isActive = true;

            return isActive;
        }
    }
}
