﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessObject
{

    public class DailyTripFiller
    {

        public void newDayFiller()
        {
            DateTime date = DateTime.Now.AddDays(61);
            List<tripdeatilstorer> tripstorer = new List<tripdeatilstorer>();
            using (iBusEntities iBusDB = new iBusEntities()) 
            {
                tripstorer = (from tripd in iBusDB.TripDetails
                              join busd in iBusDB.BusDetails
                               on tripd.BusID equals busd.BusID
                              where tripd.Status == (int)BussinessModel.AppConstant.STATUS.ACTIVE
                              select new tripdeatilstorer
                              {
                                  TripID = tripd.TripID,
                                  FreqID = tripd.FreqID,
                                  Capacity = busd.Capacity,
                                  BusID = tripd.BusID,
                                  flag = true,
                              }).ToList();

                List<tripdeatilstorer> tripsDaily = tripstorer.Where(row => row.FreqID == 1).ToList();
                List<tripdeatilstorer> tripsWeekend = tripstorer.Where(row => row.FreqID == 2).ToList();
                List<tripdeatilstorer> tripsAlternate = tripstorer.Where(row => row.FreqID == 3).ToList();


                //Fill trips with FREQ ID = Daily
                List<DailyTripDetailFiller> dailyTripDetailList = new List<DailyTripDetailFiller>();
                dailyTripDetailList = (from triprow in tripsDaily
                                       select new DailyTripDetailFiller
                                       {
                                           TripID = triprow.TripID,
                                           TripDate = date,
                                           AvailableSeats = triprow.Capacity,
                                           CreatedBy = "Hemant",
                                           CreatedDate = DateTime.Today,
                                           Status = 1
                                       }).ToList();
                foreach (DailyTripDetailFiller dailytrip in dailyTripDetailList)
                {
                    DailyTripDetail abc = new DailyTripDetail() { TripID = dailytrip.TripID, TripDate = dailytrip.TripDate, AvailableSeats = dailytrip.AvailableSeats, CreatedBy = dailytrip.CreatedBy, CreatedDate = Convert.ToDateTime(dailytrip.CreatedDate), Status = dailytrip.Status };
                    iBusDB.DailyTripDetails.Add(abc);
                    iBusDB.SaveChanges();
                }

                //Add trips with Freq = weekend
                if ((int)date.DayOfWeek == 0 || (int)date.DayOfWeek == 6)
                {
                    List<DailyTripDetailFiller> weekendTripDetailList = new List<DailyTripDetailFiller>();
                    weekendTripDetailList = (from triprow in tripsWeekend
                                             where triprow.FreqID == 2
                                             select new DailyTripDetailFiller
                                             {
                                                 TripID = triprow.TripID,
                                                 TripDate = date,
                                                 AvailableSeats = triprow.Capacity,
                                                 CreatedBy = "Hemant",
                                                 CreatedDate = DateTime.Today,
                                                 Status = 1
                                             }).ToList();

                    foreach (DailyTripDetailFiller dailytrip in weekendTripDetailList)
                    {
                        DailyTripDetail abc = new DailyTripDetail() { TripID = dailytrip.TripID, TripDate = dailytrip.TripDate, AvailableSeats = dailytrip.AvailableSeats, CreatedBy = dailytrip.CreatedBy, CreatedDate = Convert.ToDateTime(dailytrip.CreatedDate), Status = dailytrip.Status };
                        iBusDB.DailyTripDetails.Add(abc);
                        iBusDB.SaveChanges();
                    }
                }

                //Add trips with Freq = Alternate
                DateTime prevDate = date.AddDays(-1);
                List<DailyTripDetailFiller> alternateTripDetailList = new List<DailyTripDetailFiller>();
                List<int> previousDayDailyTripIDs = (from dailyTripRow in iBusDB.DailyTripDetails
                                                     join tripRow in iBusDB.TripDetails
                                                         on dailyTripRow.TripID equals tripRow.TripID
                                                     where dailyTripRow.TripDate == prevDate
                                                     select dailyTripRow.TripID).ToList();

                alternateTripDetailList = (from triprow in tripsAlternate
                                           where !previousDayDailyTripIDs.Contains(triprow.TripID)
                                           select new DailyTripDetailFiller
                                           {
                                               TripID = triprow.TripID,
                                               TripDate = date,
                                               AvailableSeats = triprow.Capacity,
                                               CreatedBy = "Hemant",
                                               CreatedDate = DateTime.Today,
                                               Status = 1
                                           }).ToList();

                foreach (DailyTripDetailFiller dailytrip in alternateTripDetailList)
                {
                    DailyTripDetail abc = new DailyTripDetail() { TripID = dailytrip.TripID, TripDate = dailytrip.TripDate, AvailableSeats = dailytrip.AvailableSeats, CreatedBy = dailytrip.CreatedBy, CreatedDate = Convert.ToDateTime(dailytrip.CreatedDate), Status = dailytrip.Status };
                    iBusDB.DailyTripDetails.Add(abc);
                    iBusDB.SaveChanges();
                }
                

            }
            
        }


        public void filldailytrip()
        {
            //CODE TO FILL TRIPS WITH FREQUENCY = DAILY
            DateTime date = DateTime.Today;
            DateTime finaldate = date.AddDays(60);

            List<tripdeatilstorer> tripstorer = new List<tripdeatilstorer>();
            iBusEntities iBusDB = new iBusEntities();
            tripstorer = (from tripd in iBusDB.TripDetails
                          join busd in iBusDB.BusDetails
                           on tripd.BusID equals busd.BusID
                          where tripd.Status == (int)BussinessModel.AppConstant.STATUS.ACTIVE
                          select new tripdeatilstorer
                          {
                              TripID = tripd.TripID,
                              FreqID = tripd.FreqID,
                              Capacity = busd.Capacity,
                              BusID = tripd.BusID,
                              flag = true,
                          }).ToList();

            List<tripdeatilstorer> tripsDaily = tripstorer.Where(row => row.FreqID == 1).ToList();
            List<tripdeatilstorer> tripsWeekend = tripstorer.Where(row => row.FreqID == 2).ToList();
            List<tripdeatilstorer> tripsAlternate = tripstorer.Where(row => row.FreqID == 3).ToList();

            while (DateTime.Compare(date, finaldate) < 1)
            {
                //Add Trips with Freq = Daily
                List<DailyTripDetailFiller> dailyTripDetailList = new List<DailyTripDetailFiller>();
                dailyTripDetailList = (from triprow in tripsDaily
                                       select new DailyTripDetailFiller
                                       {
                                           TripID = triprow.TripID,
                                           TripDate = date,
                                           AvailableSeats = triprow.Capacity,
                                           CreatedBy = "Hemant",
                                           CreatedDate = DateTime.Today,
                                           Status = 1
                                       }).ToList();
                foreach (DailyTripDetailFiller dailytrip in dailyTripDetailList)
                {
                    DailyTripDetail abc = new DailyTripDetail() { TripID = dailytrip.TripID, TripDate = dailytrip.TripDate, AvailableSeats = dailytrip.AvailableSeats, CreatedBy = dailytrip.CreatedBy, CreatedDate = Convert.ToDateTime(dailytrip.CreatedDate), Status = dailytrip.Status };
                    iBusDB.DailyTripDetails.Add(abc);
                    iBusDB.SaveChanges();
                }

                //Add trips with Freq = weekend
                if ((int)date.DayOfWeek == 0 || (int)date.DayOfWeek == 6)
                {
                    List<DailyTripDetailFiller> weekendTripDetailList = new List<DailyTripDetailFiller>();
                    weekendTripDetailList = (from triprow in tripsWeekend
                                             where triprow.FreqID == 2
                                             select new DailyTripDetailFiller
                                             {
                                                 TripID = triprow.TripID,
                                                 TripDate = date,
                                                 AvailableSeats = triprow.Capacity,
                                                 CreatedBy = "Hemant",
                                                 CreatedDate = DateTime.Today,
                                                 Status = 1
                                             }).ToList();

                    foreach (DailyTripDetailFiller dailytrip in weekendTripDetailList)
                    {
                        DailyTripDetail abc = new DailyTripDetail() { TripID = dailytrip.TripID, TripDate = dailytrip.TripDate, AvailableSeats = dailytrip.AvailableSeats, CreatedBy = dailytrip.CreatedBy, CreatedDate = Convert.ToDateTime(dailytrip.CreatedDate), Status = dailytrip.Status };
                        iBusDB.DailyTripDetails.Add(abc);
                        iBusDB.SaveChanges();
                    }
                }


                //Add trips with Freq = Alternate
                List<DailyTripDetailFiller> alternateTripDetailList = new List<DailyTripDetailFiller>();
                alternateTripDetailList = (from triprow in tripsAlternate
                                           where triprow.flag == true
                                           select new DailyTripDetailFiller
                                           {
                                               TripID = triprow.TripID,
                                               TripDate = date,
                                               AvailableSeats = triprow.Capacity,
                                               CreatedBy = "Hemant",
                                               CreatedDate = DateTime.Today,
                                               Status = 1
                                           }).ToList();

                tripsAlternate = tripsAlternate.Select(x =>
                {
                    x.flag = (x.flag == true ? false : true);
                    return x;
                }).ToList();

                foreach (DailyTripDetailFiller dailytrip in alternateTripDetailList)
                {
                    DailyTripDetail abc = new DailyTripDetail() { TripID = dailytrip.TripID, TripDate = dailytrip.TripDate, AvailableSeats = dailytrip.AvailableSeats, CreatedBy = dailytrip.CreatedBy, CreatedDate = Convert.ToDateTime(dailytrip.CreatedDate), Status = dailytrip.Status };
                    iBusDB.DailyTripDetails.Add(abc);
                    iBusDB.SaveChanges();
                }


                //increment date
                date = date.AddDays(1);
            }


        }

        
        public int fillBusType()
        {
            int result = 0;
            string str_BusType = string.Empty;
            using (iBusEntities iBusDB = new iBusEntities())
            {
                for (int i = 0; i < 4; i++)
                {
                    switch (i)
                    { 
                        case 0:
                            str_BusType = "AC Sleeper";
                            break;
                        case 1 :
                            str_BusType = "AC Semi-Sleeper";
                            break;
                        case 2:
                            str_BusType = "Non-AC Sleeper";
                            break;
                        case 3:
                            str_BusType = "Non-AC Semi-Sleeper";
                            break;
                    }

                    for (int j = 1; j <= 32; j++)
                    {
                        BusTypeDetail amenitiesRow = new BusTypeDetail() { BusType = str_BusType, AmenitiesID=j, CreatedBy = "Hemant", CreatedDate = System.DateTime.Now };
                        iBusDB.BusTypeDetails.Add(amenitiesRow);
                    }
                }
                result =  iBusDB.SaveChanges();
            }
            return result;
        }

        public int fillAmenities()
        { //waterbottle, movie , blanket,chargingpoint and reading point

            int result = 0;
            using (iBusEntities iBusDB = new iBusEntities())
            {
                for (int int_WaterBottle = 0; int_WaterBottle <= 1; int_WaterBottle++)
                {
                    bool flag_WaterBottle;
                    if (int_WaterBottle == 0)
                        flag_WaterBottle = true;
                    else
                        flag_WaterBottle = false;

                    for (int int_Movie = 0; int_Movie <= 1; int_Movie++)
                    {
                        bool flag_Movie;
                        if (int_Movie == 0)
                            flag_Movie = true;
                        else
                            flag_Movie = false;

                        for (int int_Blanket = 0; int_Blanket <= 1; int_Blanket++)
                        {
                            bool flag_Blanket;
                            if (int_Blanket == 0)
                                flag_Blanket = true;
                            else
                                flag_Blanket = false;


                            for (int int_ChargingPoint = 0; int_ChargingPoint <= 1; int_ChargingPoint++)
                            {
                                bool flag_ChargingPoint;
                                if (int_ChargingPoint == 0)
                                    flag_ChargingPoint = true;
                                else
                                    flag_ChargingPoint = false;


                                for (int int_ReadingLight = 0; int_ReadingLight <= 1; int_ReadingLight++)
                                {
                                    bool flag_ReadingLight;
                                    if (int_ReadingLight == 0)
                                        flag_ReadingLight = true;
                                    else
                                        flag_ReadingLight = false;

                                    AmenitiesDetail amenitiesRow = new AmenitiesDetail() { WaterBottle = flag_WaterBottle, Movie = flag_Movie, Blanket = flag_Blanket, ChargingPoint = flag_ChargingPoint, ReadingLight = flag_ReadingLight, CreatedBy = "Hemant", CreatedDate = System.DateTime.Now };
                                    iBusDB.AmenitiesDetails.Add(amenitiesRow);

                                }
                            }
                        }
                    }
                }

                result = iBusDB.SaveChanges();
            }
            
            

            return result;
        }
        
        public class DailyTripDetailFiller
        {
            public int DailyTripID { get; set; }
            public int TripID { get; set; }
            public System.DateTime TripDate { get; set; }
            public int AvailableSeats { get; set; }
           
            public int Status { get; set; }
            public string CreatedBy { get; set; }
            public System.DateTime CreatedDate { get; set; }
            public string ModifiedBy { get; set; }
            public Nullable<System.DateTime> ModifiedDate { get; set; }

        }

        public class tripdeatilstorer
        {
            public int TripID { get; set; }
            public int BusID { get; set; }
            public int RouteID { get; set; }
            public int FreqID { get; set; }
            public int Capacity { get; set; }
            public bool flag { get; set; }
            public System.TimeSpan DepartureTime { get; set; }
            public System.TimeSpan ArrivalTime { get; set; }
            public int Fare { get; set; }
            public int Status { get; set; }
            public string CreatedBy { get; set; }
            public System.DateTime CreatedDate { get; set; }
            public string ModifiedBy { get; set; }
            public Nullable<System.DateTime> ModifiedDate { get; set; }
        }
    }
}
